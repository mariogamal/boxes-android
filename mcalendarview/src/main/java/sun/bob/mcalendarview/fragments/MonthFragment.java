package sun.bob.mcalendarview.fragments;

import android.graphics.Typeface;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import java.util.Dictionary;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import sun.bob.mcalendarview.adapters.CalendarAdapter;
import sun.bob.mcalendarview.views.MonthView;
import sun.bob.mcalendarview.vo.MonthData;

/**
 * Created by bob.sun on 15/8/27.
 */


public class MonthFragment extends Fragment {
    private MonthData monthData;
    private int cellView = -1;
    private int markView = -1;
    private boolean hasTitle = true;

    private Map<Integer, String> MONTHS = new HashMap<Integer, String>();


    public void setData(MonthData monthData, int cellView, int markView) {
        this.monthData = monthData;
        this.cellView = cellView;
        this.markView = markView;
    }

    public void setTitle(boolean hasTitle) {
        this.hasTitle = hasTitle;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState) {
        InitializeMonths();
        LinearLayout ret = new LinearLayout(getContext());
        ret.setOrientation(LinearLayout.VERTICAL);
        ret.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        ret.setGravity(Gravity.CENTER);
        if ((monthData != null) && (monthData.getDate() != null)) {
            if (hasTitle) {
                TextView textView = new TextView(getContext());
//                textView.setText(monthData.getDate().getMonthString());
                textView.setTypeface(Typeface.DEFAULT_BOLD);
                textView.setTextSize(22);
                textView.setText(MONTHS.get(monthData.getDate().getMonth()) + ", " + monthData.getDate().getYear());
                textView.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
                ret.addView(textView);
            }
            MonthView monthView = new MonthView(getContext());
            CalendarAdapter calendarAdapter = new CalendarAdapter(getContext(), 1, monthData.getData()).setCellViews(cellView, markView);
            monthView.setAdapter(calendarAdapter);
            calendarAdapter.notifyDataSetChanged();
            ret.addView(monthView);
        }
        return ret;
    }

    private void InitializeMonths() {
        MONTHS.put(1, "January");
        MONTHS.put(2, "February");
        MONTHS.put(3, "March");
        MONTHS.put(4, "April");
        MONTHS.put(5, "May");
        MONTHS.put(6, "June");
        MONTHS.put(7, "July");
        MONTHS.put(8, "August");
        MONTHS.put(9, "September");
        MONTHS.put(10, "October");
        MONTHS.put(11, "November");
        MONTHS.put(12, "December");

    }
}
