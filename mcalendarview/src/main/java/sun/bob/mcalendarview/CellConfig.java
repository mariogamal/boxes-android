package sun.bob.mcalendarview;

import sun.bob.mcalendarview.vo.DateData;

/**
 * Created by bob.sun on 15/8/29.
 */
public class CellConfig {
    public static float cellWidth = 100;
    public static float cellHeight = 100;

    public static boolean ifMonth = true;
    // position of the middle page
    public static int middlePosition = 500;
    // The following two:
    // After clicking shrink and expand, record the position of the current page.
    public static int Month2WeekPos = 500;
    public static int Week2MonthPos = 500;
    // The following two:
    // Only one was set at the beginning, but later the data was found to be intersected (it was changed before use)
    // so split into two
    public static DateData w2mPointDate;
    public static DateData m2wPointDate;

    public static DateData weekAnchorPointDate;
}
