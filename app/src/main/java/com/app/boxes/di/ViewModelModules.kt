package com.app.boxes.di

import com.app.boxes.ui.calander.CalendarViewModel
import com.app.boxes.ui.confirmation.OrderConfirmViewModel
import com.app.boxes.ui.home.HomeViewModel
import com.app.boxes.ui.language.LanguageViewModel
import com.app.boxes.ui.resturant.tabs.menu.MenuViewModel
import com.app.boxes.ui.otp.OTPViewModel
import com.app.boxes.ui.profile.ProfileViewModel
import com.app.boxes.ui.resturant.tabs.packages.PackagesViewModel
import com.app.boxes.ui.splash.SplashViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

/**
 * Created by Mario Gamal on 12/7/20
 * Copyright © 2020 Jusclean. All rights reserved.
 */
val viewModelModule = module {
    viewModel { SplashViewModel(get()) }
    viewModel { LanguageViewModel(get()) }
    viewModel { OTPViewModel(get()) }
    viewModel { HomeViewModel(get()) }
    viewModel { PackagesViewModel(get()) }
    viewModel { MenuViewModel(get()) }
    viewModel { OrderConfirmViewModel(get()) }
    viewModel { ProfileViewModel(get()) }
    viewModel { CalendarViewModel(get()) }
}