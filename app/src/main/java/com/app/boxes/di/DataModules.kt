package com.app.boxes.di

import android.content.Context
import com.app.boxes.BuildConfig
import com.app.boxes.data.local.Preferences
import com.app.boxes.data.remote.RequestInterceptor
import com.app.boxes.data.remote.BoxesAPI
import com.readystatesoftware.chuck.ChuckInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Created by Mario Gamal on 12/7/20
 * Copyright © 2020 Jusclean. All rights reserved.
 */

val prefModule = module {
    single { Preferences(androidContext()) }
}

val networkModule = module {
    single { RequestInterceptor(get()) }
    single { provideOkHttpClient(get(), androidContext()) }
    single { provideBoxesApi(get()) }
    single { provideRetrofit(get()) }
}

fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
    return Retrofit.Builder().baseUrl(BuildConfig.API_URL).client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create()).build()
}

fun provideOkHttpClient(requestInterceptor: RequestInterceptor, context: Context): OkHttpClient {
    val loggingInterceptor = HttpLoggingInterceptor()
    loggingInterceptor.level = Level.BODY
    return OkHttpClient()
            .newBuilder()
            .addInterceptor(requestInterceptor)
            .addInterceptor(loggingInterceptor)
            .addInterceptor(ChuckInterceptor(context))
            .build()
}

fun provideBoxesApi(retrofit: Retrofit): BoxesAPI = retrofit.create(BoxesAPI::class.java)