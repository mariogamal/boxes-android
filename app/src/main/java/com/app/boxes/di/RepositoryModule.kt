package com.app.boxes.di

import com.app.boxes.data.repositories.*
import org.koin.dsl.module

/**
 * Created by Mario Gamal on 12/7/20
 * Copyright © 2020 Jusclean. All rights reserved.
 */
val repositoryModule = module {
    single { ProfileRepository(get()) }
    single { LanguageRepository(get()) }
    single { OTPRepository(get()) }
    single { HomeRepository(get()) }
    single { PackagesRepository(get()) }
    single { MenuRepository(get()) }
    single { OrderConfirmRepository(get()) }
    single { CalendarRepository(get()) }
}