package com.app.boxes.data.repositories

import com.app.boxes.base.BaseRepository
import com.app.boxes.data.models.Restaurant
import com.app.boxes.data.remote.BoxesAPI
import com.app.boxes.data.remote.Resource
import com.google.gson.JsonElement

class HomeRepository(private val api: BoxesAPI): BaseRepository() {

    suspend fun getRestaurants(): Resource<List<Restaurant>> =
            handleResponse { api.getRestaurants() }

}