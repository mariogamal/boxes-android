package com.app.boxes.data.models

import com.google.gson.annotations.SerializedName

data class UserSubscription(
        @SerializedName("user_subscribed_listing")
        val subscriptionListing: List<SubscriptionListing>,
        val validity: String
)