package com.app.boxes.data.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
data class Restaurant(
        val id: String,
        @SerializedName("listing_name")
        val name: String,
        val image: String,
        @SerializedName("listing_caption", alternate = ["caption_name"])
        val caption: String,
        @SerializedName("type_id")
        val typeId: String,
        @SerializedName("delivery_time")
        val deliveryTime: String,
        val distance: Double,
        val packages: List<Package>,
        val subscription: Int,
        val used: Int,
        var selectedWeeks: Int = 0,
        var isSelected: Boolean = false
): Parcelable