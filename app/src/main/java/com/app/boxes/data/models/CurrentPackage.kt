package com.app.boxes.data.models

import com.google.gson.annotations.SerializedName

/**
 * Created by Mario Gamal on 12/13/20
 * Copyright © 2020 Jusclean. All rights reserved.
 */
data class CurrentPackage(
        val id: String? = null,
        @SerializedName("type_name")
        val typeName: String? = null,
        @SerializedName("user_subscription")
        val userSubscription: UserSubscription? = null
)
