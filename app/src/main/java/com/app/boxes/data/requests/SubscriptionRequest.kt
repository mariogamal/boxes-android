package com.app.boxes.data.requests

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SubscriptionRequest(
        @SerializedName("listing_id")
        var listingIds: String,
        @SerializedName("package_id")
        var packageIds: String,
        @SerializedName("weekly")
        var weeksCount: String,
        @SerializedName("type_id")
        var typeId: String,
        var state: State
): Parcelable {
    enum class State {
        @SerializedName("start")
        START,

        @SerializedName("success")
        SUCCESS
    }
}