package com.app.boxes.data.repositories

import com.app.boxes.base.BaseRepository
import com.app.boxes.data.models.MenuCategory
import com.app.boxes.data.remote.BoxesAPI
import com.app.boxes.data.remote.Resource

class MenuRepository(private val api: BoxesAPI): BaseRepository() {

    suspend fun getMenu(listingId: String): Resource<List<MenuCategory>> =
            handleResponse { api.getMenu(listingId) }

}