package com.app.boxes.data.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.io.Serializable

@Parcelize
data class Package(
        val id: String,
        @SerializedName("package_name")
        val name: String,
        val price: String,
        @SerializedName("price2")
        val priceValue: Double,
        val meals: Int,
        @SerializedName("type_id")
        val typeId: String
): Parcelable