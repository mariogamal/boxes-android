package com.app.boxes.data.repositories

import com.app.boxes.base.BaseRepository
import com.app.boxes.data.models.Login
import com.app.boxes.data.remote.BoxesAPI
import com.app.boxes.data.remote.Resource
import com.app.boxes.data.remote.ResponseHandler
import com.app.boxes.data.requests.LoginRequest
import com.app.boxes.old.Model.Language
import com.google.gson.JsonElement
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception

/**
 * Created by Mario Gamal on 12/12/20
 * Copyright © 2020 Jusclean. All rights reserved.
 */
class OTPRepository(private val api: BoxesAPI) : BaseRepository() {

    suspend fun loginUser(request: LoginRequest): Resource<Login> =
            handleResponse { api.loginUser(request) }

}