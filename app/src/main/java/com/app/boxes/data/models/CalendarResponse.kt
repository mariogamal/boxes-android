package com.app.boxes.data.models

import com.google.gson.annotations.SerializedName

data class CalendarResponse(
        val vendors: List<Restaurant>,
        @SerializedName("delivery_dates")
        val deliveryDates: List<DeliveryDate>
)