package com.app.boxes.data.repositories

import com.app.boxes.base.BaseRepository
import com.app.boxes.data.remote.BoxesAPI
import com.app.boxes.data.remote.Resource
import com.app.boxes.old.Model.Language

class LanguageRepository(private val api: BoxesAPI) : BaseRepository() {

    suspend fun getLanguages(): Resource<List<Language>> =
            handleResponse { api.getLanguages() }

}