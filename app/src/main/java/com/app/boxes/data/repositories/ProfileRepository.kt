package com.app.boxes.data.repositories

import com.app.boxes.base.BaseRepository
import com.app.boxes.data.models.CurrentPackage
import com.app.boxes.data.models.User
import com.app.boxes.data.remote.BoxesAPI
import com.app.boxes.data.remote.Resource

class ProfileRepository(private val api: BoxesAPI) : BaseRepository() {

    suspend fun getCurrentPackage(): Resource<CurrentPackage> =
            handleResponse { api.getCurrentPackage() }

    suspend fun getProfile(): Resource<User> =
            handleResponse { api.getProfile() }

}