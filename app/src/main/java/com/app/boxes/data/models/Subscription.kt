package com.app.boxes.data.models

import com.google.gson.annotations.SerializedName

/**
 * Created by Mario Gamal on 12/13/20
 * Copyright © 2020 Jusclean. All rights reserved.
 */
data class Subscription(
        val id: String,
        @SerializedName("package_name")
        val name: String,
        val price: String,
        @SerializedName("price2")
        val priceValue: Double,
        val validity: String?,
        @SerializedName("user_subscribed_listing")
        val restaurants: List<Restaurant>
)
