package com.app.boxes.data.repositories

import com.app.boxes.base.BaseRepository
import com.app.boxes.data.models.CalendarResponse
import com.app.boxes.data.remote.BoxesAPI
import com.app.boxes.data.remote.Resource

class CalendarRepository(private val api: BoxesAPI) : BaseRepository() {

    suspend fun getCalendar(): Resource<CalendarResponse> =
            handleResponse { api.getCalendar() }

}