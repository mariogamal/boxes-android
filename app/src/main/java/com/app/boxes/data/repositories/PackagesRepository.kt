package com.app.boxes.data.repositories

import com.app.boxes.base.BaseRepository
import com.app.boxes.data.models.Subscription
import com.app.boxes.data.remote.BoxesAPI
import com.app.boxes.data.remote.Resource

class PackagesRepository(private val api: BoxesAPI) : BaseRepository() {

    suspend fun getPackages(listingId: String): Resource<List<Subscription>> =
            handleResponse { api.getPackages(listingId) }

}