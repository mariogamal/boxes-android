package com.app.boxes.data.local

import android.content.Context
import android.content.SharedPreferences

/**
 * Created by Mario Gamal on 12/7/20
 * Copyright © 2020 Jusclean. All rights reserved.
 */
class Preferences(context: Context) {

    private val preferences: SharedPreferences =
            context.getSharedPreferences("boxes-pref", Context.MODE_PRIVATE)

    fun saveUserToken(value: String) {
        preferences.edit().putString(TOKEN, value).apply()
    }

    fun getUserToken(): String? {
        return preferences.getString(TOKEN, null)
    }

    fun saveLanguageId(id: String) {
        preferences.edit().putString(LANGUAGE, id).apply()
    }

    fun getLanguageId(): String? {
        return preferences.getString(LANGUAGE, null)
    }

    companion object {
        const val TOKEN = "token"
        const val LANGUAGE = "language"
    }
}