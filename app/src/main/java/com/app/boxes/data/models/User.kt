package com.app.boxes.data.models

import com.google.gson.annotations.SerializedName

/**
 * Created by Mario Gamal on 12/13/20
 * Copyright © 2020 Jusclean. All rights reserved.
 */
data class User(
        val id: String,
        val phone: String,
        val image: String?,

        @SerializedName("first_name")
        val firstName: String?,

        @SerializedName("last_name")
        val lastName: String?,

        @SerializedName("otp_code")
        val otpCode: Long,

        @SerializedName("device_type")
        val deviceType: String,

        @SerializedName("device_token")
        val deviceToken: String,

        @SerializedName("is_active")
        val isActive: Long,

        @SerializedName("created_at")
        val createdAt: String,

        @SerializedName("updated_at")
        val updatedAt: String,

        @SerializedName("is_survey_filled")
        val isSurveyFilled: Long,

        @SerializedName("is_profile_filled")
        val isProfileFilled: Long
) {
    val name: String
        get() = "$firstName $lastName"
}
