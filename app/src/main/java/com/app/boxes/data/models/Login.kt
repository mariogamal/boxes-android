package com.app.boxes.data.models

import com.google.gson.annotations.SerializedName

/**
 * Created by Mario Gamal on 12/13/20
 * Copyright © 2020 Jusclean. All rights reserved.
 */
data class Login (
        @SerializedName("access_token")
        val accessToken: String,

        @SerializedName("token_type")
        val tokenType: String,

        val user: User
)