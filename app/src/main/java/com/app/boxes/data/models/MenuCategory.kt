package com.app.boxes.data.models

import com.google.gson.annotations.SerializedName

data class MenuCategory(
        @SerializedName("menu_cat_name")
        val name: String,
        val products: List<MenuProduct>
)