package com.app.boxes.data.models

import com.google.gson.annotations.SerializedName

data class DeliveryDate(
        @SerializedName("order_detail_id")
        val id: String,
        @SerializedName("delivery_date")
        val date: String,
        val status: String,
        @SerializedName("user_meals")
        val meals: List<MenuProduct>
)