package com.app.boxes.data.repositories

import com.app.boxes.base.BaseRepository
import com.app.boxes.data.remote.BoxesAPI
import com.app.boxes.data.remote.Resource
import com.app.boxes.data.requests.SubscriptionRequest

class OrderConfirmRepository(private val api: BoxesAPI) : BaseRepository() {

    suspend fun subscribePayment(request: SubscriptionRequest): Resource<String> =
            handleResponse { api.subscribePayment(request) }

}