package com.app.boxes.data.models

import com.google.gson.annotations.SerializedName

data class MenuProduct(
        @SerializedName("product_name")
        val name: String,
        @SerializedName("caption_name")
        val caption: String,
        @SerializedName("product_description", alternate = ["description_name"])
        val description: String,
        @SerializedName("product_ingredients")
        val ingredients: List<ProductIngredient>,
        val image: String,
        val rating: Double,
        val calories: Int,
        val proteins: Int,
        val carbohydrates: Int
)