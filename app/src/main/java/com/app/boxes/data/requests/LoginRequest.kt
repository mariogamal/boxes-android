package com.app.boxes.data.requests

import com.google.gson.annotations.SerializedName

/**
 * Created by Mario Gamal on 12/12/20
 * Copyright © 2020 Jusclean. All rights reserved.
 */
data class LoginRequest(
        val phone: String?,
        @SerializedName("device_type")
        val deviceType: String? = "android",
        @SerializedName("device_token")
        val deviceToken: String?,
        @SerializedName("id_token")
        val idToken: String?
)