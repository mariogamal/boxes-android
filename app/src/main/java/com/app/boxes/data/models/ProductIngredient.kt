package com.app.boxes.data.models

data class ProductIngredient(
        val name: String
)