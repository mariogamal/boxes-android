package com.app.boxes.data.models

import com.google.gson.annotations.SerializedName

data class SubscriptionListing(
        @SerializedName("listing")
        val restaurantListing: Restaurant
)