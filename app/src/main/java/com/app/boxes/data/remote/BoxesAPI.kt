package com.app.boxes.data.remote

import com.app.boxes.data.models.*
import com.app.boxes.data.requests.LoginRequest
import com.app.boxes.data.requests.SubscriptionRequest
import com.app.boxes.old.Model.Language
import com.google.gson.JsonElement
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

/**
 * Created by Mario Gamal on 12/7/20
 * Copyright © 2020 Jusclean. All rights reserved.
 */
interface BoxesAPI {

    @GET("v1/languages")
    suspend fun getLanguages(): Response<List<Language>>

    @POST("v2/login")
    suspend fun loginUser(@Body request: LoginRequest): Response<Login>

    @GET("v2/auth/my-current-package")
    suspend fun getCurrentPackage(): Response<CurrentPackage>

    @GET("v1/listings")
    suspend fun getRestaurants(
            @Query("user_latitude") latitude: String = "latitude",
            @Query("user_longitude") longitude: String = "longitude",
    ): Response<List<Restaurant>>

    @GET("v1/packages")
    suspend fun getPackages(@Query("listing_id") listingId: String) : Response<List<Subscription>>

    @GET("v1/menu")
    suspend fun getMenu(@Query("listing_id") listingId: String) : Response<List<MenuCategory>>

    @POST("v2/auth/user-subscription")
    suspend fun subscribePayment(@Body request: SubscriptionRequest) : Response<String>

    @GET("v1/auth/user")
    suspend fun getProfile(): Response<User>

    @GET("v1/auth/calendar-detail")
    suspend fun getCalendar(): Response<CalendarResponse>
}