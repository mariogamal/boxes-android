package com.app.boxes.ui.home

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.boxes.BuildConfig
import com.app.boxes.R
import com.app.boxes.data.models.Restaurant
import com.app.boxes.ui.resturant.RestaurantDetailsActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.list_multi_restaurant.view.*

class SingleRestaurantAdapter(private val dataSet: List<Restaurant>, private val withClick: Boolean = true) :
        RecyclerView.Adapter<SingleRestaurantAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val restaurantImage: ImageView = view.restaurantImage
        val restaurantName: TextView = view.restaurantName
        val restaurantCategory: TextView = view.restaurantCategory
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int) =
            ViewHolder(LayoutInflater.from(viewGroup.context)
                    .inflate(R.layout.list_single_restaurant, viewGroup, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.restaurantName.text = dataSet[position].name
        holder.restaurantCategory.text = dataSet[position].caption
        val imagePath = BuildConfig.IMG_URL + dataSet[position].image
        Picasso.get().load(imagePath).into(holder.restaurantImage)
        if (withClick)
            holder.itemView.setOnClickListener {
                val intent = Intent(it.context, RestaurantDetailsActivity::class.java)
                intent.putExtra(RestaurantDetailsActivity.RESTAURANT, dataSet[position])
                intent.putExtra(RestaurantDetailsActivity.SHOW_PACKAGES, true)
                it.context.startActivity(intent)
            }
    }

    override fun getItemCount() = dataSet.size
}