package com.app.boxes.ui.language

import androidx.lifecycle.liveData
import com.app.boxes.base.BaseViewModel
import com.app.boxes.data.remote.Resource
import com.app.boxes.data.repositories.LanguageRepository

class LanguageViewModel(private val repository: LanguageRepository) : BaseViewModel() {

    val languages = liveData {
        emit(Resource.Loading())
        emit(repository.getLanguages())
    }

}