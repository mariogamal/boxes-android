package com.app.boxes.ui.otp

import android.os.Bundle
import androidx.lifecycle.Observer
import com.app.boxes.R
import com.app.boxes.base.BaseActivity
import com.app.boxes.data.remote.Resource
import com.app.boxes.data.requests.LoginRequest
import com.app.boxes.ui.NewMainActivity
import com.app.boxes.ui.login.NewLoginActivity
import com.app.boxes.util.ProgressDialog
import com.google.firebase.FirebaseException
import com.google.firebase.auth.*
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import com.google.gson.JsonElement
import kotlinx.android.synthetic.main.activity_otp.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.concurrent.TimeUnit

class NewOTPActivity : BaseActivity() {

    private var resendToken: PhoneAuthProvider.ForceResendingToken? = null
    private lateinit var storedVerificationId: String
    private lateinit var auth: FirebaseAuth
    private var otpCode: String? = null
    private var phoneNumber: String? = null
    private val otpViewModel: OTPViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp)
        auth = Firebase.auth
        intent.getStringExtra(NewLoginActivity.PHONE)?.let { phoneNumber = it }
        phoneNumber?.let { sendOTPCode(it) }
        resendCode.setOnClickListener { phoneNumber?.let { phone -> sendOTPCode(phone) } }
        submitCode.setOnClickListener { verifyOTP() }
        otpView.setOtpCompletionListener { otpCode = it }
    }

    private fun sendOTPCode(phone: String) {
        val optionsBuilder = PhoneAuthOptions.newBuilder(auth)
                .setPhoneNumber(phone)
                .setTimeout(60L, TimeUnit.SECONDS)
                .setActivity(this)
                .setCallbacks(callbacks)
        resendToken?.let {
            optionsBuilder.setForceResendingToken(it)
        }
        PhoneAuthProvider.verifyPhoneNumber(optionsBuilder.build())
    }

    private fun verifyOTP() {
        if (otpCode != null) {
            val credential = PhoneAuthProvider.getCredential(storedVerificationId, otpCode!!)
            signInWithPhoneAuthCredential(credential)
            ProgressDialog.getInstance().show(this)
        } else
            showMessage(getString(R.string.enter_valid_otp))
    }

    private fun getLoginRequest(idToken: String?) =
            LoginRequest(phoneNumber, "android", "token", idToken)

    private val callbacks = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

        override fun onVerificationCompleted(credential: PhoneAuthCredential) {
            signInWithPhoneAuthCredential(credential)
        }

        override fun onVerificationFailed(e: FirebaseException) {
            e.printStackTrace()
            showMessage(getString(R.string.number_not_correct))
        }

        override fun onCodeSent(
                verificationId: String,
                token: PhoneAuthProvider.ForceResendingToken
        ) {
            submitCode.isEnabled = true
            showMessage(getString(R.string.otp_sent))
            storedVerificationId = verificationId
            resendToken = token
        }
    }

    private fun signInWithPhoneAuthCredential(credential: PhoneAuthCredential) {
        auth.signInWithCredential(credential)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        val user = task.result?.user
                        user?.getIdToken(true)?.addOnCompleteListener {
                            if (it.isSuccessful)
                                loginUser(getLoginRequest(it.result?.token))
                        }
                    } else {
                        if (task.exception is FirebaseAuthInvalidCredentialsException) {
                            showMessage(getString(R.string.otp_not_correct))
                        }
                    }
                }
    }

    private fun loginUser(loginRequest: LoginRequest) {
        observe(otpViewModel.loginUser(loginRequest)) {
            preferences.saveUserToken(it.tokenType + " " + it.accessToken)
            start(NewMainActivity::class.java)
        }
    }
}