package com.app.boxes.ui.resturant.tabs.packages

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.app.boxes.R
import com.app.boxes.base.BaseFragment
import com.app.boxes.ui.resturant.tabs.menu.MenuViewModel
import kotlinx.android.synthetic.main.fragment_subscription_plan.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class PackagesFragment(private val listingId: String) : BaseFragment() {

    private val viewModel: PackagesViewModel by viewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
            inflater.inflate(R.layout.fragment_subscription_plan, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        observe(viewModel.getPackages(listingId)) {
            packagesRecycler.adapter = PackagesAdapter(it)
        }
    }
}