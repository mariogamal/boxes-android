package com.app.boxes.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.boxes.R
import com.app.boxes.base.BaseActivity
import com.app.boxes.base.BaseFragment
import com.app.boxes.data.models.Restaurant
import com.app.boxes.ui.confirmation.OrderConfirmActivity
import com.app.boxes.util.gone
import com.app.boxes.util.visible
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.fragment_home.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class NewHomeFragment : BaseFragment() {

    private lateinit var singleAdapter: SingleRestaurantAdapter
    private lateinit var multiRestaurant: MultiRestaurantAdapter
    private val viewModel: HomeViewModel by viewModel()
    private var restaurants = listOf<Restaurant>()
    private var selectedRestaurants = arrayListOf<Restaurant>()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
            inflater.inflate(R.layout.fragment_home, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observe(viewModel.restaurants) {
            restaurants = it
            addTabLayoutListener()
            showMultiRestaurants()
        }
        confirmSelection.setOnClickListener {
            val args = Bundle()
            args.putParcelableArrayList(RESTAURANTS, selectedRestaurants)
            start(OrderConfirmActivity::class.java, args)
        }
    }

    private fun addTabLayoutListener() {
        subscriptionTabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabUnselected(p0: TabLayout.Tab?) {}
            override fun onTabReselected(p0: TabLayout.Tab?) {}
            override fun onTabSelected(tab: TabLayout.Tab?) {
                if (tab?.position == 0) showMultiRestaurants() else showSingleRestaurants()
            }
        })
    }

    private fun showSingleRestaurants() {
        if (!::singleAdapter.isInitialized)
            singleAdapter = SingleRestaurantAdapter(restaurants)
        restaurantsRecycler.adapter = singleAdapter
        weekCountLayout.gone()
        confirmSelection.gone()
    }

    private fun showMultiRestaurants() {
        if (!::multiRestaurant.isInitialized)
            multiRestaurant = MultiRestaurantAdapter(restaurants) {
                selectedRestaurants.clear()
                selectedRestaurants.addAll(it)
                val count = it.sumBy { r -> r.selectedWeeks }
                weekCount.text = "$count/4"
                if (count == 4)
                    confirmSelection.visible()
                else
                    confirmSelection.gone()
            }
        restaurantsRecycler.adapter = multiRestaurant
        weekCountLayout.visible()
        confirmSelection.gone()
    }

    companion object {
        const val RESTAURANTS = "restaurants"
    }
}