package com.app.boxes.ui.resturant.tabs

import android.content.Context
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.app.boxes.R
import com.app.boxes.ui.resturant.tabs.menu.NewMenuFragment
import com.app.boxes.ui.resturant.tabs.packages.PackagesFragment

class TabAdapter(
        fragmentManager: FragmentManager,
        private val fragmentCount: Int,
        private val context: Context,
        private val listingId: String
) : FragmentPagerAdapter(fragmentManager) {

    override fun getCount() = fragmentCount

    override fun getItem(position: Int) =
            if (position == 1) PackagesFragment(listingId) else NewMenuFragment(listingId)

    override fun getPageTitle(position: Int) =
            context.getString(if (position == 1) R.string.packages else R.string.menu)
}