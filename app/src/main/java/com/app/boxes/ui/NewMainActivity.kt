package com.app.boxes.ui

import android.os.Bundle
import androidx.core.view.GravityCompat
import com.app.boxes.R
import com.app.boxes.base.BaseActivity
import com.app.boxes.base.BaseFragment
import com.app.boxes.ui.home.NewHomeFragment
import com.app.boxes.ui.profile.NewProfileFragment
import kotlinx.android.synthetic.main.activity_main.*

class NewMainActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initViewsListeners()
    }

    private fun initViewsListeners() {
        initMenuIconListener()
        initDrawerNavListener()
        initBottomNavListener()
    }

    private fun initBottomNavListener() {
        bottomNav.setOnNavigationItemSelectedListener {
            openFragment(
                    when (it.itemId) {
                        R.id.home -> NewHomeFragment()
                        else -> NewProfileFragment()
                    }
            )
            true
        }
        bottomNav.selectedItemId = R.id.home
    }

    private fun initDrawerNavListener() {
        navView.setNavigationItemSelectedListener {
            openFragment(
                    when (it.itemId) {
                        R.id.order_history -> NewHomeFragment()
                        R.id.my_package -> NewHomeFragment()
                        R.id.notification -> NewHomeFragment()
                        R.id.contact_us -> NewHomeFragment()
                        R.id.settings -> NewHomeFragment()
                        R.id.faqs -> NewHomeFragment()
                        R.id.logout -> NewHomeFragment()
                        else -> NewHomeFragment()
                    }
            )
            drawerLayout.closeDrawer(GravityCompat.START)
            true
        }
    }

    private fun initMenuIconListener() {
        menuIcon.setOnClickListener {
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawer(GravityCompat.START)
            } else {
                drawerLayout.openDrawer(GravityCompat.START)
            }
        }
    }

    private fun openFragment(fragment: BaseFragment) {
        supportFragmentManager.beginTransaction().replace(R.id.fragmentHolder, fragment).commit()
    }

    override fun onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }
}