package com.app.boxes.ui.otp

import androidx.lifecycle.liveData
import com.app.boxes.base.BaseViewModel
import com.app.boxes.data.remote.Resource
import com.app.boxes.data.repositories.OTPRepository
import com.app.boxes.data.requests.LoginRequest

/**
 * Created by Mario Gamal on 12/12/20
 * Copyright © 2020 Jusclean. All rights reserved.
 */
class OTPViewModel(private val repository: OTPRepository) : BaseViewModel() {

    fun loginUser(request: LoginRequest) = liveData {
        emit(Resource.Loading())
        emit(repository.loginUser(request))
    }

}