package com.app.boxes.ui.confirmation

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.boxes.BuildConfig
import com.app.boxes.R
import com.app.boxes.data.models.Restaurant
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.list_selected_restaurant.view.*

class OrderConfirmAdapter(private val dataSet: List<Restaurant>) :
        RecyclerView.Adapter<OrderConfirmAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val image: ImageView = view.image
        val name: TextView = view.name
        val caption: TextView = view.caption
        val count: TextView = view.count
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int) =
            ViewHolder(LayoutInflater.from(viewGroup.context)
                    .inflate(R.layout.list_selected_restaurant, viewGroup, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.name.text = dataSet[position].name
        holder.caption.text = dataSet[position].caption
        holder.count.text = "Weeks : " + dataSet[position].selectedWeeks.toString()
        Picasso.get().load(BuildConfig.IMG_URL + dataSet[position].image).into(holder.image)
    }

    override fun getItemCount() = dataSet.size

}