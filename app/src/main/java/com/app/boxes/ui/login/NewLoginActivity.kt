package com.app.boxes.ui.login

import android.os.Bundle
import com.app.boxes.R
import com.app.boxes.base.BaseActivity
import com.app.boxes.ui.NewMainActivity
import com.app.boxes.ui.otp.NewOTPActivity
import kotlinx.android.synthetic.main.activity_signin.*

class NewLoginActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_signin)
        login.setOnClickListener {
            if (phone.text.length == 8)
                openVerifyOTP()
            else
                showMessage(getString(R.string.enter_valid_phone))
        }
        skip.setOnClickListener {
            start(NewMainActivity::class.java)
        }
    }

    private fun openVerifyOTP() {
        val bundle = Bundle()
        bundle.putString(PHONE, countryCode.text.toString() + phone.text.toString())
        start(NewOTPActivity::class.java, bundle)
    }

    companion object {
        const val PHONE = "phone"
    }
}