package com.app.boxes.ui.resturant.tabs.packages

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.boxes.R
import com.app.boxes.data.models.Subscription
import kotlinx.android.synthetic.main.list_restaurant_subscription.view.*

class PackagesAdapter(private val packages: List<Subscription>) :
        RecyclerView.Adapter<PackagesAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val packageName: TextView = view.packageName
        val packagePrice: TextView = view.packagePrice
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int) =
            ViewHolder(LayoutInflater.from(viewGroup.context)
                    .inflate(R.layout.list_restaurant_subscription, viewGroup, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.packageName.text = packages[position].name
        holder.packagePrice.text = packages[position].price
    }

    override fun getItemCount() = packages.size
}