package com.app.boxes.ui.payment

import android.annotation.SuppressLint
import android.os.Bundle
import android.webkit.ValueCallback
import android.webkit.WebView
import android.webkit.WebViewClient
import com.app.boxes.R
import com.app.boxes.base.BaseActivity
import com.app.boxes.data.requests.SubscriptionRequest
import com.app.boxes.ui.confirmation.OrderConfirmViewModel
import kotlinx.android.synthetic.main.activity_payment.*
import org.koin.androidx.viewmodel.ext.android.viewModel

@SuppressLint("SetJavaScriptEnabled")
class PaymentActivity : BaseActivity() {

    private val viewModel: OrderConfirmViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment)
        paymentWebView.settings.javaScriptEnabled = true
        intent.getStringExtra(PAYMENT_URL)?.let { paymentWebView.loadUrl(it) }
        paymentWebView.webViewClient = object : WebViewClient() {
            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                view?.evaluateJavascript(IS_CAPTURED) {
                    if (it.contains('1'))
                        intent.getParcelableExtra<SubscriptionRequest>(PAYMENT_REQUEST)?.let { subscription ->
                            subscription.state = SubscriptionRequest.State.SUCCESS
                            observe(viewModel.subscribePayment(subscription)) {
                                finish()
                            }
                        }
                }
            }
        }
    }

    companion object {
        const val PAYMENT_URL = "paymentUrl"
        const val PAYMENT_REQUEST = "paymentRequest"
        const val IS_CAPTURED = "document.getElementById('is_captured').value"
    }
}