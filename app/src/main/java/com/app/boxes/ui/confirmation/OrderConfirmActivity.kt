package com.app.boxes.ui.confirmation

import android.os.Bundle
import com.app.boxes.R
import com.app.boxes.base.BaseActivity
import com.app.boxes.data.models.Restaurant
import com.app.boxes.data.requests.SubscriptionRequest
import com.app.boxes.ui.home.NewHomeFragment
import com.app.boxes.ui.payment.PaymentActivity
import kotlinx.android.synthetic.main.fragment_selected_restaurant.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class OrderConfirmActivity : BaseActivity() {

    private lateinit var subscriptionRequest: SubscriptionRequest
    private val viewModel: OrderConfirmViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_selected_restaurant)
        back.setOnClickListener { finish() }
        intent.getParcelableArrayListExtra<Restaurant>(NewHomeFragment.RESTAURANTS)?.let {
            selectedRecycler.adapter = OrderConfirmAdapter(it)
            prepareCheckout(it)
        }
        checkoutButton.setOnClickListener {
            observe(viewModel.subscribePayment(subscriptionRequest)) {
                val extra = Bundle()
                extra.putString(PaymentActivity.PAYMENT_URL, it)
                extra.putParcelable(PaymentActivity.PAYMENT_REQUEST, subscriptionRequest)
                start(PaymentActivity::class.java, extra)
            }
        }
    }

    private fun prepareCheckout(restaurants: List<Restaurant>) {
        subscriptionRequest = SubscriptionRequest(
                restaurants.joinToString(SEPARATOR) { it.id },
                restaurants.joinToString(SEPARATOR) { it.packages.first().id },
                restaurants.joinToString(SEPARATOR) { it.selectedWeeks.toString() },
                restaurants.first().packages.first().typeId,
                SubscriptionRequest.State.START
        )
        checkoutButton.text = String.format(resources.getString(R.string.checkout),
                restaurants.sumByDouble { it.packages.first().priceValue })

    }

    companion object {
        const val SEPARATOR = ","
    }
}