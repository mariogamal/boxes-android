package com.app.boxes.ui.profile

import androidx.lifecycle.liveData
import com.app.boxes.base.BaseViewModel
import com.app.boxes.data.remote.Resource
import com.app.boxes.data.repositories.ProfileRepository

class ProfileViewModel(private val repository: ProfileRepository): BaseViewModel() {

    val profile = liveData {
        emit(repository.getProfile())
    }

    val packages = liveData {
        emit(Resource.Loading())
        emit(repository.getCurrentPackage())
    }
}