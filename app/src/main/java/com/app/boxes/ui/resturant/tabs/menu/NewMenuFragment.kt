package com.app.boxes.ui.resturant.tabs.menu

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.boxes.R
import com.app.boxes.base.BaseFragment
import com.app.boxes.ui.resturant.tabs.packages.PackagesAdapter
import com.app.boxes.ui.resturant.tabs.packages.PackagesViewModel
import kotlinx.android.synthetic.main.fragment_menu.*
import kotlinx.android.synthetic.main.fragment_subscription_plan.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class NewMenuFragment(private val listingId: String) : BaseFragment() {
    private val viewModel: MenuViewModel by viewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
            inflater.inflate(R.layout.fragment_menu, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        observe(viewModel.getMenu(listingId)) {
            menuList.setAdapter(MenuAdapter(it.filter { category ->
                category.products.isNotEmpty()
            }))
        }
    }
}