package com.app.boxes.ui.calander

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import at.grabner.circleprogress.CircleProgressView
import com.app.boxes.BuildConfig
import com.app.boxes.R
import com.app.boxes.data.models.Restaurant
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.list_selected_restaurant.view.*
import kotlinx.android.synthetic.main.list_selected_restaurant.view.image
import kotlinx.android.synthetic.main.list_selected_restaurant.view.name
import kotlinx.android.synthetic.main.subscribed_multi_res_item.view.*

class VendorAdapter(private val dataSet: List<Restaurant>) :
        RecyclerView.Adapter<VendorAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val image: ImageView = view.image
        val name: TextView = view.name
        val counter: TextView = view.counter
        val progress: CircleProgressView = view.progressCircular
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int) =
            ViewHolder(LayoutInflater.from(viewGroup.context)
                    .inflate(R.layout.subscribed_multi_res_item, viewGroup, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.setOnClickListener { handleSelection(position) }
        holder.name.text = dataSet[position].name
        holder.counter.text = "${dataSet[position].used}/${dataSet[position].subscription}"
        Picasso.get().load(BuildConfig.IMG_URL + dataSet[position].image).into(holder.image)
        holder.progress.blockCount = dataSet[position].subscription
        holder.progress.maxValue = dataSet[position].subscription.toFloat()
        holder.progress.setValue(dataSet[position].used.toFloat())
        if (dataSet[position].isSelected) {
            holder.name.setTextColor(Color.parseColor("#FFFFFF"))
            holder.counter.setTextColor(Color.parseColor("#FFFFFF"))
            holder.name.background = ContextCompat.getDrawable(holder.itemView.context, R.drawable.custom_oval)
            holder.counter.background = ContextCompat.getDrawable(holder.itemView.context, R.drawable.custom_oval)
        } else {
            holder.name.setTextColor(Color.parseColor("#000000"))
            holder.counter.setTextColor(Color.parseColor("#000000"))
            holder.name.setBackgroundResource(0)
            holder.counter.setBackgroundResource(0)
        }
    }

    private fun handleSelection(position: Int) {
        dataSet.forEachIndexed { index, _ ->
            dataSet[position].isSelected  = position == index
            print(dataSet[position])
        }
        notifyDataSetChanged()
    }

    override fun getItemCount() = dataSet.size

}