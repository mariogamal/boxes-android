package com.app.boxes.ui.resturant.tabs.packages

import androidx.lifecycle.liveData
import com.app.boxes.base.BaseViewModel
import com.app.boxes.data.remote.Resource
import com.app.boxes.data.repositories.PackagesRepository

class PackagesViewModel(private val repository: PackagesRepository) : BaseViewModel() {

    fun getPackages(listingId: String) = liveData {
        emit(Resource.Loading())
        emit(repository.getPackages(listingId))
    }

}