package com.app.boxes.ui.resturant.tabs.menu

import androidx.lifecycle.liveData
import com.app.boxes.base.BaseViewModel
import com.app.boxes.data.remote.Resource
import com.app.boxes.data.repositories.MenuRepository

class MenuViewModel(private val repository: MenuRepository) : BaseViewModel() {
    fun getMenu(listingId: String) = liveData {
        emit(Resource.Loading())
        emit(repository.getMenu(listingId))
    }
}