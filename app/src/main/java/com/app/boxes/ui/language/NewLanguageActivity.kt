package com.app.boxes.ui.language

import android.content.Intent
import android.os.Bundle
import com.app.boxes.BuildConfig
import com.app.boxes.R
import com.app.boxes.base.BaseActivity
import com.app.boxes.old.Model.Language
import com.app.boxes.ui.login.NewLoginActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_select_language.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class NewLanguageActivity : BaseActivity() {

    private val viewModel: LanguageViewModel by viewModel()
    private val languagesList = ArrayList<Language>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_select_language)
        observe(viewModel.languages) {
            languagesList.addAll(it)
            bindViews()
        }
        arabicBtn.setOnClickListener { saveAndNavigate(ARABIC) }
        englishBtn.setOnClickListener { saveAndNavigate(ENGLISH) }
    }

    private fun bindViews() {
        Picasso.get().load(geImage(ARABIC)).into(arabicIcon)
        Picasso.get().load(geImage(ENGLISH)).into(englishIcon)
    }

    private fun geImage(name: String) =
            BuildConfig.IMG_URL + languagesList.first { it.name == name }.image

    private fun saveAndNavigate(name: String) {
        val languageId = languagesList.filter { it.name == name }.map { it.id }.first()
        preferences.saveLanguageId(languageId)
        navigateToNext()
    }

    private fun navigateToNext() {
        startActivity(Intent(this, NewLoginActivity::class.java))
    }

    companion object {
        const val ENGLISH = "English"
        const val ARABIC = "Arabic"
    }
}