package com.app.boxes.ui.resturant.tabs.menu

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseExpandableListAdapter
import android.widget.ImageView
import android.widget.TextView
import com.app.boxes.BuildConfig
import com.app.boxes.R
import com.app.boxes.data.models.MenuCategory
import com.app.boxes.util.gone
import com.squareup.picasso.Picasso

class MenuAdapter(val menuList: List<MenuCategory>) : BaseExpandableListAdapter() {
    override fun getGroupCount() = menuList.size

    override fun getChildrenCount(groupPosition: Int) = menuList[groupPosition].products.size

    override fun getGroup(groupPosition: Int) = menuList[groupPosition]

    override fun getChild(groupPosition: Int, childPosition: Int) = menuList[groupPosition].products[childPosition]

    override fun getGroupId(groupPosition: Int) = 0L

    override fun getChildId(groupPosition: Int, childPosition: Int) = 0L

    override fun hasStableIds() = false

    override fun getGroupView(groupPosition: Int, isExpanded: Boolean, convertView: View?, parent: ViewGroup?): View {
        var view = convertView
        if (view == null) view = LayoutInflater.from(parent?.context).inflate(R.layout.menu_category, null)
        view?.findViewById<View>(R.id.countLayout)?.gone()
        view?.findViewById<TextView>(R.id.menuCategory)?.text = menuList[groupPosition].name
        val arrow: ImageView? = view?.findViewById(R.id.iv_icon)
        if (isExpanded)
            arrow?.animate()?.rotation(0f)?.start()
        else
            arrow?.animate()?.rotation(180f)?.start()
        return view!!
    }

    override fun getChildView(groupPosition: Int, childPosition: Int, isLastChild: Boolean, convertView: View?, parent: ViewGroup?): View {
        var view = convertView
        if (view == null) view = LayoutInflater.from(parent?.context).inflate(R.layout.menu_item_list, null)
        val product = menuList[groupPosition].products[childPosition]

        val image = view?.findViewById<ImageView>(R.id.iv_menu_list_icon)
        Picasso.get().load(BuildConfig.IMG_URL + product.image).into(image)

        view?.findViewById<TextView>(R.id.menu_name)?.text = product.name
        view?.findViewById<TextView>(R.id.menu_desc)?.text = product.description
        view?.findViewById<ImageView>(R.id.iv_menu_list_checked)?.gone()
        view?.findViewById<TextView>(R.id.menu_calories)?.text = product.calories.toString()
        view?.findViewById<TextView>(R.id.menu_carbs)?.text = product.carbohydrates.toString()
        view?.findViewById<TextView>(R.id.menu_protein)?.text = product.proteins.toString()
        view?.findViewById<TextView>(R.id.tv_menu_list_rating)?.text = product.rating.toString()

        return view!!
    }

    override fun isChildSelectable(groupPosition: Int, childPosition: Int) = false
}