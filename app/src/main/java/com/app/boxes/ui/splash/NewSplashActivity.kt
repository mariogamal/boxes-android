package com.app.boxes.ui.splash

import android.content.Intent
import android.os.Bundle
import com.app.boxes.R
import com.app.boxes.base.BaseActivity
import com.app.boxes.ui.NewMainActivity
import com.app.boxes.ui.language.NewLanguageActivity
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.*

class NewSplashActivity : BaseActivity() {

    private val viewModel: SplashViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        if (preferences.getUserToken() != null)
            handleTabSelection()
        else
            openLanguageSelection()
    }

    private fun handleTabSelection() {
        observe(viewModel.currentPackage) {
            openMainActivity(if (it.userSubscription == null) 0 else 1)
        }
    }

    private fun openMainActivity(tabIndex: Int) {
        val extras = Bundle()
        extras.putInt(INDEX, tabIndex)
        start(NewMainActivity::class.java, extras)
    }

    private fun openLanguageSelection() {
        Timer().schedule(object : TimerTask() {
            override fun run() {
                startActivity(Intent(this@NewSplashActivity, NewLanguageActivity::class.java))
            }
        }, 2000)
    }

    companion object {
        const val INDEX = "index"
    }
}