package com.app.boxes.ui.confirmation

import androidx.lifecycle.liveData
import com.app.boxes.base.BaseViewModel
import com.app.boxes.data.remote.Resource
import com.app.boxes.data.repositories.OrderConfirmRepository
import com.app.boxes.data.requests.SubscriptionRequest

class OrderConfirmViewModel(private val repository: OrderConfirmRepository) : BaseViewModel() {
    fun subscribePayment(request: SubscriptionRequest) = liveData {
        emit(Resource.Loading())
        emit(repository.subscribePayment(request))
    }
}