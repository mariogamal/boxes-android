package com.app.boxes.ui.home

import androidx.lifecycle.liveData
import com.app.boxes.base.BaseViewModel
import com.app.boxes.data.remote.Resource
import com.app.boxes.data.repositories.HomeRepository

class HomeViewModel(private val repository: HomeRepository) : BaseViewModel() {

    val restaurants = liveData {
        emit(Resource.Loading())
        emit(repository.getRestaurants())
    }

}