package com.app.boxes.ui.resturant

import android.os.Bundle
import com.app.boxes.BuildConfig
import com.app.boxes.R
import com.app.boxes.base.BaseActivity
import com.app.boxes.data.models.Restaurant
import com.app.boxes.ui.resturant.tabs.TabAdapter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_restaurant_details.*

class RestaurantDetailsActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_restaurant_details)
        back.setOnClickListener { finish() }
        intent.getParcelableExtra<Restaurant>(RESTAURANT)?.let {
            bindRestaurant(it)
            val showPackages = intent.getBooleanExtra(SHOW_PACKAGES, false)
            menuViewPager.adapter =
                    TabAdapter(supportFragmentManager, if (showPackages) 2 else 1, this, it.id)
            menuTabLayout.setupWithViewPager(menuViewPager)
        }
    }

    private fun bindRestaurant(restaurant: Restaurant) {
        headerName.text = restaurant.name
        restaurantName.text = restaurant.name
        restaurantCategory.text = restaurant.caption
        val imagePath = BuildConfig.IMG_URL + restaurant.image
        Picasso.get().load(imagePath).into(restaurantImage)
    }

    companion object {
        const val RESTAURANT = "restaurant"
        const val SHOW_PACKAGES = "packages"
    }
}