package com.app.boxes.ui.splash

import androidx.lifecycle.liveData
import com.app.boxes.base.BaseViewModel
import com.app.boxes.data.models.CurrentPackage
import com.app.boxes.data.remote.Resource
import com.app.boxes.data.repositories.ProfileRepository

/**
 * Created by Mario Gamal on 12/7/20
 * Copyright © 2020 Jusclean. All rights reserved.
 */
class SplashViewModel(private val repository: ProfileRepository): BaseViewModel() {

    val currentPackage = liveData {
        emit(Resource.Loading())
        val response = repository.getCurrentPackage()
        if (response.data == null)
            response.data = CurrentPackage()
        emit(response)
    }

}