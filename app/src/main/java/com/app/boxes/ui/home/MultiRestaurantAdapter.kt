package com.app.boxes.ui.home

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.app.boxes.BuildConfig
import com.app.boxes.R
import com.app.boxes.data.models.Restaurant
import com.app.boxes.ui.resturant.RestaurantDetailsActivity
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.list_multi_restaurant.view.*

class MultiRestaurantAdapter(
        private val dataSet: List<Restaurant>,
        val onCountChanged: (resturants: List<Restaurant>) -> Unit) :
        RecyclerView.Adapter<MultiRestaurantAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val restaurantImage: ImageView = view.restaurantImage
        val restaurantName: TextView = view.restaurantName
        val restaurantCategory: TextView = view.restaurantCategory
        val counterPlus: ImageView = view.plus
        val counterMinus: ImageView = view.minus
        val counterText: TextView = view.count
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int) =
            ViewHolder(LayoutInflater.from(viewGroup.context)
                    .inflate(R.layout.list_multi_restaurant, viewGroup, false))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.restaurantName.text = dataSet[position].name
        holder.restaurantCategory.text = dataSet[position].caption
        val imagePath = BuildConfig.IMG_URL + dataSet[position].image
        holder.counterText.text = dataSet[position].selectedWeeks.toString()
        Picasso.get().load(imagePath).into(holder.restaurantImage)

        holder.counterPlus.setOnClickListener {
            val count = dataSet[position].selectedWeeks
            if (getTotalWeeks() < 4 && count < 3) {
                dataSet[position].selectedWeeks = count + 1
                notifyItemChanged(position)
                onCountChanged(dataSet.filter { it.selectedWeeks > 0 })
            }
        }

        holder.counterMinus.setOnClickListener {
            val count = dataSet[position].selectedWeeks
            if (getTotalWeeks() > 0 && count > 0) {
                dataSet[position].selectedWeeks = count - 1
                notifyItemChanged(position)
                onCountChanged(dataSet.filter { it.selectedWeeks > 0 })
            }
        }

        holder.itemView.setOnClickListener {
            val intent = Intent(it.context, RestaurantDetailsActivity::class.java)
            intent.putExtra(RestaurantDetailsActivity.RESTAURANT, dataSet[position])
            it.context.startActivity(intent)
        }
    }

    private fun getTotalWeeks() = dataSet.sumBy { it.selectedWeeks }

    override fun getItemCount() = dataSet.size
}