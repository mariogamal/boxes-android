package com.app.boxes.ui.calander

import androidx.lifecycle.liveData
import com.app.boxes.base.BaseViewModel
import com.app.boxes.data.remote.Resource
import com.app.boxes.data.repositories.CalendarRepository

class CalendarViewModel(private val repository: CalendarRepository) : BaseViewModel() {

    val calendar = liveData {
        emit(Resource.Loading())
        emit(repository.getCalendar())
    }

}