package com.app.boxes.ui.calander

import android.os.Bundle
import android.transition.TransitionManager
import com.app.boxes.R
import com.app.boxes.base.BaseActivity
import kotlinx.android.synthetic.main.activity_calander.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import sun.bob.mcalendarview.CellConfig

class CalendarActivity : BaseActivity() {

    private val viewModel: CalendarViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_calander)
        observe(viewModel.calendar) {
            venodrsRecycler.adapter = VendorAdapter(it.vendors)
        }

        changeSize.setOnClickListener {
            if (CellConfig.ifMonth)
                calendarView.shrink()
            else
                calendarView.expand()
            TransitionManager.beginDelayedTransition(containerLayout)
        }
    }
}