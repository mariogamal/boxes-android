package com.app.boxes.ui.profile

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.app.boxes.BuildConfig
import com.app.boxes.R
import com.app.boxes.base.BaseFragment
import com.app.boxes.data.models.CurrentPackage
import com.app.boxes.data.models.User
import com.app.boxes.ui.NewMainActivity
import com.app.boxes.ui.calander.CalendarActivity
import com.app.boxes.ui.home.SingleRestaurantAdapter
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.fragment_profile.*
import org.koin.androidx.viewmodel.ext.android.viewModel

class NewProfileFragment : BaseFragment() {

    private val viewModel: ProfileViewModel by viewModel()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
            inflater.inflate(R.layout.fragment_profile, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        observe(viewModel.profile) { displayProfile(it) }
        observe(viewModel.packages) { displayPackages(it) }
    }

    private fun displayPackages(packages: CurrentPackage) {
        type.text = packages.typeName
        date.text = packages.userSubscription?.validity
        if (packages.userSubscription != null) {
            profileAction.text = getString(R.string.choose_your_meals)
            profileAction.setOnClickListener {
                startActivity(Intent(activity, CalendarActivity::class.java))
            }
        } else {
            profileAction.text = getString(R.string.subscribed_now)
            profileAction.setOnClickListener {
                (activity as NewMainActivity).bottomNav.selectedItemId = R.id.home
            }
        }

        packages.userSubscription?.subscriptionListing?.map { it.restaurantListing }?.let {
            restaurantsRecycler.adapter = SingleRestaurantAdapter(it, false)
        }
    }

    private fun displayProfile(profile: User) {
        profileName.text = profile.name
        if (!profile.image.isNullOrEmpty())
            Picasso.get()
                    .load(BuildConfig.IMG_URL + profile.image)
                    .into(profileImage)
    }
}