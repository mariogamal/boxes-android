package com.app.boxes.base

import com.app.boxes.data.models.Response
import com.app.boxes.data.remote.Resource
import com.app.boxes.data.remote.ResponseHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception

open class BaseRepository {

    suspend fun <T> handleResponse(apiMethod: suspend () -> Response<T>): Resource<T> =
            withContext(Dispatchers.IO) {
                try {
                    ResponseHandler.handleSuccess(apiMethod())
                } catch (ex: Exception) {
                    ex.printStackTrace()
                    ResponseHandler.handleException(ex)
                }
            }
}