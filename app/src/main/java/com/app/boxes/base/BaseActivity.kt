package com.app.boxes.base

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import com.app.boxes.R
import com.app.boxes.data.local.Preferences
import com.app.boxes.data.remote.Resource
import com.app.boxes.util.ProgressDialog
import com.app.boxes.util.snack
import org.koin.android.ext.android.inject

open class BaseActivity : AppCompatActivity() {

    val preferences: Preferences by inject()
    lateinit var rootView: View

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        rootView = findViewById(android.R.id.content)
    }

    fun start(activity: Class<out BaseActivity>, extras: Bundle? = null) {
        val intent = Intent(this, activity)
        extras?.let { intent.putExtras(it)}
        startActivity(intent)
    }

    fun <T> observe(liveData: LiveData<Resource<T>>, result: (data: T) -> Unit) {
        liveData.observe(this, {
            when (it) {
                is Resource.Loading -> ProgressDialog.getInstance().show(this)
                is Resource.Success -> {
                    ProgressDialog.getInstance().dismiss()
                    it.data?.let { data -> result(data) }
                }
                is Resource.Error -> {
                    ProgressDialog.getInstance().dismiss()
                    it.message?.let { error -> rootView.snack(error) }
                }
            }
        })
    }

    fun showMessage(message: String) {
        rootView.snack(message)
    }
}