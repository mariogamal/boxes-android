package com.app.boxes.base

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import com.app.boxes.data.local.Preferences
import com.app.boxes.data.remote.Resource
import com.app.boxes.util.ProgressDialog
import com.app.boxes.util.snack
import org.koin.android.ext.android.inject

open class BaseFragment: Fragment() {

    val preferences: Preferences by inject()
    private lateinit var rootView: View

    override fun onAttach(context: Context) {
        super.onAttach(context)
        rootView = (context as BaseActivity).rootView
    }

    fun start(activity: Class<out BaseActivity>, extras: Bundle? = null) {
        val intent = Intent(requireActivity(), activity)
        extras?.let { intent.putExtras(it)}
        startActivity(intent)
    }

    fun <T> observe(liveData: LiveData<Resource<T>>, result: (data: T) -> Unit) {
        liveData.observe(viewLifecycleOwner, {
            when (it) {
                is Resource.Loading -> ProgressDialog.getInstance().show(activity)
                is Resource.Success -> {
                    ProgressDialog.getInstance().dismiss()
                    it.data?.let { data -> result(data) }
                }
                is Resource.Error -> {
                    ProgressDialog.getInstance().dismiss()
                    it.message?.let { error -> rootView.snack(error) }
                }
            }
        })
    }

    fun showMessage(message: String) {
        rootView.snack(message)
    }
}