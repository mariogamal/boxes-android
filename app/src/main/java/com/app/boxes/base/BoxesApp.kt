package com.app.boxes.base

import android.app.Application
import com.app.boxes.di.networkModule
import com.app.boxes.di.prefModule
import com.app.boxes.di.repositoryModule
import com.app.boxes.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

/**
 * Created by Mario Gamal on 12/7/20
 * Copyright © 2020 Jusclean. All rights reserved.
 */
class BoxesApp: Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@BoxesApp)
            modules(listOf(prefModule, networkModule, repositoryModule, viewModelModule))
        }
    }
}