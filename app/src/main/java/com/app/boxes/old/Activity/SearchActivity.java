package com.app.boxes.old.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.SearchView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.boxes.old.API.API;
import com.app.boxes.old.Adapter.SearchHistoryAdapter;
import com.app.boxes.old.Adapter.SingleRestaurantListAdapter;
import com.app.boxes.old.Listener.RecyclerItemClickListener;
import com.app.boxes.old.Model.Language;
import com.app.boxes.old.Model.SingleRestaurant;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SearchActivity extends AppCompatActivity implements SearchHistoryAdapter.SearchHistoryClickListener {

    private static final String TAG = "SearchActivity";
    String typeId = null;

    private SearchView searchView;
    private RecyclerView rvSearchResult, rvHistory;
    private View snackbarView;

    private ArrayList<SingleRestaurant> singleRestaurantArrayList = new ArrayList<>();
    private ArrayList<String> searchList = new ArrayList<>();
    private MySharedPreferences mPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        initializer();

        final Language language = mPref.getLangObject();

        Log.d(TAG, "LanguageId: " + language.getId() + " Name: " + language.getName() + " Code: " + language.getCode());

        Intent getIntent = getIntent();
        typeId = getIntent.getStringExtra("type_id");

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                if (s.length() > 2) {
                    // check if history recyclreview is visible then hide it
                    if (rvHistory.getVisibility() == View.VISIBLE)
                        rvHistory.setVisibility(View.GONE);
                    // check if gym recyclerview is not visible then show it
                    if (rvSearchResult.getVisibility() == View.GONE)
                        rvSearchResult.setVisibility(View.VISIBLE);

                    if (typeId != null)
                        search(s, language.getId(), typeId);
                    if (!searchList.contains(s))
                        searchList.add(s);
                    mPref.saveSearchHistoryArrayList(searchList);
                } else {
                    singleRestaurantArrayList.clear();
                }
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });

        // Catch event on [x] button inside search view
        int searchCloseButtonId = searchView.getContext().getResources()
                .getIdentifier("android:id/search_close_btn", null, null);
        ImageView closeButton = this.searchView.findViewById(searchCloseButtonId);

        // Set on click listener
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Hiding recyclerview on close button
                if (rvSearchResult.getVisibility() == View.VISIBLE)
                    rvSearchResult.setVisibility(View.GONE);
                // Showing history recyclerview on close button
                if (rvHistory.getVisibility() == View.GONE)
                    rvHistory.setVisibility(View.VISIBLE);

                searchView.clearFocus();
                searchView.setQueryHint(getResources().getString(R.string.search_here));
                searchView.setIconified(true);
            }
        });
    }

    // component initialization
    void initializer() {
        searchView = findViewById(R.id.sv_search_activity);

        rvSearchResult = findViewById(R.id.rv_search_result);
        rvHistory = findViewById(R.id.rv_search_history);

        snackbarView = findViewById(android.R.id.content);

        mPref = new MySharedPreferences(this);

        searchList = mPref.getSearchHistroyArrayList();
        if (searchList != null) {
            rvSearchResult.setVisibility(View.GONE);
            Log.d(TAG, "onCreate: searchList 1 : " + searchList.size());
            SearchHistoryAdapter searchAdapter = new SearchHistoryAdapter(searchList, SearchActivity.this);
            rvHistory.setAdapter(searchAdapter);
//                searchAdapter.setOnItemClickListener(this);
            rvHistory.addOnItemTouchListener(new RecyclerItemClickListener(this, rvHistory, new RecyclerItemClickListener.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    String text = searchList.get(position);
                    searchView.setQuery(text, false);
                    searchView.requestFocus();
                    searchView.setFocusable(true);
                }

                @Override
                public void onLongItemClick(View view, int position) {

                }
            }));
        } else {
            searchList = new ArrayList<>();
        }
    }

    // searching from server
    void search(String query, String langId, String typeId) {
        if (query.contains(" "))
            query.replace(" ", "%20");
        String URL = API.getAPI() + "search?keyword=" + query + "&language_id=" + langId + "&type_id=" + typeId;
        Log.d(TAG, "Search URL: " + URL);

        RequestQueue requestQueue;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Search Response: " + response);

                try {
                    singleRestaurantArrayList.clear();
                    JSONObject jsonObject = new JSONObject(response);

                    int status = jsonObject.optInt("status");
                    String message = jsonObject.optString("message");

                    Snackbar snackbar = Snackbar.make(snackbarView, message, Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));

                    if (status == 1) {
                        JSONArray dataArray = jsonObject.getJSONArray("data");

                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObj = dataArray.getJSONObject(i);

                            String id = dataObj.getString("id");
                            String listing_name = dataObj.getString("listing_name");
                            String image = dataObj.getString("image");
                            String listing_caption = dataObj.getString("listing_caption");
                            String type_id = dataObj.getString("type_id");

                            String imageUrl = API.getImageAPI() + image;

                            SingleRestaurant singleRestaurant = new SingleRestaurant(id, imageUrl, listing_name, listing_caption);
                            singleRestaurantArrayList.add(singleRestaurant);

                            SingleRestaurantListAdapter adapter = new SingleRestaurantListAdapter(getApplicationContext(), singleRestaurantArrayList, null);
//                            adapter.filter(listing_name);
                            rvSearchResult.setAdapter(adapter);

                            Log.d(TAG, "Data Object: " + id + " " + listing_name + " " + API.getImageAPI() + image + " " + listing_caption + " " + type_id);
                        }

                        snackbar.show();
                    } else if (status == 0) {
                        singleRestaurantArrayList.clear();
                        snackbar.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "Search Error: " + error.toString());
            }
        });
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    @Override
    public void onHistoryItemClickListener(int pos, View view) {

    }
}
