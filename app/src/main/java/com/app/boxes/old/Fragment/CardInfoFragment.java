package com.app.boxes.old.Fragment;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavOptions;
import androidx.navigation.Navigation;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.app.boxes.old.API.API;
import com.app.boxes.old.Activity.MainActivity;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class CardInfoFragment extends Fragment {


    private String TAG = "CardInfoFragment";
    private int isSave = 0;

    private EditText etCardName, etCardNumber, etExpiryDate, etCvc;
    private TextView tvPackageNameAndPrice;
    private ImageView ivBtnBack, ivChecked, ivUnhecked;
    private Button btnApply;
    private LinearLayout loaderLayout;

    private MySharedPreferences mPref;

    private Bundle getPackageData;

    View snackbarView;

    public CardInfoFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_card_info, container, false);

        //initialize widgets
        initializer(rootView);

        //if arabic language is selected icon changed to forward
        if (mPref.getLocale().equals("ar"))
            ivBtnBack.setImageResource(R.drawable.ic_arrow_forward_white);

        //navigate back
        ivBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*if (getFragmentManager() != null) {
                    getFragmentManager().popBackStack();
                }*/
                Navigation.findNavController(mContext, R.id.main_content).navigateUp();
            }
        });

        //get the bundle data
        getPackageData = getArguments();

        //check if bundle is not empty
        if (getPackageData != null) {

            final String packageId = getPackageData.getString("package_id");
            String packageName = getPackageData.getString("package_name");
            String packagePrice = getPackageData.getString("package_price");
            final String listingId = getPackageData.getString("listing_id");
            final String typeId = getPackageData.getString("type_id");

            String packageNameAndPrice = packageName + " " + packagePrice;

            //set the package name and price in title
            tvPackageNameAndPrice.setText(packageNameAndPrice);

            //store card info
            btnApply.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    String token = mPref.getAccessTokenObject().getAccessToken();

                    if (token != null) {

                        Log.d(TAG, "onClick: " + token);
                        saveCardInfo(packageId, listingId, typeId, isSave, token);
                    }
                }
            });

            Log.d(TAG, "bundle Data: " + packageId + " " + packageName + " " + packagePrice + " listing id: "
                    + listingId + " type id: " + typeId);
        } else {
            Log.d(TAG, "bundle data is empty");
        }

        etExpiryDate.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if (editable.length() > 0 && (editable.length() % 3) == 0) {
                    final char c = editable.charAt(editable.length() - 1);
                    if ('/' == c) {
                        editable.delete(editable.length() - 1, editable.length());
                    }
                }
                if (editable.length() > 0 && (editable.length() % 3) == 0) {
                    char c = editable.charAt(editable.length() - 1);
                    if (Character.isDigit(c) && TextUtils.split(editable.toString(), String.valueOf("/")).length <= 2) {
                        editable.insert(editable.length() - 1, String.valueOf("/"));
                    }
                }
            }
        });

//        //uncheck if checked
        ivChecked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ivChecked.setVisibility(View.GONE);
                ivUnhecked.setVisibility(View.VISIBLE);

                isSave = 0;
            }
        });

        //check if unchecked
        ivUnhecked.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ivUnhecked.setVisibility(View.GONE);
                ivChecked.setVisibility(View.VISIBLE);

                isSave = 1;
            }
        });

        /*rootView.setFocusableInTouchMode(true);
        rootView.requestFocus();
        rootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_BACK) {

                    getActivity().getSupportFragmentManager();
                    FragmentTransaction trans = null;
                    if (getFragmentManager() != null) {
                        trans = getFragmentManager().beginTransaction();
                        trans.remove(CardInfoFragment.this);
                        trans.commit();
                    }

//                    if (getFragmentManager() != null) {
//                        getFragmentManager()
//                                .popBackStack();
//                    }
                    return true;
                }
                return false;
            }
        });*/

        return rootView;
    }

    private Activity mContext;

    @Override
    public void onAttach(Context context) {
        this.mContext = (Activity) context;
        super.onAttach(context);
    }

    private void initializer(View rootView) {
        tvPackageNameAndPrice = rootView.findViewById(R.id.tv_card_info_package_name_and_price);

        etCardName = rootView.findViewById(R.id.et_card_info_card_holder_name);
        etCardNumber = rootView.findViewById(R.id.et_card_info_card_number);
        etExpiryDate = rootView.findViewById(R.id.et_card_info_expiry_date);
        etCvc = rootView.findViewById(R.id.et_card_info_cvc);

        ivBtnBack = rootView.findViewById(R.id.iv_card_info_back);
        ivChecked = rootView.findViewById(R.id.iv_card_info_check);
        ivUnhecked = rootView.findViewById(R.id.iv_card_info_uncheck);

        loaderLayout = rootView.findViewById(R.id.ll_loader_card_info);
        loaderLayout.setVisibility(View.GONE);

        btnApply = rootView.findViewById(R.id.btn_card_info_apply);

        snackbarView = mContext.findViewById(android.R.id.content);

        mPref = new MySharedPreferences(getActivity());
    }

    //store card info
    private void saveCardInfo(String packageId, String listingId, String typeId, int isSave, final String accessToken) {

        String cardHolderName = etCardName.getText().toString();
        String cardNumber = etCardNumber.getText().toString();
        String expiryDate = etExpiryDate.getText().toString();
        String cvc = etCvc.getText().toString();

        if (cardHolderName.isEmpty()) {
            Snackbar snackbar = Snackbar.make(snackbarView, "Please fill the required field", Snackbar.LENGTH_SHORT);
            View getSnackbarView = snackbar.getView();
            getSnackbarView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
            tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));
            snackbar.show();
        } else if (cardNumber.isEmpty()) {
            Snackbar snackbar = Snackbar.make(snackbarView, "Please fill the required field", Snackbar.LENGTH_SHORT);
            View getSnackbarView = snackbar.getView();
            getSnackbarView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
            tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));
            snackbar.show();
        } else if (cardNumber.length() < 16) {
            Snackbar snackbar = Snackbar.make(snackbarView, "card number should be 16 digit", Snackbar.LENGTH_SHORT);
            View getSnackbarView = snackbar.getView();
            getSnackbarView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
            tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));
            snackbar.show();
        } else if (expiryDate.isEmpty()) {
            Snackbar snackbar = Snackbar.make(snackbarView, "Please fill the required field", Snackbar.LENGTH_SHORT);
            View getSnackbarView = snackbar.getView();
            getSnackbarView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
            tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));
            snackbar.show();
        } else if (cvc.isEmpty()) {
            Snackbar snackbar = Snackbar.make(snackbarView, "Please fill the required field", Snackbar.LENGTH_SHORT);
            View getSnackbarView = snackbar.getView();
            getSnackbarView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
            tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));
            snackbar.show();
        } else if (cvc.length() < 3) {
            Snackbar snackbar = Snackbar.make(snackbarView, "cvc should be 3 digits", Snackbar.LENGTH_SHORT);
            View getSnackbarView = snackbar.getView();
            getSnackbarView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
            tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));
            snackbar.show();
            etCvc.requestFocus();
        } else {
            Log.d(TAG, "saveCardInfo: " + cardHolderName + " " + cardNumber + " " + expiryDate + " " + cvc);

            if (cardHolderName.contains(" "))
                cardHolderName = cardHolderName.replace(" ", "%20");

            String url = API.getAPI() + "auth/user-subscription?package_id=" + packageId + "&listing_id=" + listingId + "&type_id=" + typeId + "&card_holder_name=" + cardHolderName
                    + "&card_number=" + cardNumber + "&expiry_date=" + expiryDate + "&cvv=" + cvc + "&is_save=" + isSave;
            Log.d(TAG, "Card info URL: " + url);

            loaderLayout.setVisibility(View.VISIBLE);
            RequestQueue requestQueue;
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d(TAG, "Card info onResponse: " + response);
                    loaderLayout.setVisibility(View.GONE);

                    try {
                        int status = response.optInt("status");
                        String message = response.getString("message");

                        if (status == 1) {
                            //JSONObject data = jsonObject.getJSONObject("data");

                            Snackbar snackbar = Snackbar.make(snackbarView, message, Snackbar.LENGTH_SHORT);
                            View getSnackbarView = snackbar.getView();
                            getSnackbarView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
                            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                            tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));
                            snackbar.show();

                            new CountDownTimer(1500, 1000) {

                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    /*if (getFragmentManager() != null) {
                                        getFragmentManager().beginTransaction()
                                                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                                                .replace(R.id.main_content, new ThankYouFragment())
                                                .commit();
                                    }*/
                                    Bundle bundle = new Bundle();
                                    bundle.putString("from", "cardInfo");
                                    ((MainActivity) mContext).navigationController(R.id.thankYouFragment, bundle, null);
                                }
                            }.start();
                        } else {
                            Snackbar snackbar = Snackbar.make(snackbarView, message, Snackbar.LENGTH_SHORT);
                            View getSnackbarView = snackbar.getView();
                            getSnackbarView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
                            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                            tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));
                            snackbar.show();

                            new CountDownTimer(1500, 1000) {

                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    /*if (getFragmentManager() != null) {
                                        getFragmentManager().beginTransaction()
                                                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                                                .replace(R.id.main_content, new FoodFragment())
                                                .commit();
                                    }*/
                                    NavOptions navOptions = new NavOptions.Builder().setPopUpTo(R.id.flowerHomeFragment, false).build();
                                    ((MainActivity) mContext).navigationController(R.id.foodFragment, navOptions);
                                }
                            }.start();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                        Log.d(TAG, "onException: " + e.getMessage());
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loaderLayout.setVisibility(View.GONE);
                    Log.d(TAG, "Volley Error: " + error.toString());
                    if (error instanceof NoConnectionError) {
                        Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.no_network), Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));
                        snackbar.show();
                    } else {
                        Snackbar snackbar = Snackbar.make(snackbarView, error.getMessage(), Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));
                        snackbar.show();
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    if (accessToken != null) {
                        params.put("Authorization", accessToken);
                        Log.d(TAG, "getHeaders " + accessToken);
                    }
                    return params;
                }
            };
            requestQueue = Volley.newRequestQueue(mContext);
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(jsonObjectRequest);
        }
    }

}
