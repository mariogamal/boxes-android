package com.app.boxes.old.Fragment;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.boxes.old.API.API;
import com.app.boxes.old.Adapter.MyPackagesAdapter;
import com.app.boxes.old.Model.Language;
import com.app.boxes.old.Model.MyPackages;
import com.app.boxes.old.Model.User;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.app.boxes.old.UI.LoginBottomSheet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */

//@Puppet
public class MyPackagesFragment extends Fragment {

    private static final String TAG = "MyPackagesFragment";

    private RecyclerView recyclerView;
    private View snackbarView;
    private RelativeLayout rlNoResult;
    private LinearLayout loaderLayout;

    private MyPackagesAdapter adapter;
    private ArrayList<MyPackages> myPackagesArrayList = new ArrayList<MyPackages>();

    private Activity mContext;

    @Override
    public void onAttach(Context context) {
        this.mContext = (Activity) context;
        super.onAttach(context);
    }

    public MyPackagesFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview = inflater.inflate(R.layout.fragment_my_packages, container, false);

        //initialize widgets
        recyclerView = rootview.findViewById(R.id.rv_my_packages);

        rlNoResult = rootview.findViewById(R.id.rl_my_packages_no_result);
        rlNoResult.setVisibility(View.GONE);

        loaderLayout = rootview.findViewById(R.id.ll_loader_my_packages);
        loaderLayout.setVisibility(View.GONE);

        snackbarView = mContext.findViewById(android.R.id.content);

        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));

        MySharedPreferences sPref = new MySharedPreferences(mContext);

        Button btnHome = rootview.findViewById(R.id.btn_my_packages_no);

        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.onBackPressed();
            }
        });

        Language language = sPref.getLangObject();

        if (sPref.getAccessTokenObject() != null) {
            User.AccessToken token = sPref.getAccessTokenObject();
            if (token != null && language != null)
                getPackagesHitsory(language.getId(), token.getAccessToken());
        } else {
            LoginBottomSheet loginBottomSheet = new LoginBottomSheet();
            loginBottomSheet.setCancelable(false);
            if (getFragmentManager() != null) {
                loginBottomSheet.show(getFragmentManager(), "login_bottom_sheet");
            }
        }

        return rootview;
    }

    private void alertDialog(final Context context) {

        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(getString(R.string.message));
        alertDialog.setMessage(getString(R.string.message_content));
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                alertDialog.dismiss();
                dialogInterface.dismiss();
                mContext.finish();
            }
        });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
//                alertDialog.cancel();
                alertDialog.dismiss();
//                dialogInterface.dismiss();

                //getFragmentManager().popBackStackImmediate();
            }
        });

        alertDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int keyCode, KeyEvent keyEvent) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    alertDialog.dismiss();
                }
                return true;
            }
        });
        alertDialog.show();
    }

    private void getPackagesHitsory(String langId, final String accessToken) {
        String BASE_URL = API.getAPI() + "auth/my-packages?language_id=" + langId;
        Log.d(TAG, "Packages History URL: " + BASE_URL);
        loaderLayout.setVisibility(View.VISIBLE);

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, BASE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                rlNoResult.setVisibility(View.GONE);

                Log.d(TAG, "onResponse: " + response);
                loaderLayout.setVisibility(View.GONE);

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    int status = jsonObject.optInt("status");
                    String msg = jsonObject.optString("message");

                    MyPackages history;

                    if (status == 1) {
                        myPackagesArrayList.clear();
                        JSONArray dataArray = jsonObject.optJSONArray("data");
                        if (dataArray != null) {
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObj = dataArray.getJSONObject(i);

                                String pkgName = dataObj.getString("package_name");
                                String typeName = dataObj.getString("type_name");
                                String created_at = dataObj.getString("created_at");
                                String expired_at = dataObj.getString("expired_at");

                                history = new MyPackages(pkgName, created_at, expired_at, typeName);
                                myPackagesArrayList.add(history);
                            }

                            adapter = new MyPackagesAdapter(mContext, myPackagesArrayList);
                            recyclerView.setAdapter(adapter);

                            /*recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(mContext, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                    Log.d("isItemClick1", isItemClicked + "");
                                    if (!isItemClicked) {
                                        OrderHistory orderHistory = myPackagesArrayList.get(position);
                                        String itemClicked = orderHistory.getOrderId();

                                        Bundle bundle = new Bundle();
                                        bundle.putString("orderId", itemClicked);

                                        OrderHistoryDetailFragment fragment = new OrderHistoryDetailFragment();
                                        fragment.setArguments(bundle);

                                        getFragmentManager()
                                                .beginTransaction()
                                                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                                                .replace(R.id.main_content, fragment)
                                                .addToBackStack(null)
                                                .commit();

                                        isItemClicked = true;
                                        Log.d("isItemClick2", isItemClicked + "");

                                        //timer to make isItemClicked = false
                                        new CountDownTimer(1000, 1000) {
                                            @Override
                                            public void onTick(long l) {

                                            }

                                            @Override
                                            public void onFinish() {
                                                isItemClicked = false;
                                                Log.d("isItemClick3", isItemClicked + "");
                                            }
                                        }.start();
                                    }
                                }

                                @Override
                                public void onLongItemClick(View view, int position) {

                                }
                            }));*/
                        }
                    } else {
                        rlNoResult.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    Log.d(TAG, "JSONException: " + e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loaderLayout.setVisibility(View.GONE);
                Log.d(TAG, "onErrorResponse: " + error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (accessToken != null) {
                    params.put("Authorization", accessToken);
                    Log.d(TAG, "getHeaders: " + accessToken);
                }
                return params;
            }
        };

        requestQueue = Volley.newRequestQueue(mContext);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
}
