package com.app.boxes.old.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.app.boxes.old.Model.SingleRestaurant;
import com.app.boxes.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import at.grabner.circleprogress.CircleProgressView;
import de.hdodenhof.circleimageview.CircleImageView;

public class SubscribedMultiRestaurantAdapter extends RecyclerView.Adapter<SubscribedMultiRestaurantAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<SingleRestaurant> subscribedRestaurantArrayList;
    private LayoutInflater inflater;
    private int sSelected = 0;

    private static SubscribedMultiRestaurantClickListener sClickListener;

    public SubscribedMultiRestaurantAdapter(Context context, ArrayList<SingleRestaurant> subscribedRestaurantArrayList, SubscribedMultiRestaurantClickListener listener) {
        this.context = context;
        this.subscribedRestaurantArrayList = subscribedRestaurantArrayList;
        sClickListener = listener;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView = inflater.inflate(R.layout.subscribed_multi_res_item, parent, false);

        return new MyViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        SingleRestaurant item = subscribedRestaurantArrayList.get(position);
        Picasso.get().load(item.getImages()).into(holder.ivRestaurantIcon);

        holder.progressView.setBlockCount(Integer.parseInt(item.getMealOutOff()));
        holder.progressView.setMaxValue(Integer.parseInt(item.getMealOutOff()));
        holder.progressView.setValue(Integer.parseInt(item.getMealCount()));
        // holder.progressView.setBlockScale(0.9f);
        holder.tvRestaurantName.setText(item.getName());
        holder.tvMealCount.setText(item.getMealCount());
        holder.tvMealOutOff.setText(item.getMealOutOff());

//        if (item.isSelected()) {
//            holder.linearLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.active_tab_color));
//        } else {
//            holder.linearLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
//        }

        if (sSelected == position) {
            item.setSelected(true);
//            holder.tvRestaurantName.setBackgroundColor(ContextCompat.getColor(context, R.color.active_tab_color));
            holder.tvRestaurantName.setTextColor(ContextCompat.getColor(context, R.color.white));
            holder.tvRestaurantName.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.custom_oval));
            holder.ll.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.custom_oval));
            holder.tvMealCount.setTextColor(ContextCompat.getColor(context, R.color.white));
            holder.tvSlash.setTextColor(ContextCompat.getColor(context, R.color.white));
            holder.tvMealOutOff.setTextColor(ContextCompat.getColor(context, R.color.white));
        } else {
            item.setSelected(false);
//            holder.tvRestaurantName.setBackgroundColor(ContextCompat.getColor(context, android.R.color.transparent));
            holder.tvRestaurantName.setTextColor(ContextCompat.getColor(context, R.color.inactive_tab_color));
            holder.tvRestaurantName.setBackgroundDrawable(null);
            holder.ll.setBackgroundDrawable(null);
            holder.tvMealCount.setTextColor(ContextCompat.getColor(context, R.color.inactive_tab_color));
            holder.tvSlash.setTextColor(ContextCompat.getColor(context, R.color.inactive_tab_color));
            holder.tvMealOutOff.setTextColor(ContextCompat.getColor(context, R.color.inactive_tab_color));

        }

    }

    @Override
    public int getItemCount() {
        return subscribedRestaurantArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        CircleImageView ivRestaurantIcon;
        LinearLayout ll;
        TextView tvRestaurantName, tvMealCount, tvMealOutOff, tvSlash;
//        LinearLayout linearLayout;

        CircleProgressView progressView;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

//            ivRestaurantIcon = itemView.findViewById(R.id.iv_subscribed_multi_restaurant);
//            tvRestaurantName = itemView.findViewById(R.id.tv_subscribed_multi_restaurant_name);
//            tvMealCount = itemView.findViewById(R.id.tv_subscribed_multi_item);
//            tvSlash = itemView.findViewById(R.id.tv_subscribed_slash);
//            tvMealOutOff = itemView.findViewById(R.id.tv_subscribed_multi_item_out_of);

//            ll = itemView.findViewById(R.id.ll_subscribed_multi);
//            linearLayout = itemView.findViewById(R.id.ll_multi_image);

            progressView = itemView.findViewById(R.id.progress_circular);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            sSelected = getAdapterPosition();
            sClickListener.onMultiResItemClick(getAdapterPosition(), view);
        }
    }

    public void selected() {
        notifyDataSetChanged();
    }

    public interface SubscribedMultiRestaurantClickListener {
        void onMultiResItemClick(int position, View view);
    }
}