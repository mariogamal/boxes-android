package com.app.boxes.old.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.boxes.old.Model.EcommerceHistory;
import com.app.boxes.R;

import java.util.ArrayList;

public class EcommerceHistoryAdapter extends RecyclerView.Adapter<EcommerceHistoryAdapter.ViewHolder> {

    private Context context;
    private ArrayList<EcommerceHistory> historyArrayList;

    public EcommerceHistoryAdapter(Context context, ArrayList<EcommerceHistory> historyArrayList) {
        this.context = context;
        this.historyArrayList = historyArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.ecommerce_history_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        EcommerceHistory item = historyArrayList.get(position);

        holder.tvTitle.setText(item.getListingName());
        holder.tvProducts.setText(item.getProducts());
        holder.tvPrice.setText(item.getTotalPrice());
    }

    @Override
    public int getItemCount() {
        return historyArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvTitle, tvProducts, tvPrice;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvTitle = itemView.findViewById(R.id.tv_ecommerce_history_vendor);
            tvProducts = itemView.findViewById(R.id.tv_ecommerce_history_items);
            tvPrice = itemView.findViewById(R.id.tv_ecommerce_history_price);
        }
    }
}
