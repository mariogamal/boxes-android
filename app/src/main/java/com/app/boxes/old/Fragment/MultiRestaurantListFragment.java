package com.app.boxes.old.Fragment;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.boxes.old.API.API;
import com.app.boxes.old.Activity.MainActivity;
import com.app.boxes.old.Adapter.MultiRestaurantListAdapter;
import com.app.boxes.old.Model.MultiRestaurant;
import com.app.boxes.old.Model.RestaurantSubscriptionList;
import com.app.boxes.old.Model.SelectedID;
import com.app.boxes.R;
import com.app.boxes.old.RestaurantSearchSingleton;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
//@Puppet
public class MultiRestaurantListFragment extends Fragment implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private MultiRestaurantListAdapter adapter;
    private ArrayList<MultiRestaurant> multiRestaurantArrayList = new ArrayList<>();
    private ArrayList<MultiRestaurant> searchedList = new ArrayList<>();
    private ArrayList<MultiRestaurant> cacheList = new ArrayList<>();
    private ArrayList<RestaurantSubscriptionList> subscriptionLists;
    private ArrayList<String> listingIdArrayList = new ArrayList<>();
    private ArrayList<MultiRestaurant> selectedRestaurantArrayList = new ArrayList<>();

    private RecyclerView recyclerView;
    private TextView tvCardInfo, tvRestaurantCount;
    private Button cvCheckout;
    private View snackbarView, rootView;
    private LinearLayout loaderLayout;

    private MySharedPreferences sPref;

    private static String TAG = "multiRestaurantFragment";
    private String languageId = "";

    String id = "", name = "", image = "", caption = "", catSubId = "", catSubTypeName = "", catSubParentId = "", catId = "", typeId = "", getSubTypeId = "", getCatSubTypeName = "";

    private Bundle getCategoryBundle;

    // for location
    private GoogleApiClient mGoogleApiClient;
    private Location mLocation;
    private LocationManager mLocationManager;

    private LocationRequest mLocationRequest;
    private long UPDATE_INTERVAL = 300000;  /* 10 secs */
    private long FASTEST_INTERVAL = 300000; /* 2 sec */
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 5445;

    private String tabId = "", query = "";

    private Bundle bundle;

    public MultiRestaurantListFragment() {
        // Required empty public constructor
    }

    public MultiRestaurantListFragment(String tabId) {
        // get tabId
        this.tabId = tabId;
    }

    public MultiRestaurantListFragment(String tabId, String query) {
        // get tabId
        this.tabId = tabId;

        //get bundle data
        this.query = query;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_multi_restaurant_list, container, false);

        //show the toolbar
        ((MainActivity) mContext).getSupportActionBar().show();

        //initialize widgets
        recyclerView = rootView.findViewById(R.id.rv_multi_restaurant_list);

        tvCardInfo = rootView.findViewById(R.id.tv_restaurant_selection);
        tvRestaurantCount = rootView.findViewById(R.id.tv_multi_restaurant_selection_minimum);

        snackbarView = mContext.findViewById(android.R.id.content);

        cvCheckout = rootView.findViewById(R.id.cv_multi_restaurant_checkout);

        int temp = RestaurantSearchSingleton.getInstance().selectedRestaurants.size();
        if (temp >= 2)
            cvCheckout.setVisibility(View.VISIBLE);
        else
            cvCheckout.setVisibility(View.GONE);

        loaderLayout = rootView.findViewById(R.id.ll_loader_multi_restaurant);
        loaderLayout.setVisibility(View.GONE);

        //initialize mysharedpreference class
        sPref = new MySharedPreferences(mContext);

        //checkout button is not visible when restaurant selection is 0
//        sPref.setColorCount(0);

        //get bundle data
        getCategoryBundle = getArguments();

        subscriptionLists = new ArrayList<>();

        //check which language is selected
        if (sPref.getLangObject() != null) {
            languageId = sPref.getLangObject().getId();
            Log.d(TAG, "language id " + sPref.getLangObject().getId());
        }

        //set layout manager for recyclerview
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));

        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mLocationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        checkLocation(); //check whether location service is enable or not in your  phone

        adapter = new MultiRestaurantListAdapter(mContext, multiRestaurantArrayList);
        recyclerView.setAdapter(adapter);

        // set restaurant count to textview
//        tvRestaurantCount.setText(sPref.getColorCount() + "");

        adapter.setOnItemClickListener(new MultiRestaurantListAdapter.MultiRestaurantClickListener() {
            @Override
            public void onItemClick(int position, boolean b, MultiRestaurant item) {
                //adapter.selected(position);

                /*    MultiRestaurant item = multiRestaurantArrayList.get(position);*/
                String itemId = item.getId();

                if (b && selectedRestaurantArrayList.size() < 4) {
                    if (!selectedRestaurantArrayList.contains(item)) {
                        selectedRestaurantArrayList.add(item);
                        multiRestaurantArrayList.get(position).setChecked(1);
                      //  RestaurantSearchSingleton.getInstance().selectionCount++;
                    }
                    if (!listingIdArrayList.contains(itemId)) {
                        listingIdArrayList.add(itemId);
                    }
                } else {
                    multiRestaurantArrayList.get(position).setChecked(0);
                    selectedRestaurantArrayList.remove(item);
                    listingIdArrayList.remove(itemId);
                //    RestaurantSearchSingleton.getInstance().selectionCount--;
                }

                Log.d(TAG, "onItemClick: listingIdArrayList size: " + listingIdArrayList.size());
                for (String s : listingIdArrayList) {
                    Log.d(TAG, "onItemClick: listingId items: " + s);
                }

                //store subtype id
                sPref.setTypeId(itemId);

//                if ((sPref.getColorCount() <= 4)) {
//                    if (isAdded()) {
//                        tvRestaurantCount.setText(sPref.getColorCount() + "");
//                    }
//                } else {
                RestaurantSearchSingleton.getInstance().setSelectedRestaurants(selectedRestaurantArrayList);
                tvRestaurantCount.setText(RestaurantSearchSingleton.getInstance().selectedRestaurants.size()+"");
//                    sPref.setColorCount(0);
//                }
                //recyclerView.setAdapter(new MultiRestaurantListAdapter(getActivity(), multiRestaurantArrayList));
                adapter.notifyDataSetChanged();

                if (RestaurantSearchSingleton.getInstance().selectedRestaurants.size() >= 2)
                    cvCheckout.setVisibility(View.VISIBLE);
                else
                    cvCheckout.setVisibility(View.GONE);

            }

            @Override
            public void onShowMenuClick(int position) {
//                                    Toast.makeText(mContext, "working", Toast.LENGTH_SHORT).show();
                Bundle bundleData = new Bundle();

                final MultiRestaurant item = multiRestaurantArrayList.get(position);
                final String itemId = item.getId();
                String name = item.getName();
                String desc = item.getDescription();
                String itemImage = item.getImages();

                //store item data in bundle
                bundleData.putString("listing_name", name);
                bundleData.putString("listing_desc", desc);
                bundleData.putString("listing_image", itemImage);

                //store listing id in cache
                sPref.setListingId(itemId);

//                Fragment fragment = new RestaurantMenuFragment();
//                fragment.setArguments(bundleData);

                ((MainActivity) mContext).navigationController(R.id.restaurantMenuFragment, bundleData, null);
            }
        });

        bundle = new Bundle();

        // get colorCount from SharedPreference and checking it's value and implementing logic accordingly
        cvCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // declare AlertDialog Builder
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                // declare dialog layout
                View dialogView = LayoutInflater.from(mContext).inflate(R.layout.ok_dialog, null);
                // set layout to dialog
                builder.setView(dialogView);
                // set builder to alertDialog
                final AlertDialog alertDialog = builder.create();

                // get access to layout components
                TextView tvDialogMsg = dialogView.findViewById(R.id.tv_ok_dialog_msg);
                Button btnDialogOk = dialogView.findViewById(R.id.btn_ok_dialog);
                ImageView btnClose = dialogView.findViewById(R.id.iv_ok_dialog_close);

                // set message to dialog
                tvDialogMsg.setText("Please select at least 2 restaurants..");
                // set onclick to dialog ok button
                btnDialogOk.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });
                // set onclick to dialog close button
                btnClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        alertDialog.dismiss();
                    }
                });
                RestaurantSearchSingleton.getInstance().setSelectedRestaurants(selectedRestaurantArrayList);

                int colorCount = RestaurantSearchSingleton.getInstance().selectedRestaurants.size();
                if ((colorCount > 1) && (colorCount <= 4)) {
                    if (subscriptionLists.size() > 0) {
                        sPref.setTypeId(tabId);

                        bundle.putParcelableArrayList("selected_restaurant", selectedRestaurantArrayList);
                        bundle.putParcelableArrayList("packages_list", subscriptionLists);
                        bundle.putStringArrayList("listing_ids", listingIdArrayList);

                        ((MainActivity) mContext).navigationController(R.id.selectedRestaurantFragment, bundle, null);
                    } else {
                        alertDialog.show();
                    }
                } else {
                    alertDialog.show();
                }
            }
        });
//
//        if (bundle != null) {
//            search(bundle.getString("search_result"), languageId);
//        }
        return rootView;
    }


    private Activity mContext;

    @Override
    public void onAttach(Context context) {
        this.mContext = (Activity) context;
        super.onAttach(context);
    }


    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (rootView != null) {
            ViewGroup viewGroup = (ViewGroup) rootView.getParent();
            if (viewGroup != null) {
                viewGroup.removeAllViews();
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    //get multi restaurant listing from server
    private void getMultiRestaurantListing(double latitude, double longitude, final String languageId) {
        String url = API.getAPI() + "listings?user_longitude=" + longitude + "&user_latitude=" + latitude + "&language_id=" + languageId;
        Log.d(TAG, "url " + url);
        loaderLayout.setVisibility(View.VISIBLE);

        RequestQueue requestQueue;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                loaderLayout.setVisibility(View.GONE);
                Log.d(TAG, response.toString());
                try {
                    multiRestaurantArrayList.clear();

                    int status = response.getInt("status");
                    String message = response.getString("message");
                    if (status == 1) {
                        JSONArray dataArray = response.getJSONArray("data");

                        for (int i = 0; i < dataArray.length(); i++) {

                            JSONObject jsonObject = dataArray.getJSONObject(i);
                            id = jsonObject.getString("id");
                            name = jsonObject.getString("listing_name");
                            image = jsonObject.getString("image");
                            caption = jsonObject.getString("listing_caption");

                            String imageUrl = API.getImageAPI() + image;

                            MultiRestaurant multiRestaurant = new MultiRestaurant(id, imageUrl, name, caption);

                            multiRestaurantArrayList.add(multiRestaurant);
                        }

                        mapSelected();

                        adapter.notifyDataSetChanged();
                        cacheMainList();
                    }
                } catch (Exception ex) {
                    Log.d(TAG, "onException" + ex.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loaderLayout.setVisibility(View.GONE);
                Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                View getSnackbarView = snackbar.getView();
                getSnackbarView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
                TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));
                snackbar.show();
            }
        });

        requestQueue = Volley.newRequestQueue(mContext);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }

    private void mapSelected() {
        ArrayList<MultiRestaurant> restaurants = RestaurantSearchSingleton.getInstance().getSelectedRestaurants();
        if (restaurants != null) {


            tvRestaurantCount.setText(restaurants.size() + "");
            if (restaurants.size() > 0) {
                for (int i = 0; i < multiRestaurantArrayList.size(); i++) {
                    for (int j = 0; j < restaurants.size(); j++) {
                        if (multiRestaurantArrayList.get(i).equals(restaurants.get(j))) {
                            multiRestaurantArrayList.get(i).setChecked(1);
                        }
                    }
                }
            }
        }
    }

    private void getPackages(String typeId, String languageId) {
        String url = API.getAPI() + "packages?type_id=" + typeId + "&language_id=" + languageId;
        Log.d(TAG, "getPackages URL: " + url);
        loaderLayout.setVisibility(View.VISIBLE);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                loaderLayout.setVisibility(View.GONE);
                try {
                    int status = response.getInt("status");
                    String message = response.getString("message");

                    if (status == 1) {
                        JSONArray jsonArrayData = response.getJSONArray("data");

                        String subscriptionId, subscriptionPlan, subscriptionPrice, subscriptionMeals, subscriptionTypeId;
                        RestaurantSubscriptionList restaurantSubscriptionList;

                        for (int i = 0; i < jsonArrayData.length(); i++) {
                            JSONObject jsonObject = jsonArrayData.getJSONObject(i);
                            subscriptionId = jsonObject.getString("id");
                            subscriptionPlan = jsonObject.getString("package_name");
                            subscriptionPrice = jsonObject.getString("price");
                            subscriptionMeals = jsonObject.getString("meals");
                            subscriptionTypeId = jsonObject.getString("type_id");

                            restaurantSubscriptionList = new RestaurantSubscriptionList();

                            restaurantSubscriptionList.setId(subscriptionId);
                            restaurantSubscriptionList.setSubscriptionPlan(subscriptionPlan);
                            restaurantSubscriptionList.setSubcriptionPrice(subscriptionPrice);
                            restaurantSubscriptionList.setSunscriptionMeals(subscriptionMeals);
                            restaurantSubscriptionList.setSubscriptionTypeId(subscriptionTypeId);

                            subscriptionLists.add(restaurantSubscriptionList);
                        }
                        Log.d(TAG, "onResponse: subscriptionLists size: " + subscriptionLists.size());
                    }
                } catch (Exception ex) {
                    Log.d(TAG, "" + ex.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loaderLayout.setVisibility(View.GONE);
                Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                View getSnackbarView = snackbar.getView();
                getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                snackbar.show();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED)
                Snackbar.make(snackbarView, "Permission denied by user", Snackbar.LENGTH_LONG).show();
            else if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                startLocationUpdates();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        // Request location updates
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_COARSE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(mContext,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);

                return;
            }
        }

        startLocationUpdates();

        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (mLocation == null) {
            startLocationUpdates();
        }

        if (mLocation != null) {
            // mLatitudeTextView.setText(String.valueOf(mLocation.getLatitude()));
            //mLongitudeTextView.setText(String.valueOf(mLocation.getLongitude()));
        } else {
//            Toast.makeText(activity, , Toast.LENGTH_SHORT).show();
            Log.d(TAG, "Location not Detected");
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Connection Suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(TAG, "Connection failed. Error: " + connectionResult.getErrorCode());
    }

    private void startLocationUpdates() {
        // Create the location request
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL);
        // Request location updates
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_COARSE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(mContext,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);

//                nearbyGymLayout.setVisibility(View.GONE);
                return;
            }
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                mLocationRequest, this);
        Log.d("reque", "--->>>>");
    }

    Location lll;

    @Override
    public void onLocationChanged(Location location) {

        lll = location;
        String msg = "Updated Location: " +
                location.getLatitude() + "," +
                location.getLongitude();

        Log.d(TAG, "onLocationChanged: " + msg);

        if (location != null) {
            getMultiRestaurantListing(location.getLatitude(), location.getLongitude(), languageId);
            getPackages(tabId, languageId);
        } else {
            Log.d(TAG, "category Bundle is empty");
            Log.d(TAG, "onLocationChanged Lat: " + location.getLatitude() + " long: " + location.getLongitude());
        }

    }

    private void checkLocation() {
        if (!isLocationEnabled()) {
            showAlert();
        } else
            isLocationEnabled();
    }

    private boolean isLocationEnabled() {
        mLocationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        assert mLocationManager != null;
        return mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private void showAlert() {
        final androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(mContext);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.location_dialog, null);
        builder.setView(dialogView);

        final androidx.appcompat.app.AlertDialog alertDialog = builder.show();

        Button btnLocation = dialogView.findViewById(R.id.btn_location_dialog_settings);
        Button btnCancel = dialogView.findViewById(R.id.btn_location_dialog_cancel);

        btnLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(myIntent);
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
    }

    // searching from server
    private void search(String query, String langId) {

        query = RestaurantSearchSingleton.getInstance().getQuery();

        if (query.contains(" "))
            query = query.replace(" ", "%20");
        String URL = API.getAPI() + "search?keyword=" + query + "&language_id=" + langId;
        Log.d(TAG, "Search URL: " + URL);
        loaderLayout.setVisibility(View.VISIBLE);

        RequestQueue requestQueue;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                loaderLayout.setVisibility(View.GONE);
                Log.d(TAG, "Search Response: " + response);

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    int status = jsonObject.optInt("status");
                    String message = jsonObject.optString("message");

                    Snackbar snackbar = Snackbar.make(snackbarView, message, Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));

                    if (status == 1) {
                        JSONArray dataArray = jsonObject.getJSONArray("data");


                        searchedList.clear();

                        //ArrayList<MultiRestaurant> tempArrayList = new ArrayList<>();

                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObj = dataArray.getJSONObject(i);

                            String id = dataObj.getString("id");
                            String listing_name = dataObj.getString("listing_name");
                            String image = dataObj.getString("image");
                            String listing_caption = dataObj.getString("listing_caption");
                            String type_id = dataObj.getString("type_id");

                            String imageUrl = API.getImageAPI() + image;

                            MultiRestaurant multiRestaurant = new MultiRestaurant(id, imageUrl, listing_name, listing_caption);
                            searchedList.add(multiRestaurant);

                            Log.d(TAG, "Data Object: " + id + " " + listing_name + " " + API.getImageAPI() + image + " " + listing_caption + " " + type_id);
                        }

                        loadSearchedIntoMain();

                        //adapter = new MultiRestaurantListAdapter(mContext, filterArrayLists(tempArrayList));
                        //adapter.filter(RestaurantSearchSingleton.getInstance().getQuery());
                        //recyclerView.setAdapter(adapter);

                        adapter.notifyDataSetChanged();

                        if (searchedList.size() == cacheList.size()) {

                            ArrayList<SelectedID> selectedIDS = new ArrayList<>();

                            for (int i = 0; i < multiRestaurantArrayList.size(); i++) {
                                SelectedID selectedID = new SelectedID();
                                if (multiRestaurantArrayList.get(i).getChecked() == 1) {
                                    selectedID.setFlag(true);

                                } else {
                                    selectedID.setFlag(false);
                                }
                                selectedID.setId(multiRestaurantArrayList.get(i).getId());
                                selectedIDS.add(selectedID);
                            }
                            multiRestaurantArrayList.clear();
                            for (int i = 0; i < searchedList.size(); i++) {
                                for (int j = 0; j < cacheList.size(); j++) {
                                    if (cacheList.get(i).getId().equals(selectedIDS.get(j).getId())) {
                                        if (selectedIDS.get(j).getFlag()) {
                                            cacheList.get(i).setChecked(1);
                                        }
                                    }
                                }
                                multiRestaurantArrayList.add(cacheList.get(i));
                            }
                        }
                    } else if (status == 0) {
//                        singleRestaurantArrayList.clear();
                        snackbar.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loaderLayout.setVisibility(View.GONE);
                Log.d(TAG, "Search Error: " + error.toString());
                Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                View getSnackbarView = snackbar.getView();
                getSnackbarView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
                TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));
                snackbar.show();
            }
        });
        requestQueue = Volley.newRequestQueue(mContext);
        requestQueue.add(stringRequest);
    }

    private void loadSearchedIntoMain() {
        multiRestaurantArrayList.clear();
        for (int i = 0; i < searchedList.size(); i++) {
            for (int j = 0; j < cacheList.size(); j++) {
                if (searchedList.get(i).getId().equals(cacheList.get(j).getId())) {
                    if (!multiRestaurantArrayList.contains(cacheList.get(j)))
                        multiRestaurantArrayList.add(cacheList.get(j));
                }
            }
        }
    }

    private void cacheMainList() {
        // save multi restaurant data to cache list
        cacheList.clear();
        for (int i = 0; i < multiRestaurantArrayList.size(); i++) {
            cacheList.add(multiRestaurantArrayList.get(i));
        }
    }

    private ArrayList<MultiRestaurant> filterArrayLists(ArrayList<MultiRestaurant> searchedList) {
        ArrayList<MultiRestaurant> temoList = new ArrayList<>();
        for (int i = 0; i < searchedList.size(); i++) {
            for (int j = 0; j < multiRestaurantArrayList.size(); j++) {
                if (searchedList.get(i).getId().equals(multiRestaurantArrayList.get(j).getId())) {
                    if (!temoList.contains(multiRestaurantArrayList.get(j)))
                        temoList.add(multiRestaurantArrayList.get(j));
                }
            }
        }
        return temoList;
    }

    public void refresh() {
        // multiRestaurantArrayList.clear();
        // if (!RestaurantSearchSingleton.getInstance().getQuery().equals(""))
        search(RestaurantSearchSingleton.getInstance().getQuery(), sPref.getLangObject().getId());
    }

    public void refreshAll() {
        multiRestaurantArrayList.clear();
        getMultiRestaurantListing(mLocation.getLatitude(), mLocation.getLongitude(), sPref.getLangObject().getId());
    }

    @Override
    public void onPause() {
        super.onPause();
        if (listingIdArrayList != null && listingIdArrayList.size() > 0)
            RestaurantSearchSingleton.getInstance().setSelectedRestaurantsList(listingIdArrayList);
    }

    @Override
    public void onResume() {
        super.onResume();

        /*ArrayList<String> restaurantIds = RestaurantSearchSingleton.getInstance().getSelectedRestaurantsList();
        if (restaurantIds != null) {
            tvRestaurantCount.setText(restaurantIds.size() + "");
            RestaurantSearchSingleton.getInstance().selectionCount = restaurantIds.size();
        }*/
    }
}