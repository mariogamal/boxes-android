package com.app.boxes.old.Activity;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.boxes.old.API.API;
import com.app.boxes.old.Model.Allergy;
import com.app.boxes.old.Model.Diet;
import com.app.boxes.old.Model.Language;
import com.app.boxes.old.Model.User;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.app.boxes.old.multiselect.MultiSelectDialog;
import com.app.boxes.old.multiselect.MultiSelectModel;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SurveyFormActivity extends AppCompatActivity {

    private static final String TAG = "SurveyFormActivity";

    private ImageView ivBack;
    private TextView tvSkip;
    private EditText edtAge, edtHeightFeet, edtHeightInches, edtWeightKgs, edtBmiInches, edtAdditionalPreferences, edtPrefferedDiet, edtAllergies, edtIngredients;
    private Button btnProceed;
    private LinearLayout loaderLayout;
    private View snackbarView;

    private ArrayList<Allergy> allergyList;
    private ArrayList<Diet> dietList;

    private String age, prefferedDiet, heightFeet, heightInches, weightKgs, bmiInches, allergies, additionalPreferences, dietId, allergyId;
    private String selectedIngredientsIds;
    private MySharedPreferences sPref;
    private ArrayList<String> preSelectedIds = new ArrayList<>();

    float heightfeet, heightinches, weight = 0.0F;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_survey_form);
        //initialize widgets
        Initializer();
        Intent getData = getIntent();
        if (getData != null) {
            String from = getData.getStringExtra("from");
            if (from != null && from.equals("settings")) {
                tvSkip.setVisibility(View.GONE);
                ivBack.setVisibility(View.VISIBLE);
            } else {
                ivBack.setVisibility(View.GONE);
                tvSkip.setVisibility(View.VISIBLE);
            }
        } else {
            ivBack.setVisibility(View.GONE);
            tvSkip.setVisibility(View.VISIBLE);
        }

        if (sPref.getLocale().equals("ar"))
            ivBack.setImageResource(R.drawable.ic_arrow_forward_white);

        //finish the current activity
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        //navigate to main activity
        tvSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SurveyFormActivity.this, MainActivity.class));
                finish();
            }
        });

        final User.AccessToken token = sPref.getAccessTokenObject();
        final Language language = sPref.getLangObject();

        if (language != null && token != null) {
            getSurveyDetails(language.getId(), token.getAccessToken());

            Log.d(TAG, "onCreate: token: " + token.getAccessToken());
        }

        if (language != null) {
            dietData(language.getId());
            allergyData(language.getId());

            Log.d(TAG, "onCreate: " + language.getId());
        }


        //submit Data
        btnProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (token != null)
                    SubmitData(token.getAccessToken());
            }
        });
    }

    //intialize the widgets
    private void Initializer() {
        sPref = new MySharedPreferences(this);

        ivBack = findViewById(R.id.iv_survey_back);

        tvSkip = findViewById(R.id.tv_survey_skip);

        edtAge = findViewById(R.id.edt_survey_form_age);
        edtHeightFeet = findViewById(R.id.edt_survey_form_height_ft);
        edtHeightInches = findViewById(R.id.edt_survey_form_height_inches);
        edtWeightKgs = findViewById(R.id.edt_survey_form_weight_kgs);
        edtBmiInches = findViewById(R.id.edt_survey_form_bmi_inches);
        edtAdditionalPreferences = findViewById(R.id.edt_survey_form_additional_preferences);

        edtAdditionalPreferences.setImeOptions(EditorInfo.IME_ACTION_DONE);
        edtAdditionalPreferences.setRawInputType(InputType.TYPE_CLASS_TEXT);

        edtPrefferedDiet = findViewById(R.id.sp_survey_form_preferred_diet);
        edtAllergies = findViewById(R.id.sp_survey_form_allergies);
        edtIngredients = findViewById(R.id.ingredientsAlegries);
        btnProceed = findViewById(R.id.btn_survey_form_proceed);

        loaderLayout = findViewById(R.id.ll_loader_survey_form);
        loaderLayout.setVisibility(View.GONE);

        snackbarView = findViewById(android.R.id.content);

        allergyList = new ArrayList<>();
        dietList = new ArrayList<>();

        edtHeightFeet.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edtHeightFeet.getText().toString().length() > 0)
                    calculateBMI();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edtHeightInches.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edtHeightInches.getText().toString().length() > 0)
                    calculateBMI();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edtWeightKgs.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (edtWeightKgs.getText().toString().length() > 0)
                    calculateBMI();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    void ageDialog() {

        String[] ageArray = new String[61];
        int j = 10;
        for (int i = 0; i < ageArray.length; i++) {
            ageArray[i] = String.valueOf(j);
            j++;
        }

        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(SurveyFormActivity.this);
        builder.setSingleChoiceItems(ageArray, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                ListView lv = ((AlertDialog) dialogInterface).getListView();
                Object item = lv.getAdapter().getItem(lv.getCheckedItemPosition());
                dialogInterface.dismiss();

                edtAge.setText(item.toString());
            }
        });

        final androidx.appcompat.app.AlertDialog dialog = builder.create();
        edtAge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.show();
            }
        });
    }

    void heightFeetDialog() {

        String[] heightArray = new String[5];
        int j = 3;
        for (int i = 0; i < heightArray.length; i++) {
            heightArray[i] = String.valueOf(j);
            j++;
        }

        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(SurveyFormActivity.this);
        builder.setSingleChoiceItems(heightArray, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                ListView lv = ((AlertDialog) dialogInterface).getListView();
                Object item = lv.getAdapter().getItem(lv.getCheckedItemPosition());
                dialogInterface.dismiss();

                edtHeightFeet.setText(item.toString());
            }
        });

        final androidx.appcompat.app.AlertDialog dialog = builder.create();
        edtHeightFeet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.show();
            }
        });
    }

    void heightInchDialog() {

        String[] heightArray = new String[13];
        int j = 0;
        for (int i = 0; i < heightArray.length; i++) {
            heightArray[i] = String.valueOf(j);
            j++;
        }

        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(SurveyFormActivity.this);
        builder.setSingleChoiceItems(heightArray, -1, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                ListView lv = ((AlertDialog) dialogInterface).getListView();
                Object item = lv.getAdapter().getItem(lv.getCheckedItemPosition());
                dialogInterface.dismiss();

                edtHeightInches.setText(item.toString());
            }
        });

        final androidx.appcompat.app.AlertDialog dialog = builder.create();
        edtHeightInches.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.show();
            }
        });
    }

    // populate diet spinner
    void dietData(String id) {
        String URL = API.getAPI() + "allergies-or-diet?type=Diet&language_id=" + id;
        Log.d(TAG, "Diet URL: " + URL);
        loaderLayout.setVisibility(View.VISIBLE);

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Diet onResponse" + response);
                loaderLayout.setVisibility(View.GONE);

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    int status = jsonObject.optInt("status");
                    if (status == 1) {
                        dietList.clear();
                        String id, names;
                        Diet diet;
                        JSONArray dataArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObj = dataArray.getJSONObject(i);

                            diet = new Diet();

                            id = dataObj.getString("id");
                            names = dataObj.getString("names");
                            diet.setId(id);
                            diet.setName(names);

                            dietList.add(diet);
                        }

                        // just getting the names from countries and adding in namelist and set that namelist in adapter
                        String[] langs = new String[0];
                        String name;
                        ArrayList<String> nameList = new ArrayList<>();
                        for (Diet all : dietList) {
                            name = all.getName();
                            nameList.add(name);
                            langs = nameList.toArray(new String[nameList.size()]);
                        }

                        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(SurveyFormActivity.this);
                        builder.setSingleChoiceItems(langs, -1, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ListView lv = ((AlertDialog) dialogInterface).getListView();
                                Object item = lv.getAdapter().getItem(lv.getCheckedItemPosition());
                                dialogInterface.dismiss();

                                dietId = dietList.get(i).getId();

                                edtPrefferedDiet.setText(item.toString());
                            }
                        });

                        final androidx.appcompat.app.AlertDialog dialog = builder.create();
                        edtPrefferedDiet.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.show();
                            }
                        });

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loaderLayout.setVisibility(View.GONE);
                Log.d(TAG, "Diet onErrorResponse" + error.toString());
            }
        });
        requestQueue.add(stringRequest);
    }

    // populate allergy spinner
    void allergyData(String id) {
        String URL = API.getAPI() + "allergies-or-diet?type=Allergy&language_id=" + id;
        Log.d(TAG, "Allergy URL: " + URL);
        loaderLayout.setVisibility(View.VISIBLE);

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Allergy onResponse" + response);
                loaderLayout.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    int status = jsonObject.optInt("status");
                    if (status == 1) {
                        allergyList.clear();
                        String id = null;
                        String names;
                        Allergy allergy = null;
                        JSONArray dataArray = jsonObject.getJSONArray("data");
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObj = dataArray.getJSONObject(i);

                            allergy = new Allergy();

                            id = dataObj.getString("id");
                            names = dataObj.getString("names");
                            allergy.setId(id);
                            allergy.setName(names);
                            allergyList.add(allergy);
                        }

                        // just getting the names from countries and adding in namelist and set that namelist in adapter
                        String[] langs = new String[0];
                        String name;
                        ArrayList<String> nameList = new ArrayList<>();
                        for (Allergy all : allergyList) {
                            name = all.getName();
                            nameList.add(name);
                            langs = nameList.toArray(new String[nameList.size()]);
                        }

                        androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(SurveyFormActivity.this);
                        builder.setSingleChoiceItems(langs, -1, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                ListView lv = ((AlertDialog) dialogInterface).getListView();
                                Object item = lv.getAdapter().getItem(lv.getCheckedItemPosition());
                                dialogInterface.dismiss();

                                allergyId = allergyList.get(i).getId();

                                edtAllergies.setText(item.toString());
                            }
                        });

                        final androidx.appcompat.app.AlertDialog dialog = builder.create();
                        edtAllergies.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                dialog.show();
                            }
                        });
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loaderLayout.setVisibility(View.GONE);
                Log.d(TAG, "Allergy onErrorResponse" + error.toString());
            }
        });
        requestQueue.add(stringRequest);
    }

    // get survey details
    private void getSurveyDetails(String languageId, final String accessToken) {
        final String BASE_URL = API.getAPI() + "auth/survey-details?language_id=" + languageId;
        Log.d(TAG, "Get Survey Details URL: " + BASE_URL);

        final RequestQueue rQueue = Volley.newRequestQueue(this);
        loaderLayout.setVisibility(View.VISIBLE);
        StringRequest volleyMultipartRequest = new StringRequest(Request.Method.GET, BASE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, new String(response));
                        rQueue.getCache().clear();
                        loaderLayout.setVisibility(View.GONE);

                        try {
                            JSONObject jsonObject = new JSONObject((response));

                            int status = jsonObject.optInt("status");
                            String message = jsonObject.optString("message");

                            if (status == 1) {

                                JSONObject dataObj = jsonObject.getJSONObject("data");

//                                String id = dataObj.getString("id");
//                                String user_id = dataObj.getString("user_id");
                                String age = dataObj.getString("age");
                                String height = dataObj.getString("height");
                                String feet = dataObj.getString("feet");
                                String inches = dataObj.getString("inches");
                                String weight = dataObj.getString("weight");
                                String bmi = dataObj.getString("bmi");
                                String additional_preferences = dataObj.getString("additional_preferences");

                                //get allergies data
                                JSONObject allergiesObj = dataObj.getJSONObject("allergy");
                                allergyId = allergiesObj.getString("id");
                                String allergyName = allergiesObj.getString("allergy_name");

                                //get diet data
                                JSONObject dietObj = dataObj.getJSONObject("diet");
                                dietId = dietObj.getString("id");
                                String dietName = dietObj.getString("diet_name");

                                JSONArray ingArray = dataObj.getJSONArray("ingredients");
                                preSelectedIds.clear();
                                String ingredientsAlegry = "";
                                for (int i = 0; i < ingArray.length(); i++) {
                                    JSONObject obj = ingArray.getJSONObject(i);
                                    String id = obj.getString("id");
                                    String name = obj.getString("ing_name");
                                    ingredientsAlegry += ", " + name;
                                    preSelectedIds.add(id);
                                }
                                getIngredients();
                                if (!ingredientsAlegry.equals("")) {
                                    edtIngredients.setText(ingredientsAlegry.substring(1));
                                }

                                if (!age.equals("null"))
                                    edtAge.setText(age);
                                if (!height.equals("null")) {
                                    edtHeightFeet.setText(feet);
                                    edtHeightInches.setText(inches);
                                }
                                if (!weight.equals("null"))
                                    edtWeightKgs.setText(weight);
                                if (!bmi.equals("null"))
                                    edtBmiInches.setText(bmi);
                                if (!additional_preferences.equals("null"))
                                    edtAdditionalPreferences.setText(additional_preferences);
                                if (!allergyName.equals("null"))
                                    edtAllergies.setText(allergyName);
                                if (!dietName.equals("null"))
                                    edtPrefferedDiet.setText(dietName);

                            } else {
                                Log.d(TAG, "onResponse: get survey: " + message);
                            }
                        } catch (JSONException e) {
                            Log.d(TAG, "onResponse: Exception" + e.getMessage());
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        loaderLayout.setVisibility(View.GONE);
                        Log.d(TAG, "OnErrorResponse: " + error.toString());
                        if (error instanceof NoConnectionError) {
                            Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.no_network), Snackbar.LENGTH_SHORT);
                            View getSnackbarView = snackbar.getView();
                            getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
                            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                            tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
                            snackbar.show();
                        } else {
                            Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                            View getSnackbarView = snackbar.getView();
                            getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
                            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                            tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
                            snackbar.show();
                        }
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (accessToken != null) {
                    params.put("Authorization", accessToken);
                    Log.d(TAG, "getHeaders " + accessToken);
                }
                return params;
            }
        };
        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rQueue.add(volleyMultipartRequest);
    }

    //validate and submit data
    private void SubmitData(final String accessToken) {
        age = edtAge.getText().toString();
        prefferedDiet = edtPrefferedDiet.getText().toString();
        heightFeet = edtHeightFeet.getText().toString();
        heightInches = edtHeightInches.getText().toString();
        weightKgs = edtWeightKgs.getText().toString();
        bmiInches = edtBmiInches.getText().toString();
        allergies = edtAllergies.getText().toString();
        additionalPreferences = edtAdditionalPreferences.getText().toString();

        if (age.isEmpty()) {
            edtAge.setError("Please fill the required field");
            edtAge.requestFocus();
        } else if (prefferedDiet.isEmpty()) {
            Snackbar snackbar = Snackbar.make(snackbarView, "Preferred Diet not defined", Snackbar.LENGTH_SHORT);
            View getSnackbarView = snackbar.getView();
            getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
            tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
            snackbar.show();
            edtPrefferedDiet.requestFocus();
        } else if (heightFeet.isEmpty()) {
            edtHeightFeet.setError("Please fill the required field");
            edtHeightFeet.requestFocus();
        } else if (heightInches.isEmpty()) {
            edtHeightInches.setError("Please fill the required field");
            edtHeightInches.requestFocus();
        } else if (weightKgs.isEmpty()) {
            edtWeightKgs.setError("Please fill the required field");
            edtWeightKgs.requestFocus();
        } else if (bmiInches.isEmpty()) {
            edtBmiInches.setError("Please fill the required field");
            edtBmiInches.requestFocus();
        } else if (allergies.isEmpty()) {
            Snackbar snackbar = Snackbar.make(snackbarView, "Allergy not defined", Snackbar.LENGTH_SHORT);
            View getSnackbarView = snackbar.getView();
            getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
            tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
            snackbar.show();
            edtAllergies.requestFocus();
        } else {

            if (additionalPreferences.trim().isEmpty()) {
                additionalPreferences = "none";
            }

            final String BASE_URL = API.getAPI() + "auth/store-survey?" +
                    "age=" + age +
                    "&diet_id=" + dietId +
                    "&feet=" + heightFeet +
                    "&inches=" + heightInches +
                    "&weight=" + weightKgs +
                    "&bmi=" + bmiInches +
                    "&allergy_id=" + allergyId +
                    "&additional_preferences=" + additionalPreferences +
                    "&ingredients=" + selectedIngredientsIds;
            Log.d(TAG, "Survey URL: " + BASE_URL);

            final RequestQueue rQueue = Volley.newRequestQueue(this);
            loaderLayout.setVisibility(View.VISIBLE);
            StringRequest volleyMultipartRequest = new StringRequest(Request.Method.POST, BASE_URL,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("ressssssoo", new String(response));
                            rQueue.getCache().clear();
                            loaderLayout.setVisibility(View.GONE);

                            try {
                                JSONObject jsonObject = new JSONObject((response));

                                int status = jsonObject.optInt("status");
                                String message = jsonObject.optString("message");

                                if (status == 1) {

                                    Snackbar snackbar = Snackbar.make(snackbarView, message, Snackbar.LENGTH_SHORT);
                                    View getSnackbarView = snackbar.getView();
                                    getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
                                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                                    tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
                                    snackbar.show();

                                    new CountDownTimer(2000, 1000) {

                                        @Override
                                        public void onTick(long l) {

                                        }

                                        @Override
                                        public void onFinish() {
                                            startActivity(new Intent(SurveyFormActivity.this, MainActivity.class));
                                            finishAffinity();
                                        }
                                    }.start();

                                } else if (status == 0) {

                                    Snackbar snackbar = Snackbar.make(snackbarView, message, Snackbar.LENGTH_SHORT);
                                    View getSnackbarView = snackbar.getView();
                                    getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
                                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                                    tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
                                    snackbar.show();

                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            loaderLayout.setVisibility(View.GONE);
                            Log.d(TAG, "OnErrorResponse: " + error.getMessage());
                            if (error instanceof NoConnectionError) {
                                Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.no_network), Snackbar.LENGTH_SHORT);
                                View getSnackbarView = snackbar.getView();
                                getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
                                TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                                tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
                                snackbar.show();
                            } else {
                                Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                                View getSnackbarView = snackbar.getView();
                                getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
                                TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                                tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
                                snackbar.show();
                            }
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    if (accessToken != null) {
                        params.put("Authorization", accessToken);
                        Log.d(TAG, "getHeaders " + accessToken);
                    }
                    return params;
                }
            };
            volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            rQueue.add(volleyMultipartRequest);
        }
    }

    private void calculateBMI() {
        String heightFeet = edtHeightFeet.getText().toString();
        String heightInch = edtHeightInches.getText().toString();
        String weightStr = edtWeightKgs.getText().toString();

        if ((!heightFeet.equals("") &&
                (!heightInch.equals("")
                        && (!weightStr.equals("") && weightStr.length() > 1)))) {

            double heightincm = (((Double.parseDouble(heightFeet) * 12) + Double.parseDouble(heightInch)) / 12) * 30;

            //double bmi = ((Double.parseDouble(weightStr))/(heightincm)/(heightincm))*10000;


            double weightInKilos = (Double.parseDouble(weightStr));
            double heightInMeters = ((((Double.parseDouble(heightFeet) * 12) + Double.parseDouble(heightInch)) * .0254));
            double bmi = weightInKilos / Math.pow(heightInMeters, 2.0);

            Log.d("heighjt in cm", String.valueOf(heightincm));

            edtBmiInches.setText(new DecimalFormat("##.##").format(bmi) + "");
//            float heightValue = Float.parseFloat(heightStr) / 100;
//            float weightValue = Float.parseFloat(weightStr);

//            float bmi = weightValue / (heightValue * heightValue);

        }
    }

    private void getIngredients() {
        final String BASE_URL = API.getAPI() + "ingredients";
        Log.d(TAG, "ingredients URL: " + BASE_URL);
        final RequestQueue rQueue = Volley.newRequestQueue(this);
        StringRequest volleyMultipartRequest = new StringRequest(Request.Method.GET, BASE_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        try {
                            JSONObject jsonObject = new JSONObject((response));

                            int status = jsonObject.optInt("status");
                            String message = jsonObject.optString("message");

                            if (status == 1) {
                                JSONArray dataArray = jsonObject.getJSONArray("data");
                                final ArrayList<MultiSelectModel> list = new ArrayList<>();
                                for (int i = 0; i < dataArray.length(); i++) {
                                    JSONObject dataObj = dataArray.getJSONObject(i);
                                    String id = dataObj.getString("id");
                                    String name = dataObj.getString("ing_name");
                                    MultiSelectModel model = new MultiSelectModel(id, name);
                                    list.add(model);
                                }
                                edtIngredients.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        displayDialog(list);
                                    }
                                });
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                });
        volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                0,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        rQueue.add(volleyMultipartRequest);
    }

    private void displayDialog(ArrayList<MultiSelectModel> list) {
        MultiSelectDialog multiSelectDialog = new MultiSelectDialog()
                .preSelectIDsList(preSelectedIds)
                .multiSelectList(list)
                .setMinSelectionLimit(0)
                .title("Select Ingredients")
                .onSubmit(new MultiSelectDialog.SubmitCallbackListener() {
                    @Override
                    public void onSelected(ArrayList<String> selectedIds, ArrayList<String> selectedNames, String commonSeperatedData, String selectedIdsString) {
                        edtIngredients.setText(commonSeperatedData);
                        selectedIngredientsIds = selectedIdsString;
                    }

                    @Override
                    public void onCancel() {

                    }
                });
        multiSelectDialog.show(getSupportFragmentManager(), "multiSelectDialog");
    }
}