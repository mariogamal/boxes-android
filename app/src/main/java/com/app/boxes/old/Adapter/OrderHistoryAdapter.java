package com.app.boxes.old.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.boxes.old.Model.OrderHistory;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;

import java.util.ArrayList;

public class OrderHistoryAdapter extends RecyclerView.Adapter<OrderHistoryAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<OrderHistory> orderHistoryArrayList;
    private LayoutInflater inflater;

    MySharedPreferences sPref;

    public OrderHistoryAdapter(Context context, ArrayList<OrderHistory> orderHistoryArrayList) {
        this.context = context;
        this.orderHistoryArrayList = orderHistoryArrayList;
        sPref = new MySharedPreferences(context);

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_order_history, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.tvDate.setText(orderHistoryArrayList.get(position).getCreatedDate());
        holder.tvValidDate.setText(orderHistoryArrayList.get(position).getValidity());
        holder.tvRestaurantName.setText(orderHistoryArrayList.get(position).getListingName());

        if (sPref.getLocale().equals("ar"))
            holder.ivBtnRight.setImageResource(R.drawable.date_arrow_left);
    }

    @Override
    public int getItemCount() {
        return orderHistoryArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvDate, tvValidDate, tvRestaurantName;
        ImageView ivBtnRight;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            tvDate = itemView.findViewById(R.id.tv_order_history_date);
            tvValidDate = itemView.findViewById(R.id.tv_order_history_valid_date);
            tvRestaurantName = itemView.findViewById(R.id.tv_order_history_restaurant_name);
            ivBtnRight = itemView.findViewById(R.id.iv_order_history_right);
        }
    }
}
