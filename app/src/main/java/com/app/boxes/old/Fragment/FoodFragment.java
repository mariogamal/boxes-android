package com.app.boxes.old.Fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.SearchView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.app.boxes.old.API.API;
import com.app.boxes.old.Activity.MainActivity;
import com.app.boxes.old.Adapter.TabForSubscriptionAdapter;
import com.app.boxes.old.Model.TabModel;
import com.app.boxes.R;
import com.app.boxes.old.RestaurantSearchSingleton;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.app.boxes.util.ProgressDialog;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class FoodFragment extends Fragment {

    private String currentQuery = "";
    private String previousQuery = "";

    private static final String TAG = "FoodFragment";
    int positionTab = 0;
    private TabForSubscriptionAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private TextView tvToolbarText;
    private SearchView etSearch;
    private MySharedPreferences mPref;

    ArrayList<TabModel> tabName = new ArrayList<>();

    private String languageId = "";

    public FoodFragment() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_food, container, false);

        // show toolbar
        ((MainActivity) mContext).getSupportActionBar().show();
        ((MainActivity) mContext).cartLayout.setVisibility(View.GONE);

        // move bottom view selection to nav_menu_box
//        BottomNavigationView bnv = mContext.findViewById(R.id.bnv_navigation);
//        bnv.setSelectedItemId(R.id.nav_menu_box);

        //initialize widgets
        initializer(rootView);

        if (mPref.getLangObject() != null) {
            languageId = mPref.getLangObject().getId();
            getTabs(languageId, 1, "");
        }

        // set toolbar text
        tvToolbarText.setText("Food");

        return rootView;
    }

    private void initializer(View rootView) {

        tvToolbarText = ((MainActivity) mContext).findViewById(R.id.tv_toolbar_title);

        tabLayout = rootView.findViewById(R.id.tabLayout);
        viewPager = rootView.findViewById(R.id.viewPager);
        tvToolbarText = ((MainActivity) mContext).findViewById(R.id.tv_toolbar_title);

        mPref = new MySharedPreferences(mContext);

        etSearch = rootView.findViewById(R.id.search_view);
        etSearch.setFocusable(false);
        etSearch.setIconified(false);
        etSearch.clearFocus();

        etSearch.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                RestaurantSearchSingleton.getInstance().setQuery(s);
                adapter.refresh(positionTab);
//                Toast.makeText(mContext, "" + searchQuery, Toast.LENGTH_SHORT).show();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                RestaurantSearchSingleton.getInstance().setQuery(s);
                if (s.equals("") || s.length() == 0) {
                    adapter.refresh(positionTab);
                 //   ((InputMethodManager) (getActivity().getSystemService(Context.INPUT_METHOD_SERVICE))).toggleSoftInput(InputMethodManager.HIDE_NOT_ALWAYS, 0);
                }
                return false;
            }
        });

    }

    private Activity mContext;

    @Override
    public void onAttach(Context context) {
        this.mContext = (Activity) context;
        super.onAttach(context);
    }

    @Override
    public void onResume() {
        // show toolbar
        ((MainActivity) mContext).getSupportActionBar().show();
    try {
        etSearch.setQuery("", false);
        etSearch.setIconified(false);
        etSearch.clearFocus();
    }catch (Exception e){
        Log.d("shahzain", e.getMessage());
    }

        super.onResume();
    }

    private void getTabs(String language_id, int subscriptionType, String query) {

        tabName.clear();

        String url = API.getAPI() + "types?language_id=" + language_id + "&subscription_type=" + subscriptionType;
        Log.d(TAG, "URL: " + url);
        ProgressDialog.getInstance().show(getActivity());

        RequestQueue requestQueue;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                ProgressDialog.getInstance().dismiss();
                try {
                    int status = response.getInt("status");
                    String message = response.getString("message");

                    if (status == 1) {

                        JSONArray dataArray = response.getJSONArray("data");

                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject jsonDataObject = dataArray.getJSONObject(i);

                            String id = jsonDataObject.getString("id");
                            String typeName = jsonDataObject.getString("type_name");

                            tabName.add(new TabModel(id, typeName));
                        }

                        adapter = new TabForSubscriptionAdapter(getChildFragmentManager(), tabName, mContext);
                        adapter.notifyDataSetChanged();

                        Log.d(TAG, "onResponse: " + tabName.size());

                        //set the viewpager for tablayout
                        viewPager.setAdapter(adapter);
                        tabLayout.setupWithViewPager(viewPager);

                        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                            @Override
                            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                            }

                            @Override
                            public void onPageSelected(int position) {

                                previousQuery = currentQuery;
                                currentQuery = RestaurantSearchSingleton.getInstance().getQuery();

                                if (!previousQuery.equals(currentQuery)) {
                                    adapter.refresh(position);
                                }
                                positionTab = position;
                            }

                            @Override
                            public void onPageScrollStateChanged(int state) {

                            }
                        });
                    } else {
                        Log.d(TAG, message);
                    }

                } catch (Exception ex) {
                    Log.d(TAG, "exception: " + ex.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                ProgressDialog.getInstance().dismiss();
                Log.d(TAG, "onError: " + error.toString());
            }
        });

        requestQueue = Volley.newRequestQueue(mContext);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }

}