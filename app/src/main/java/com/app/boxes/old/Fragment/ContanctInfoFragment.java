package com.app.boxes.old.Fragment;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.app.boxes.old.Model.User;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContanctInfoFragment extends Fragment {

    private String TAG = "ContanctInfoFragment";

    private ImageView btnBack;
    private EditText etFirstName, etLastName, etEmail, etPhone;
    private Button btnSubmit;

    private MySharedPreferences sPref;

    public ContanctInfoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_contanct_info, container, false);

        // initialize components
        initializer(view);

        // button click listeners
        listeners();

        // check if user object is not null in shared preferences
        if (sPref.getUserObject() != null) {
            // check if first name is not equals null
            if (sPref.getUserObject().getFirstName() != null)
                if (!sPref.getUserObject().getFirstName().equals("null"))
                    etFirstName.setText(sPref.getUserObject().getFirstName());
            // check if last name is not equals null
            if (sPref.getUserObject().getLastName() != null)
                if (!sPref.getUserObject().getLastName().equals("null"))
                    etLastName.setText(sPref.getUserObject().getLastName());
            // check if email is not equals null
            if (sPref.getUserObject().getEmail() != null)
                if (!sPref.getUserObject().getEmail().equals("null"))
                    etEmail.setText(sPref.getUserObject().getEmail());
            // check if phone number is not equals null
            if (sPref.getUserObject().getPhoneNumber() != null)
                if (!sPref.getUserObject().getPhoneNumber().equals("null"))
                    etPhone.setText(sPref.getUserObject().getPhoneNumber());
        }

        //if arabic language is selected so icon changed
        if (sPref.getLocale().equals("ar"))
            btnBack.setImageResource(R.drawable.ic_arrow_forward_white);

        //enable back pressed navigation
//        view.setFocusableInTouchMode(true);
//        view.requestFocus();
//        view.setOnKeyListener(new View.OnKeyListener() {
//            @Override
//            public boolean onKey(View view, int i, KeyEvent keyEvent) {
//                if (keyEvent.getAction() == KeyEvent.ACTION_DOWN && i == KeyEvent.KEYCODE_BACK) {
//
//                    if (getFragmentManager() != null) {
////                    getFragmentManager().beginTransaction().remove(DeliveryDetailsFragment.this).commit();
//                        getFragmentManager().popBackStack();
//                    }
//
//                    return true;
//                }
//                return false;
//            }
//        });

        return view;
    }

    // get user details
//    private void getAndSetUserDetails(final String accessToken) {
//        String url = API.getAPI() + "auth/user";
//        Log.d(TAG, "User URL: " + url);
//
////        loaderLayout.setVisibility(View.VISIBLE);
//        RequestQueue requestQueue;
//        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                Log.d(TAG, "User onResponse: " + response);
////                loaderLayout.setVisibility(View.GONE);
//
//                try {
//                    JSONObject jsonObject = new JSONObject(response);
//
//                    int status = jsonObject.optInt("status");
//
//                    if (status == 1) {
//                        JSONObject data = jsonObject.getJSONObject("data");
//
//                        String first_name = data.optString("first_name");
//                        String last_name = data.optString("last_name");
//                        String email = data.optString("email");
//                        String getImage = data.getString("image");
//
//                        if ((!first_name.equals("null")) && (!last_name.equals("null"))) {
//                            etFirstName.setText(first_name);
//                            etLastName.setText(last_name);
//                        }
//                        if (!email.equals("null")) {
//
//                        }
//
////                        sPref.saveUserObject(user);
//
////                        Log.d("User-Response", "Id: " + id + " first_name: " + first_name + " last_name: " + last_name + "  email: " + getEmail + " phone: " + getPhoneNumber);
//                    }
//                } catch (JSONException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
////                loaderLayout.setVisibility(View.GONE);
//                Log.d(TAG, "Volley Error: " + error.toString());
//                if (error instanceof NoConnectionError) {
//                    Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.no_network), Snackbar.LENGTH_SHORT);
//                    View getSnackbarView = snackbar.getView();
//                    getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
//                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
//                    tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
//                    snackbar.show();
//                } else {
//                    Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
//                    View getSnackbarView = snackbar.getView();
//                    getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
//                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
//                    tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
//                    snackbar.show();
//                }
//            }
//        }) {
//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                Map<String, String> params = new HashMap<>();
//                if (accessToken != null) {
//                    params.put("Authorization", accessToken);
//                    Log.d(TAG, "getHeaders " + accessToken);
//                }
//                return params;
//            }
//        };
//        requestQueue = Volley.newRequestQueue(mContext);
//        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        requestQueue.add(stringRequest);
//    }

    private void updateContactInfo() {
        String firstName = etFirstName.getText().toString();
        String lastName = etLastName.getText().toString();
        String email = etEmail.getText().toString();
        String phone = etPhone.getText().toString();

        if (firstName.isEmpty()) {
            etFirstName.setError("Please fill the required field..");
            etFirstName.requestFocus();
        } else if (lastName.isEmpty()) {
            etLastName.setError("Please fill the required field..");
            etLastName.requestFocus();
        } else if (email.isEmpty()) {
            etEmail.setError("Please fill the required field..");
            etEmail.requestFocus();
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            etEmail.requestFocus();
            etEmail.setError("Invalid Email..");
        } else {
            User user = new User();
            user.setId(sPref.getUserObject().getId());
            user.setFirstName(firstName);
            user.setLastName(lastName);
            user.setEmail(email);
            user.setPhoneNumber(phone);

            sPref.saveUserObject(user);

            if (getFragmentManager() != null) {
                getFragmentManager().popBackStack();
            }
        }
    }

    private void initializer(View view) {
        btnBack = view.findViewById(R.id.iv_contact_info_back);

        etFirstName = view.findViewById(R.id.et_contact_info_first_name);
        etLastName = view.findViewById(R.id.et_contact_info_last_name);
        etEmail = view.findViewById(R.id.et_contact_info_email);
        etPhone = view.findViewById(R.id.et_contact_info_phone);

        btnSubmit = view.findViewById(R.id.btn_contact_info_submit);

        sPref = new MySharedPreferences(mContext);
    }

    private void listeners() {
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mContext.onBackPressed();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateContactInfo();
            }
        });
    }

    private Activity mContext;

    @Override
    public void onAttach(@NonNull Context context) {
        mContext = (Activity) context;
        super.onAttach(context);
    }
}