package com.app.boxes.old.Model;

public class OrderHistoryMenuItem {

    private int Checked, Review;
    private String Images, Id, Name, Description, Calories, Carbs, Protein;

    public OrderHistoryMenuItem(String id, String images, String name, String description, String calories, String carbs, String protein, int checked, int review) {
        Id = id;
        Images = images;
        Name = name;
        Description = description;
        Calories = calories;
        Carbs = carbs;
        Protein = protein;
        Checked = checked;
        Review = review;
    }

    public String getId() {
        return Id;
    }

    public String getImages() {
        return Images;
    }

    public void setImages(String images) {
        Images = images;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getCalories() {
        return Calories;
    }

    public void setCalories(String calories) {
        Calories = calories;
    }

    public String getCarbs() {
        return Carbs;
    }

    public void setCarbs(String carbs) {
        Carbs = carbs;
    }

    public String getProtein() {
        return Protein;
    }

    public void setProtein(String protein) {
        Protein = protein;
    }

    public int getChecked() {
        return Checked;
    }

    public void setChecked(int checked) {
        Checked = checked;
    }

    public int getReview() {
        return Review;
    }

    public void setReview(int review) {
        Review = review;
    }
}