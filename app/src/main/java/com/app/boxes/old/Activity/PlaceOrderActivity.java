package com.app.boxes.old.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.boxes.old.API.API;
import com.app.boxes.old.Adapter.MyCartAdapter;
import com.app.boxes.old.Fragment.ContanctInfoFragment;
import com.app.boxes.old.Fragment.DeliveryDetailsFragment;
import com.app.boxes.old.Model.Area;
import com.app.boxes.old.Model.Cart;
import com.app.boxes.old.Model.User;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.google.android.material.snackbar.Snackbar;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class PlaceOrderActivity extends AppCompatActivity implements MyCartAdapter.MyCartAdapterCallback {

    private static final String TAG = "PlaceOrderActivity";

    private ImageView ivPlaceOrderBack, ivCheck, ivUnCheck;
    private TextView tvCardSelection, tvTax, tvSubtotal, tvAddress, tvDeliveryCharges, tvDiscount, tvTotal, tvVoucher, tvEmail, tvDeliveryTime;
    private EditText etVoucher;
    private Button btnVoucher, btnPlaceOrder;
    private View snackbarView;
    private LinearLayout llVoucherPanel, llContactInfo;
    private AVLoadingIndicatorView avi;
    private RecyclerView rvCartItems;

    private String cardType = "";

    private int checked = 0;

    private MySharedPreferences sPref;

    private ArrayList<Cart> cartArrayList = new ArrayList<>();

    private MyCartAdapter myCartAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_place_order);

        //intialize widgets
        initializer();

        rvCartItems.setLayoutManager(new LinearLayoutManager(this));

        //if arabic language is selected so icon changed
        if (sPref.getLocale().equals("ar"))
            ivPlaceOrderBack.setImageResource(R.drawable.ic_arrow_forward_white);

        //navigate back to flower details
        ivPlaceOrderBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (getFragmentManager() != null) {
//                    getFragmentManager()
//                            .popBackStack();
//                }
                finish();
            }
        });

        //display cart data
        if (sPref.getLangObject() != null && sPref.getAccessTokenObject() != null) {
            myCart(sPref.getLangObject().getId(), sPref.getAccessTokenObject().getAccessToken());
        }

        //show and hide voucher panel
        tvVoucher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (llVoucherPanel.getVisibility() != View.VISIBLE) {
                    llVoucherPanel.setVisibility(View.VISIBLE);
                } else {
                    llVoucherPanel.setVisibility(View.GONE);
                }
            }
        });

        btnVoucher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String voucherText = etVoucher.getText().toString();
                if (voucherText.isEmpty()) {
                    Snackbar snackbar = Snackbar.make(snackbarView, "Please fill the required field", Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(getResources().getColor(R.color.active_tab_color));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(getResources().getColor(R.color.white));
                    snackbar.show();
                } else if (sPref.getAccessTokenObject() != null) {
                    String token = sPref.getAccessTokenObject().getAccessToken();

                    applyVoucher(voucherText, token);
                }
            }
        });

        // user delivery address
        tvAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                        .add(R.id.rl_main_content, new DeliveryDetailsFragment())
                        .addToBackStack(null)
                        .commit();

            }
        });

        // contact info to get user contact info
        llContactInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                        .add(R.id.rl_main_content, new ContanctInfoFragment())
                        .addToBackStack(null)
                        .commit();

            }
        });

        // alert dialog for card selection
        tvCardSelection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CardSelectionDialog(PlaceOrderActivity.this);
            }
        });

        // check if checkBox is unchecked
        ivCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ivUnCheck.setVisibility(View.VISIBLE);
                ivCheck.setVisibility(View.GONE);
                checked = 0;
            }
        });

        // check if checkBox is checked
        ivUnCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ivCheck.setVisibility(View.VISIBLE);
                ivUnCheck.setVisibility(View.GONE);
                checked = 1;
            }
        });

        if (sPref.getAccessTokenObject() != null && sPref.getUserContactInfo() != null && sPref.getAreaObject() != null) {
            final User user = sPref.getUserContactInfo();
            final Area area = sPref.getAreaObject();

            // place order
            btnPlaceOrder.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (checked == 0) {
                        Snackbar snackbar = Snackbar.make(snackbarView, "You must agree with our terms and conditions to place order", Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(getResources().getColor(R.color.active_tab_color));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(getResources().getColor(R.color.white));
                        snackbar.show();
                    } else {
                        if (/*(area.getLocation() != null) &&*/ (area.getBuildingNo() != null) && (area.getStreetNo() != null) && (area.getArea() != null)) {
                            if ((!user.getFirstName().equals("null")) && (!user.getLastName().equals("null"))
                                    && (!user.getEmail().equals("null")) && (!user.getPhoneNumber().equals("null"))) {

                                placeOrder(user.getFirstName(), user.getLastName(), user.getEmail(), user.getPhoneNumber(),
                                        "", area.getBuildingNo(), area.getStreetNo(), area.getArea(),
                                        sPref.getAccessTokenObject().getAccessToken());

                            } else {
                                Snackbar snackbar = Snackbar.make(snackbarView, "No contact info provided!", Snackbar.LENGTH_SHORT);
                                View getSnackbarView = snackbar.getView();
                                getSnackbarView.setBackgroundColor(getResources().getColor(R.color.active_tab_color));
                                TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                                tvSnackBar.setTextColor(getResources().getColor(R.color.white));
                                snackbar.show();
                            }
                        } else {
                            Snackbar snackbar = Snackbar.make(snackbarView, "No location provided!", Snackbar.LENGTH_SHORT);
                            View getSnackbarView = snackbar.getView();
                            getSnackbarView.setBackgroundColor(getResources().getColor(R.color.active_tab_color));
                            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                            tvSnackBar.setTextColor(getResources().getColor(R.color.white));
                            snackbar.show();
                        }

                    }
                }
            });
        }
    }

    private void initializer() {
        ivPlaceOrderBack = findViewById(R.id.iv_place_order_back);
        ivCheck = findViewById(R.id.iv_place_order_check);
        ivUnCheck = findViewById(R.id.iv_place_order_uncheck);

        avi = findViewById(R.id.avi_place_order);
        avi.setVisibility(View.GONE);

        tvCardSelection = findViewById(R.id.tv_card_selection);
        tvSubtotal = findViewById(R.id.tv_order_subtotal_price);
        tvDeliveryCharges = findViewById(R.id.tv_order_delivery_price);
        tvDiscount = findViewById(R.id.tv_order_discount);
        tvTotal = findViewById(R.id.tv_order_total);
        tvVoucher = findViewById(R.id.tv_order_voucher);
        tvAddress = findViewById(R.id.tv_place_order_location);
        tvTax = findViewById(R.id.tv_place_order_tax);
        tvEmail = findViewById(R.id.tv_place_order_email);
        tvDeliveryTime = findViewById(R.id.tv_order_delivery_time);

        etVoucher = findViewById(R.id.et_place_order_voucher_text);

        btnVoucher = findViewById(R.id.btn_apply_voucher);
        btnPlaceOrder = findViewById(R.id.btn_place_order);

        snackbarView = findViewById(android.R.id.content);

        rvCartItems = findViewById(R.id.rv_place_order_cart_items);

        llVoucherPanel = findViewById(R.id.ll_voucher_panel);
        llContactInfo = findViewById(R.id.ll_place_order_contact_info);

        sPref = new MySharedPreferences(this);
    }

    private void CardSelectionDialog(final Context context) {

        AlertDialog.Builder cardSelectionDialog = new AlertDialog.Builder(context, R.style.myalertdialog);
        cardSelectionDialog.setTitle(getString(R.string.select_card));

        View view = getLayoutInflater().inflate(R.layout.cards_alert_dialog, null, false);
        final RadioButton rbCreditCard = view.findViewById(R.id.rb_credit_card);
        RadioButton rbVisaCard = view.findViewById(R.id.rb_visa_card);

        cardSelectionDialog.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (rbCreditCard.isChecked()) {
                    cardType = getString(R.string.credit_card);
//                    Toast.makeText(context, "Card Type: " + cardType, Toast.LENGTH_SHORT).show();
                } else {
                    cardType = getString(R.string.visa_card);
//                    Toast.makeText(context, "Card Type: " + cardType, Toast.LENGTH_SHORT).show();
                }
                tvCardSelection.setText(cardType);
                dialogInterface.dismiss();
            }
        });

        cardSelectionDialog.setView(view);
        cardSelectionDialog.show();

    }

    private void addCartItem(String productId, int quantity, final String accessToken) {
        String url = API.getAPI() + "auth/cart/add-cart-item?product_id=" + productId + "&quantity=" + quantity;
        Log.d(TAG, url);

        avi.setVisibility(View.VISIBLE);
        RequestQueue requestQueue;
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "addCardItem onResponse: " + response.toString());
                avi.setVisibility(View.GONE);

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    String msg = jsonObject.getString("message");

                    if (status == 1) {
                        JSONObject dataObj = jsonObject.getJSONObject("data");
                        String subTotal = dataObj.getString("sub_total");
                        String voucherDiscount = dataObj.getString("voucher_discount");
                        String totalDiscount = dataObj.getString("total_discount");
                        String deliveryCharges = dataObj.getString("delivery_charges");
                        String tax = dataObj.getString("tax");
                        String total = dataObj.getString("total");

                        tvSubtotal.setText(subTotal);
                        tvDiscount.setText(totalDiscount);
                        tvDeliveryCharges.setText(deliveryCharges);
                        tvTax.setText(tax);
                        tvTotal.setText(total);

                        Snackbar snackbar = Snackbar.make(snackbarView, msg, Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(getResources().getColor(R.color.active_tab_color));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(getResources().getColor(R.color.white));
                        snackbar.show();
                    } else {
                        Snackbar snackbar = Snackbar.make(snackbarView, msg, Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(getResources().getColor(R.color.active_tab_color));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(getResources().getColor(R.color.white));
                        snackbar.show();
                    }
                } catch (Exception ex) {
                    Log.d(TAG, "onException: " + ex.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse: " + error.getMessage());
                avi.setVisibility(View.GONE);
                if (error instanceof NoConnectionError) {
                    Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.no_network), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(getResources().getColor(R.color.active_tab_color));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(getResources().getColor(R.color.white));
                    snackbar.show();
                } else {
                    Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(getResources().getColor(R.color.active_tab_color));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(getResources().getColor(R.color.white));
                    snackbar.show();
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (accessToken != null) {
                    params.put("Authorization", accessToken);
                    Log.d(TAG, "getHeaders: " + accessToken);
                }
                return params;
            }
        };
        requestQueue = Volley.newRequestQueue(this);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }

    private void removeCartItem(String productId, final String accessToken) {
        String url = API.getAPI() + "auth/cart/remove-cart-item?product_id=" + productId;
        Log.d(TAG, url);

        avi.setVisibility(View.VISIBLE);
        RequestQueue requestQueue;
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "onResponse: " + response.toString());
                avi.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    String message = jsonObject.getString("message");

                    if (status == 1) {
                        Snackbar snackbar = Snackbar.make(snackbarView, message, Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(getResources().getColor(R.color.active_tab_color));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(getResources().getColor(R.color.white));
                        snackbar.show();
                    } else {
                        Snackbar snackbar = Snackbar.make(snackbarView, message, Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(getResources().getColor(R.color.active_tab_color));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(getResources().getColor(R.color.white));
                        snackbar.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse: " + error.getMessage());
                avi.setVisibility(View.GONE);
                if (error instanceof NoConnectionError) {
                    Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.no_network), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(getResources().getColor(R.color.active_tab_color));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(getResources().getColor(R.color.white));
                    snackbar.show();
                } else {
                    Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(getResources().getColor(R.color.active_tab_color));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(getResources().getColor(R.color.white));
                    snackbar.show();
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (accessToken != null) {
                    params.put("Authorization", accessToken);
                    Log.d(TAG, "getHeaders: " + accessToken);
                }
                return params;
            }
        };

        requestQueue = Volley.newRequestQueue(this);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);

    }

    private void myCart(String languageId, final String accessToken) {
        String url = API.getAPI() + "auth/cart/my-cart?language_id=" + languageId;
        Log.d(TAG, url);

        avi.setVisibility(View.VISIBLE);
        RequestQueue requestQueue;
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "onResponse: " + response.toString());
                avi.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    String msg = jsonObject.getString("message");

                    if (status == 1) {
                        JSONObject jsonObj = jsonObject.getJSONObject("data");
                        Log.d(TAG, "onResponse jsonObj: " + jsonObj);
                        cartArrayList.clear();

                        Cart cart;

                        //get the cart array item
                        JSONArray cartArray = jsonObj.getJSONArray("cart_items");
                        for (int i = 0; i < cartArray.length(); i++) {

                            Log.d(TAG, "onResponse cartArray: " + cartArray);

                            JSONObject jsonCart = cartArray.getJSONObject(i);

                            String id = jsonCart.getString("id");
                            String prodName = jsonCart.getString("product_name");
                            String prodPrice = jsonCart.getString("price");
                            String discountPrice = jsonCart.getString("discount_price");
                            String quantity = jsonCart.getString("quantity");

//                            tvProductName.setText(prodName);
//                            tvProductPrice.setText(prodPrice);
//                            tvQuantity.setText(quantity);

                            cart = new Cart();
                            cart.setProdId(id);
                            cart.setProdName(prodName);
                            cart.setProdPrice(prodPrice);
                            cart.setProdQuantity(quantity);

                            Log.d(TAG, "onResponse cart array:" + id + " " + prodName);
                            cartArrayList.add(cart);
                        }

                        myCartAdapter = new MyCartAdapter(PlaceOrderActivity.this, cartArrayList, PlaceOrderActivity.this);
                        rvCartItems.setAdapter(myCartAdapter);

                        //get the details item
                        JSONObject totalObj = jsonObj.getJSONObject("total");
                        Log.d(TAG, "onResponse detailsObj: " + totalObj);

                        String subTotal = totalObj.getString("sub_total");
                        String voucherDiscount = totalObj.getString("voucher_discount");
                        String productDiscount = totalObj.getString("total_discount");
                        String deliveryCharges = totalObj.getString("delivery_charges");
                        String tax = totalObj.getString("tax");
                        String total = totalObj.getString("total");

                        tvSubtotal.setText(subTotal);
                        tvDeliveryCharges.setText(deliveryCharges);
                        tvDiscount.setText(productDiscount);
                        tvTotal.setText(total);
                        tvTax.setText(tax);
                    }
                } catch (Exception ex) {
                    Log.d(TAG, "onException: " + ex.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse: " + error.getMessage());
                avi.setVisibility(View.GONE);
                if (error instanceof NoConnectionError) {
                    Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.no_network), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(getResources().getColor(R.color.active_tab_color));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(getResources().getColor(R.color.white));
                    snackbar.show();
                } else {
                    Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(getResources().getColor(R.color.active_tab_color));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(getResources().getColor(R.color.white));
                    snackbar.show();
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (accessToken != null) {
                    params.put("Authorization", accessToken);
                    Log.d(TAG, "getHeaders: " + accessToken);
                }
                return params;
            }
        };

        requestQueue = Volley.newRequestQueue(this);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);

    }

    private void clearCart(String cartId) {
        String url = API.getAPI() + "auth/cart/clear-cart?cart_id=" + cartId;

        Log.d(TAG, url);

        RequestQueue requestQueue;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "onResponse: " + response.toString());
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse: " + error.getMessage());
                if (error instanceof NoConnectionError) {
                    Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.no_network), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(getResources().getColor(R.color.active_tab_color));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(getResources().getColor(R.color.white));
                    snackbar.show();
                } else {
                    Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(getResources().getColor(R.color.active_tab_color));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(getResources().getColor(R.color.white));
                    snackbar.show();
                }
            }
        });

        requestQueue = Volley.newRequestQueue(this);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);

    }

    private void applyVoucher(String voucherName, final String accessToken) {
        if (voucherName.contains(" "))
            voucherName = voucherName.replace(" ", "%20");
        String url = API.getAPI() + "auth/cart/apply-voucher?voucher_name=" + voucherName;
        Log.d(TAG, url);

        avi.setVisibility(View.VISIBLE);
        RequestQueue requestQueue;
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "voucher onResponse: " + response.toString());
                avi.setVisibility(View.GONE);

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    String msg = jsonObject.getString("message");

                    if (status == 1) {

                        Snackbar snackbar = Snackbar.make(snackbarView, msg, Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(getResources().getColor(R.color.active_tab_color));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(getResources().getColor(R.color.white));
                        snackbar.show();

                        JSONObject dataObj = jsonObject.getJSONObject("data");
                        JSONObject totalObj = dataObj.getJSONObject("total");

                        String subtotal = totalObj.getString("sub_total");
                        String productDiscount = totalObj.getString("product_discount");
                        String voucherDiscount = totalObj.getString("voucher_discount");
                        String totalDiscount = totalObj.getString("total_discount");
                        String deliveryCharges = totalObj.getString("delivery_charges");
                        String tax = totalObj.getString("tax");
                        String total = totalObj.getString("total");


                        tvSubtotal.setText(subtotal);
                        tvDiscount.setText(productDiscount);
                        tvDeliveryCharges.setText(deliveryCharges);
                        tvTax.setText(tax);
                        tvTotal.setText(total);

                    } else {
                        Snackbar snackbar = Snackbar.make(snackbarView, msg, Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(getResources().getColor(R.color.active_tab_color));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(getResources().getColor(R.color.white));
                        snackbar.show();
                    }
                } catch (Exception ex) {
                    Log.d(TAG, "onException: " + ex.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse: " + error.getMessage());
                avi.setVisibility(View.GONE);
                if (error instanceof NoConnectionError) {
                    Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.no_network), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(getResources().getColor(R.color.active_tab_color));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(getResources().getColor(R.color.white));
                    snackbar.show();
                } else {
                    Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(getResources().getColor(R.color.active_tab_color));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(getResources().getColor(R.color.white));
                    snackbar.show();
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (accessToken != null) {
                    params.put("Authorization", accessToken);
                    Log.d(TAG, "getHeaders: " + accessToken);
                }
                return params;
            }
        };
        requestQueue = Volley.newRequestQueue(this);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }

    private void placeOrder(String firstName, String lastName, String email, String phone, String currentLocation, String building, String street, String area, final String accessToken) {
        if (currentLocation.isEmpty()) {
            Snackbar snackbar = Snackbar.make(snackbarView, "No location provided!", Snackbar.LENGTH_SHORT);
            View getSnackbarView = snackbar.getView();
            getSnackbarView.setBackgroundColor(getResources().getColor(R.color.active_tab_color));
            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
            tvSnackBar.setTextColor(getResources().getColor(R.color.white));
            snackbar.show();
        } else if (building.isEmpty()) {
            Snackbar snackbar = Snackbar.make(snackbarView, "No building provided!", Snackbar.LENGTH_SHORT);
            View getSnackbarView = snackbar.getView();
            getSnackbarView.setBackgroundColor(getResources().getColor(R.color.active_tab_color));
            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
            tvSnackBar.setTextColor(getResources().getColor(R.color.white));
            snackbar.show();
        } else if (street.isEmpty()) {
            Snackbar snackbar = Snackbar.make(snackbarView, "No street provided!", Snackbar.LENGTH_SHORT);
            View getSnackbarView = snackbar.getView();
            getSnackbarView.setBackgroundColor(getResources().getColor(R.color.active_tab_color));
            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
            tvSnackBar.setTextColor(getResources().getColor(R.color.white));
            snackbar.show();
        } else if (area.isEmpty()) {
            Snackbar snackbar = Snackbar.make(snackbarView, "No area provided!", Snackbar.LENGTH_SHORT);
            View getSnackbarView = snackbar.getView();
            getSnackbarView.setBackgroundColor(getResources().getColor(R.color.active_tab_color));
            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
            tvSnackBar.setTextColor(getResources().getColor(R.color.white));
            snackbar.show();
        } else {
            if (currentLocation.contains(" "))
                currentLocation = currentLocation.replace(" ", "%20");
            if (building.contains(" "))
                building = building.replace(" ", "%20");
            if (street.contains(" "))
                street = street.replace(" ", "%20");
            if (area.contains(" "))
                area = area.replace(" ", "%20");
            String url = API.getAPI() + "auth/cart/place-order?first_name=" + firstName + "&last_name=" + lastName + "&email=" + email + "&phone=" + phone +
                    "&current_location=" + currentLocation + "&building=" + building + "&street=" + street + "&area=" + area;
            Log.d(TAG, url);

            avi.setVisibility(View.VISIBLE);
            RequestQueue requestQueue;
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d(TAG, "onResponse: " + response.toString());
                    avi.setVisibility(View.GONE);
                    try {
                        int status = response.getInt("status");
                        String msg = response.getString("message");

                        if (status == 1) {
//                        JSONObject jsonObj = response.getJSONObject("data");
//                        Log.d(TAG, "onResponse jsonObj: " + jsonObj);

                            Snackbar snackbar = Snackbar.make(snackbarView, msg, Snackbar.LENGTH_SHORT);
                            View getSnackbarView = snackbar.getView();
                            getSnackbarView.setBackgroundColor(getResources().getColor(R.color.active_tab_color));
                            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                            tvSnackBar.setTextColor(getResources().getColor(R.color.white));
                            snackbar.show();

                            new CountDownTimer(2000, 1000) {

                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    startActivity(new Intent(PlaceOrderActivity.this, MainActivity.class));
                                    finish();
                                }
                            }.start();

                        } else {
                            Snackbar snackbar = Snackbar.make(snackbarView, msg, Snackbar.LENGTH_SHORT);
                            View getSnackbarView = snackbar.getView();
                            getSnackbarView.setBackgroundColor(getResources().getColor(R.color.active_tab_color));
                            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                            tvSnackBar.setTextColor(getResources().getColor(R.color.white));
                            snackbar.show();
                        }
                    } catch (Exception ex) {
                        Log.d(TAG, "onException: " + ex.toString());
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(TAG, "onErrorResponse: " + error.toString());
                    avi.setVisibility(View.GONE);
                    if (error instanceof NoConnectionError) {
                        Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.no_network), Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(getResources().getColor(R.color.active_tab_color));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(getResources().getColor(R.color.white));
                        snackbar.show();
                    } else {
                        Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(getResources().getColor(R.color.active_tab_color));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(getResources().getColor(R.color.white));
                        snackbar.show();
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    if (accessToken != null) {
                        params.put("Authorization", accessToken);
                        Log.d(TAG, "getHeaders: " + accessToken);
                    }
                    return params;
                }
            };

            requestQueue = Volley.newRequestQueue(this);
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(jsonObjectRequest);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();

        // check if user area has populated in Shared Preferences and it is not null
        if (sPref.getAreaObject() != null) {
            Area area = sPref.getAreaObject();
            if (/*(area.getLocation() != null) && */(area.getBuildingNo() != null) && (area.getStreetNo() != null) && (area.getArea() != null)) {
                tvAddress.setText(area.getBuildingNo() + " " + area.getFloor() + " " + area.getStreetNo() + " " + area.getArea());
                Log.d(TAG, "onStart: " + area.getArea() + " " + area.getBuildingNo() + " " + area.getStreetNo() + " " + area.getFloor() + " " + area.getAddInst());
            } else {
                tvAddress.setText("Please Enter Your Address");
            }
        } else {
            tvAddress.setText("Please Enter Your Address");
        }

        if (sPref.getUserContactInfo() != null) {
            User contactInfo = sPref.getUserContactInfo();
            if (contactInfo.getEmail() != null) {
                tvEmail.setText(contactInfo.getEmail());
            } else {
                tvEmail.setText("No Email");
            }
        } else {
            tvEmail.setText("No Email");
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        checked = 0;
        // check if user area has populated in Shared Preferences and it is not null
        if (sPref.getAreaObject() != null) {
            Toast.makeText(this, "", Toast.LENGTH_SHORT).show();
            Area area = sPref.getAreaObject();
            if (/*(area.getLocation() != null) && */(area.getBuildingNo() != null) && (area.getStreetNo() != null) && (area.getArea() != null)) {
                tvAddress.setText("Building#: " + area.getBuildingNo() + " Floor#: " + area.getFloor() + " Streer#: " + area.getStreetNo() + " Area: " + area.getArea());
                Log.d(TAG, "onResume: " + area.getArea() + " " + area.getBuildingNo() + " " + area.getStreetNo() + " " + area.getFloor() + " " + area.getAddInst());
            } else {
                tvAddress.setText("Please Enter Your Address");
            }
        } else {
            tvAddress.setText("Please Enter Your Address");
        }

        if (sPref.getUserContactInfo() != null) {
            User contactInfo = sPref.getUserContactInfo();
            if (contactInfo.getEmail() != null) {
                tvEmail.setText(contactInfo.getEmail());
            } else {
                tvEmail.setText("No Email");
            }
        } else {
            tvEmail.setText("No Email");
        }
    }

    @Override
    public void onMyCartAddClicked(Cart cart) {
        if (sPref.getAccessTokenObject() != null) {
            int intCount = Integer.parseInt(cart.getProdQuantity());
            int quantity = intCount + 1;
            addCartItem(cart.getProdId(), quantity, sPref.getAccessTokenObject().getAccessToken());
            cart.setProdQuantity(String.valueOf(quantity));
            myCartAdapter.notifyDataSetChanged();
        }
    }

    @Override
    public void onMyCartSubClicked(Cart cart, int position) {
        if (Integer.parseInt(cart.getProdQuantity()) == 1) {
            cartArrayList.remove(position);
            myCartAdapter.notifyDataSetChanged();
            if (sPref.getAccessTokenObject() != null) {
                removeCartItem(cart.getProdId(), sPref.getAccessTokenObject().getAccessToken());
                myCartAdapter.notifyDataSetChanged();
            }
        } else {
            int intCount = Integer.parseInt(cart.getProdQuantity());
            int quantity = intCount - 1;
            cart.setProdQuantity(String.valueOf(quantity));

            if (sPref.getAccessTokenObject() != null) {
                addCartItem(cart.getProdId(), quantity, sPref.getAccessTokenObject().getAccessToken());
                myCartAdapter.notifyDataSetChanged();
            }
        }
    }

}
