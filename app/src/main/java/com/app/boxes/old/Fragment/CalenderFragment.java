package com.app.boxes.old.Fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.boxes.old.API.API;
import com.app.boxes.old.Activity.MainActivity;
import com.app.boxes.old.Adapter.SubscribedMultiRestaurantAdapter;
import com.app.boxes.old.Adapter.SubscribedRestaurantAdapter;
import com.app.boxes.old.Model.DeliveryDates;
import com.app.boxes.old.Model.SingleRestaurant;
import com.app.boxes.old.Model.User;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.app.boxes.old.UI.LoginBottomSheet;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONObject;
import org.threeten.bp.LocalDate;
import org.threeten.bp.format.DateTimeFormatter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import me.everything.android.ui.overscroll.OverScrollDecoratorHelper;
import sun.bob.mcalendarview.MCalendarView;
import sun.bob.mcalendarview.MarkStyle;
import sun.bob.mcalendarview.listeners.OnDateClickListener;
import sun.bob.mcalendarview.vo.DateData;

/**
 * A simple {@link Fragment} subclass.
 */
public class CalenderFragment extends Fragment {

    private String TAG = "CalenderFragment";
    private String restaurantId = "", languageId = "";

    private View snackbarView;
    private ImageView ivBtnBack;
    private MCalendarView calendarView;
    private RecyclerView rvSubscribedRestaurant;
    private LinearLayout llLoader;
    private TextView textViewToolbar;

    private SubscribedRestaurantAdapter subscribedRestaurantAdapter;
    private SubscribedMultiRestaurantAdapter multiRestaurantAdapter;

    private MySharedPreferences mPref;

    private Bundle bundle;

    private ArrayList<DeliveryDates> deliveryDatesArrayList = new ArrayList<>();
    private ArrayList<DeliveryDates> allDatesArrayList = new ArrayList<>();

    private View view;

    public CalenderFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_calender, container, false);

        //show the toolbar
        ((MainActivity) mContext).getSupportActionBar().show();

        //initialize the widget
        Initializer(view);

        // move bottom view selection to nav_menu_history
     //   BottomNavigationView bnv = mContext.findViewById(R.id.bnv_navigation);
      //  bnv.setSelectedItemId(R.id.nav_menu_history);

        // if arabic language is selected then back icon change into forward icon
        mPref = new MySharedPreferences(mContext);

        mPref.saveSelectedYear(0);
        mPref.saveSelectedMonth(0);
        mPref.saveSelectedDate(0);

        // if language object and type id is not empty then get current package
        if (mPref.getLangObject() != null && mPref.getTypeId() != null) {
            //check which language is selected
            languageId = mPref.getLangObject().getId();
        }

        //region on date click listener
        calendarView.setOnDateClickListener(new OnDateClickListener() {
            @Override
            public void onDateClick(View view, DateData date) {
                LocalDate clickDate = LocalDate.of(date.getYear(), date.getMonth(), date.getDay());
                DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("EEE, MMM dd, yyyy");
                String forToolbar = clickDate.format(formatter1);

                // save date in shared preferences
                mPref.saveSelectedDay(forToolbar);

                if ((date.getMarkStyle().getColor() == mContext.getResources().getColor(R.color.green))
                        || (date.getMarkStyle().getColor() == mContext.getResources().getColor(R.color.red))) {
                    colorCheck(date, deliveryDatesArrayList);
                } else if (date.getMarkStyle().getColor() == mContext.getResources().getColor(R.color.alpha_green)
                        || date.getMarkStyle().getColor() == mContext.getResources().getColor(R.color.alpha_red)
                        || date.getMarkStyle().getColor() == mContext.getResources().getColor(R.color.alpha_blue)) {
                    Snackbar snackbar = Snackbar.make(snackbarView, "You can't select date from other restaurant", Snackbar.LENGTH_LONG);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                    snackbar.show();
                } else if (date.getMarkStyle().getColor() == mContext.getResources().getColor(R.color.blue)) {
                    return;
                } else if (!restaurantId.equals("")) {

                    int color = date.getMarkStyle().getColor();

                    //get current date time with Date()
//                    String todayDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                    int year = Integer.parseInt(new SimpleDateFormat("yyyy", Locale.getDefault()).format(new Date()));
                    int month = Integer.parseInt(new SimpleDateFormat("MM", Locale.getDefault()).format(new Date()));
                    int cdate = Integer.parseInt(new SimpleDateFormat("dd", Locale.getDefault()).format(new Date()));

//                    Date vv = new Date();
                    LocalDate fm = LocalDate.of(year, month, cdate);
                    fm = fm.plusDays(thresholdInDays);

                    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("EEE, MMM d, ''yy");
                    String forMessage = fm.format(formatter);

                    // Date date22 = Date.from(fm.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
                    //    String forMessage = new SimpleDateFormat("EEE, MMM d, ''yy",Locale.getDefault()).format(fm) ;

//                    Date date1 = new Date(date.getYear(), date.getMonth(), date.getDay());
//                    int days = 2;
//                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
//                    String strDate = formatter.format(date1.getTime() + (days * 86400000L));

                    LocalDate temp1 = LocalDate.of(date.getYear(), date.getMonth(), date.getDay());
                    LocalDate localDate = LocalDate.of(year, month, cdate);

                    Log.d("MYDATES", temp1 + " " + localDate);

//                    String[] temp = strDate.split("-");
//                    year = Integer.parseInt(temp[0]);
//                    month = Integer.parseInt(temp[1]);
//                    cdate = Integer.parseInt(temp[2]);

                    //  temp1 = temp1.plusDays(2);

                    if (temp1.isAfter(localDate)) {
                        mPref.saveSelectedDate(date.getDay());
                        mPref.saveSelectedMonth(date.getMonth());
                        mPref.saveSelectedYear(date.getYear());

                        Fragment selectedFragment = new CalenderDetailsFragment();
                        selectedFragment.setArguments(bundle);
                        openFragment(selectedFragment);

//                        ((MainActivity)mContext).navigationController(R.id.calenderDetailsFragment, bundle, null);

                    } else {
                        Snackbar snackbar = Snackbar.make(snackbarView, "You need to select date after current date", Snackbar.LENGTH_LONG);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                        snackbar.show();
                    }

                    Log.d("strDate", temp1.getYear() + "-" + temp1.getMonthValue() + "-" + temp1.getDayOfMonth() + "\nlocal date: " + localDate);

//                    Date date1 = new Date();
//                    Calendar cal = Calendar.getInstance();
//                    cal.setTime(date1);
//                    cal.add(Calendar.DATE, 24);
//                    date1 = cal.getTime();
//
//                    int year = date1.getYear();
//                    int month = date1.getMonth();
//                    int cdate = date1.getDate();
//                    temp1.

                    Log.d(TAG, "onDateClick: threshold days added: " /*+ daysAdded*/ + " current day: " + date.getDay());
                } else {
                    Snackbar snackbar = Snackbar.make(snackbarView, mContext.getResources().getString(R.string.please_select_a_restaurant), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                    snackbar.show();
                }
            }

            @Override
            public void onDateLongClick(View view, DateData date) {

            }
        });
        //endregion

        //select the current date
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        String dateString = formatter.format(new Date());
        String[] temp = dateString.split("/");
        Log.d(TAG, "onCreateView: dateString " + dateString + " temp " + temp[0] + "-" + temp[1] + "-" + temp[2]);

        return view;
    }

    private void colorCheck(DateData date, ArrayList<DeliveryDates> arrayList) {
        for (DeliveryDates dates : arrayList) {
            int mDay = dates.getDeliveryDay();
            if (date.getDay() == mDay) {
                mPref.saveSelectedDate(date.getDay());
                mPref.saveSelectedMonth(date.getMonth());
                mPref.saveSelectedYear(date.getYear());

                bundle.putSerializable("delivery_dates", arrayList);
                bundle.putString("delivery_status", dates.getDeliveryStatus());
                bundle.putString("order_id", dates.getOrderDetailId());

                Log.d(TAG, "onDateClick: dates: " + dates.getDeliveryDay());

                Fragment selectedFragment = new CalenderDetailsFragment();
                selectedFragment.setArguments(bundle);

                openFragment(selectedFragment);
//                ((MainActivity)mContext).navigationController(R.id.calenderDetailsFragment, bundle, null);
                            /*add break when condition is true
                            and it needs to be tested*/
                break;
            }
            Log.d(TAG, "colorCheck: arraylist size: " + arrayList.size());
        }
    }

    //region initialize the widgets
    private void Initializer(View view) {
//        ivBtnBack = view.findViewById(R.id.iv_calendar_back);
//        rvSubscribedRestaurant = view.findViewById(R.id.rv_restaurant_list_calender);

//        calendarView = view.findViewById(R.id.calendar);

//        llLoader = view.findViewById(R.id.ll_loader_calendar);
        llLoader.setVisibility(View.GONE);

        snackbarView = mContext.findViewById(android.R.id.content);

        // initialize the bundle object
        bundle = new Bundle();

        textViewToolbar = mContext.findViewById(R.id.tv_toolbar_title);
        textViewToolbar.setText(R.string.calendar);
    }
    //endregion

    private Activity mContext;

    //region onAttach method
    @Override
    public void onAttach(Context context) {
        this.mContext = (Activity) context;
        super.onAttach(context);
    }
    //endregion

    //    private int day; // to get day from server
    int daysAdded = 0, thresholdInDays = 0;

    //region get subscribed restaurant method
    private void getSubscribedRestaurant(final String languageId, final String accessToken) {
        String url = API.getAPI() + "auth/calendar-detail?language_id=" + languageId;
        Log.d(TAG, "getSubscribedRestaurant " + "URL: " + url);

        llLoader.setVisibility(View.VISIBLE);
        RequestQueue requestQueue;
        final StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                llLoader.setVisibility(View.GONE);
                Log.d(TAG, "onResponse: " + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    String message = jsonObject.getString("message");


                    final ArrayList<SingleRestaurant> restaurantList = new ArrayList<>();
//                    final ArrayList<Integer> deliveryDatesArrayList = new ArrayList<>();

                    if (status == 1) {

                        final JSONArray dataArray = jsonObject.getJSONArray("data");

                        SingleRestaurant singleRestaurant;

                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObj = dataArray.getJSONObject(i);
                            String id = dataObj.getString("id");
                            String listingName = dataObj.getString("listing_name");
                            String image = dataObj.getString("image");
                            String outOfMeals = dataObj.getString("out_of_meals");
                            String consumedMeals = dataObj.getString("consumed_meals");
                            thresholdInDays = dataObj.getInt("threshold_in_days");

                            String imgURL = API.getImageAPI() + image;

                            singleRestaurant = new SingleRestaurant();
                            singleRestaurant.setId(id);
                            singleRestaurant.setName(listingName);
                            singleRestaurant.setImages(imgURL);
                            singleRestaurant.setMealCount(consumedMeals);
                            singleRestaurant.setMealOutOff(outOfMeals);
                            singleRestaurant.setThresholdDays(thresholdInDays);

                            restaurantList.add(singleRestaurant);
                        }

                        // set the subscribed restaurant in recyclerview
                        rvSubscribedRestaurant.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false));

                        if (dataArray.length() == 1) {
                            subscribedRestaurantAdapter = new SubscribedRestaurantAdapter(mContext, restaurantList, new SubscribedRestaurantAdapter.SubscribedRestaurantClickListener() {
                                @Override
                                public void onSingleResItemClick(int position, View view) {
                                    SingleRestaurant itemClicked = restaurantList.get(position);

                                    if (itemClicked.isSelected()) {
                                        itemClicked.setSelected(false);
                                        restaurantId = "";
                                    } else {
                                        // set true if not selected
                                        itemClicked.setSelected(true);
                                        restaurantId = itemClicked.getId();
                                        bundle.putString("restaurant_id", restaurantId);
                                        bundle.putString("restaurant_name", itemClicked.getName());
                                        bundle.putInt("thresholdDays", itemClicked.getThresholdDays());
                                        getCalenderDetail(restaurantId, languageId, accessToken);
                                    }

                                    Log.d(TAG, "onSingleResItemClick: RestaurantId: " + restaurantId + " thresholdDays: " + restaurantList.get(0).getThresholdDays());
                                    subscribedRestaurantAdapter.notifyDataSetChanged();
                                }
                            });

                            // highlight the first restaurant by default
                            restaurantList.get(0).setSelected(true);
                            restaurantId = restaurantList.get(0).getId();
                            bundle.putString("restaurant_id", restaurantId);
                            bundle.putString("restaurant_name", restaurantList.get(0).getName());
                            bundle.putInt("threshold_days", restaurantList.get(0).getThresholdDays());
                            getCalenderDetail(restaurantId, languageId, accessToken);

                            Log.d(TAG, "onResponse: resId: " + restaurantId + " thresholdDays: " + restaurantList.get(0).getThresholdDays());

                            // for smooth scrolling
                            OverScrollDecoratorHelper.setUpOverScroll(rvSubscribedRestaurant, OverScrollDecoratorHelper.ORIENTATION_HORIZONTAL);

                            // set adapter
                            rvSubscribedRestaurant.setAdapter(subscribedRestaurantAdapter);
                            subscribedRestaurantAdapter.notifyDataSetChanged();
                        } else {
                            multiRestaurantAdapter = new SubscribedMultiRestaurantAdapter(mContext, restaurantList, new SubscribedMultiRestaurantAdapter.SubscribedMultiRestaurantClickListener() {
                                @Override
                                public void onMultiResItemClick(int position, View view) {
                                    SingleRestaurant itemClicked = restaurantList.get(position);

                                    if (itemClicked.isSelected()) {
                                        itemClicked.setSelected(false);
                                        restaurantId = "";
                                    } else {
                                        deliveryDatesArrayList.clear();

                                        itemClicked.setSelected(true);
                                        restaurantId = itemClicked.getId();
                                        bundle.putString("restaurant_id", restaurantId);
                                        bundle.putString("restaurant_name", itemClicked.getName());
                                        bundle.putInt("threshold_days", itemClicked.getThresholdDays());
                                        multiRestaurantAdapter.selected();

                                        getCalenderDetail(restaurantId, languageId, accessToken);
                                    }
//                                    multiRestaurantAdapter.selected();
                                }
                            });

                            // highlight the first restaurant by default
                            restaurantList.get(0).setSelected(true);
                            restaurantId = restaurantList.get(0).getId();
                            bundle.putString("restaurant_id", restaurantId);
                            bundle.putString("restaurant_name", restaurantList.get(0).getName());
                            bundle.putInt("threshold_days", restaurantList.get(0).getThresholdDays());
                            getCalenderDetail(restaurantId, languageId, accessToken);
                            Log.d(TAG, "onResponse: resId " + restaurantId + " thresholdDays: " + restaurantList.get(0).getThresholdDays());

                            // for smooth scrolling
                            OverScrollDecoratorHelper.setUpOverScroll(rvSubscribedRestaurant, OverScrollDecoratorHelper.ORIENTATION_HORIZONTAL);
                            rvSubscribedRestaurant.setAdapter(multiRestaurantAdapter);
                        }
                    } else if (status == 0) {

                        Snackbar snackbar = Snackbar.make(snackbarView, message, Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                        snackbar.show();

                        new CountDownTimer(1500, 1000) {

                            @Override
                            public void onTick(long l) {

                            }

                            @Override
                            public void onFinish() {
                                // open home fragment and show restaurant listing if user is not subscribed
                                if (getFragmentManager() != null) {
                                    getFragmentManager()
                                            .beginTransaction()
                                            .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                                            .replace(R.id.main_content, new FoodFragment())
                                            .commit();
                                }
                                // move bottom view selection to nav_menu_box
//                                BottomNavigationView bnv = mContext.findViewById(R.id.bnv_navigation);
//                                bnv.setSelectedItemId(R.id.nav_menu_box);
                            }
                        }.start();
                    }
                } catch (
                        Exception ex) {
                    Log.d(TAG, "calender fragment onException: " + ex.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                llLoader.setVisibility(View.GONE);

                Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                View getSnackbarView = snackbar.getView();
                getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                snackbar.show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (accessToken != null) {
                    params.put("Authorization", accessToken);
                    Log.d(TAG, "getHeaders " + accessToken);
                }
                return params;
            }
        };

        requestQueue = Volley.newRequestQueue(mContext);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);

    }
    //endregion

    //region get calender details method
    private void getCalenderDetail(final String listingId, String languageId, final String accessToken) {
        calendarView = null;
//        calendarView = view.findViewById(R.id.calendar);
        deliveryDatesArrayList.clear();

        // while (true) {
        int size = calendarView.getMarkedDates().getAll().size();

        for (int i = 0; i < calendarView.getMarkedDates().getAll().size(); i++) {
            //for laga k sab peelay unmark
            Log.i("For Loop Dates", "fhfgchdzg" + calendarView.getMarkedDates().getAll().get(i).getDay() + calendarView.getMarkedDates().getAll().get(i).getMonth() + calendarView.getMarkedDates().getAll().get(i).getYear());
            //if (calendarView.getMarkedDates().getAll().get(i).getMarkStyle().getColor() == getResources().getColor(R.color.mint_orange)) {
            calendarView.getMarkedDates().getAll().get(i).getMarkStyle().setColor(-16739085);
            //calendarView.unMarkDate(calendarView.getMarkedDates().getAll().get(i));
            //}
        }

        if (size == 0) {
            //break;
        }
        // }
        String url = API.getAPI() + "auth/calendar-detail?language_id=" + languageId;
        Log.d(TAG, "getCalenderDetail " + "URL: " + url);

        llLoader.setVisibility(View.VISIBLE);
        RequestQueue requestQueue;
        final StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                llLoader.setVisibility(View.GONE);
                Log.d(TAG, "onResponse: " + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    String message = jsonObject.getString("message");

                    if (status == 1) {
                        calendarView.getMarkedDates().removeAdd();
                        JSONArray dataArray = jsonObject.getJSONArray("data");
                        deliveryDatesArrayList = new ArrayList<>();
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObj = dataArray.getJSONObject(i);
                            String deliveredlistingId = dataObj.getString("id");

//                            if (deliveredlistingId.equals(listingId)) {
                            Log.d(TAG, "onResponse: listingId: " + listingId);

                            //delivery details array
                            JSONArray deliveryDatesArray = dataObj.getJSONArray("delivery_dates");
                            DeliveryDates deliveryDates;

                            Log.d(TAG, "onResponse: delivery id: " + deliveredlistingId + " listing id: " + listingId);

                            for (int j = 0; j < deliveryDatesArray.length(); j++) {
                                if (!deliveredlistingId.equals(listingId))
                                    continue;
                                JSONObject deliveryDateObj = deliveryDatesArray.getJSONObject(j);

                                String orderDetailsId = deliveryDateObj.getString("order_detail_id");
                                String deliveryDate = deliveryDateObj.getString("delivery_date");
                                String deliveryStatus = deliveryDateObj.getString("status");

                                deliveryDates = new DeliveryDates();

                                deliveryDates.setOrderDetailId(orderDetailsId);
                                deliveryDates.setDeliveryDate(deliveryDate);
                                deliveryDates.setDeliveryStatus(deliveryStatus);
                                if (!deliveredlistingId.equals("")) {
                                    deliveryDates.setResId(deliveredlistingId);
                                }

                                String[] date = deliveryDate.split("-");
                                int year = Integer.parseInt(date[0]);
                                int month = Integer.parseInt(date[1]);
                                int day = Integer.parseInt(date[2]);

                                deliveryDates.setDeliveryDay(day);
                                deliveryDates.setDeliveryMonth(month);
                                deliveryDates.setDeliveryYear(year);

                                deliveryDatesArrayList.add(deliveryDates);


                                Log.d(TAG, "onResponse: date: " + year + "-" + month + "-" + day + " size " + deliveryDatesArrayList.size() + " delivery status: " + deliveryStatus);
                            }
                            populateCalender(deliveryDatesArrayList, true);
//                            } else {
                            //delivery details array
                            deliveryDatesArray = dataObj.getJSONArray("delivery_dates");
//                                DeliveryDates deliveryDates;

                            Log.d(TAG, "onResponse: delivery id: " + deliveredlistingId + " listing id: " + listingId);

                            allDatesArrayList = new ArrayList<>();
                            for (int j = 0; j < deliveryDatesArray.length(); j++) {

                                JSONObject deliveryDateObj = deliveryDatesArray.getJSONObject(j);

                                if (deliveredlistingId.equals(listingId))
                                    continue;

                                String orderDetailsId = deliveryDateObj.getString("order_detail_id");
                                String deliveryDate = deliveryDateObj.getString("delivery_date");
                                String deliveryStatus = deliveryDateObj.getString("status");

                                // deliveryStatus = deliveryStatus + "-alpha";
                                Log.d(TAG, "onResponse: not equals " + deliveryStatus);

                                deliveryDates = new DeliveryDates();

                                deliveryDates.setOrderDetailId(orderDetailsId);
                                deliveryDates.setDeliveryDate(deliveryDate);
                                deliveryDates.setDeliveryStatus(deliveryStatus);
                                if (!deliveredlistingId.equals("")) {
                                    deliveryDates.setResId(deliveredlistingId);
                                }

                                String[] date = deliveryDate.split("-");
                                int year = Integer.parseInt(date[0]);
                                int month = Integer.parseInt(date[1]);
                                int day = Integer.parseInt(date[2]);

                                deliveryDates.setDeliveryDay(day);
                                deliveryDates.setDeliveryMonth(month);
                                deliveryDates.setDeliveryYear(year);

                                allDatesArrayList.add(deliveryDates);

                            }
                            populateCalender(allDatesArrayList, false);
//                            }
                        }
                    } else if (status == 0) {

                        Snackbar snackbar = Snackbar.make(snackbarView, message, Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                        snackbar.show();

                        new CountDownTimer(1500, 1000) {

                            @Override
                            public void onTick(long l) {

                            }

                            @Override
                            public void onFinish() {
                                // open home fragment and show restaurant listing if user is not subscribed
                                if (getFragmentManager() != null) {
                                    getFragmentManager()
                                            .beginTransaction()
                                            .setCustomAnimations(android.R.anim.slide_in_left,
                                                    android.R.anim.slide_out_right)
                                            .replace(R.id.main_content, new FoodFragment())
                                            .commit();
                                }
                            }
                        }.start();
                    }
                } catch (
                        Exception ex) {
                    Log.d(TAG, "calender fragment onException: " + ex.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                llLoader.setVisibility(View.GONE);

                Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                View getSnackbarView = snackbar.getView();
                getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                snackbar.show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (accessToken != null) {
                    params.put("Authorization", accessToken);
                    Log.d(TAG, "getHeaders " + accessToken);
                }
                return params;
            }
        };

        requestQueue = Volley.newRequestQueue(mContext);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);

    }
    //endregion

    //region populate calender method
    private void populateCalender(ArrayList<DeliveryDates> deliveryDatesArrayList) {

        calendarView.getMarkedDates().removeAdd();

        // 1 = show current restaurant dates, 2 = show all da
//        if (count == 1) {
        if (deliveryDatesArrayList != null) {
            for (int i = 0; i < deliveryDatesArrayList.size(); i++) {

                // highlight the date according to status
                switch (deliveryDatesArrayList.get(i).getDeliveryStatus()) {
                    case "pending":
                        calendarView.markDate(new DateData(deliveryDatesArrayList.get(i).getDeliveryYear(),
                                deliveryDatesArrayList.get(i).getDeliveryMonth(), deliveryDatesArrayList.get(i).getDeliveryDay()).setMarkStyle(new MarkStyle(MarkStyle.BACKGROUND, getResources().getColor(R.color.green))));
                        break;
                    case "pause":
                        calendarView.markDate(new DateData(deliveryDatesArrayList.get(i).getDeliveryYear(),
                                deliveryDatesArrayList.get(i).getDeliveryMonth(), deliveryDatesArrayList.get(i).getDeliveryDay()).setMarkStyle(new MarkStyle(MarkStyle.BACKGROUND, getResources().getColor(R.color.red))));
                        break;
                    case "delivered":
                        calendarView.markDate(new DateData(deliveryDatesArrayList.get(i).getDeliveryYear(),
                                deliveryDatesArrayList.get(i).getDeliveryMonth(), deliveryDatesArrayList.get(i).getDeliveryDay()).setMarkStyle(new MarkStyle(MarkStyle.BACKGROUND, getResources().getColor(R.color.blue))));
                        break;
                }
                Log.d(TAG, "onCreateView:  deliveryDay: " + deliveryDatesArrayList.get(i).getDeliveryDay() + " status: " + deliveryDatesArrayList.get(i).getDeliveryStatus());
            }
        }
//        }
    }

    private void populateCalender(ArrayList<DeliveryDates> deliveryDatesArrayList, boolean isCurrent) {

//        calendarView.getMarkedDates().removeAdd();

        // 1 = show current restaurant dates, 2 = show all da
//        if (count == 1) {
        if (deliveryDatesArrayList != null) {

            MarkStyle fullGreenStyle = new MarkStyle(MarkStyle.BACKGROUND, getResources().getColor(R.color.green));
            MarkStyle fullRedStyle = new MarkStyle(MarkStyle.BACKGROUND, getResources().getColor(R.color.red));
            MarkStyle fullBlueStyle = new MarkStyle(MarkStyle.BACKGROUND, getResources().getColor(R.color.blue));

            MarkStyle halfGreenStyle = new MarkStyle(MarkStyle.BACKGROUND, getResources().getColor(R.color.alpha_green));
            MarkStyle halfRedStyle = new MarkStyle(MarkStyle.BACKGROUND, getResources().getColor(R.color.alpha_red));
            MarkStyle halfBlueStyle = new MarkStyle(MarkStyle.BACKGROUND, getResources().getColor(R.color.alpha_blue));

            MarkStyle finalGreen = isCurrent ? fullGreenStyle : halfGreenStyle;
            MarkStyle finalRed = isCurrent ? fullRedStyle : halfRedStyle;
            MarkStyle finalBlue = isCurrent ? fullBlueStyle : halfBlueStyle;

            for (int i = 0; i < deliveryDatesArrayList.size(); i++) {

                // highlight the date according to status
                switch (deliveryDatesArrayList.get(i).getDeliveryStatus()) {
                    case "pending":
                        calendarView.markDate(new DateData(deliveryDatesArrayList.get(i).getDeliveryYear(),
                                deliveryDatesArrayList.get(i).getDeliveryMonth(), deliveryDatesArrayList.get(i).getDeliveryDay()).setMarkStyle(finalGreen));
                        break;
                    case "pause":
                        calendarView.markDate(new DateData(deliveryDatesArrayList.get(i).getDeliveryYear(),
                                deliveryDatesArrayList.get(i).getDeliveryMonth(), deliveryDatesArrayList.get(i).getDeliveryDay()).setMarkStyle(finalRed));
                        break;
                    case "delivered":
                        calendarView.markDate(new DateData(deliveryDatesArrayList.get(i).getDeliveryYear(),
                                deliveryDatesArrayList.get(i).getDeliveryMonth(), deliveryDatesArrayList.get(i).getDeliveryDay()).setMarkStyle(finalBlue));
                        break;
                }
                Log.d(TAG, "onCreateView:  deliveryDay: " + deliveryDatesArrayList.get(i).getDeliveryDay() + " status: " + deliveryDatesArrayList.get(i).getDeliveryStatus());
            }
        }
//        }
    }
    //endregion

    //region populate calender method
    private void populateAllCalenderDates(ArrayList<DeliveryDates> deliveryDatesArrayList) {

        calendarView.getMarkedDates().removeAdd();

        // 1 = show current restaurant dates, 2 = show all da
//        if (count == 1) {
        if (deliveryDatesArrayList != null) {
            for (int i = 0; i < deliveryDatesArrayList.size(); i++) {

                // highlight the date according to status
                switch (deliveryDatesArrayList.get(i).getDeliveryStatus()) {
                    case "pending-alpha":
                        calendarView.markDate(new DateData(deliveryDatesArrayList.get(i).getDeliveryYear(),
                                deliveryDatesArrayList.get(i).getDeliveryMonth(), deliveryDatesArrayList.get(i).getDeliveryDay()).setMarkStyle(new MarkStyle(MarkStyle.BACKGROUND, getResources().getColor(R.color.green))));
                        break;
                    case "pause-alpha":
                        calendarView.markDate(new DateData(deliveryDatesArrayList.get(i).getDeliveryYear(),
                                deliveryDatesArrayList.get(i).getDeliveryMonth(), deliveryDatesArrayList.get(i).getDeliveryDay()).setMarkStyle(new MarkStyle(MarkStyle.BACKGROUND, getResources().getColor(R.color.red))));
                        break;
                    case "delivered-alpha":
                        calendarView.markDate(new DateData(deliveryDatesArrayList.get(i).getDeliveryYear(),
                                deliveryDatesArrayList.get(i).getDeliveryMonth(), deliveryDatesArrayList.get(i).getDeliveryDay()).setMarkStyle(new MarkStyle(MarkStyle.BACKGROUND, getResources().getColor(R.color.blue))));
                        break;
                }
                Log.d(TAG, "onCreateView:  deliveryDay: " + deliveryDatesArrayList.get(i).getDeliveryDay() + " status: " + deliveryDatesArrayList.get(i).getDeliveryStatus());
            }
        }
//        }
    }
    //endregion

    //region open fragment method
    private void openFragment(Fragment selectedFragment) {
        if (getFragmentManager() != null) {
            getFragmentManager()
                    .beginTransaction()
                    .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                    .add(R.id.main_content, selectedFragment, "calendar")
                    .addToBackStack("null")
                    .commit();
        }
    }
    //endregion

    //region onResume method
    @Override
    public void onResume() {
        super.onResume();
        mPref = new MySharedPreferences(mContext);

        mPref.saveSelectedYear(0);
        mPref.saveSelectedMonth(0);
        mPref.saveSelectedDate(0);

        // if language object and type id is not empty then get current packages
        if (mPref.getLangObject() != null && mPref.getTypeId() != null) {
            //check which language is selected
            languageId = mPref.getLangObject().getId();
        }

        if (mPref.getAccessTokenObject() != null) {
            User.AccessToken token = mPref.getAccessTokenObject();
            if (token != null) {
                //get access token
                String accessToken = token.getAccessToken();
                //get calender detail
                getSubscribedRestaurant(languageId, accessToken);
            }
        } else {
            LoginBottomSheet loginBottomSheet = new LoginBottomSheet();
            loginBottomSheet.setCancelable(false);
            if (getFragmentManager() != null) {
                loginBottomSheet.show(getFragmentManager(), "login_bottom_sheet");
            }
        }

    }
    //endregion
}
