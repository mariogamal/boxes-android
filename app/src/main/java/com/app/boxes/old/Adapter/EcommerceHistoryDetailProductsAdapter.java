package com.app.boxes.old.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.boxes.old.Model.EcommerceHistory;
import com.app.boxes.R;

import java.util.ArrayList;

public class EcommerceHistoryDetailProductsAdapter extends RecyclerView.Adapter<EcommerceHistoryDetailProductsAdapter.ViewHolder> {

    private Context context;
    private ArrayList<EcommerceHistory> historyArrayList;

    public EcommerceHistoryDetailProductsAdapter(Context context, ArrayList<EcommerceHistory> historyArrayList) {
        this.context = context;
        this.historyArrayList = historyArrayList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_ecommerce_order_history_products, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        EcommerceHistory item = historyArrayList.get(position);

        holder.tvQuantity.setText(item.getQuantity());
        holder.tvProdName.setText(item.getProducts());
        holder.tvPrice.setText(item.getPrice());
    }

    @Override
    public int getItemCount() {
        return historyArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvQuantity, tvProdName, tvPrice;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvQuantity = itemView.findViewById(R.id.tv_ecommerce_order_history_detail_prod_quantity);
            tvProdName = itemView.findViewById(R.id.tv_ecommerce_order_history_detail_prod_name);
            tvPrice = itemView.findViewById(R.id.tv_ecommerce_order_history_detail_prod_price);
        }
    }
}
