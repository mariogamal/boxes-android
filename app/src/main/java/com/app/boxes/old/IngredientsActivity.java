package com.app.boxes.old;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.app.boxes.R;
import com.app.boxes.old.Adapter.IngredientsAdapter;

import java.util.ArrayList;

public class IngredientsActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ingredients);
        RecyclerView ingredientsList = findViewById(R.id.ingredients);
        if (getIntent().hasExtra("ingredients")) {
            ArrayList<String> ingredients = getIntent().getStringArrayListExtra("ingredients");
            IngredientsAdapter adapter = new IngredientsAdapter(ingredients);
            ingredientsList.setAdapter(adapter);
        }
    }
}