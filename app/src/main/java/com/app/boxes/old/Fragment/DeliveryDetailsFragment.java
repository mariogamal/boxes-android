package com.app.boxes.old.Fragment;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.app.boxes.old.Model.Area;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;

/**
 * A simple {@link Fragment} subclass.
 */
public class DeliveryDetailsFragment extends Fragment {

    private String TAG = "DeliveryDetailsFragment";

    private ImageView ivBtnBack;
    private EditText etCurrentLocation, etBuildingNumber, etStreet, etArea, etFloorUnit, etNetToRide;
    private Button btnApply;
    private View snackbarView;

    private MySharedPreferences sPref;

    public DeliveryDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_delivery_details, container, false);

        //initialize widgets
        initializer(rootView);

        //if arabic language is selected then back icon change
        if (sPref.getLocale().equals("ar"))
            ivBtnBack.setImageResource(R.drawable.ic_arrow_forward_white);

        //navigate back to place order fragment
        ivBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mContext.onBackPressed();
            }
        });

//        if (sPref.getUserArea() != null) {

        // if area obj is not null then get the data and set to edittext
//        if (sPref.getUserArea() != null) {
//        }
        if (sPref.getAreaObject() != null) {
            if (sPref.getAreaObject().getFloor() != null)
                etBuildingNumber.setText(sPref.getAreaObject().getBuildingNo());
            if (sPref.getAreaObject().getStreetNo() != null)
                etStreet.setText(sPref.getAreaObject().getStreetNo());
            if (sPref.getAreaObject().getArea() != null)
                etArea.setText(sPref.getAreaObject().getArea());
            if (sPref.getAreaObject().getFloor() != null)
                etFloorUnit.setText(sPref.getAreaObject().getFloor());
        }
//        }

        //navigate to cardInfo
        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                placeOrder();
            }
        });

        //enable back pressed navigation
        /*rootView.setFocusableInTouchMode(true);
        rootView.requestFocus();
        rootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (keyEvent.getAction() == KeyEvent.ACTION_DOWN && i == KeyEvent.KEYCODE_BACK) {

                    if (getFragmentManager() != null) {
//                    getFragmentManager().beginTransaction().remove(DeliveryDetailsFragment.this).commit();
                        getFragmentManager().popBackStack();
                    }

                    return true;
                }
                return false;
            }
        });*/

        return rootView;
    }

    private Activity mContext;

    @Override
    public void onAttach(Context context) {
        this.mContext = (Activity) context;
        super.onAttach(context);
    }

    private void initializer(View rootView) {
        ivBtnBack = rootView.findViewById(R.id.iv_delivery_details_back);

        etCurrentLocation = rootView.findViewById(R.id.et_delivery_details_current_location);
        etBuildingNumber = rootView.findViewById(R.id.et_delivery_details_building_number);
        etStreet = rootView.findViewById(R.id.et_delivery_details_street);
        etArea = rootView.findViewById(R.id.et_delivery_details_area);
        etFloorUnit = rootView.findViewById(R.id.et_delivery_details_floor_unit);
        etNetToRide = rootView.findViewById(R.id.et_delivery_details_net_to_ride);

        btnApply = rootView.findViewById(R.id.btn_delivery_details_apply);

        snackbarView = mContext.findViewById(android.R.id.content);

        sPref = new MySharedPreferences(mContext);
    }

    private void placeOrder() {
//        String currentLocation = etCurrentLocation.getText().toString();
        String building = etBuildingNumber.getText().toString();
        String street = etStreet.getText().toString();
        String area = etArea.getText().toString();
        String floor = etFloorUnit.getText().toString();
        String addIns = etNetToRide.getText().toString();
        /*if (currentLocation.isEmpty()) {
            etCurrentLocation.requestFocus();
            Snackbar snackbar = Snackbar.make(snackbarView, "Please allow location to get area!", Snackbar.LENGTH_SHORT);
            View getSnackbarView = snackbar.getView();
            getSnackbarView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
            tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));
            snackbar.show();
        } else*/
        if (building.isEmpty()) {
            etBuildingNumber.requestFocus();
            etBuildingNumber.setError("Field must not be empty");
//            Snackbar snackbar = Snackbar.make(snackbarView, "Fields must not be empty!", Snackbar.LENGTH_SHORT);
//            View getSnackbarView = snackbar.getView();
//            getSnackbarView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
//            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
//            tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));
//            snackbar.show();
        } else if (street.isEmpty()) {
            etStreet.requestFocus();
            etStreet.setError("Field must not be empty");
//            Snackbar snackbar = Snackbar.make(snackbarView, "Fields must not be empty!", Snackbar.LENGTH_SHORT);
//            View getSnackbarView = snackbar.getView();
//            getSnackbarView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
//            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
//            tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));
//            snackbar.show();
        } else if (area.isEmpty()) {
            etArea.requestFocus();
            etArea.setError("Field must not be empty");
            /*Snackbar snackbar = Snackbar.make(snackbarView, "Fields must not be empty!", Snackbar.LENGTH_SHORT);
            View getSnackbarView = snackbar.getView();
            getSnackbarView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
            tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));
            snackbar.show();*/
        } else if (area.trim().isEmpty()) {
            etArea.requestFocus();
            etArea.setError("Field must not be empty");
            /*Snackbar snackbar = Snackbar.make(snackbarView, "Fields must not be empty!", Snackbar.LENGTH_SHORT);
            View getSnackbarView = snackbar.getView();
            getSnackbarView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
            tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));
            snackbar.show();*/
        } else if (floor.isEmpty()) {
            etFloorUnit.requestFocus();
            etFloorUnit.setError("Field must not be empty");
            /*Snackbar snackbar = Snackbar.make(snackbarView, "Fields must not be empty!", Snackbar.LENGTH_SHORT);
            View getSnackbarView = snackbar.getView();
            getSnackbarView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
            tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));
            snackbar.show();*/
        } else {
            if (addIns.isEmpty())
                addIns = "none";
            else if (addIns.trim().isEmpty())
                addIns = "none";

            Area mArea = new Area();
//            mArea.setLocation(currentLocation);
            mArea.setBuildingNo(building);
            mArea.setStreetNo(street);
            mArea.setArea(area);
            mArea.setFloor(floor);
            mArea.setAddInst(addIns);

            sPref.saveAreaObject(mArea);

            mContext.onBackPressed();
        }

     /*avi.setVisibility(View.VISIBLE);
        RequestQueue requestQueue;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "PlaceOrder onResponse: " + response);
//                avi.setVisibility(View.GONE);

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    int status = jsonObject.getInt("status");

                    if (status == 1) {
                        JSONObject data = jsonObject.getJSONObject("data");


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
//                avi.setVisibility(View.GONE);

                Log.d(TAG, "Volley Error: " + error.toString());
                if (error instanceof NoConnectionError) {
                    Snackbar snackbar = Snackbar.make(snackbarView, "No Route To Host!", Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
                    snackbar.show();
                } else {
                    Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
                    snackbar.show();
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (accessToken != null) {
                    params.put("Authorization", accessToken);
                    Log.d(TAG, "getHeaders " + accessToken);
                }
                return params;
            }
        };

        requestQueue = Volley.newRequestQueue(mContext);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(0, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);*/
    }

}