package com.app.boxes.old.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.boxes.old.API.API;
import com.app.boxes.old.Model.User;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.app.boxes.old.UI.LoginBottomSheet;
import com.google.android.material.snackbar.Snackbar;
import com.hbb20.CountryCodePicker;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ChangeNumberActivity extends AppCompatActivity {

    private static final String TAG = "ChangeNumberActivity";

    private EditText etCurrentPhone, etNewPhone;
    private Button btnSubmit;
    private ImageView ivBack;
    private AVLoadingIndicatorView avi;
    private View snackbarView;
    private CountryCodePicker ccp;

    private MySharedPreferences sPref;

    private String currentNumber = "", newNumber = "", countryCode = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_number);

        // initialize components
        initializer();

    }

    void initializer() {
        etCurrentPhone = findViewById(R.id.edt_change_number_current_phone);
        etNewPhone = findViewById(R.id.edt_change_number_new_phone);

        btnSubmit = findViewById(R.id.btn_change_number);

        ivBack = findViewById(R.id.iv_change_number_back);

        avi = findViewById(R.id.avi_change_number);
        avi.setVisibility(View.GONE);

        snackbarView = findViewById(android.R.id.content);

        ccp = findViewById(R.id.ccp_change_number);
        ccp.setTypeFace(ResourcesCompat.getFont(this, R.font.ptsans_regular));
        countryCode = ccp.getSelectedCountryCodeWithPlus();
        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                countryCode = ccp.getSelectedCountryCodeWithPlus();
            }
        });

        sPref = new MySharedPreferences(this);
        if (sPref.getAccessTokenObject() != null) {
            User.AccessToken token = sPref.getAccessTokenObject();
            if (token != null) {
                String accessToken = token.getAccessToken();
                getUserPhoneNo(accessToken);
                clickListeners(accessToken);
            }
        } else {
            LoginBottomSheet loginBottomSheet = new LoginBottomSheet();
            loginBottomSheet.setCancelable(false);
            if (getFragmentManager() != null) {
                loginBottomSheet.show(getSupportFragmentManager(), "login_bottom_sheet");
            }
        }

        if (sPref.getLocale().equals("ar"))
            ivBack.setImageResource(R.drawable.ic_arrow_forward_white);
    }

    // set onclick listeners
    void clickListeners(final String accessToken) {

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeNumber(accessToken);
            }
        });
    }

    // get user phone no
    private void getUserPhoneNo(final String accessToken) {
        String url = API.getAPI() + "auth/user";
        Log.d(TAG, "User URL: " + url);

        avi.setVisibility(View.VISIBLE);
        RequestQueue requestQueue;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "User onResponse: " + response);
                avi.setVisibility(View.GONE);

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    int status = jsonObject.optInt("status");

                    if (status == 1) {
                        JSONObject data = jsonObject.getJSONObject("data");

                        String getPhoneNumber = data.optString("phone");

                        if (!getPhoneNumber.equals("null") || !getPhoneNumber.equals("")) {
                            etCurrentPhone.setText(getPhoneNumber);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                avi.setVisibility(View.GONE);
                Log.d(TAG, "Volley Error: " + error.toString());
                if (error instanceof NoConnectionError) {
                    Snackbar.make(snackbarView, getString(R.string.no_network), Snackbar.LENGTH_SHORT).show();
                } else {
                    Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT).show();
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (accessToken != null) {
                    params.put("Authorization", accessToken);
                    Log.d(TAG, "getHeaders " + accessToken);
                }
                return params;
            }
        };
        requestQueue = Volley.newRequestQueue(this);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    // change user phone number
    private void changeNumber(final String accessToken) {
        currentNumber = etCurrentPhone.getText().toString();
        newNumber = etNewPhone.getText().toString();
        //check if phone fields are empty
        if (currentNumber.isEmpty()) {
            Snackbar.make(snackbarView, "Please fill the required field", Snackbar.LENGTH_SHORT).show();
            etCurrentPhone.requestFocus();
        } else if (newNumber.isEmpty()) {
            Toast.makeText(this, "Please fill the required field", Toast.LENGTH_SHORT).show();
            etNewPhone.requestFocus();
        } else if (countryCode.isEmpty()) {
            Toast.makeText(this, "Please Select Country Code", Toast.LENGTH_SHORT).show();
            etNewPhone.requestFocus();
        } else {
            String url = API.getAPI() + "auth/change-number?old_number=" + currentNumber + "&new_number=" + newNumber;
            Log.d(TAG, "Change Number URL: " + url);

            avi.setVisibility(View.VISIBLE);
            RequestQueue requestQueue;
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d(TAG, "Response" + response);
                    avi.setVisibility(View.GONE);
                    try {
                        int status = response.getInt("status");
                        String message = response.getString("message");

                        Log.d(TAG, "status " + status + " message" + message);
                        if (status == 0) {
                            if (response.has("parameters")) {
                                JSONObject paramObj = response.getJSONObject("parameters");
                                JSONArray paramArray = paramObj.getJSONArray("new_number");
                                String paramError = paramArray.toString();
                                Snackbar.make(snackbarView, paramError, Snackbar.LENGTH_SHORT).show();
                            }
                        } else {
                            Snackbar.make(snackbarView, message, Snackbar.LENGTH_SHORT).show();
                            new CountDownTimer(2000, 1000) {

                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    logoutUser(accessToken);
                                    Intent otpIntent = new Intent(ChangeNumberActivity.this, OTPActivity.class);
                                    otpIntent.putExtra("phone_number", newNumber);
                                    startActivity(otpIntent);
                                }
                            }.start();
                        }
                    } catch (Exception ex) {
                        Log.d("Login", ex.toString());
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    avi.setVisibility(View.GONE);
                    Log.d("Forgot-Password-Error", "Volley Error: " + error.toString());

                    if (error instanceof NoConnectionError) {
                        Snackbar.make(snackbarView, getString(R.string.no_network), Snackbar.LENGTH_SHORT).show();
                    } else {
                        Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT).show();
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    if (accessToken != null) {
                        params.put("Authorization", accessToken);
                        Log.d(TAG, "getHeaders " + accessToken);
                    }
                    return params;
                }
            };

            requestQueue = Volley.newRequestQueue(this);
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000,  DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(jsonObjectRequest);
        }
    }

    // logging out user from app and server
    void logoutUser(final String accessToken) {
        String URL = API.getAPI() + "auth/logout";
        Log.d(TAG, "Logout URL : " + URL);

        avi.setVisibility(View.VISIBLE);
        RequestQueue requestQueue;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "onResponse " + response);
                avi.setVisibility(View.GONE);
                sPref.clearAllPreferences();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                avi.setVisibility(View.GONE);
                Log.d(TAG, "onErrorResponse " + error.toString());

                if (error instanceof NoConnectionError) {
                    Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.no_network), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
                    snackbar.show();
                } else {
                    Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
                    snackbar.show();
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (accessToken != null) {
                    params.put("Authorization", accessToken);
                    Log.d(TAG, "getHeaders " + accessToken);
                }
                return params;
            }
        };
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }
}