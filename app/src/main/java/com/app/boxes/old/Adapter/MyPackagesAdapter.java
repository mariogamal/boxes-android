package com.app.boxes.old.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.boxes.old.Model.MyPackages;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;

import java.util.ArrayList;

public class MyPackagesAdapter extends RecyclerView.Adapter<MyPackagesAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<MyPackages> myPackagesArrayList;
    private LayoutInflater inflater;

    MySharedPreferences sPref;

    public MyPackagesAdapter(Context context, ArrayList<MyPackages> myPackagesArrayList) {
        this.context = context;
        this.myPackagesArrayList = myPackagesArrayList;
        sPref = new MySharedPreferences(context);
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.my_packages_list, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        holder.PlanName.setText(myPackagesArrayList.get(position).getPlanName());
        holder.StartDate.setText(myPackagesArrayList.get(position).getStartDate());
        holder.EndDate.setText(myPackagesArrayList.get(position).getEndDate());
        holder.PackageType.setText(myPackagesArrayList.get(position).getPackageType());

        if (sPref.getLocale().equals("ar"))
            holder.ivBtnRight.setImageResource(R.drawable.date_arrow_left);
    }

    @Override
    public int getItemCount() {
        return myPackagesArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView PlanName, StartDate, EndDate, PackageType;
        ImageView ivBtnRight;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            PlanName = itemView.findViewById(R.id.tv_my_packages_plan_name);
            StartDate = itemView.findViewById(R.id.tv_my_packages_start_date);
            EndDate = itemView.findViewById(R.id.tv_my_packages_end_date);
            PackageType = itemView.findViewById(R.id.tv_my_packages_type);
            ivBtnRight = itemView.findViewById(R.id.iv_my_packages_right);

        }
    }

}
