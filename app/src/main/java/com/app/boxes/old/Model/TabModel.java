package com.app.boxes.old.Model;

public class TabModel {
    private String id,tabName;

    public TabModel(String id, String tabName) {
        this.id = id;
        this.tabName = tabName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTabName() {
        return tabName;
    }

    public void setTabName(String tabName) {
        this.tabName = tabName;
    }
}
