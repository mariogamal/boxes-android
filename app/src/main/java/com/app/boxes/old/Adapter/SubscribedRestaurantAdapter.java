package com.app.boxes.old.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.app.boxes.old.Model.SingleRestaurant;
import com.app.boxes.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class SubscribedRestaurantAdapter extends RecyclerView.Adapter<SubscribedRestaurantAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<SingleRestaurant> subscribedRestaurantArrayList;
    private LayoutInflater inflater;

    private static SubscribedRestaurantClickListener sClickListener;

    public SubscribedRestaurantAdapter(Context context, ArrayList<SingleRestaurant> subscribedRestaurantArrayList, SubscribedRestaurantClickListener listener) {
        this.context = context;
        this.subscribedRestaurantArrayList = subscribedRestaurantArrayList;
        sClickListener = listener;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView = inflater.inflate(R.layout.list_subscribed_restaurant, parent, false);

        return new MyViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        SingleRestaurant item = subscribedRestaurantArrayList.get(position);
        Picasso.get().load(item.getImages()).into(holder.ivRestaurantIcon);
        holder.tvRestaurantName.setText(item.getName());
        holder.tvMealCount.setText(item.getMealCount());
        holder.tvMealOutOf.setText(item.getMealOutOff());

        if (item.isSelected()) {
            holder.linearLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.active_tab_color));
        } else {
            holder.linearLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.white));
        }

    }

    @Override
    public int getItemCount() {
        return subscribedRestaurantArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        ImageView ivRestaurantIcon;
        TextView tvRestaurantName, tvMealCount, tvMealOutOf;
        LinearLayout linearLayout;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            ivRestaurantIcon = itemView.findViewById(R.id.iv_subscribed_restaurant);
            tvRestaurantName = itemView.findViewById(R.id.tv_subscribed_restaurant_name);
            tvMealCount = itemView.findViewById(R.id.tv_subscribed_single_item);
            tvMealOutOf = itemView.findViewById(R.id.tv_subscribed_single_item_out_of);
            linearLayout = itemView.findViewById(R.id.ll_image);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            sClickListener.onSingleResItemClick(getAdapterPosition(), view);
        }
    }


    public interface SubscribedRestaurantClickListener {
        void onSingleResItemClick(int position, View view);
    }
}
