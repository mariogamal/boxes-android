package com.app.boxes.old.Fragment;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.app.boxes.old.Activity.MainActivity;
import com.app.boxes.old.Adapter.TabAdapter;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.google.android.material.tabs.TabLayout;

/**
 * A simple {@link Fragment} subclass.
 */
public class RestaurantMultiSubscriptionFragment extends Fragment {

    private TabAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    public RestaurantMultiSubscriptionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_multi_restaurant_subscription, container, false);

        ((MainActivity) mContext).getSupportActionBar().hide();

        ImageView ivBtnBack = rootView.findViewById(R.id.iv_multi_restaurant_sub_back);

        MySharedPreferences sPref = new MySharedPreferences(getActivity());
        sPref.setCardInfoFragment(1);

        ivBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                getFragmentManager().popBackStack();


            }
        });

        viewPager = rootView.findViewById(R.id.multi_restaurant_subscription_viewPager);
        tabLayout = rootView.findViewById(R.id.multi_restaurant_subscription_tabLayout);

        adapter = new TabAdapter(getFragmentManager());
        adapter.addFragment(new SubscriptionPlanFragment(), getString(R.string.subscription));
        adapter.addFragment(new MenuFragment(), getString(R.string.menu));

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        if (sPref.getLocale().equals("ar")) {
            ivBtnBack.setImageResource(R.drawable.ic_arrow_forward_white);

//            tabLayout.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            ViewCompat.setLayoutDirection(tabLayout, ViewCompat.LAYOUT_DIRECTION_RTL);
        }

        viewPager.addOnPageChangeListener(
                new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(
                new TabLayout.ViewPagerOnTabSelectedListener(viewPager));

        /*rootView.setFocusableInTouchMode(true);
        rootView.requestFocus();
        rootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_BACK) {

                    Log.d("onbackpressed", "Back stack entry count" + getFragmentManager().getBackStackEntryCount());

                    getFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                            .replace(R.id.main_content, new HomeFragment())
                            .commit();


//                    startActivity(new Intent(mContext, MainActivity.class));
//                    mContext.finish();

                    return true;
                }
                return false;
            }
        });*/


        return rootView;
    }

    private Activity mContext;

    @Override
    public void onAttach(Context context) {
        this.mContext = (Activity) context;
        super.onAttach(context);
    }

}
