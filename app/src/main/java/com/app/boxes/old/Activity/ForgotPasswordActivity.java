package com.app.boxes.old.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.app.boxes.old.API.API;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;

import org.json.JSONObject;

public class ForgotPasswordActivity extends AppCompatActivity {

    private static final String TAG = "ForgotPasswordActivity";

    private TextView tvSkip;

    private EditText edtEmail;

    private Button btnLogin;

    private MySharedPreferences mPref;

    private String email;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPref = new MySharedPreferences(this);
        mPref.getLocale();
        Log.d(TAG, "Default Language is: " + mPref.getLocale());

        setContentView(R.layout.activity_forgot_password);

        //initialize the widget
        Initializer();

        //navigate to main activity
        tvSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(ForgotPasswordActivity.this, MainActivity.class));
                finish();
            }
        });

        //For login
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Login();
            }
        });

    }

    private void Initializer() {

        //initialize widgets
        edtEmail = findViewById(R.id.edt_forgot_pass_email);

        tvSkip = findViewById(R.id.tv_forget_pass_skip);

        btnLogin = findViewById(R.id.btn_forgot_pass_login);
    }

    private void Login() {
        email = edtEmail.getText().toString();

        //check if email field is empty
        if (email.isEmpty()) {
            Toast.makeText(this, "Please fill the required field", Toast.LENGTH_SHORT).show();
            edtEmail.requestFocus();
        }
        //check if email field has valid email address
        else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            Toast.makeText(this, "Please enter the valid email address", Toast.LENGTH_SHORT).show();
            edtEmail.requestFocus();
        } else {
            String url = API.getAPI() + "forgot-password?email=" + email;
            Log.d(TAG, "URL: " + url);

            RequestQueue requestQueue;
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d(TAG, "Response" + response);

                    try {
                        int status = response.getInt("status");
                        String message = response.getString("message");

                        Log.d(TAG, "status " + status + " message" + message);
                        if (status == 1) {
                            Toast.makeText(ForgotPasswordActivity.this, message, Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(ForgotPasswordActivity.this, SignInActivity.class));
                            finish();
                        } else {
                            Toast.makeText(ForgotPasswordActivity.this, message, Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception ex) {
                        Log.d("Login", ex.toString());
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("Forgot-Password-Error", "Volley Error: " + error.toString());

                    String getError = error.toString();
                    if (getError.contains("AuthFailure")) {
                        Toast.makeText(ForgotPasswordActivity.this, "Authentication problem", Toast.LENGTH_SHORT).show();
                    } else if (getError.contains("NoConnectionError")) {
                        Toast.makeText(ForgotPasswordActivity.this, "No route to host", Toast.LENGTH_SHORT).show();
                    }
                }
            });

            requestQueue = Volley.newRequestQueue(this);
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000,  0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(jsonObjectRequest);
        }
    }
}
