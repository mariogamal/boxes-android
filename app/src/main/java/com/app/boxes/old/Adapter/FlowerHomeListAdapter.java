package com.app.boxes.old.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.boxes.old.Model.Flower;
import com.app.boxes.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class FlowerHomeListAdapter extends RecyclerView.Adapter<FlowerHomeListAdapter.MyViewHolder> {

    Context context;
    ArrayList<Flower> flowerHomeArrayList;
    LayoutInflater inflater;

    public FlowerHomeListAdapter(Context context, ArrayList<Flower> flowerHomeArrayList) {
        this.context = context;
        this.flowerHomeArrayList = flowerHomeArrayList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView = inflater.inflate(R.layout.list_flower_home, parent, false);

        return new MyViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Picasso.get().load(flowerHomeArrayList.get(position).getImages()).fit().centerCrop().placeholder(R.drawable.boxes_logo).into(holder.ivImage);

//        holder.ivImage.setImageResource(flowerHomeArrayList.get(position).getImages());
        holder.tvName.setText(flowerHomeArrayList.get(position).getName());
        holder.tvDescription.setText(flowerHomeArrayList.get(position).getCaption());
    }

    @Override
    public int getItemCount() {
        return flowerHomeArrayList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView ivImage;
        private TextView tvName, tvDescription;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            ivImage = itemView.findViewById(R.id.iv_flower_home_icon);
            tvName = itemView.findViewById(R.id.tv_flower_home_name);
            tvDescription = itemView.findViewById(R.id.tv_flower_home_category);
        }
    }
}
