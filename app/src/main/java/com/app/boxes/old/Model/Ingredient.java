package com.app.boxes.old.Model;

/**
 * Created by Mario Gamal on 10/17/20
 * Copyright © 2020 Jusclean. All rights reserved.
 */
public class Ingredient {
    private String id;
    private String name;

    public Ingredient(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
