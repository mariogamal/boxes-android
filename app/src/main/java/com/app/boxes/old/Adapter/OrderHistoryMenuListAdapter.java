package com.app.boxes.old.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.boxes.old.Model.OrderHistoryMenu;
import com.app.boxes.old.Model.OrderHistoryMenuItem;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import jp.wasabeef.picasso.transformations.RoundedCornersTransformation;

public class OrderHistoryMenuListAdapter extends BaseExpandableListAdapter {

    private static final String TAG = "OrderHistoryAdapter";

    private Context context;
    private ArrayList<OrderHistoryMenu> orderHistoryMenus;
    private LayoutInflater inflater;
    private MySharedPreferences sPref;
    private OnReviewClickListener listener;

    public OrderHistoryMenuListAdapter(Context context, ArrayList<OrderHistoryMenu> orderHistoryMenus, OnReviewClickListener listener) {
        this.context = context;
        this.orderHistoryMenus = orderHistoryMenus;
        this.listener = listener;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        sPref = new MySharedPreferences(context);
    }

    //get number of category
    @Override
    public int getGroupCount() {
        return orderHistoryMenus.size();
    }

    //get number of menu item
    @Override
    public int getChildrenCount(int groupPosition) {
        return orderHistoryMenus.get(groupPosition).orderHistoryMenuItemArrayList.size();
    }

    //get menu category
    @Override
    public Object getGroup(int groupPosition) {
        return orderHistoryMenus.get(groupPosition);
    }

    //get single menu
    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return orderHistoryMenus.get(groupPosition).orderHistoryMenuItemArrayList.get(childPosition);
    }

    //get menu category id
    @Override
    public long getGroupId(int i) {
        return 0;
    }

    //get menu item id
    @Override
    public long getChildId(int i, int i1) {
        return i;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }


    //get menu category row
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.menu_category, null);
        }

        ImageView ivIcon = convertView.findViewById(R.id.iv_icon);

        //check if child is expanded so it set the arrow down
        if (isExpanded) {
            ivIcon.setImageResource(R.drawable.arrow_down_up);
        } else {
            ivIcon.setImageResource(R.drawable.arrow_down);
        }
        //get menu item
        OrderHistoryMenu orderHistoryMenu = (OrderHistoryMenu) getGroup(groupPosition);

        //set menu item
        TextView menuName = convertView.findViewById(R.id.menuCategory);

        String category = orderHistoryMenu.Category;
        menuName.setText(category);

        return convertView;
    }

    //get menu item row
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.order_history_menu_item, null);
        }

//        //get child/menu item name
        final OrderHistoryMenuItem orderHistoryMenuItem = (OrderHistoryMenuItem) getChild(groupPosition, childPosition);
//
//        //set child/menu item name
        ImageView menuImage = convertView.findViewById(R.id.iv_order_history_menu_item_icon);
        TextView menuName = convertView.findViewById(R.id.tv_order_history_menu_item_name);
        TextView menuDesc = convertView.findViewById(R.id.tv_order_history_menu_item_desc);
        ImageView menuChecked = convertView.findViewById(R.id.iv_order_history_checked);
        TextView menuCalories = convertView.findViewById(R.id.tv_order_history_menu_item_calories);
        TextView menuCarbs = convertView.findViewById(R.id.tv_order_history_menu_item_carbs);
        TextView menuProtein = convertView.findViewById(R.id.tv_order_history_menu_item_protein);
        Button menuReview = convertView.findViewById(R.id.iv_order_history_menu_item_add_review);

        Picasso.get().load(orderHistoryMenuItem.getImages()).transform(new RoundedCornersTransformation(50, 0)).placeholder(R.drawable.boxes_logo).into(menuImage);
        menuName.setText(orderHistoryMenuItem.getName());
        menuDesc.setText(orderHistoryMenuItem.getDescription());
        menuChecked.setImageResource(orderHistoryMenuItem.getChecked());
        menuCalories.setText(orderHistoryMenuItem.getCalories());
        menuCarbs.setText(orderHistoryMenuItem.getCarbs());
        menuProtein.setText(orderHistoryMenuItem.getProtein());

        if (sPref.getAccessTokenObject() != null) {
            menuReview.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onReviewButtonClick(orderHistoryMenuItem);
//                    reviewDialog(context, orderHistoryMenuItem, sPref.getAccessTokenObject().getAccessToken());
                }
            });
        }

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }

    public interface OnReviewClickListener {
        void onReviewButtonClick(OrderHistoryMenuItem item);
    }
}