package com.app.boxes.old.Adapter;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.app.boxes.old.Fragment.FlowerSubListingFragment;
import com.app.boxes.old.Model.Flower;
import com.app.boxes.old.SharedPreference.MySharedPreferences;

import java.util.ArrayList;

public class FlowerPagerAdapter extends FragmentStatePagerAdapter {

    ArrayList<String> tabName;
    ArrayList<ArrayList<Flower>> flowers;
    Context context;
    MySharedPreferences mPref;

    public FlowerPagerAdapter(FragmentManager fm, ArrayList<String> tabName, ArrayList<ArrayList<Flower>> flowers, Context c) {
        super(fm);
        this.tabName = tabName;
        this.flowers = flowers;
        this.context = c;

        mPref = new MySharedPreferences(context);
    }

    @Override
    public Fragment getItem(int position) {
//        TinyDB tinyDB = new TinyDB(context);
//        tinyDB.putListFlower("flower_list", flowers.get(position));

        Fragment fragment = new FlowerSubListingFragment();

//        mPref.saveFlowerArrayList(flowers.get(position));

        Bundle bundle = new Bundle();
        if (!flowers.get(position).isEmpty())
            bundle.putParcelableArrayList("flowers_list", flowers.get(position));
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public int getCount() {
        return tabName.size();

    }

    //for tab names
    @Override
    public CharSequence getPageTitle(int position) {
        return tabName.get(position);
    }
}