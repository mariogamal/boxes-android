package com.app.boxes.old.Fragment;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.app.boxes.old.API.API;
import com.app.boxes.old.Model.User;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.google.android.material.snackbar.Snackbar;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
//@Puppet
public class ContactUsFragment extends Fragment {

    private static final String TAG = "ContactUsFragment";
    private String phone = "", email = "", firstName = "", lastName = "", message = "";

    private EditText etFirstName, etLastName, etPhone, etEmail, etMsg;
    private Button btnSubmit;
    private AVLoadingIndicatorView avi;
    private View snackbarView;

    public ContactUsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_contact_us, container, false);

        etFirstName = rootView.findViewById(R.id.et_contact_us_first_name);
        etLastName = rootView.findViewById(R.id.et_contact_us_last_name);
        etPhone = rootView.findViewById(R.id.et_contact_us_phone);
        etEmail = rootView.findViewById(R.id.et_contact_us_email);
        etMsg = rootView.findViewById(R.id.et_contact_us_msg);

        avi = rootView.findViewById(R.id.avi_contact_us);
        avi.setVisibility(View.GONE);

        btnSubmit = rootView.findViewById(R.id.btn_contact_us_submit);

        snackbarView = mContext.findViewById(android.R.id.content);

        MySharedPreferences sPref = new MySharedPreferences(mContext);

        if (sPref.getUserObject() != null) {
            User user = sPref.getUserObject();
            Log.d(TAG, "onCreateView User: " + user.getFirstName() + " " + user.getLastName() + " " + user.getEmail() + " " + user.getPhoneNumber());

            if (user.getFirstName() != null && user.getLastName() != null && user.getEmail() != null && user.getPhoneNumber() != null) {
                if (!user.getFirstName().equals("null") && !user.getLastName().equals("null") && !user.getEmail().equals("null") && !user.getPhoneNumber().equals("null")) {
                    etFirstName.setText(user.getFirstName());
                    etLastName.setText(user.getLastName());
                    etEmail.setText(user.getEmail());
                    etPhone.setText(user.getPhoneNumber());
                }
            }
        }

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                contactUs();
            }
        });

        /*rootView.setFocusableInTouchMode(true);
        rootView.requestFocus();
        rootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_BACK) {

                    if (getFragmentManager() != null) {
                        getFragmentManager().popBackStack();
                    }
                    return true;
                }
                return false;
            }
        });*/

        return rootView;
    }

    private void alertDialog(final Context context) {

        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(getString(R.string.message));
        alertDialog.setMessage(getString(R.string.message_content));
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                alertDialog.dismiss();
                dialogInterface.dismiss();
                mContext.finish();
            }
        });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                alertDialog.dismiss();
            }
        });

        alertDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int keyCode, KeyEvent keyEvent) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    alertDialog.dismiss();
                }
                return true;
            }
        });
        alertDialog.show();
    }

    private Activity mContext;

    @Override
    public void onAttach(Context context) {
        this.mContext = (Activity) context;
        super.onAttach(context);
    }

    // to send user query to server
    private void contactUs() {

        firstName = etFirstName.getText().toString();
        lastName = etLastName.getText().toString();
        email = etEmail.getText().toString();
        phone = etPhone.getText().toString();
        message = etMsg.getText().toString();

        if (firstName.isEmpty()) {
            etFirstName.setError("Please fill the required field..");
            etFirstName.requestFocus();
        } else if (lastName.isEmpty()) {
            etLastName.setError("Please fill the required field..");
            etLastName.requestFocus();
        } else if (email.isEmpty()) {
            etEmail.setError("Please fill the required field..");
            etEmail.requestFocus();
        } else if (phone.isEmpty()) {
            Snackbar snackbar = Snackbar.make(snackbarView, "Please enter your phone number", Snackbar.LENGTH_SHORT);
            View getSnackbarView = snackbar.getView();
            getSnackbarView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
            tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));
            snackbar.show();
            etPhone.setError("Field should not be empty");
            etPhone.requestFocus();
        } else if (message.isEmpty()) {
            etMsg.setError("Please fill the required field..");
            etMsg.requestFocus();
        } else {

            String url = API.getAPI() + "contact/submit-query"/*?first_name=" + firstName + "&last_name=" + lastName + "&email=" + email + "&phone=" + countryCode + phone*/;
            Log.d(TAG, "Contact URL: " + url);

            avi.setVisibility(View.VISIBLE);
            RequestQueue requestQueue;
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public void onResponse(JSONObject response) {
                    Log.d(TAG, "Contact Response" + response.toString());
                    avi.setVisibility(View.GONE);

                    try {
                        int status = response.getInt("status");
                        String msg = response.getString("message");

                        Snackbar snackbar = Snackbar.make(snackbarView, msg, Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));
                        snackbar.show();

                        if (status == 1) {
                            etFirstName.setText("");
                            etLastName.setText("");
                            etEmail.setText("");
                            etPhone.setText("");
                            etMsg.setText("");

                            if (getFragmentManager() != null) {
                                getFragmentManager().popBackStack();
                            }
                        }

                    } catch (Exception ex) {
                        Log.d(TAG, ex.toString());
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    avi.setVisibility(View.GONE);
                    if (error instanceof NoConnectionError) {
                        Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.no_network), Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));
                        snackbar.show();
                    } else {
                        Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));
                        snackbar.show();
                    }
                }
            }) {
                @Override
                public byte[] getBody() {
                    Map<String, String> params = new HashMap<>();
                    Log.d(TAG, "getBody params: " + firstName + " " + lastName + " " + email + " " + phone + " " + message);
                    params.put("first_name", firstName);
                    params.put("last_name", lastName);
                    params.put("email", email);
                    params.put("phone", phone);
                    params.put("message", message);
                    return new JSONObject(params).toString().getBytes();
                }

                @Override
                public String getBodyContentType() {
                    return "application/json";
                }
            };

            requestQueue = Volley.newRequestQueue(mContext);
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(jsonObjectRequest);
        }
    }
}