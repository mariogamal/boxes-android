package com.app.boxes.old.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.boxes.R;

import java.util.ArrayList;

public class SearchHistoryAdapter extends RecyclerView.Adapter<SearchHistoryAdapter.MyViewHolder>{

    private ArrayList<String> gymNames;
    private SearchHistoryClickListener searchClickListener;

    public SearchHistoryAdapter(ArrayList<String> gymNames, SearchHistoryClickListener listener) {
        this.gymNames = gymNames;
        this.searchClickListener = listener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.search_history_item, parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        holder.tvSearch.setText(gymNames.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                searchClickListener.onHistoryItemClickListener(position, view);
            }
        });
    }

    @Override
    public int getItemCount() {
        return gymNames.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvSearch;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            tvSearch = itemView.findViewById(R.id.tv_search_history);
        }
    }

    public interface SearchHistoryClickListener {
        void onHistoryItemClickListener(int pos, View view);
    }
}
