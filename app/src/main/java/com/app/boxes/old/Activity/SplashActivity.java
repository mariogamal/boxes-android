package com.app.boxes.old.Activity;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import android.view.WindowManager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.boxes.old.API.API;
import com.app.boxes.old.Model.Language;
import com.app.boxes.old.Model.User;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SplashActivity extends AppCompatActivity {

    private Boolean isFirstTime = true;
    public static boolean shouldShowProfile = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        // Hide status bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        MySharedPreferences spref = new MySharedPreferences(SplashActivity.this);
        isFirstTime = spref.getAppFirstTime();

        if (spref.getLangObject() != null && spref.getAccessTokenObject() != null) {
            final Language language = spref.getLangObject();
            final User.AccessToken token = spref.getAccessTokenObject();
            if (token != null)
                getCurrentPackage(language.getId(), token.getAccessToken());
        } else
            handleNextPage();
    }

    void openLogin() {
        startActivity(new Intent(SplashActivity.this, SignInActivity.class));
        finish();
    }

    void openSelectLanguage() {
        Intent mIntent = new Intent(SplashActivity.this, SelectLanguageActivity.class);
        mIntent.putExtra("from", "splash");
        startActivity(mIntent);
        finish();
    }

    private void getCurrentPackage(String languageId, final String accessToken) {
        String url = API.getAPI() + "auth/my-current-package?language_id=" + languageId;
        RequestQueue requestQueue;
        final StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    if (status == 1) {
                        JSONObject dataObj = jsonObject.getJSONObject("data");
                        JSONObject userSubscriptionObj = dataObj.optJSONObject("user_subscription");
                        shouldShowProfile = userSubscriptionObj != null;
                        handleNextPage();
                    }
                } catch (Exception ignore) {
                    handleNextPage();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                handleNextPage();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (accessToken != null) {
                    params.put("Authorization", accessToken);
                }
                return params;
            }
        };

        requestQueue = Volley.newRequestQueue(this);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);

    }

    private void handleNextPage() {
        if (!isFirstTime)
            openLogin();
        else
            openSelectLanguage();
    }
}