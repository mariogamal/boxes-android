package com.app.boxes.old.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.boxes.old.Model.DaysOfTheWeek;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;

import java.util.ArrayList;

public class DayOfTheWeekAdapter extends RecyclerView.Adapter<DayOfTheWeekAdapter.MyViewHolder> {
    private static int NB_OF_ITEM_TO_DISPLAY = 5;
    private String TAG = "DayOfTheWeekAdapter";
    private Context context;
    private ArrayList<DaysOfTheWeek> daysOfTheWeekArrayList = new ArrayList<>();
    private LayoutInflater inflater;
    boolean highlighted = false;
    private int rowWidth;
    private static int sPosition;

    private MySharedPreferences mPref;

    private static SparseBooleanArray sSelectedItems, sSelectedItems1;
    private static DayOfTheWeekClickListener sClickListener;
    private static DayOfTheWeekPauseClickListener pClickListener;

    public DayOfTheWeekAdapter(Context context, ArrayList<DaysOfTheWeek> daysOfTheWeekArrayList) {
        this.context = context;
        this.daysOfTheWeekArrayList = daysOfTheWeekArrayList;

        sSelectedItems = new SparseBooleanArray();
        sSelectedItems1 = new SparseBooleanArray();

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mPref = new MySharedPreferences(context);
    }

    public void setItemHeight(int parentHeightInPx) {
        // Height of a row is just parent height divided by number of row to display
        this.rowWidth = parentHeightInPx / DayOfTheWeekAdapter.NB_OF_ITEM_TO_DISPLAY;
        // Notify adapter because items will need to be redraw to use newly set height
        this.notifyDataSetChanged();
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView = inflater.inflate(R.layout.list_days_of_the_week, parent, false);

        return new MyViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {

        DaysOfTheWeek item = daysOfTheWeekArrayList.get(position);

        holder.tvDays.setText(String.valueOf(item.getDays()));
        holder.ivStatus.setImageResource(item.getIcon());

        // highlight the selected day on click listener
        if (sSelectedItems.get(position)) {
            holder.tvDays.setBackgroundResource(R.drawable.highlight_days);
            holder.tvDays.setTextColor(Color.WHITE);
        } else {
            holder.tvDays.setBackgroundResource(R.drawable.highlight_days_inactive);
            holder.tvDays.setTextColor(Color.WHITE);
        }
        holder.tvDays.setSelected(sSelectedItems.get(position, false));

        //play and resume on specific day
//        if (sSelectedItems1.get(position)) {
//            holder.ivStatus.setImageResource(R.drawable.ic_play_arrow_black_24dp);
//            holder.tvStatus.setText(R.string.resume);
//        } else {
//            holder.ivStatus.setImageResource(R.drawable.ic_pause_black_24dp);
//            holder.tvStatus.setText(R.string.pause);
//        }
        holder.ivStatus.setSelected(sSelectedItems1.get(position, false));

//        // highlight the selected date
//        if (mPref.getSelectedDate() == daysOfTheWeekArrayList.get(position).getDays()) {
//            daysOfTheWeekArrayList.get(position).setHighlighted(true);
//
//            Log.d("position:", "if result " + mPref.getSelectedDate() + " position:" + position + " ishighlighted " + daysOfTheWeekArrayList.get(position).isHighlighted());
//
//        } else {
//            daysOfTheWeekArrayList.get(position).setHighlighted(false);
//
//            Log.d("position:", "else result position:" + position + " ishighlighted " + daysOfTheWeekArrayList.get(position).isHighlighted());
//        }

        //check if highlighted is true or not
        if (item.isHighlighted()) {
            Log.d(TAG, "onBindViewHolder: " + item.getDays());
            holder.tvDays.setBackgroundResource(R.drawable.highlight_days);
            holder.tvDays.setTextColor(Color.WHITE);

//            holder.ivStatus.setImageResource(R.drawable.ic_pause_black_24dp);
//            holder.tvStatus.setText(R.string.pause);
            if (item.getStatus().equals("new")) {
                holder.ivStatus.setImageResource(0);
                holder.tvStatus.setText("");
            } else if (item.getStatus().equals("pending")) {
                holder.ivStatus.setImageResource(R.drawable.ic_pause_black_24dp);
                holder.tvStatus.setText(R.string.pause);
                item.setPaused(true);
            } else if (item.getStatus().equals("pause")) {
                holder.ivStatus.setImageResource(R.drawable.ic_play_arrow_black_24dp);
                holder.tvStatus.setText(R.string.resume);
                item.setPaused(false);

            }

//            if (item.isPaused()) {
//                holder.ivStatus.setImageResource(R.drawable.ic_pause_black_24dp);
//                holder.tvStatus.setText(R.string.pause);
//                item.setPaused(false);
//            } else {
//                holder.ivStatus.setImageResource(R.drawable.ic_play_arrow_black_24dp);
//                holder.tvStatus.setText(R.string.resume);
//                item.setPaused(true);
//            }

            Log.d(TAG, "highlightedDate: highlighted");

        } else {
            holder.tvDays.setBackgroundResource(R.drawable.highlight_days_inactive);
            holder.tvDays.setTextColor(Color.WHITE);

            holder.ivStatus.setImageResource(0);
            holder.tvStatus.setText("");

            Log.d(TAG, "highlightedDate: not highlighted");
        }


        holder.tvDays.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sSelectedItems.get(position, false)) {
                    sSelectedItems.delete(position);

                } else {
                    sSelectedItems.put(position, true);
                }
                sClickListener.onItemClick(position);
            }
        });

        holder.ivStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (sSelectedItems1.get(position, false)) {
//                    sSelectedItems1.delete(position);
//                } else {
//                    sSelectedItems1.put(position, true);
//                }
                pClickListener.onPauseItemClick(position);
            }
        });
    }


    @Override
    public int getItemCount() {
        return daysOfTheWeekArrayList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tvDays, tvStatus;
        private ImageView ivStatus;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            tvDays = itemView.findViewById(R.id.tv_numeric_day);
            ivStatus = itemView.findViewById(R.id.iv_days_pause);
            tvStatus = itemView.findViewById(R.id.tv_status);
        }
    }

    public void selected(int position) {
        sPosition = position;
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(DayOfTheWeekClickListener clickListener) {
        sClickListener = clickListener;
    }

    public void setOnPauseClickListener(DayOfTheWeekPauseClickListener pauClickListener) {
        pClickListener = pauClickListener;
    }

    public interface DayOfTheWeekClickListener {
        void onItemClick(int position);
    }

    public interface DayOfTheWeekPauseClickListener {
        void onPauseItemClick(int position);
    }
}