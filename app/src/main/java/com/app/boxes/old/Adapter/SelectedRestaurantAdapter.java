package com.app.boxes.old.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.boxes.old.Model.MultiRestaurant;
import com.app.boxes.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class SelectedRestaurantAdapter extends RecyclerView.Adapter<SelectedRestaurantAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<MultiRestaurant> selectedRestaurants;
    LayoutInflater inflater;
    private SelectedRestaurantClickListener sClickListener;

    public SelectedRestaurantAdapter(Context context, ArrayList<MultiRestaurant> selectedRestaurants, SelectedRestaurantClickListener listener) {
        this.context = context;
        this.selectedRestaurants = selectedRestaurants;
        this.sClickListener = listener;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView = inflater.inflate(R.layout.list_selected_restaurant, parent, false);

        return new MyViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        final MultiRestaurant item = selectedRestaurants.get(position);

        Picasso.get().load(item.getImages()).fit().placeholder(R.drawable.boxes_logo).into(holder.ivImage);
        holder.tvRestaurantName.setText(item.getName());
        holder.tvRestaurantCategory.setText(item.getDescription());
        holder.ivDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sClickListener.onItemDelete(item, position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return selectedRestaurants.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView ivImage, ivDelete;
        private TextView tvRestaurantName, tvRestaurantCategory;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
//            ivImage = itemView.findViewById(R.id.iv_selected_restaurant_icon);
//            ivDelete = itemView.findViewById(R.id.iv_selected_restaurant_delete);
//            ivDeleteItem = itemView.findViewById(R.id.iv_delete_item);
//            tvRestaurantName = itemView.findViewById(R.id.tv_selected_restaurant_name);
//            tvRestaurantCategory = itemView.findViewById(R.id.tv_selected_restaurant_category);
        }
    }

    public interface SelectedRestaurantClickListener {
        void onItemDelete(MultiRestaurant multiRestaurant, int position);
    }
}
