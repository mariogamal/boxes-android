package com.app.boxes.old.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.app.boxes.old.API.API;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONObject;

public class PrivacyPolicyActivity extends AppCompatActivity {

    private String TAG = "PrivacyPolicy";
    private String pageName = "privacy_policy", languageId = "";


    private ImageView ivBack;
    private TextView tvDescription;
    private AVLoadingIndicatorView avl;
    private TextView tvStatus;

    private MySharedPreferences mPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_privacy_policy);

        ivBack = findViewById(R.id.iv_privacy_policy_back);
        tvDescription = findViewById(R.id.tv_privacy_policy_content);
        avl = findViewById(R.id.avi_privacy_policy);
        avl.setVisibility(View.GONE);

        tvStatus = findViewById(R.id.tv_privacy_policy_status);

        mPref = new MySharedPreferences(this);

        if (mPref.getLangObject() != null) {
            languageId = mPref.getLangObject().getId();
            Log.d(TAG, "language id " + mPref.getLangObject().getId());
        }

        mPref = new MySharedPreferences(this);
        if (mPref.getLocale().equals("ar"))
            ivBack.setImageResource(R.drawable.ic_arrow_forward_white);

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        //get privacy policy from server
        getPrivacyPolicy(pageName, languageId);
    }

    private void getPrivacyPolicy(String page_name, String language_id) {
        String url = API.getAPI() + "page?page_name=" + page_name + "&language_id=" + language_id;
        Log.d(TAG, "URL: " + url);

        avl.setVisibility(View.VISIBLE);

        RequestQueue requestQueue;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d(TAG, response.toString());
                avl.setVisibility(View.GONE);

                try {
                    int status = response.getInt("status");
                    String message = response.getString("message");

                    if (status == 1) {
                        tvStatus.setVisibility(View.GONE);
                        JSONArray dataArray = response.getJSONArray("data");

                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject jsonDataObject = dataArray.getJSONObject(i);

                            String pageDesc = jsonDataObject.getString("page_desc");

                            tvDescription.setText(Html.fromHtml(pageDesc));
                        }

                    } else {
                        tvStatus.setVisibility(View.VISIBLE);
                        Log.d(TAG, message);
                    }

                } catch (Exception ex) {
                    Log.d(TAG, "exception: " + ex.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                avl.setVisibility(View.GONE);
                Log.d(TAG, "onError: " + error.toString());
            }
        });

        requestQueue = Volley.newRequestQueue(this);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);

    }
}
