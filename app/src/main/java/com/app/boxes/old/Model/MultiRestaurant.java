package com.app.boxes.old.Model;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.Nullable;

public class MultiRestaurant implements Parcelable {

    String id, images, name, description;
    int checked, count;

    public int getChecked() {
        return checked;
    }

    public void setChecked(int checked) {
        this.checked = checked;
    }

    public MultiRestaurant() {
    }

    public MultiRestaurant(String id, String images, String name, String description) {
        this.id = id;
        this.images = images;
        this.name = name;
        this.description = description;
    }

    protected MultiRestaurant(Parcel in) {
        id = in.readString();
        images = in.readString();
        name = in.readString();
        description = in.readString();
        checked = in.readInt();
    }

    public static final Creator<MultiRestaurant> CREATOR = new Creator<MultiRestaurant>() {
        @Override
        public MultiRestaurant createFromParcel(Parcel in) {
            return new MultiRestaurant(in);
        }

        @Override
        public MultiRestaurant[] newArray(int size) {
            return new MultiRestaurant[size];
        }
    };

    public String getId() {
        return id;
    }

    public String getImages() {
        return images;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        if (this.count < 1) return;
        this.count = count;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(images);
        parcel.writeString(name);
        parcel.writeString(description);
        parcel.writeInt(checked);
    }

    @Override
    public boolean equals(@Nullable Object object) {
        boolean result = false;
        if (object == null || object.getClass() != getClass()) {
            result = false;
        } else {
            MultiRestaurant employee = (MultiRestaurant) object;
            if (this.id.equals(employee.getId()) && this.name.equals(employee.getName()) && this.images.equals( employee.getImages()) && this.description.equals( employee.getDescription()) ) {
                result = true;
            }
        }
        return result;
    }
}
