package com.app.boxes.old.Model;

public class OrderHistory {
    private String id, package_name, meals, price, meal_selection_case, validity, createdDate, subscription_id, listing_id, consumed_meals, out_of_meals, listing_name, deliveryDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPackageName() {
        return package_name;
    }

    public void setPackageName(String package_name) {
        this.package_name = package_name;
    }

    public String getMeals() {
        return meals;
    }

    public void setMeals(String meals) {
        this.meals = meals;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getMealSelectionCase() {
        return meal_selection_case;
    }

    public void setMealSelectionCase(String meal_selection_case) {
        this.meal_selection_case = meal_selection_case;
    }

    public String getValidity() {
        return validity;
    }

    public void setValidity(String validity) {
        this.validity = validity;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getSubscriptionId() {
        return subscription_id;
    }

    public void setSubscriptionId(String subscription_id) {
        this.subscription_id = subscription_id;
    }

    public String getListingId() {
        return listing_id;
    }

    public void setListingId(String listing_id) {
        this.listing_id = listing_id;
    }

    public String getConsumedMeals() {
        return consumed_meals;
    }

    public void setConsumedMeals(String consumed_meals) {
        this.consumed_meals = consumed_meals;
    }

    public String getOutOfMeals() {
        return out_of_meals;
    }

    public void setOutOfMeals(String out_of_meals) {
        this.out_of_meals = out_of_meals;
    }

    public String getListingName() {
        return listing_name;
    }

    public void setListingName(String listing_name) {
        this.listing_name = listing_name;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }
}
