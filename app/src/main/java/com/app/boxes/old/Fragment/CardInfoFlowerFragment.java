package com.app.boxes.old.Fragment;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.app.boxes.old.API.API;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class CardInfoFlowerFragment extends Fragment {

    private String TAG = "CardInfoFlower";
    private String name, cardNumber, expiryDate, cvc;

    private TextView tvProductNameAndPrice;
    private EditText edtName, edtCardNumber, edtExpiryDate, edtCvc;
    private Button btnApply;
    private ImageView ivBtnBack;
    private View snackbarView;
    private LinearLayout loaderLayout;

    private MySharedPreferences mPref;

    public CardInfoFlowerFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_card_info_flower, container, false);

        //initialize widgets
        Initializer(rootView);

        //check if selected language is arabic then change the back icon to forward icon
        MySharedPreferences sPref = new MySharedPreferences(getActivity());
        if (sPref.getLocale().equals("ar"))
            ivBtnBack.setImageResource(R.drawable.ic_arrow_forward_white);

        //navigate back to delivery detail fragment
        ivBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getFragmentManager() != null) {
                    getFragmentManager().popBackStack();
                }
            }
        });

        //navigate to thank you fragment
        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String token = mPref.getAccessTokenObject().getAccessToken();

                if (token != null) {

                    Log.d(TAG, "onClick: " + token);
                    saveCardInfo(token);
                }
            }
        });

        return rootView;
    }

    private Activity mContext;

    @Override
    public void onAttach(Context context) {
        this.mContext = (Activity) context;
        super.onAttach(context);
    }

    //initialize widgets
    private void Initializer(View rootView) {
        ivBtnBack = rootView.findViewById(R.id.iv_card_info_flower_back);

        tvProductNameAndPrice = rootView.findViewById(R.id.tv_product_name_and_price);

        edtName = rootView.findViewById(R.id.edt_card_info_flower_name);
        edtCardNumber = rootView.findViewById(R.id.edt_card_info_flower_card_number);
        edtExpiryDate = rootView.findViewById(R.id.edt_card_info_flower_month_year);
        edtCvc = rootView.findViewById(R.id.edt_card_info_flower_cvc);

        btnApply = rootView.findViewById(R.id.btn_card_info_flower_apply);

        loaderLayout = rootView.findViewById(R.id.ll_loader_card_info_flower);
        loaderLayout.setVisibility(View.GONE);

        snackbarView = mContext.findViewById(android.R.id.content);

        mPref = new MySharedPreferences(getActivity());

    }

    private void saveCardInfo(final String accessToken) {
        name = edtName.getText().toString();
        cardNumber = edtCardNumber.getText().toString();
        expiryDate = edtExpiryDate.getText().toString();
        cvc = edtCvc.getText().toString();

        if (name.isEmpty()) {
            Toast.makeText(getActivity(), "Please fill the required field", Toast.LENGTH_SHORT).show();
            edtName.requestFocus();
        } else if (cardNumber.isEmpty()) {
            Toast.makeText(getActivity(), "Please fill the required field", Toast.LENGTH_SHORT).show();
            edtCardNumber.requestFocus();
        } else if (expiryDate.isEmpty()) {
            Toast.makeText(getActivity(), "Please fill the required field", Toast.LENGTH_SHORT).show();
            edtExpiryDate.requestFocus();
        } else if (cvc.isEmpty()) {
            Toast.makeText(getActivity(), "Please fill the required field", Toast.LENGTH_SHORT).show();
            edtCvc.requestFocus();
        } else {

            Log.d(TAG, "saveCardInfo: " + name + " " + cardNumber + " " + expiryDate + " " + cvc);

            String url = API.getAPI() + "auth/store-card-info?card_holder_name=" + name + "&card_number=" + cardNumber + "&expiry_date=" + expiryDate + "&cvv=" + cvc;
            Log.d(TAG, "Card info URL: " + url);

            loaderLayout.setVisibility(View.VISIBLE);
            RequestQueue requestQueue;
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d(TAG, "Card info onResponse: " + response);
                    loaderLayout.setVisibility(View.GONE);

                    try {
                        int status = response.optInt("status");
                        String message = response.getString("message");

                        if (status == 1) {
                            //JSONObject data = jsonObject.getJSONObject("data");

                            if (getFragmentManager() != null) {
                                getFragmentManager().beginTransaction()
                                        .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                                        .replace(R.id.main_content, new ThankYouFragment())
                                        .commit();
                            }
                        } else {
                            Snackbar snackbar = Snackbar.make(snackbarView, message, Snackbar.LENGTH_SHORT);
                            View getSnackbarView = snackbar.getView();
                            getSnackbarView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
                            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                            tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));
                            snackbar.show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                        Log.d(TAG, "onException: " + e.getMessage());
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loaderLayout.setVisibility(View.GONE);
                    Log.d(TAG, "Volley Error: " + error.toString());
                    if (error instanceof NoConnectionError) {
                        Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.no_network), Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));
                        snackbar.show();
                    } else {
                        Snackbar snackbar = Snackbar.make(snackbarView, error.getMessage(), Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));
                        snackbar.show();
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    if (accessToken != null) {
                        params.put("Authorization", accessToken);
                        Log.d(TAG, "getHeaders " + accessToken);
                    }
                    return params;
                }
            };
            requestQueue = Volley.newRequestQueue(mContext);
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000,  DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(jsonObjectRequest);
        }

    }

}
