package com.app.boxes.old;

import com.app.boxes.old.Model.Category;

import java.util.ArrayList;

public class CategorySingleton {
    private static final CategorySingleton ourInstance = new CategorySingleton();
    public static CategorySingleton getInstance() {
        return ourInstance;
    }

    private ArrayList<Category> categoryArrayList = new ArrayList<>();
    public ArrayList<Category> categoryImagesList = new ArrayList<>();
    public int categoryPosition = -1;
    public String categoryId = null;
    public int selectedPosition = 0;
    public int isFood = 0;

    private CategorySingleton() {
    }

    public ArrayList<Category> getCategoryArrayList() {
        return categoryArrayList;
    }

    public void setCategoryArrayList(ArrayList<Category> categoryArrayList) {
        this.categoryArrayList = new ArrayList<>();
        for (int i = 0; i < categoryArrayList.size(); i++) {
            if (!this.categoryArrayList.contains(categoryArrayList.get(i)))
                this.categoryArrayList.add(categoryArrayList.get(i));
        }
    }
}
