package com.app.boxes.old.Adapter;

import android.content.Context;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.app.boxes.old.Model.MultiRestaurant;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

public class MultiRestaurantListAdapter extends RecyclerView.Adapter<MultiRestaurantListAdapter.MyViewHolder> {

    private String TAG = "MultiRestaurantListAdapter";
    Context context;

    ArrayList<MultiRestaurant> multiRestaurantArrayList;
    private ArrayList<MultiRestaurant> arraylist;

    private static int sPosition;
    private static SparseBooleanArray sSelectedItems;
    private static MultiRestaurantClickListener sClickListener;

    LayoutInflater inflater;

    int colorCount; // to store the number of colored linear layouts

    public MultiRestaurantListAdapter(Context context, ArrayList<MultiRestaurant> multiRestaurantArrayList) {
        this.context = context;
        this.multiRestaurantArrayList = multiRestaurantArrayList;
        sSelectedItems = new SparseBooleanArray();
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView = inflater.inflate(R.layout.list_multi_restaurant, null);

        return new MyViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        try {
            Picasso.get().load(multiRestaurantArrayList.get(position).getImages()).fit().placeholder(R.drawable.boxes_logo).into(holder.multiRestaurantimage);
            holder.tvRestaurantName.setText(multiRestaurantArrayList.get(position).getName());
            holder.tvRestaurantCategory.setText(multiRestaurantArrayList.get(position).getDescription());

            /*if (sSelectedItems.get(position)) {
                holder.meriid.setBackgroundColor(ContextCompat.getColor(context, R.color.active_tab_color));
             } else {
                holder.meriid.setBackgroundColor(ContextCompat.getColor(context, android.R.color.transparent));
            }*/

            if (multiRestaurantArrayList.get(position).getChecked() == 1) {
                //holder.meriid.setBackgroundColor(ContextCompat.getColor(context, R.color.active_tab_color));
                holder.meriid.setBackground(ContextCompat.getDrawable(context, R.drawable.custom_button_background));
            } else {
                holder.meriid.setBackgroundColor(ContextCompat.getColor(context, android.R.color.transparent));
            }

            holder.cardView.setSelected(sSelectedItems.get(position, false));

            holder.showMenu.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    sClickListener.onShowMenuClick(position);
                }
            });

            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (multiRestaurantArrayList.get(position).getChecked() == 1) {
                        sSelectedItems.delete(position);
                /*colorCount--;
                Log.d("COLOR", "IF Count: " + colorCount);
                sPref.setColorCount(colorCount);*/
                    } else {
                        sSelectedItems.put(position, true);
                /*colorCount++;
                Log.d("COLOR", "ELSE Count: " + colorCount);
                sPref.setColorCount(colorCount);*/
                    }
                    sClickListener.onItemClick(position, sSelectedItems.get(position, false), multiRestaurantArrayList.get(position));
                }
            });

            holder.plus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int count = multiRestaurantArrayList.get(position).getCount();
                    multiRestaurantArrayList.get(position).setCount(++count);
                    Toast.makeText(context, ""+count, Toast.LENGTH_SHORT).show();
                    notifyItemChanged(position);
                }
            });

            holder.minus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int count = multiRestaurantArrayList.get(position).getCount();
                    multiRestaurantArrayList.get(position).setCount(--count);
                    Toast.makeText(context, ""+count, Toast.LENGTH_SHORT).show();
                    notifyItemChanged(position);
                }
            });

            holder.count.setText(String.valueOf((multiRestaurantArrayList.get(position).getCount())));
        } catch (Exception ex) {
            Log.d(TAG, "onBindViewHolder: " + ex.getMessage());
        }
    }

    @Override
    public int getItemCount() {
        return multiRestaurantArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private MySharedPreferences sPref;
        private ImageView multiRestaurantimage, plus, minus;
        private LinearLayout showMenu;
        private CardView cardView;
        private TextView tvRestaurantName, tvRestaurantCategory, count;
        private LinearLayout meriid;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            multiRestaurantimage = itemView.findViewById(R.id.restaurantImage);
            showMenu = itemView.findViewById(R.id.ll_show_menu);
            cardView = itemView.findViewById(R.id.linear_layout);
            tvRestaurantName = itemView.findViewById(R.id.restaurantName);
            tvRestaurantCategory = itemView.findViewById(R.id.restaurantCategory);
            meriid = itemView.findViewById(R.id.merid);
            plus = itemView.findViewById(R.id.plus);
            minus = itemView.findViewById(R.id.minus);
            count = itemView.findViewById(R.id.count);

            sPref = new MySharedPreferences(context);
        }
    }

    public void selected(int position) {
        sPosition = position;
        notifyDataSetChanged();
    }

    public void setOnItemClickListener(MultiRestaurantClickListener clickListener) {
        sClickListener = clickListener;
    }

    public interface MultiRestaurantClickListener {
        void onItemClick(int position, boolean b, MultiRestaurant multiRestaurant);

        void onShowMenuClick(int position);
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        multiRestaurantArrayList.clear();
        if (charText.length() == 0) {
            multiRestaurantArrayList.addAll(arraylist);
        } else {
            if (arraylist != null)
                for (MultiRestaurant wp : arraylist) {
                    if (wp.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                        multiRestaurantArrayList.add(wp);
                    }
                }
        }
        notifyDataSetChanged();
    }
}