package com.app.boxes.old.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.boxes.old.Model.MenuItem;
import com.app.boxes.R;

import java.util.ArrayList;

public class ProductItemAdapter extends RecyclerView.Adapter<ProductItemAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<MenuItem> menusItemArrayList = new ArrayList<>();
//    private ArrayList<Menu> menusArrayList = new ArrayList<>();
    private LayoutInflater inflater;

//    public ProductItemAdapter(Context context, ArrayList<Menu> menusArrayList, LayoutInflater inflater) {
////        this.context = context;
////        this.menusArrayList = menusArrayList;
////        this.inflater = inflater;
////    }

    public ProductItemAdapter(Context context, ArrayList<MenuItem> menusItemArrayList) {
        this.context = context;
        this.menusItemArrayList = menusItemArrayList;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView = inflater.inflate(R.layout.menu_item_list, parent, false);

        return new MyViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        try {
            holder.menuName.setText(menusItemArrayList.get(position).getProductName());
            holder.menuDesc.setText(menusItemArrayList.get(position).getDecripton());
            holder.menuCalories.setText(menusItemArrayList.get(position).getCalories());
            holder.menuCarbs.setText(menusItemArrayList.get(position).getCarbohydrates());
            holder.menuProtein.setText(menusItemArrayList.get(position).getProteins());
        } catch (Exception ex) {
            Log.d("exception", ex.toString());
        }
    }


    @Override
    public int getItemCount() {
        return menusItemArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView menuName, menuDesc, menuCalories, menuCarbs, menuProtein;


        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            menuName = itemView.findViewById(R.id.menu_name);
            menuDesc = itemView.findViewById(R.id.menu_desc);
//            final ImageView menuChecked = findViewById(R.id.iv_menu_list_checked);
            menuCalories = itemView.findViewById(R.id.menu_calories);
            menuCarbs = itemView.findViewById(R.id.menu_carbs);
            menuProtein = itemView.findViewById(R.id.menu_protein);
        }
    }
}
