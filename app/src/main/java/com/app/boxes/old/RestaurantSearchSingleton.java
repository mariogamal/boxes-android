package com.app.boxes.old;

import com.app.boxes.old.Model.MultiRestaurant;

import java.util.ArrayList;

public class RestaurantSearchSingleton {

    private static RestaurantSearchSingleton ourInstance = new RestaurantSearchSingleton();

    public static RestaurantSearchSingleton getInstance() {
        return ourInstance;
    }

    private ArrayList<String> selectedRestaurantsIds = new ArrayList<>();
    public ArrayList<MultiRestaurant> selectedRestaurants = new ArrayList<>();

    private RestaurantSearchSingleton() {
    }

    public String query = "";

    public int selectionCount = 0;

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public ArrayList<String> getSelectedRestaurantsList() {
        return selectedRestaurantsIds;
    }

    public void setSelectedRestaurantsList(ArrayList<String> arrayList) {
        selectedRestaurantsIds = new ArrayList<>();

        for (int i = 0; i < arrayList.size(); i++) {
            if (!selectedRestaurantsIds.contains(arrayList.get(i)))
                selectedRestaurantsIds.add(arrayList.get(i));
        }
        //selectedRestaurantsIds = arrayList;
    }

    public ArrayList<MultiRestaurant> getSelectedRestaurants() {
        return selectedRestaurants;
    }

    public void setSelectedRestaurants(ArrayList<MultiRestaurant> arrayList) {
        selectedRestaurants.clear();
        //  if (!selectedRestaurants.contains(arrayList.get(i))) {
        //  }
        selectedRestaurants.addAll(arrayList);
    }

    public void reinit() {
        ourInstance = new RestaurantSearchSingleton();
        selectedRestaurantsIds = new ArrayList<>();
    }
}
