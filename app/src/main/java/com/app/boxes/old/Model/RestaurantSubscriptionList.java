package com.app.boxes.old.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class RestaurantSubscriptionList implements Parcelable {

    String id, subscriptionPlan, subcriptionPrice, sunscriptionMeals, subscriptionTypeId, subcriptionStatus;

    public RestaurantSubscriptionList() {
    }

    protected RestaurantSubscriptionList(Parcel in) {
        id = in.readString();
        subscriptionPlan = in.readString();
        subcriptionPrice = in.readString();
        sunscriptionMeals = in.readString();
        subscriptionTypeId = in.readString();
        subcriptionStatus = in.readString();
    }

    public static final Creator<RestaurantSubscriptionList> CREATOR = new Creator<RestaurantSubscriptionList>() {
        @Override
        public RestaurantSubscriptionList createFromParcel(Parcel in) {
            return new RestaurantSubscriptionList(in);
        }

        @Override
        public RestaurantSubscriptionList[] newArray(int size) {
            return new RestaurantSubscriptionList[size];
        }
    };

    public void setId(String id) {
        this.id = id;
    }

    public void setSubscriptionPlan(String subscriptionPlan) {
        this.subscriptionPlan = subscriptionPlan;
    }

    public void setSubcriptionPrice(String subcriptionPrice) {
        this.subcriptionPrice = subcriptionPrice;
    }

    public void setSunscriptionMeals(String sunscriptionMeals) {
        this.sunscriptionMeals = sunscriptionMeals;
    }

    public void setSubscriptionTypeId(String subscriptionTypeId) {
        this.subscriptionTypeId = subscriptionTypeId;
    }

    public void setSubcriptionStatus(String subcriptionStatus) {
        this.subcriptionStatus = subcriptionStatus;
    }

    public String getId() {
        return id;
    }

    public String getSubscriptionPlan() {
        return subscriptionPlan;
    }

    public String getSubcriptionPrice() {
        return subcriptionPrice;
    }

    public String getSunscriptionMeals() {
        return sunscriptionMeals;
    }

    public String getSubscriptionTypeId() {
        return subscriptionTypeId;
    }

    public String getSubcriptionStatus() {
        return subcriptionStatus;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(subscriptionPlan);
        parcel.writeString(subcriptionPrice);
        parcel.writeString(sunscriptionMeals);
        parcel.writeString(subscriptionTypeId);
        parcel.writeString(subcriptionStatus);
    }
}
