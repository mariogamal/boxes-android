package com.app.boxes.old.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.boxes.old.Model.SingleRestaurant;
import com.app.boxes.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class SubscribedRestaurantListAdapter extends RecyclerView.Adapter<SubscribedRestaurantListAdapter.SubsViewHolder>{

    private ArrayList<SingleRestaurant> singleRestaurantArrayList;
    private LayoutInflater inflater;

    public SubscribedRestaurantListAdapter(Context context, ArrayList<SingleRestaurant> singleRestaurantArrayList) {
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.singleRestaurantArrayList = singleRestaurantArrayList;
    }

    @NonNull
    @Override
    public SubsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView = inflater.inflate(R.layout.list_subscribed_restaurant_item, parent, false);

        return new SubsViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull SubsViewHolder holder, int position) {
        SingleRestaurant item = singleRestaurantArrayList.get(position);
        Picasso.get().load(item.getImages()).placeholder(R.drawable.boxes_logo).into(holder.ivImage);
        holder.tvRestaurantName.setText(item.getName());
        holder.tvRestaurantCategory.setText(item.getCategory());
    }

    @Override
    public int getItemCount() {
        return singleRestaurantArrayList.size();
    }

    public class SubsViewHolder extends RecyclerView.ViewHolder {

        private ImageView ivImage;
        private TextView tvRestaurantName, tvRestaurantCategory;

        public SubsViewHolder(@NonNull View itemView) {
            super(itemView);
            ivImage = itemView.findViewById(R.id.iv_subscribed_restaurant_icon);
            tvRestaurantName = itemView.findViewById(R.id.tv_subscribed_restaurant_name);
            tvRestaurantCategory = itemView.findViewById(R.id.tv_subscribed_restaurant_category);
        }
    }
}
