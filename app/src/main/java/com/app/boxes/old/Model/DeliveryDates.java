package com.app.boxes.old.Model;

import androidx.annotation.Nullable;

import java.io.Serializable;

public class DeliveryDates implements Serializable {
    private String resId, orderDetailId, mealOrderDetailId, deliveryDate, deliveryStatus;
    private int deliveryDay, deliveryMonth, deliveryYear;

    public String getResId() {
        return resId;
    }

    public void setResId(String resId) {
        this.resId = resId;
    }

    public String getOrderDetailId() {
        return orderDetailId;
    }

    public void setOrderDetailId(String orderDetailId) {
        this.orderDetailId = orderDetailId;
    }

    public String getMealOrderDetailId() {
        return mealOrderDetailId;
    }

    public void setMealOrderDetailId(String mealOrderDetailId) {
        this.mealOrderDetailId = mealOrderDetailId;
    }

    public String getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public String getDeliveryStatus() {
        return deliveryStatus;
    }

    public void setDeliveryStatus(String deliveryStatus) {
        this.deliveryStatus = deliveryStatus;
    }

    public int getDeliveryDay() {
        return deliveryDay;
    }

    public void setDeliveryDay(int deliveryDay) {
        this.deliveryDay = deliveryDay;
    }

    public int getDeliveryMonth() {
        return deliveryMonth;
    }

    public void setDeliveryMonth(int deliveryMonth) {
        this.deliveryMonth = deliveryMonth;
    }

    public int getDeliveryYear() {
        return deliveryYear;
    }

    public void setDeliveryYear(int deliveryYear) {
        this.deliveryYear = deliveryYear;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        boolean result = false;
        if (obj == null || obj.getClass() != getClass()) {
            result = false;
        } else {
            DeliveryDates employee = (DeliveryDates) obj;
            if (this.orderDetailId.equals(employee.getOrderDetailId()) && this.deliveryDate.equals(employee.getDeliveryDate())) {
                result = true;
            }
        }
        return result;
    }
}
