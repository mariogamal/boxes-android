package com.app.boxes.old.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class Product implements Parcelable {

    private String id, listId, listCatName;

    public Product(String id, String listId, String listCatName) {
        this.id = id;
        this.listId = listId;
        this.listCatName = listCatName;
    }

    protected Product(Parcel in) {
        id = in.readString();
        listId = in.readString();
        listCatName = in.readString();
    }

    public static final Creator<Product> CREATOR = new Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    public String getId() {
        return id;
    }

    public String getListId() {
        return listId;
    }

    public String getListCatName() {
        return listCatName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(listId);
        parcel.writeString(listCatName);
    }
}