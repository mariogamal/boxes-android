package com.app.boxes.old.Fragment;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.app.boxes.old.Activity.ChangeNumberActivity;
import com.app.boxes.old.Activity.PrivacyPolicyActivity;
import com.app.boxes.old.Activity.SelectLanguageActivity;
import com.app.boxes.old.Activity.SurveyFormActivity;
import com.app.boxes.old.Activity.TermsAndConditionActivity;
import com.app.boxes.old.Adapter.SettingsAdapter;
import com.app.boxes.BuildConfig;
import com.app.boxes.old.Listener.RecyclerItemClickListener;
import com.app.boxes.old.Model.User;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
//@Puppet
public class SettingsFragment extends Fragment {

    private RecyclerView settingsRecyclerView;

    private SettingsAdapter settingsAdapter;

    private ArrayList<String> settingsArrayList = new ArrayList<>();

    private boolean isItemClicked = false;

    private MySharedPreferences sPref;

    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_settings, container, false);

        // intialize widgets
        settingsRecyclerView = rootView.findViewById(R.id.rv_settings_list);

        sPref = new MySharedPreferences(mContext);

        // set the layout manager for recyclerview
        settingsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        // add the data in arraylist
        User user = sPref.getUserObject();
        if (user != null) {
            settingsArrayList.add(getString(R.string.terms_and_conditions));
            settingsArrayList.add(getString(R.string.privacy_policy));
            settingsArrayList.add(getString(R.string.survey));
            settingsArrayList.add(getString(R.string.change_number));
            settingsArrayList.add(getString(R.string.change_language));
            settingsArrayList.add(getString(R.string.app_version));

            // call the adapter and set the arraylist in adapter
            settingsAdapter = new SettingsAdapter(mContext, settingsArrayList);
            // set adapter in recyclerview
            settingsRecyclerView.setAdapter(settingsAdapter);
            // check which item category is selected then it navigate to those fragment or screen
            settingsRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), settingsRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    Log.d("isItemClick1", isItemClicked + "");
                    if (!isItemClicked) {
                        switch (position) {

                            case 0:
                                // terms and conditions
                                startActivity(new Intent(mContext, TermsAndConditionActivity.class));
                                break;

                            case 1:
                                // terms and conditions
                                startActivity(new Intent(mContext, PrivacyPolicyActivity.class));
                                break;

                            case 2:
                                // survey form
                                Intent sIntent = new Intent(mContext, SurveyFormActivity.class);
                                sIntent.putExtra("from", "settings");
                                startActivity(sIntent);
                                break;

                            case 3:
                                // change number
                                startActivity(new Intent(mContext, ChangeNumberActivity.class));
                                break;

                            case 4:
                                // change language
                                startActivity(new Intent(mContext, SelectLanguageActivity.class));
                                break;

                            case 5:
                                // show version dialog
                                appVersionDialog(mContext);
                                break;

                            default:
                                Toast.makeText(getContext(), R.string.toast_message_category, Toast.LENGTH_SHORT).show();
                        }

                        isItemClicked = true;
                        Log.d("isItemClick2", isItemClicked + "");

                        //timer to make isItemClicked = false
                        new CountDownTimer(1000, 1000) {
                            @Override
                            public void onTick(long l) {

                            }

                            @Override
                            public void onFinish() {
                                isItemClicked = false;
                                Log.d("isItemClick3", isItemClicked + "");
                            }
                        }.start();
                    }
                }

                @Override
                public void onLongItemClick(View view, int position) {

                }
            }));
        }
        else {
            settingsArrayList.add(getString(R.string.terms_and_conditions));
            settingsArrayList.add(getString(R.string.privacy_policy));
            settingsArrayList.add(getString(R.string.change_language));
            settingsArrayList.add(getString(R.string.app_version));

            // call the adapter and set the arraylist in adapter
            settingsAdapter = new SettingsAdapter(mContext, settingsArrayList);
            // set adapter in recyclerview
            settingsRecyclerView.setAdapter(settingsAdapter);
            // check which item category is selected then it navigate to those fragment or screen
            settingsRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), settingsRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    Log.d("isItemClick1", isItemClicked + "");
                    if (!isItemClicked) {
                        switch (position) {

                            case 0:
                                // terms and conditions
                                startActivity(new Intent(mContext, TermsAndConditionActivity.class));
                                break;

                            case 1:
                                // terms and conditions
                                startActivity(new Intent(mContext, PrivacyPolicyActivity.class));
                                break;

                            case 2:
                                // change language
                                startActivity(new Intent(mContext, SelectLanguageActivity.class));
                                break;

                            case 3:
                                // show version dialog
                                appVersionDialog(mContext);
                                break;

                            default:
                                Toast.makeText(getContext(), R.string.toast_message_category, Toast.LENGTH_SHORT).show();
                        }

                        isItemClicked = true;
                        Log.d("isItemClick2", isItemClicked + "");

                        //timer to make isItemClicked = false
                        new CountDownTimer(1000, 1000) {
                            @Override
                            public void onTick(long l) {

                            }

                            @Override
                            public void onFinish() {
                                isItemClicked = false;
                                Log.d("isItemClick3", isItemClicked + "");
                            }
                        }.start();
                    }
                }

                @Override
                public void onLongItemClick(View view, int position) {

                }
            }));
        }

        /*rootView.setFocusableInTouchMode(true);
        rootView.requestFocus();
        rootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_BACK) {

                    if (getFragmentManager() != null) {
                        getFragmentManager().popBackStack();
                    }
                    return true;
                }
                return false;
            }
        });*/

        return rootView;
    }

    private Activity mContext;

    @Override
    public void onAttach(Context context) {
        this.mContext = (Activity) context;
        super.onAttach(context);
    }

    private void appVersionDialog(Context context) {
        TextView tvTitle, tvDescription, tvVersion;
        ImageView ivIcon;
        Button btnClose;

        View view = getLayoutInflater().inflate(R.layout.app_version_dialog, null);

        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();

        //initialize the app version layout widgets id
        tvTitle = view.findViewById(R.id.tv_app_version_title);
        tvDescription = view.findViewById(R.id.tv_app_version_description);
        tvVersion = view.findViewById(R.id.tv_app_version);

        ivIcon = view.findViewById(R.id.iv_app_version_icon);

        btnClose = view.findViewById(R.id.btn_app_version_close);

//        Typeface typePtRegular =

        //set the data in widgets
        tvTitle.setText(R.string.app_version);
        tvDescription.setText(R.string.app_version_text);

        //set app version
        String version = BuildConfig.VERSION_NAME;
        tvVersion.setText(version);

        ivIcon.setImageResource(R.drawable.boxes_logo);

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        //set the view in alert dialog
        alertDialog.setView(view);

        alertDialog.show();
    }

}
