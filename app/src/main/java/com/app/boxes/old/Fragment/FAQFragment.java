package com.app.boxes.old.Fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.app.boxes.old.API.API;
import com.app.boxes.old.Adapter.ExpandableAdapter;
import com.app.boxes.old.Model.Faq;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.wang.avi.AVLoadingIndicatorView;


import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

//@Puppet
public class FAQFragment extends Fragment {

    private static String TAG = "FaqFragment";
    private String pageName = "faq", languageId = "";

    private RecyclerView recyclerView;
    private ExpandableAdapter expandableAdapter;
    private AVLoadingIndicatorView avl;
    private MySharedPreferences mPref;
//    private TextView tvStatus;

    private ArrayList<Faq> faqArrayList = new ArrayList<>();

    public FAQFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        //change the status bar color
        // getActivity().getWindow().setStatusBarColor(getResources().getColor(R.color.orangeColor));

        // Inflate the layout for this fragment
        View parentView = inflater.inflate(R.layout.fragment_faq, container, false);

        recyclerView = parentView.findViewById(R.id.recycler_view_faq);
        avl = parentView.findViewById(R.id.avi_faq);
        avl.setVisibility(View.GONE);

        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));

     //   tvStatus = parentView.findViewById(R.id.tv_faq_status);

        mPref = new MySharedPreferences(mContext);

        if (mPref != null) {
            languageId = mPref.getLangObject().getId();
            Log.d(TAG, "language id " + mPref.getLangObject().getId());
        }

        //get faq from server
        getFAQS(pageName, languageId);

        /*parentView.setFocusableInTouchMode(true);
        parentView.requestFocus();
        parentView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_BACK) {

                    if (getFragmentManager() != null) {
                        getFragmentManager()
                                .popBackStack();
                    }
                    return true;
                }
                return false;
            }
        });*/

        return parentView;
    }

    private Activity mContext;

    @Override
    public void onAttach(Context context) {
        this.mContext = (Activity) context;
        super.onAttach(context);
    }

    private void getFAQS(String page_name, String language_id) {
        String url = API.getAPI() + "page?page_name=" + page_name + "&language_id=" + language_id;
        Log.d(TAG, "URL: " + url);

        avl.setVisibility(View.VISIBLE);

        RequestQueue requestQueue;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d(TAG, response.toString());
                avl.setVisibility(View.GONE);

                try {
                    int status = response.getInt("status");
                    String message = response.getString("message");

                    if (status == 1) {
//                        tvStatus.setVisibility(View.GONE);
                        JSONArray dataArray = response.getJSONArray("data");

                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject jsonDataObject = dataArray.getJSONObject(i);

                            String questionDescription = jsonDataObject.getString("question_desc");
                            String answerDescription = jsonDataObject.getString("answer_desc");

                            Faq faq = new Faq(questionDescription, answerDescription);

                            faqArrayList.add(faq);

                            expandableAdapter = new ExpandableAdapter(mContext, faqArrayList);
                            recyclerView.setAdapter(expandableAdapter);
                        }
                    } else {
//                        tvStatus.setVisibility(View.VISIBLE);
                        Log.d(TAG, message);

                    }

                } catch (
                        Exception ex) {
                    Log.d(TAG, "exception: " + ex.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                avl.setVisibility(View.GONE);
                Log.d(TAG, "onError: " + error.toString());
            }
        });

        requestQueue = Volley.newRequestQueue(mContext);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);

    }
}
