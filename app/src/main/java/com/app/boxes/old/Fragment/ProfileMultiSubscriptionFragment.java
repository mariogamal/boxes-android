package com.app.boxes.old.Fragment;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.app.boxes.old.API.API;
import com.app.boxes.old.Activity.MainActivity;
import com.app.boxes.R;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileMultiSubscriptionFragment extends Fragment {

    private String TAG = "ProfileMultiSubscription";
    private String getPackageId = "";

    private Button btnChooseMeal;
    private View snackbarView;

    public ProfileMultiSubscriptionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_profile_multi_subscription, container, false);

        snackbarView = mContext.findViewById(android.R.id.content);

        ((MainActivity) getActivity()).getSupportActionBar().show();

        btnChooseMeal = rootView.findViewById(R.id.btn_multi_chose_your_meal);

        btnChooseMeal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                        .replace(R.id.main_content, new CalenderFragment())
                        .commit();
            }
        });

        return rootView;
    }

    private Activity mContext;

    @Override
    public void onAttach(Context context) {
        this.mContext = (Activity) context;
        super.onAttach(context);
    }

    //get current package
    private void getCurrentPackage(String languageId, final String accessToken) {
        String url = API.getAPI() + "auth/my-current-package?language_id=" + languageId;

        RequestQueue requestQueue;
        final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "onResponse: " + response.toString());

                try {
                    int status = response.getInt("status");
                    String message = response.getString("message");


                    if (status == 1) {

                        JSONObject jsonObject = response.getJSONObject("data");
                        getPackageId = jsonObject.getString("package_id");

                        Log.d(TAG, "onCurrentPackageMethod: " + getPackageId);

                    } else {
                        Snackbar snackbar = Snackbar.make(snackbarView, message, Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                        snackbar.show();
                    }
                } catch (Exception ex) {
                    Log.d(TAG, "onException: " + ex.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                View getSnackbarView = snackbar.getView();
                getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                snackbar.show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (accessToken != null) {
                    params.put("Authorization", accessToken);
                    Log.d(TAG, "getHeaders " + accessToken);
                }
                return params;
            }
        };

        requestQueue = Volley.newRequestQueue(mContext);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);

    }


}
