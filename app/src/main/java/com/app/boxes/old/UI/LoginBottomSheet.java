package com.app.boxes.old.UI;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.app.boxes.old.Activity.MainActivity;
import com.app.boxes.old.Activity.PrivacyPolicyActivity;
import com.app.boxes.old.Activity.SignInActivity;
import com.app.boxes.old.Activity.TermsAndConditionActivity;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

public class LoginBottomSheet extends BottomSheetDialogFragment {

    private TextView tvTermsText;

    private MySharedPreferences sPref;

    @Nullable
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        sPref = new MySharedPreferences(getActivity());

        View view = inflater.inflate(R.layout.login_bottom_sheet, container, false);

        ImageView btnClose = view.findViewById(R.id.iv_login_bottom_sheet_close);
//        TextView tvLogin = view.findViewById(R.id.tv_test_login_bottom);

        Button btnSignIn = view.findViewById(R.id.btn_login_bottom_sheet_signin);
//
//        Bundle bundle = getArguments();
//        assert bundle != null;
//        String title = bundle.getString("bottom_title");
//
//        tvLogin.setText("Title is: " + title);

        tvTermsText = view.findViewById(R.id.tv_login_bottom_tnc);

        if (sPref.getUserObject() != null)
            dismiss();

        btnSignIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), SignInActivity.class));
//                getActivity().finish();
            }
        });

        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                startActivity(new Intent(getActivity(), MainActivity.class));
                getActivity().finish();
                getActivity().finishAffinity();
            }
        });

        changeSubStringColor();

        return view;
    }

    // changing the color of specific text in textview
    private void changeSubStringColor() {
        String text1 = "By signing up, you confirm that you agree\n to our Terms of Use and have read and\n understood our Privacy Policy\n";

        //get text which we want to change color of specific substring
        SpannableString spannableString1 = new SpannableString(text1);
        //================================================================

        //get color for text
        ForegroundColorSpan frgred1 = new ForegroundColorSpan(Color.parseColor("#D1262F"));
        ForegroundColorSpan frgred2 = new ForegroundColorSpan(Color.parseColor("#D1262F"));
        //====================================================================

        //set color in specific string length
        spannableString1.setSpan(frgred1, 50, 63, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString1.setSpan(frgred2, 97, 112, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        //====================================================================

        //Terms and condition in textview is clickable
        ClickableSpan termAndConditionClick = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View view) {
                Log.d("termandcondition", "clicked");
                startActivity(new Intent(getActivity(), TermsAndConditionActivity.class));
            }

            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);

                ds.setColor(getActivity().getResources().getColor(R.color.active_tab_color));
                ds.setUnderlineText(false);
            }
        };

        //login in textview is clickable
        ClickableSpan privacyAndPolicyClick = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View view) {
                startActivity(new Intent(getActivity(), PrivacyPolicyActivity.class));
            }

            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);

                ds.setColor(getResources().getColor(R.color.active_tab_color));
                ds.setUnderlineText(false);
            }
        };

        //set color in specific string length
        spannableString1.setSpan(termAndConditionClick, 50, 63, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString1.setSpan(privacyAndPolicyClick, 97, 112, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        //====================================================================

        //set spannable text
        tvTermsText.setText(spannableString1);
        tvTermsText.setMovementMethod(LinkMovementMethod.getInstance());
        //=============================================================
    }
}