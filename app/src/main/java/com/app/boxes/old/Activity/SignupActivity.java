package com.app.boxes.old.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.boxes.old.API.API;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.mukesh.countrypicker.CountryPicker;
import com.mukesh.countrypicker.CountryPickerListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;

public class SignupActivity extends AppCompatActivity {

    private static final String TAG = "SignupActivity";

    private TextView tvSkip, tvTermsAndConditions, tvPhoneCode;
    private EditText edtFirstName, edtLastName, edtEmail, edtPhone, edtPass, edtConfirmPass;
    private Button btnCreateAccount;
    private ImageView ivCountryFlag;
    private CountryPicker picker;

    private MySharedPreferences mPref;

    private String countryCode, firstName, lastName, email, phone, pass, confirmPass, mFirebaseToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //initialize mysharedpreferences object
        mPref = new MySharedPreferences(this);
        mPref.getLocale();
        Log.d(TAG, "Default Language is: " + mPref.getLocale());

        setContentView(R.layout.activity_signup);

        //Initialize widgets
        Initializer();

        //get the firebase token id
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        if (task.getResult() != null) {
                            // Get new Instance ID token
                            mFirebaseToken = task.getResult().getToken();
                            Log.d(TAG, "Firebase-Token: " + mFirebaseToken);
                        }

                        // Log and toast
                        Log.d(TAG, "Firebase-Message Token: " + mFirebaseToken);
                    }
                });

        //set the text on textview
        tvTermsAndConditions.setText(R.string.terms_and_conditions);

        //set the underline on textview
        tvTermsAndConditions.setPaintFlags(Paint.UNDERLINE_TEXT_FLAG);

        //navigate to terms and condition
        tvTermsAndConditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SignupActivity.this, TermsAndConditionActivity.class));
                finish();
            }
        });

        //navigate to main activity
        tvSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SignupActivity.this, MainActivity.class));
                finish();
            }
        });

        //set the custom adapter in spinner
        picker = CountryPicker.newInstance("Select Country");  // dialog title
        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode, int flagDrawableResID) {
                tvPhoneCode.setText(dialCode);
                countryCode = dialCode;
                ivCountryFlag.setImageResource(flagDrawableResID);
                picker.dismiss();
            }
        });

        tvPhoneCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                picker.show(getSupportFragmentManager(), "Select Country");
            }
        });

        ivCountryFlag.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                picker.show(getSupportFragmentManager(), "Select Country");
            }
        });

        //navigate to survey form activity
        btnCreateAccount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RegisterUser(mFirebaseToken);
            }
        });

    }

    private void Initializer() {
        //initialize widgets
        tvSkip = findViewById(R.id.tv_signup_fragment_skip);
        tvTermsAndConditions = findViewById(R.id.tv_signup_fragment_terms);
        tvPhoneCode = findViewById(R.id.tv_signup_phone_code);

        edtFirstName = findViewById(R.id.edt_signup_first_name);
        edtLastName = findViewById(R.id.edt_signup_last_name);
        edtEmail = findViewById(R.id.edt_signup_email);
        edtPhone = findViewById(R.id.edt_signup_phone);
        edtPass = findViewById(R.id.edt_signup_password);
        edtConfirmPass = findViewById(R.id.edt_signup_confirm_password);

        ivCountryFlag = findViewById(R.id.iv_signup_drop_down_contact);

        btnCreateAccount = findViewById(R.id.btn_signup_create_account);

    }

    private void RegisterUser(final String token) {
        firstName = edtFirstName.getText().toString();
        lastName = edtLastName.getText().toString();
        email = edtEmail.getText().toString();
        phone = edtPhone.getText().toString();
        pass = edtPass.getText().toString();
        confirmPass = edtConfirmPass.getText().toString();

        if (firstName.isEmpty()) {
            Toast.makeText(this, "Please fill the required field", Toast.LENGTH_SHORT).show();
            edtFirstName.requestFocus();
        } else if (lastName.isEmpty()) {
            Toast.makeText(this, "Please fill the required field", Toast.LENGTH_SHORT).show();
            edtLastName.requestFocus();
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            Toast.makeText(this, "Please enter the valid email address", Toast.LENGTH_SHORT).show();
            edtEmail.requestFocus();
        } else if (countryCode.isEmpty()) {
            Toast.makeText(this, "No country code is selected", Toast.LENGTH_SHORT).show();
        } else if (phone.isEmpty()) {
            Toast.makeText(this, "Please fill the required field", Toast.LENGTH_SHORT).show();
            edtPhone.requestFocus();
        } else if (pass.isEmpty()) {
            Toast.makeText(this, "Please fill the required field", Toast.LENGTH_SHORT).show();
            edtPass.requestFocus();
        } else if (pass.length() <= 8 && pass.length() >= 16) {
            Toast.makeText(this, "Password length should be between 8 and 16", Toast.LENGTH_SHORT).show();
            edtPass.requestFocus();
        } else if (confirmPass.isEmpty()) {
            Toast.makeText(this, "Please fill the required field", Toast.LENGTH_SHORT).show();
            edtConfirmPass.requestFocus();
        } else if (confirmPass.length() <= 8 && confirmPass.length() >= 16) {
            Toast.makeText(this, "Password length should be between 8 and 16", Toast.LENGTH_SHORT).show();
            edtConfirmPass.requestFocus();
        } else if (!pass.equals(confirmPass)) {
            Toast.makeText(this, "Password did not match", Toast.LENGTH_SHORT).show();
            edtConfirmPass.requestFocus();
        } else {


            final String phoneNumber = countryCode + phone;
            final String device_type = "android";
            Log.d("SignUpInfo", " " + firstName + " " + lastName + " " + email + " " + countryCode + phone + " " + pass + " " + confirmPass + " " + device_type + " " + token);

//            String url = API.getAPI() + "sign-up?first_name=" + firstName + "&last_name=" + lastName + "&email=" + email + "&password=" + pass + "&phone=" + phoneNumber + "&device_type=" + device_type + "&device_token=" + mFirebaseToken;
            String url = API.getAPI() + "sign-up";
            Log.d(" SignUp ", " URL:" + url);

            RequestQueue requestQueue;
            StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.d("SignUp-response", response.toString());

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        String msg = jsonObject.getString("message");
                        int status = jsonObject.getInt("status");

                        if (status == 1) {
                            Toast.makeText(SignupActivity.this, "" + msg, Toast.LENGTH_SHORT).show();

                            startActivity(new Intent(SignupActivity.this, MainActivity.class));
                            finish();
//                            Animatoo.animateSlideLeft(SignupActivity.this);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("SignUp-Error", "VolleyError: " + error.toString());
                    if (error instanceof NoConnectionError)
                        Toast.makeText(SignupActivity.this, getString(R.string.no_network), Toast.LENGTH_SHORT).show();
//                    else {
//                        parseVolleyError(error);
//                    }
                }
            }) {
//                protected Map<String, String> getParams() {
//                    Map<String, String> params = new HashMap<>();
//                    params.put("first_name", firstName);
//                    params.put("last_name", lastName);
//                    params.put("email", email);
//                    params.put("password", pass);
//                    params.put("phone", phoneNumber);
//                    params.put("device_type", device_type);
//                    params.put("device_token", token);
//                    return params;
//                }

                @Override
                public byte[] getBody() {
                    HashMap<String, String> params = new HashMap<String, String>();
                    params.put("first_name", firstName);
                    params.put("last_name", lastName);
                    params.put("email", email);
                    params.put("password", pass);
                    params.put("phone", phoneNumber);
                    params.put("device_type", device_type);
                    params.put("device_token", token);
                    return new JSONObject(params).toString().getBytes();
                }


                @Override
                public String getBodyContentType() {
                    return "application/json";
                }
            };
            requestQueue = Volley.newRequestQueue(this);
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000,  DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(stringRequest);
        }
    }

    public void parseVolleyError(VolleyError error) {
        try {
            String responseBody = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
                responseBody = new String(error.networkResponse.data, StandardCharsets.UTF_8);
            }
            JSONObject data = new JSONObject(responseBody);
            String message = data.getString("message");
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            Log.d(TAG, "Signup, Volley Error Msg: " + message);
        } catch (JSONException e) {
            Log.d(TAG, "Signup, Volley Error: " + e);
        }
    }
}
