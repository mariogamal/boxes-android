package com.app.boxes.old.Fragment;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.app.boxes.old.Activity.MainActivity;
import com.app.boxes.old.Adapter.TabAdapter;
import com.app.boxes.R;
import com.google.android.material.tabs.TabLayout;

/**
 * A simple {@link Fragment} subclass.
 */

//@Puppet
public class OrderHistoryFragment extends Fragment implements BoxesHistoryFragment.onItemListener {

    private static final String TAG = "OrderHistoryFragment";

    private TabAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    private Activity mContext;

    public OrderHistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        this.mContext = (Activity) context;
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_order_history, container, false);

        //show the toolbar
        ((MainActivity) mContext).getSupportActionBar().show();

        viewPager = rootView.findViewById(R.id.order_history_viewPager);
        tabLayout = rootView.findViewById(R.id.order_history_tabLayout);

        adapter = new TabAdapter(getChildFragmentManager());
        adapter.addFragment(new BoxesHistoryFragment(this), getString(R.string.subscription));
        //adapter.addFragment(new EcommerceHistoryFragment(), getString(R.string.ecommerce));

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

//        BottomNavigationView bottomNavigationView = mContext.findViewById(R.id.bnv_navigation);
//        bottomNavigationView.setSelectedItemId(0);

        /*rootView.setFocusableInTouchMode(true);
        rootView.requestFocus();
        rootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_BACK) {

                    if (getFragmentManager() != null) {
                        getFragmentManager().popBackStack();
                    }

                    return true;
                }
                return false;
            }
        });*/

        return rootView;
    }

    @Override
    public void onItemClicked(Bundle bundle) {
         // NavOptions n = new NavOptions.Builder().setPopUpTo(R.id.flowerHomeFragment, true).setLaunchSingleTop(true).build();
           ((MainActivity)mContext).navigationController(R.id.orderHistoryDetailFragment, bundle, null);
    }
}
