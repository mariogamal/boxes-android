package com.app.boxes.old.Activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.app.boxes.old.API.API;
import com.app.boxes.old.Model.Language;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Locale;

public class SelectLanguageActivity extends AppCompatActivity {

    private static String TAG = "selectLanguage";

    private ImageView ivLanguageCodeEnglishIcon, ivLanguageCodeArabicIcon, ivClose;
    private TextView tvEnglish, tvArabic;
    private LinearLayout btnEnglish, btnArabic;
    private AVLoadingIndicatorView avi;
    private View snackbarView;

    private MySharedPreferences mPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPref = new MySharedPreferences(this);
        mPref.getLocale();
        Log.d(TAG, "Default Language is: " + mPref.getLocale());

        setContentView(R.layout.activity_select_language);

        // initialize widgets
        initializer();

        // on click listeners
        listener();
    }

    void initializer() {
//        ivLanguageCodeEnglishIcon = findViewById(R.id.iv_select_language_english_icon);
//        ivLanguageCodeArabicIcon = findViewById(R.id.iv_select_language_arabic_icon);
//        ivClose = findViewById(R.id.iv_select_language_close);
//
//        tvEnglish = findViewById(R.id.tv_select_language_english);
//        tvArabic = findViewById(R.id.tv_select_language_arabic);
//
//        btnEnglish = findViewById(R.id.btn_lang_english);
//        btnArabic = findViewById(R.id.btn_lang_arabic);
//
//        avi = findViewById(R.id.avi_select_language);
        avi.setVisibility(View.GONE);

        snackbarView = findViewById(android.R.id.content);

        getLanguages();

        showHideCloseBtn();
    }

    void showHideCloseBtn() {
        Intent getIntent = getIntent();
        String from = getIntent.getStringExtra("from");
        if (from != null && from.equals("splash")) {
            ivClose.setVisibility(View.GONE);
        }
    }

    void listener() {
        //navigate to signin activity
        btnEnglish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                restartActivity();
//                recreate();
                setLanguage("en");

//                finishAffinity();
            }
        });

        //navigate to signin activity
        btnArabic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                restartActivity();
//                recreate();
                setLanguage("ar");

//                finishAffinity();
            }
        });

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

//    private void restartActivity() {
//        Intent intent = getIntent();
//        startActivity(intent);
//        finish();
//    }

    private void setLocale(String lang) {
        Log.d(TAG, "Locale set to: " + lang);

        Locale locale = new Locale(lang);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getResources().updateConfiguration(config, getResources().getDisplayMetrics());

//        recreate();

        mPref.saveLocale(lang);
    }

    private void getLanguages() {
        String url = API.getAPI() + "languages";
        Log.d(TAG, "Language URL: " + url);

        avi.setVisibility(View.VISIBLE);
        RequestQueue requestQueue;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                avi.setVisibility(View.GONE);
                try {
                    int status = response.getInt("status");
                    if (status > 0) {
                        JSONArray dataArray = response.getJSONArray("data");

                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject jsonObject = dataArray.getJSONObject(i);

                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String image = jsonObject.getString("image");
                            String code = jsonObject.getString("code");

                            String imgUrl = API.getImageAPI() + image;

                            Log.d(TAG, "onResponse: img url" + API.getImageAPI() + imgUrl);
                            if (i == 0) {
                                tvEnglish.setText(name);
                                Picasso.get().load(imgUrl).into(ivLanguageCodeEnglishIcon);
                            } else if (i == 1) {
                                tvArabic.setText(name);
                                Picasso.get().load(imgUrl).into(ivLanguageCodeArabicIcon);
                            }
                        }
                    }
                } catch (Exception ex) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        Log.d(TAG, ex.toString());
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                avi.setVisibility(View.GONE);
                Log.d(TAG, "Error: " + error);

                if (error instanceof NoConnectionError) {
                    Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.no_network), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
                    snackbar.show();
                } else {
                    Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
                    snackbar.show();
                }
            }
        });

        requestQueue = Volley.newRequestQueue(this);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }

    private void setLanguage(final String langCode) {
        String url = API.getAPI() + "languages";
        Log.d(TAG, "Language URL: " + url +
                "\n Language Code: " + langCode);

        avi.setVisibility(View.VISIBLE);
        RequestQueue requestQueue;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                avi.setVisibility(View.GONE);
                try {
                    int status = response.getInt("status");

                    Language language = null;

                    if (status == 1) {
                        setLocale(langCode);
                        JSONArray dataArray = response.getJSONArray("data");

                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject jsonObject = dataArray.getJSONObject(i);

                            String id = jsonObject.getString("id");
                            String name = jsonObject.getString("name");
                            String image = jsonObject.getString("image");
                            String code = jsonObject.getString("code");

                            String imgUrl = API.getImageAPI() + image;

                            language = new Language();
                            if (langCode.equals(code)) {
                                language.setId(id);
                                language.setName(name);
                                language.setImage(imgUrl);
                                language.setCode(code);

                                mPref.saveLangObject(language);

                                Log.d(TAG, "id " + id + " name " + name + " code " + code);
                            }
                            Log.d(TAG, "id " + language.getId() + " name " + language.getName() + " code " + language.getCode() + " image: " + imgUrl);
                        }
                        startActivity(new Intent(SelectLanguageActivity.this, SignInActivity.class));
                        finish();
                    }
                } catch (Exception ex) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        Log.d(TAG, ex.toString());
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                avi.setVisibility(View.GONE);
                Log.d(TAG, "Error: " + error);

                if (error instanceof NoConnectionError) {
                    Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.no_network), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
                    snackbar.show();
                } else {
                    Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
                    snackbar.show();
                }
            }
        });

        requestQueue = Volley.newRequestQueue(this);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }
}