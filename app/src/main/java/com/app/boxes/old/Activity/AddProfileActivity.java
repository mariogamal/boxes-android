package com.app.boxes.old.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.MediaStore;
import android.provider.Settings;
import android.text.InputType;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.boxes.old.API.API;
import com.app.boxes.old.Model.User;
import com.app.boxes.old.Multipart.VolleyMultipartRequest;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.material.snackbar.Snackbar;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class AddProfileActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private static final String TAG = "AddProfile";
    private String image = "";

    private static int GALLERY = 1;

    private TextView tvSkip;
    private EditText etFirstName, etLastName, etEmail, etAddress;
    private ImageView btnUploadImg;
    private Button btnAddProfile;
    private View snackbarView;
    private CircleImageView circleImageView;
    private LinearLayout loaderLayout;

    private Bitmap bitmap;

    private MySharedPreferences sPref;

    // for location
    private GoogleApiClient mGoogleApiClient;
    private Location mLocation;
    private LocationManager mLocationManager;

    private LocationRequest mLocationRequest;
    private long UPDATE_INTERVAL = 300000;  /* 10 secs */
    private long FASTEST_INTERVAL = 300000; /* 2 sec */
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 5445;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_profile);

        // initialize components
        initializer();

        requestMultiplePermissions();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        checkLocation(); //check whether location service is enable or not in your  phone
    }

    void initializer() {
        sPref = new MySharedPreferences(this);

        tvSkip = findViewById(R.id.tv_add_profile_skip);

        circleImageView = findViewById(R.id.civ_add_profile_profile);
        btnUploadImg = findViewById(R.id.iv_add_profile_upload);

        etFirstName = findViewById(R.id.et_add_profile_first_name);
        etLastName = findViewById(R.id.et_add_profile_last_name);
        etEmail = findViewById(R.id.et_add_profile_email);
        etAddress = findViewById(R.id.et_add_profile_address);

        etAddress.setImeOptions(EditorInfo.IME_ACTION_DONE);
        etAddress.setRawInputType(InputType.TYPE_CLASS_TEXT);

        btnAddProfile = findViewById(R.id.btn_add_profile);

        loaderLayout = findViewById(R.id.ll_loader_add_profile);
        loaderLayout.setVisibility(View.GONE);

        snackbarView = findViewById(android.R.id.content);

        circleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "Upload Image");
                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(galleryIntent, GALLERY);
            }
        });

        btnUploadImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "Upload Image");
                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(galleryIntent, GALLERY);
            }
        });

        final User.AccessToken token = sPref.getAccessTokenObject();
        String accessToken;
        if (token != null) {
            accessToken = token.getAccessToken();
            getAndSetUserDetails(accessToken);

            final String finalAccessToken = accessToken;
            btnAddProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    updateUserInfo(finalAccessToken);
                }
            });

        }

        tvSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(AddProfileActivity.this, MainActivity.class));
                finish();
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {

                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), contentURI);//compress(Bitmap.CompressFormat.JPEG, 50, );
                    bitmap = getScaledBitmap(bitmap, 500, 500);
                    circleImageView.setImageBitmap(bitmap);
                    image = "image";

                } catch (IOException e) {
                    e.printStackTrace();
                    Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.failed), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
                    snackbar.show();
                }
            }
        }
    }

    public static Bitmap getScaledBitmap(Bitmap b, int reqWidth, int reqHeight) {
        Matrix m = new Matrix();
        m.setRectToRect(new RectF(0, 0, b.getWidth(), b.getHeight()), new RectF(0, 0, reqWidth, reqHeight), Matrix.ScaleToFit.CENTER);
        return Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), m, true);
    }

    private byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        if (bitmap != null)
            bitmap.compress(Bitmap.CompressFormat.PNG, 10, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    private void requestMultiplePermissions() {
        Dexter.withActivity(this)
                .withPermissions(

                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
//                                Snackbar.make(activity.findViewById(android.R.id.content), "Permission granted to access gallery", Snackbar.LENGTH_SHORT).show();
                            Log.d(TAG, "Permission granted to access gallery");
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.permission_required_to_access_gallery), Snackbar.LENGTH_SHORT);
                            View getSnackbarView = snackbar.getView();
                            getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
                            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                            tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
                            snackbar.show();

                            Log.d(TAG, "Permission required to access gallery");
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }

                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Log.d(TAG, "DexterError: " + error.toString());

                        Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.some_error), Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
                        snackbar.show();
                    }
                })
                .onSameThread()
                .check();
    }

    // get user details
    private void getAndSetUserDetails(final String accessToken) {
        String url = API.getAPI() + "auth/user";
        Log.d(TAG, "User URL: " + url);

        loaderLayout.setVisibility(View.VISIBLE);
        RequestQueue requestQueue;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "User onResponse: " + response);
                loaderLayout.setVisibility(View.GONE);

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    int status = jsonObject.optInt("status");

                    if (status == 1) {
                        JSONObject data = jsonObject.getJSONObject("data");

                        String id = data.optString("id");
                        String first_name = data.optString("first_name");
                        String last_name = data.optString("last_name");
                        String getEmail = data.optString("email");
                        String getAddress = data.optString("address");
                        String getPhoneNumber = data.optString("phone");
                        String getImage = data.getString("image");

                        String imageUrl = API.getImageAPI() + getImage;

                        User user = new User();

                        user.setId(id);
                        user.setFirstName(first_name);
                        user.setLastName(last_name);
                        user.setEmail(getEmail);
                        user.setPhoneNumber(getPhoneNumber);

                        if (!first_name.equals("null"))
                            etFirstName.setText(first_name);

                        if (!last_name.equals("null"))
                            etLastName.setText(last_name);

                        if (!getEmail.equals("null"))
                            etEmail.setText(getEmail);

                        if (!getAddress.equals("null"))
                            etAddress.setText(getAddress);

                        if (!getImage.equals("null")) {
                            Picasso.get().load(imageUrl).fit().into(circleImageView);
                            user.setImage(imageUrl);
                        } else {
                            circleImageView.setImageResource(R.drawable.avatar);
                            user.setImage(null);
                        }

                        sPref.saveUserObject(user);

                        Log.d("User-Response", "Id: " + id + " first_name: " + first_name + " last_name: " + last_name + "  email: " + getEmail + " phone: " + getPhoneNumber + " address: " + getAddress);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loaderLayout.setVisibility(View.GONE);
                Log.d(TAG, "Volley Error: " + error.toString());
                if (error instanceof NoConnectionError) {
                    Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.no_network), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
                    snackbar.show();
                } else {
                    Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
                    snackbar.show();
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (accessToken != null) {
                    params.put("Authorization", accessToken);
                    Log.d(TAG, "getHeaders " + accessToken);
                }
                return params;
            }
        };
        requestQueue = Volley.newRequestQueue(this);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    // edit and update user details
    private void updateUserInfo(final String accessToken) {
        String firstName = etFirstName.getText().toString();
        String lastName = etLastName.getText().toString();
        String email = etEmail.getText().toString();
        String address = etAddress.getText().toString();

        Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.please_fill_the_required_field), Snackbar.LENGTH_SHORT);
        View getSnackbarView = snackbar.getView();
        getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
        tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));

        if (firstName.isEmpty()) {
            etFirstName.requestFocus();
            snackbar.show();
        } else if (lastName.isEmpty()) {
            etLastName.requestFocus();
            snackbar.show();
        } else if (email.isEmpty()) {
            etEmail.requestFocus();
            snackbar.show();
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            etEmail.requestFocus();
            snackbar.setText("Please enter valid email");
            snackbar.show();
        } else if (address.trim().isEmpty()) {
            etAddress.requestFocus();
            snackbar.show();
        } else if (image.isEmpty()) {
            Snackbar snackbar2 = Snackbar.make(snackbarView, getString(R.string.please_select_an_image), Snackbar.LENGTH_SHORT);
            View getSnackbarView2 = snackbar2.getView();
            getSnackbarView2.setBackgroundColor(getResources().getColor(android.R.color.white));
            TextView tvSnackBar2 = getSnackbarView2.findViewById(com.google.android.material.R.id.snackbar_text);
            tvSnackBar2.setTextColor(getResources().getColor(R.color.active_tab_color));
            snackbar2.show();
        } else {
            if (address.contains(" "))
                address = address.replace(" ", "%20");

            final String BASE_URL = API.getAPI() + "auth/store-profile?" +
                    "first_name=" + firstName +
                    "&last_name=" + lastName +
                    "&email=" + email +
                    "&address=" + address;
            Log.d(TAG, "Add Profile URL: " + BASE_URL);

            final RequestQueue rQueue = Volley.newRequestQueue(this);
            loaderLayout.setVisibility(View.VISIBLE);
            VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, BASE_URL,
                    new Response.Listener<NetworkResponse>() {
                        @Override
                        public void onResponse(NetworkResponse response) {
                            Log.d("ressssssoo", new String(response.data));
                            rQueue.getCache().clear();
                            loaderLayout.setVisibility(View.GONE);

                            try {
                                JSONObject jsonObject = new JSONObject(new String(response.data));
//                                Snackbar.make(mContext.findViewById(android.R.id.content), jsonObject.getString("message"), Snackbar.LENGTH_SHORT).show();

                                int status = jsonObject.optInt("status");
                                String message = jsonObject.optString("message");

                                if (status == 1) {

                                    Snackbar snackbar = Snackbar.make(snackbarView, message, Snackbar.LENGTH_SHORT);
                                    View getSnackbarView = snackbar.getView();
                                    getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
                                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                                    tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
                                    snackbar.show();

                                    User user = new User();

                                    JSONObject dataObj = jsonObject.getJSONObject("data");
                                    String userId = dataObj.getString("id");
                                    String firstName = dataObj.getString("first_name");
                                    String lastName = dataObj.getString("last_name");
                                    String phoneNo = dataObj.getString("phone");
                                    String email = dataObj.getString("email");
                                    String image = dataObj.getString("image");

                                    user.setId(userId);
                                    user.setFirstName(firstName);
                                    user.setLastName(lastName);
                                    user.setPhoneNumber(phoneNo);
                                    user.setEmail(email);

                                    if (!image.equals("")) {
                                        String imgUrl = API.getImageAPI() + image;
                                        user.setImage(imgUrl);
                                    } else {
                                        user.setImage(null);
                                    }

                                    sPref.saveUserObject(user);

                                    new CountDownTimer(2000, 1000) {

                                        @Override
                                        public void onTick(long l) {

                                        }

                                        @Override
                                        public void onFinish() {
                                            startActivity(new Intent(AddProfileActivity.this, SurveyFormActivity.class));
                                            finish();
                                        }
                                    }.start();

                                } else if (status == 0) {

                                    JSONObject paramObj = jsonObject.optJSONObject("parameters");

                                    if (paramObj != null) {
                                        JSONArray email = paramObj.optJSONArray("email");
                                        JSONArray image = paramObj.optJSONArray("image");
                                        if (email != null) {
                                            Snackbar snackbar = Snackbar.make(snackbarView, "Error: " + email.toString(), Snackbar.LENGTH_SHORT);
                                            View getSnackbarView = snackbar.getView();
                                            getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
                                            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                                            tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
                                            snackbar.show();
                                        } else if (image != null) {
                                            Snackbar snackbar = Snackbar.make(snackbarView, "Error: " + image.toString(), Snackbar.LENGTH_SHORT);
                                            View getSnackbarView = snackbar.getView();
                                            getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
                                            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                                            tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
                                            snackbar.show();
                                        }
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            loaderLayout.setVisibility(View.GONE);
                            Log.d(TAG, "OnErrorResponse: " + error.getMessage());
                            if (error instanceof NoConnectionError) {
                                Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.no_network), Snackbar.LENGTH_SHORT);
                                View getSnackbarView = snackbar.getView();
                                getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
                                TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                                tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
                                snackbar.show();
                            } else {
                                Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                                View getSnackbarView = snackbar.getView();
                                getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
                                TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                                tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
                                snackbar.show();
                            }
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    if (accessToken != null) {
                        params.put("Authorization", accessToken);
                        Log.d(TAG, "getHeaders " + accessToken);
                    }
                    return params;
                }

                /*
                 *pass files using below method
                 * */
                @Override
                protected Map<String, DataPart> getByteData() {
                    Map<String, DataPart> params = new HashMap<>();
                    if (bitmap != null) {
                        long imagename = System.currentTimeMillis();
                        params.put("image", new DataPart(imagename + ".png", getFileDataFromDrawable(bitmap)));
                        Log.d("GetByteData", "image" + new DataPart(imagename + ".png",
                                getFileDataFromDrawable(bitmap)));
                    }
                    return params;
                }
            };
            volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            rQueue.add(volleyMultipartRequest);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED)
                Snackbar.make(snackbarView, "Permission denied by user", Snackbar.LENGTH_LONG).show();
            else if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                startLocationUpdates();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        // Request location updates
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);

                return;
            }
        }

        startLocationUpdates();

        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (mLocation == null) {
            startLocationUpdates();
        }

        if (mLocation != null) {
            // mLatitudeTextView.setText(String.valueOf(mLocation.getLatitude()));
            //mLongitudeTextView.setText(String.valueOf(mLocation.getLongitude()));
        } else {
//            Toast.makeText(activity, , Toast.LENGTH_SHORT).show();
            Log.d(TAG, "Location not Detected");
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Connection Suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(TAG, "Connection failed. Error: " + connectionResult.getErrorCode());
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    private void startLocationUpdates() {
        // Create the location request
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL);
        // Request location updates
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);

//                nearbyGymLayout.setVisibility(View.GONE);
                return;
            }
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                mLocationRequest, this);
        Log.d("reque", "--->>>>");
    }

    @Override
    public void onLocationChanged(Location location) {

        String msg = "Updated Location: " +
                location.getLatitude() + "," +
                location.getLongitude();

        Log.d(TAG, "onLocationChanged: " + msg);
    }

    private void checkLocation() {
        if (!isLocationEnabled()) {
            showAlert();
        } else
            isLocationEnabled();
    }

    private boolean isLocationEnabled() {
        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        assert mLocationManager != null;
        return mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private void showAlert() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.location_dialog, null);
        builder.setView(dialogView);

        final AlertDialog alertDialog = builder.show();

        Button btnLocation = dialogView.findViewById(R.id.btn_location_dialog_settings);
        Button btnCancel = dialogView.findViewById(R.id.btn_location_dialog_cancel);

        btnLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(myIntent);
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
    }

    private void userAddress(double lat, double longg) {
        try {
            Geocoder geocoder = new Geocoder(this, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(lat, longg, 1);

            String address = addresses.get(0).getAddressLine(0);
            String locality = addresses.get(0).getLocality();
            String subLocality = addresses.get(0).getSubLocality();
            String premises = addresses.get(0).getPremises();
            String adminArea = addresses.get(0).getSubAdminArea();

            Log.d(TAG, "userAddress: " + address + " " + locality + " " + subLocality + " " + premises);
//            Area area = new Area();
//            area.setArea(subLocality);

            Log.d(TAG, "Address: " + address + "\n" + "Locality: " + locality + "\n" + "Sub Locality: " + subLocality + "\n" + "Premises: " + premises + " area " + adminArea);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}