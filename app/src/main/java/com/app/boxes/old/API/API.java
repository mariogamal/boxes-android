package com.app.boxes.old.API;

public class API {

    private static String PROTOCOL = "http://";
    private static String PROJECT_NAME = "boxes-app.com";
    private static String IP = "/api/v1/";
    private static String IMG_IP = "/public/uploads/";

    public static String getAPI() {

        return PROTOCOL + PROJECT_NAME + IP;
    }

    public static String getImageAPI() {
        return PROTOCOL + PROJECT_NAME + IMG_IP;
    }
}
