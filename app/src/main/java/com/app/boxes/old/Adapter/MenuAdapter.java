package com.app.boxes.old.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.boxes.old.Model.Menu;
import com.app.boxes.R;

import java.util.ArrayList;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.MyViewHolder> {
    private Context context;
    private ArrayList<Menu> menusArrayList = new ArrayList<>();
    private LayoutInflater inflater;

    public MenuAdapter(Context context, ArrayList<Menu> menusArrayList) {
        this.context = context;
        this.menusArrayList = menusArrayList;
//        this.menuItemArrayList = menuItemArrayList;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView = inflater.inflate(R.layout.menu_category, parent, false);

        return new MyViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.tvCategory.setText(menusArrayList.get(position).getCategory());

//        try {
//            ProductItemAdapter productItemAdapter = new ProductItemAdapter(context, menusArrayList.get(position).getMenuItems());
//            holder.rvMenuItems.setLayoutManager(new LinearLayoutManager(context));
//            holder.rvMenuItems.setAdapter(productItemAdapter);
//            holder.rvMenuItems.setNestedScrollingEnabled(false);
//        } catch (Exception ex) {
//            Log.d("exception", ex.getMessage());
//        }

    }

    @Override
    public int getItemCount() {
        return menusArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView tvCategory;

//        private RecyclerView rvMenuItems;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvCategory = itemView.findViewById(R.id.menuCategory);
//            rvMenuItems = itemView.findViewById(R.id.rv_menu_item);
        }
    }
}
