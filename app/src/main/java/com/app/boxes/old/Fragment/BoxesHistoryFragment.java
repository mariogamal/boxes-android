package com.app.boxes.old.Fragment;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.boxes.old.API.API;
import com.app.boxes.old.Activity.MainActivity;
import com.app.boxes.old.Adapter.OrderHistoryAdapter;
import com.app.boxes.old.Listener.RecyclerItemClickListener;
import com.app.boxes.old.Model.Language;
import com.app.boxes.old.Model.OrderHistory;
import com.app.boxes.old.Model.User;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.app.boxes.old.UI.LoginBottomSheet;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class BoxesHistoryFragment extends Fragment {

    private String TAG = "BoxesHistoryFragment";

    private RecyclerView recyclerView;
    private AVLoadingIndicatorView avi;
    private View snackbarView, rootView;
    private TextView textView;
    private RelativeLayout rlEmptyState;
    private Button btnNo;

    private OrderHistoryAdapter adapter;

    private ArrayList<OrderHistory> orderHistoryArrayList = new ArrayList<OrderHistory>();

    private boolean isItemClicked = false;

    private Activity mContext;

    public BoxesHistoryFragment() {
        // Required empty public constructor
    }

    onItemListener listener;

    public BoxesHistoryFragment(onItemListener listener) {
        this.listener = listener;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        if (rootView == null)
            rootView = inflater.inflate(R.layout.fragment_boxes_history, container, false);

        ((MainActivity) mContext).getSupportActionBar().show();

        avi = rootView.findViewById(R.id.avi_boxes_order_history);
        avi.setVisibility(View.GONE);

        btnNo = rootView.findViewById(R.id.btn_order_no);
        rlEmptyState = rootView.findViewById(R.id.rl_order_no_result);
        rlEmptyState.setVisibility(View.GONE);

        snackbarView = mContext.findViewById(android.R.id.content);

//        textView = rootView.findViewById(R.id.tv_boxes_order_history);

        recyclerView = rootView.findViewById(R.id.rv_boxes_order_history);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.onBackPressed();
            }
        });

        adapter = new OrderHistoryAdapter(mContext, orderHistoryArrayList);
        recyclerView.setAdapter(adapter);

        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(mContext, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Log.d("isItemClick1", isItemClicked + "");
                if (!isItemClicked) {
                    OrderHistory orderHistory = orderHistoryArrayList.get(position);
                    String itemClicked = orderHistory.getId();

                    Bundle bundle = new Bundle();
                    bundle.putString("id", itemClicked);

                    listener.onItemClicked(bundle);

                    isItemClicked = true;
                    Log.d("isItemClick2", isItemClicked + "");

                    //timer to make isItemClicked = false
                    new CountDownTimer(1000, 1000) {
                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {
                            isItemClicked = false;
                            Log.d("isItemClick3", isItemClicked + "");
                        }
                    }.start();
                }
            }

            @Override
            public void onLongItemClick(View view, int position) {

            }
        }));

        MySharedPreferences sPref = new MySharedPreferences(mContext);

        Language language = sPref.getLangObject();

        if (sPref.getAccessTokenObject() != null) {
            User.AccessToken token = sPref.getAccessTokenObject();
            if (token != null && language != null) {
                getOrderHitsory(language.getId(), token.getAccessToken());
            } else {
                LoginBottomSheet loginBottomSheet = new LoginBottomSheet();
                loginBottomSheet.setCancelable(false);
                if (getFragmentManager() != null) {
                    loginBottomSheet.show(getFragmentManager(), "login_bottom_sheet");
                }
            }
        }

//        rootView.setFocusableInTouchMode(true);
//        rootView.requestFocus();
     /*   rootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_BACK) {

                    if (getFragmentManager() != null) {
                        getFragmentManager().beginTransaction().remove(BoxesHistoryFragment.this).commit();
                    }

                    return true;
                }
                return false;
            }
        });*/

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (rootView != null) {
            ViewGroup viewGroup = (ViewGroup) rootView.getParent();
            if (viewGroup != null)
                viewGroup.removeAllViews();
        }
    }

    @Override
    public void onAttach(Context context) {
        this.mContext = (Activity) context;
        super.onAttach(context);
    }


    private void getOrderHitsory(String langId, final String accessToken) {
        String BASE_URL = API.getAPI() + "auth/meals-history?language_id=" + langId;
        Log.d(TAG, "Order History URL: " + BASE_URL);

        avi.setVisibility(View.VISIBLE);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, BASE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "onResponse: " + response);
                avi.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    int status = jsonObject.optInt("status");
                    String msg = jsonObject.optString("message");

                    OrderHistory history = null;

                    if (status == 1) {
//                        textView.setVisibility(View.GONE);
                        orderHistoryArrayList.clear();
                        JSONArray dataArray = jsonObject.optJSONArray("data");
                        if (dataArray != null) {
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObj = dataArray.getJSONObject(i);

                                String id = dataObj.getString("id");
                                String package_name = dataObj.getString("package_name");
                                String meals = dataObj.getString("meals");
                                String price = dataObj.getString("price");
                                String meal_selection_case = dataObj.getString("meal_selection_case");
                                String validity = dataObj.getString("validity");
                                String created_at = dataObj.getString("created_at");
                                String listings = dataObj.getString("listings");

                                /*JSONArray listingArray = dataObj.optJSONArray("user_subscribed_listing");
                                if (listingArray != null) {
                                    for (int j = 0; j < listingArray.length(); j++) {
                                        JSONObject listingObj = listingArray.getJSONObject(j);
                                        String subscriptionId = listingObj.getString("subscription_id");
                                        String listing_id = listingObj.getString("listing_id");
                                        String consumed_meals = listingObj.getString("consumed_meals");
                                        String out_of_meals = listingObj.getString("out_of_meals");
                                        String listing_name = listingObj.getString("listing_name");

                                        history.setListingId(listing_id);
                                        history.setConsumedMeals(consumed_meals);
                                        history.setOutOfMeals(out_of_meals);
                                        history.setListingName(listing_name);

                                    }
                                }*/

                                history = new OrderHistory();
                                history.setId(id);
                                history.setPackageName(package_name);
                                history.setMeals(meals);
                                history.setPrice(price);
                                history.setMealSelectionCase(meal_selection_case);
                                history.setValidity(validity);
                                history.setCreatedDate(created_at);
                                history.setListingName(listings);

                                orderHistoryArrayList.add(history);
                            }
                            adapter.notifyDataSetChanged();
                        } else {
                            rlEmptyState.setVisibility(View.VISIBLE);
                        }
                    } else {
                        rlEmptyState.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    Log.d(TAG, "JSONException: " + e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                avi.setVisibility(View.GONE);
                Log.d(TAG, "onErrorResponse: " + error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (accessToken != null) {
                    params.put("Authorization", accessToken);
                    Log.d(TAG, "getHeaders: " + accessToken);
                }
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

    public interface onItemListener {
        void onItemClicked(Bundle bundle);
    }

}