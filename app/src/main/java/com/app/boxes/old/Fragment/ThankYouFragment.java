package com.app.boxes.old.Fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavOptions;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;

import com.app.boxes.old.Activity.MainActivity;
import com.app.boxes.old.OnBackPressListener;
import com.app.boxes.R;
import com.google.android.material.bottomnavigation.BottomNavigationView;


public class ThankYouFragment extends Fragment implements OnBackPressListener {

    private Button btnDone;
    private String from;
    private BottomNavigationView bnv;
    private TextView tv;

    public ThankYouFragment() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_thank_you, container, false);


//        ivThankYouBack = rootView.findViewById(R.id.iv_thank_you_back);
//        ivThankYouDone = rootView.findViewById(R.id.iv_thank_you_done);
        btnDone = rootView.findViewById(R.id.btn_thank_you_done);
        tv = rootView.findViewById(R.id.tv_thankyou);
//        bnv = mContext.findViewById(R.id.bnv_navigation);

        Bundle bundle = getArguments();
        if (bundle != null) {
            from = bundle.getString("from");
            if (from != null) {
                if (from.equals("placeOrder")) {
                    tv.setText(mContext.getResources().getString(R.string.order_text));
                } else {
                    tv.setText(mContext.getResources().getString(R.string.subscription_text));
                }
            }
        }

        ((MainActivity)mContext).updateBackPress(this);

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // check if user came from place order or card info
                if (from.equals("placeOrder")) {
                    NavOptions n = new NavOptions.Builder().setPopUpTo(R.id.navigation_graph, true).build();
                    ((MainActivity)mContext).navigationController(R.id.flowerHomeFragment, n);
                    // move bottom view selection to nav_menu_home
//                    bnv.setSelectedItemId(R.id.nav_menu_home);
                } else {
                    NavOptions n = new NavOptions.Builder().setPopUpTo(R.id.flowerHomeFragment, false).build();
                    ((MainActivity)mContext).navigationController(R.id.foodFragment, n);
                    // move bottom view selection to nav_menu_box
//                    bnv.setSelectedItemId(R.id.nav_menu_box);
                }

            }
        });

        return rootView;
    }

    @Override
    public void onDestroy() {
        ((MainActivity)mContext).updateBackPress(null);

        super.onDestroy();
    }

    private Activity mContext;

    @Override
    public void onAttach(Context context) {
        this.mContext = (Activity) context;
        super.onAttach(context);
    }

    @Override
    public void onMyBackPress() {
        // check if user came from place order or card info
        if (from.equals("placeOrder")) {
            NavOptions n = new NavOptions.Builder().setPopUpTo(R.id.navigation_graph, true).build();
            ((MainActivity) mContext).navigationController(R.id.flowerHomeFragment, n);
            // move bottom view selection to nav_menu_home
//            bnv.setSelectedItemId(R.id.nav_menu_home);
        } else {
            NavOptions n = new NavOptions.Builder().setPopUpTo(R.id.flowerHomeFragment, false).build();
            ((MainActivity) mContext).navigationController(R.id.foodFragment, n);
            // move bottom view selection to nav_menu_box
//            bnv.setSelectedItemId(R.id.nav_menu_box);
        }
    }
}
