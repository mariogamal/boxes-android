package com.app.boxes.old.Fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.app.boxes.old.API.API;
import com.app.boxes.old.Adapter.MenuItemAdapter;
import com.app.boxes.old.IngredientsActivity;
import com.app.boxes.old.Model.Menu;
import com.app.boxes.old.Model.MenuItem;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuFragment extends Fragment {
    private ExpandableListView expandableListView;


    private MenuItemAdapter adapter;
    private AVLoadingIndicatorView avlMenu;
    private View snackbarView, rootView;

    private ArrayList<Menu> menuCategoryArrayList = new ArrayList<Menu>();

    private String TAG = "MenuFragment";

    private String menuId = "", menuCategoryName = "", languageId = "", listingId = "";

    private MySharedPreferences mPref;


    public MenuFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_menu, container, false);

        //initialize widgets
//        avlMenu = rootView.findViewById(R.id.avl_menu);
        expandableListView = rootView.findViewById(R.id.menuList);
        snackbarView = mContext.findViewById(R.id.main_content);

        //initialize the mysharedpreferences
        mPref = new MySharedPreferences(mContext);

        if (mPref.getLangObject() != null) {
            //check which language is selected
            languageId = mPref.getLangObject().getId();
            listingId = mPref.getListingId();
            String[] temp = listingId.split("@");

            Log.d(TAG, "language id " + languageId + " listingId " + listingId);

            getMenuList(listingId, languageId);
        } else {
            Log.d(TAG, "No value found");
        }

        return rootView;
    }

    private Activity mContext;

    @Override
    public void onAttach(Context context) {
        this.mContext = (Activity) context;
        super.onAttach(context);
    }

    @Override
    public void onDestroyView() {
        if (rootView != null) {
            ViewGroup viewGroup = (ViewGroup) rootView.getParent();

            if (viewGroup != null) {
                viewGroup.removeAllViews();
            }
        }
        super.onDestroyView();
    }

    private void getMenuList(String listingId, String languageId) {
        avlMenu.setVisibility(View.VISIBLE);
        String url = API.getAPI() + "menu?listing_id=" + listingId + "&language_id=" + languageId;
        Log.d(TAG, "getMenuList URL: " + url);

        RequestQueue requestQueue;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                try {
                    menuCategoryArrayList.clear();

                    int status = response.getInt("status");
                    String message = response.getString("message");

                    if (status == 1) {
                        avlMenu.setVisibility(View.GONE);
                        JSONArray jsonArrayData = response.getJSONArray("data");

                        Menu menu;
                        for (int i = 0; i < jsonArrayData.length(); i++) {
                            JSONObject jsonObject = jsonArrayData.getJSONObject(i);
                            menuId = jsonObject.getString("id");
                            menuCategoryName = jsonObject.getString("menu_cat_name");
                            menu = new Menu();
                            menu.setCategory(menuCategoryName);

                            JSONArray jsonArrayProducts = jsonObject.getJSONArray("products");
                            if (jsonArrayProducts.length() != 0) {
                                for (int j = 0; j < jsonArrayProducts.length(); j++) {
                                    JSONObject jsonProducts = jsonArrayProducts.getJSONObject(j);
                                    String menuItemId = jsonProducts.getString("id");
                                    String menuCategoryId = jsonProducts.getString("menu_category_id");
                                    String menuListingId = jsonProducts.getString("listing_id");
                                    String menuItemName = jsonProducts.getString("product_name");
                                    String menuItemCaptionName = jsonProducts.getString("caption_name");
                                    String menuItemPrice = jsonProducts.getString("price");
                                    String menuItemImage = jsonProducts.getString("image");
                                    String menuItemDescription = jsonProducts.getString("product_description");
                                    String menuItemDiscountPrice = jsonProducts.getString("discount");
                                    String menuItemDiscountUnit = jsonProducts.getString("discount_unit");
                                    String menuItemCarbohydrates = jsonProducts.getString("carbohydrates");
                                    String menuItemProteins = jsonProducts.getString("proteins");
                                    String menuItemCalories = jsonProducts.getString("calories");
                                    String menuItemRating = jsonProducts.getString("rating");
                                    JSONArray menuIngredients = jsonProducts.getJSONArray("product_ingredients");
                                    ArrayList<String> ingredients = new ArrayList<>();
                                    for (int ii = 0; ii < menuIngredients.length(); ii++) {
                                        JSONObject jsonIngredient = menuIngredients.getJSONObject(ii);
                                        String ingredient = jsonIngredient.getString("ing_name");
                                        ingredients.add(ingredient);
                                    }

                                    float ratings = Float.parseFloat(menuItemRating);

                                    String imageUrl = API.getImageAPI() + menuItemImage;

                                    MenuItem menuItem = new MenuItem();
                                    menuItem.setId(menuItemId);
                                    menuItem.setMenuCategoryId(menuCategoryId);
                                    menuItem.setListingId(menuListingId);
                                    menuItem.setProductName(menuItemName);
                                    menuItem.setCaptionName(menuItemCaptionName);
                                    menuItem.setImage(imageUrl);
                                    menuItem.setPrice(menuItemPrice);
                                    menuItem.setDecripton(menuItemDescription);
                                    menuItem.setDiscountPrice(menuItemDiscountPrice);
                                    menuItem.setDiscountUnit(menuItemDiscountUnit);
                                    menuItem.setCarbohydrates(menuItemCarbohydrates);
                                    menuItem.setProteins(menuItemProteins);
                                    menuItem.setCalories(menuItemCalories);
                                    menuItem.setRating(ratings);
                                    menuItem.setIcon(R.drawable.check_box_unactive);
                                    menuItem.setChecked(false);
                                    menuItem.setIngredients(ingredients);
                                    menu.menuItems.add(menuItem);
                                }
                            }
                            //add data on menu category arraylist
                            menuCategoryArrayList.add(menu);
                        }
                        //initialize the menu item adapter
                        adapter = new MenuItemAdapter(mContext, menuCategoryArrayList, null);
                        adapter.notifyDataSetChanged();
                        //set the adapter on expandable listview
                        expandableListView.setAdapter(adapter);
                        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                            @Override
                            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                                //todo show snackbar, loader
                                if (menuCategoryArrayList.get(i).getMenuItems().size() == 0) {
                                    MenuItem menuItem = new MenuItem();
                                    menuItem.setMessage("No meals found");
                                    menuCategoryArrayList.get(i).menuItems.add(menuItem);
                                }
                                return false;
                            }
                        });
                        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
                            @Override
                            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                                Intent intent = new Intent(getActivity(), IngredientsActivity.class);
                                intent.putStringArrayListExtra("ingredients",
                                        menuCategoryArrayList.get(groupPosition).menuItems.get(childPosition).getIngredients());
                                startActivity(intent);
                                return false;
                            }
                        });
                    } else {
                        Toast.makeText(mContext, "" + message, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception ex) {
                    Log.d(TAG, "" + ex.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse: " + error.toString());
            }
        });

        requestQueue = Volley.newRequestQueue(mContext);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }
}