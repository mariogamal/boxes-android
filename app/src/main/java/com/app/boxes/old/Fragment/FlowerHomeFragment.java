package com.app.boxes.old.Fragment;


import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.CountDownTimer;
import android.provider.Settings;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.app.boxes.old.API.API;
import com.app.boxes.old.Activity.MainActivity;
import com.app.boxes.old.Activity.SearchActivity;
import com.app.boxes.old.Adapter.CategoryAdapter;
import com.app.boxes.old.Adapter.FlowerHomeListAdapter;
import com.app.boxes.old.CategorySingleton;
import com.app.boxes.old.Listener.RecyclerItemClickListener;
import com.app.boxes.old.Model.CategorySubType;
import com.app.boxes.old.Model.Flower;
import com.app.boxes.old.Model.Category;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.material.snackbar.Snackbar;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
//@Puppet
public class FlowerHomeFragment extends Fragment implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    private ArrayList<Flower> flowerHomeArrayList = new ArrayList<Flower>();
    static ArrayList<Category> categoryImagesList = new ArrayList<>();
    private ArrayList<CategorySubType> categorySubTypeArrayList = new ArrayList<>();

    private LinearLayout llSearchView;
    private RecyclerView categoryRecyclerView, flowerListRecyclerView;
    private RelativeLayout rlNoResult;
    private AVLoadingIndicatorView avl;

    private boolean isItemClicked = false;

    private MySharedPreferences mPref;

    private FlowerHomeListAdapter flowerHomeListAdapter;
    private CategoryAdapter categoryAdapter;

    private static String TAG = "flowerHomeFragment";

    float distance = 0;
    String listingId = "", listingName = "", listingImage = "", listingCaption = "", categoryId = "", categoryName = "", categoryImage = "",
            categoryDefault = "", categoryIsFood = "", subCategoryId = "", subCategoryParentId = "", subCategoryType = "", languageId = "", typeId = "";

    private Bundle bundleCategories, bundleSubCategory, getBundleCategory;

    private Fragment selectedFragment;

    private View snackbarView;
    static boolean isData = false;

    // for location
    private GoogleApiClient mGoogleApiClient;
    private Location mLocation;
    private LocationManager mLocationManager;

    private LocationRequest mLocationRequest;
    private long UPDATE_INTERVAL = 300000;  /* 10 secs */
    private long FASTEST_INTERVAL = 300000; /* 2 sec */
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 5445;

    public FlowerHomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //inflate the layout
        View parentView = inflater.inflate(R.layout.fragment_flower_home, container, false);

//        //show the actionbar
        ((MainActivity) mContext).getSupportActionBar().show();

        //initialize widgets
        llSearchView = parentView.findViewById(R.id.ll_flower_search_view);
        rlNoResult = parentView.findViewById(R.id.rl_flower_home_no_result);
        rlNoResult.setVisibility(View.GONE);
        categoryRecyclerView = parentView.findViewById(R.id.rv_horizontal_home_categories);
        flowerListRecyclerView = parentView.findViewById(R.id.rv_flower_home_listing);
        snackbarView = mContext.findViewById(android.R.id.content);
        avl = parentView.findViewById(R.id.avi_flower_home);
        avl.setVisibility(View.GONE);

        //initialize MySharedPreference
        mPref = new MySharedPreferences(mContext);

        //initialize bundle
        bundleCategories = new Bundle();
        bundleSubCategory = new Bundle();

        //set horizontal list
        LinearLayoutManager horizontalLayout = new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false);
        categoryRecyclerView.setLayoutManager(horizontalLayout);
        // scroll to position that's clicked
        int temp = CategorySingleton.getInstance().categoryPosition;
        horizontalLayout.scrollToPosition(temp);
        //set flower list
        flowerListRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, RecyclerView.VERTICAL, false));

        //check which langauge is selected
        if (mPref != null) {
            languageId = mPref.getLangObject().getId();
//            typeId = mPref.getCategoryObject();

            //get categories from server

            getCategories(languageId);

            Log.d(TAG, "language id " + mPref.getLangObject().getId());
        }

        llSearchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, SearchActivity.class);
                intent.putExtra("type_id", typeId);
                startActivity(intent);
            }
        });

        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mLocationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        checkLocation(); //check whether location service is enable or not in your  phone

        return parentView;
    }

    private void alertDialog(final Context context) {

        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle("Message");
        alertDialog.setMessage("Are you sure want to quit?");
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                alertDialog.dismiss();
                dialogInterface.dismiss();
                mContext.finish();
            }
        });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
//                alertDialog.cancel();
                alertDialog.dismiss();
//                dialogInterface.dismiss();

                //getFragmentManager().popBackStackImmediate();
            }
        });

        alertDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int keyCode, KeyEvent keyEvent) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    alertDialog.dismiss();
                }
                return true;
            }
        });
        alertDialog.show();
    }

    private Activity mContext;

    @Override
    public void onAttach(Context context) {
        this.mContext = (Activity) context;
        super.onAttach(context);
    }

    boolean isHighlighted = false;

    private void getCategories(String language_id) {
        //categoryImagesList.clear();
        categorySubTypeArrayList.clear();

        String url = API.getAPI() + "types?language_id=" + language_id;
        Log.d(TAG, "URL: " + url);

        RequestQueue requestQueue;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d(TAG, response.toString());
                try {
                    int status = response.getInt("status");
                    String message = response.getString("message");

                    if (status == 1) {

                        JSONArray dataArray = response.getJSONArray("data");

                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject jsonDataObject = dataArray.getJSONObject(i);

                            categoryId = jsonDataObject.getString("id");
                            categoryName = jsonDataObject.getString("type_name");
                            categoryImage = jsonDataObject.getString("image");
                            categoryDefault = jsonDataObject.getString("default");
                            categoryIsFood = jsonDataObject.getString("is_food");

                            String imageUrl = API.getImageAPI() + categoryImage;

                            if (mPref.getCurrentCategory() == 1) {
                                isHighlighted = false;
                            } else {
                                isHighlighted = true;
                            }


                            Category category = new Category(categoryId, imageUrl, categoryName, categoryDefault, categoryIsFood, isHighlighted);
                            Log.d(TAG, "on response: " + categoryId + " " + categoryName + " " + categoryImage + " " + categoryDefault + " " + categoryIsFood);

                            if (!categoryImagesList.contains(category) && !isData)
                                categoryImagesList.add(category);

                            //categoryImagesList = CategorySingleton.getInstance().getCategoryArrayList();

                            categoryAdapter = new CategoryAdapter(mContext, categoryImagesList);

                            categoryRecyclerView.setAdapter(categoryAdapter);

//                            bundleCategories.putString("dummy", "hello android");

                            //get sub type for food category
                            JSONArray subDataArray = jsonDataObject.getJSONArray("sub_type");

                            for (int j = 0; j < subDataArray.length(); j++) {
                                JSONObject jsonSubDataObject = subDataArray.getJSONObject(j);

                                subCategoryId = jsonSubDataObject.getString("id");
                                subCategoryParentId = jsonSubDataObject.getString("parent_id");
                                subCategoryType = jsonSubDataObject.getString("sub_type_name");

                                Log.d(TAG, "on response subTypes: " + subCategoryId + " " + subCategoryParentId + " " + subCategoryType);

                                //set the category subtype data in category subtype model
                                CategorySubType categorySubType = new CategorySubType();
                                categorySubType.setId(subCategoryId);
                                categorySubType.setParentId(subCategoryParentId);
                                categorySubType.setSubType(subCategoryType);

                                categorySubTypeArrayList.add(categorySubType);

                                //store the categorySubtype object in bundle

                                bundleSubCategory.putSerializable("category", categoryImagesList);
                                bundleSubCategory.putSerializable("categorySubType", categorySubTypeArrayList);

                            }
                        }
                        isData = true;
//                        CategorySingleton.getInstance().setCategoryArrayList(categoryImagesList);

                        //navigate to home fragment on recyclerview item click
                        categoryRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(mContext, categoryRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                CategorySingleton.getInstance().categoryPosition = position;
                                onCategoryClick(position);
                            }

                            @Override
                            public void onLongItemClick(View view, int position) {

                            }
                        }));

                    } else {
                        Log.d(TAG, message);

                    }

                } catch (Exception ex) {
                    Log.d(TAG, "exception: " + ex.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onError: " + error.toString());
            }
        });

        requestQueue = Volley.newRequestQueue(mContext);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }

    private void onCategoryClick(int position) {
        Log.d(TAG, "onCategoryClick: position: " + position);

        String item = categoryImagesList.get(position).getIsFood();
        String categoryId = categoryImagesList.get(position).getId();
        String catName = categoryImagesList.get(position).getName();

        for (int i = 0; i < categoryImagesList.size(); i++) {
            if (categoryImagesList.get(i).getCheckDefault().equals("1")) {
                categoryImagesList.get(i).setCheckDefault("0");
                break;
            }
        }

        categoryImagesList.get(position).setCheckDefault("1");
        CategorySingleton.getInstance().categoryId = categoryId;
        CategorySingleton.getInstance().isFood = Integer.parseInt(item);
        CategorySingleton.getInstance().setCategoryArrayList(categoryImagesList);
        categoryAdapter.notifyDataSetChanged();

        bundleCategories.putString("categoryId", categoryId);

        //save category in shared preferences
        mPref.setCategoryName(CategorySingleton.getInstance().getCategoryArrayList().get(position).getName());

        selectedFragment = new FlowerHomeFragment2();
        selectedFragment.setArguments(bundleCategories);
        if (getFragmentManager() != null) {
            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.main_content, selectedFragment)
                    .commit();
        }
    }

    //get flower listing from server
    private void getFlowerListing(String typeId, double latitude, double longitude, String languageId) {
        String url = API.getAPI() + "listings?" + "type_id=" + typeId + "&user_longitude=" + longitude + "&user_latitude=" + latitude + "&language_id=" + languageId;

        Log.d(TAG, "url " + url);

        avl.setVisibility(View.VISIBLE);

        RequestQueue requestQueue;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d(TAG, response.toString());
                avl.setVisibility(View.GONE);

                try {
                    flowerHomeArrayList.clear();
                    int status = response.getInt("status");
                    String message = response.getString("message");
                    if (status == 1) {
                        rlNoResult.setVisibility(View.GONE);

                        JSONArray dataArray = response.getJSONArray("data");

                        for (int i = 0; i < dataArray.length(); i++) {

                            JSONObject jsonObject = dataArray.getJSONObject(i);
                            listingId = jsonObject.getString("id");
                            listingName = jsonObject.getString("listing_name");
                            listingImage = jsonObject.getString("image");
                            listingCaption = jsonObject.getString("listing_caption");

                            String imageUrl = API.getImageAPI() + listingImage;

                            Flower flower = new Flower(listingId, imageUrl, listingName, listingCaption);

                            //add data on flower home arraylist
                            flowerHomeArrayList.add(flower);

                            //set the flower home listing
                            flowerHomeListAdapter = new FlowerHomeListAdapter(mContext, flowerHomeArrayList);
                            flowerListRecyclerView.setAdapter(flowerHomeListAdapter);
                        }

                        //navigate to flower listing fragment on recyclerview item click
                        flowerListRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(mContext, flowerListRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                Log.d("isItemClick1", isItemClicked + "");
                                if (!isItemClicked) {
                                    Flower item = flowerHomeArrayList.get(position);

                                    String itemId = item.getId();
                                    String itemName = item.getName();
                                    String itemCaption = item.getCaption();
                                    String itemImage = item.getImages();

                                    bundleCategories.putString("flower_id", itemId);
                                    bundleCategories.putString("flower_name", itemName);
                                    bundleCategories.putString("flower_caption", itemCaption);
                                    bundleCategories.putString("flower_image", itemImage);

                                    //set vendor name in shared preference
                                    mPref.setListingName(itemName);

                                    selectedFragment = new FlowerListingFragment();
                                    selectedFragment.setArguments(bundleCategories);

                                    if (getFragmentManager() != null) {
                                        getFragmentManager()
                                                .beginTransaction()
                                                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                                                .replace(R.id.main_content, selectedFragment, "flower_home")
                                                .addToBackStack("flower_home")
                                                .commit();
                                    }

                                    isItemClicked = false;
                                    Log.d("isItemClick2", isItemClicked + "");

                                    //timer to make isItemClicked = false
                                    new CountDownTimer(1000, 1000) {
                                        @Override
                                        public void onTick(long l) {

                                        }

                                        @Override
                                        public void onFinish() {
                                            isItemClicked = false;
                                            Log.d("isItemClick3", isItemClicked + "");
                                        }
                                    }.start();
                                }
                            }

                            @Override
                            public void onLongItemClick(View view, int position) {

                            }
                        }));

                    } else {
                        rlNoResult.setVisibility(View.VISIBLE);
//                        Snackbar snackbar = Snackbar.make(snackbarView, message, Snackbar.LENGTH_SHORT);
//                        View getSnackbarView = snackbar.getView();
//                        getSnackbarView.setBackgroundColor(getResources().getColor(R.color.white));
//                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
//                        tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
//                        snackbar.show();
                    }

                } catch (Exception ex) {
                    Log.d(TAG, "onException" + ex.toString());

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                avl.setVisibility(View.GONE);
                Log.d(TAG, "onErrorResponse:" + error.toString());

            }
        });

        requestQueue = Volley.newRequestQueue(mContext);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED)
                Snackbar.make(snackbarView, "Permission denied by user", Snackbar.LENGTH_LONG).show();
            else if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                startLocationUpdates();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        // Request location updates
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_COARSE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(mContext,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);

                return;
            }
        }

        startLocationUpdates();

        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (mLocation == null) {
            startLocationUpdates();
        }

        if (mLocation != null) {
            // mLatitudeTextView.setText(String.valueOf(mLocation.getLatitude()));
            //mLongitudeTextView.setText(String.valueOf(mLocation.getLongitude()));
        } else {
//            Toast.makeText(activity, , Toast.LENGTH_SHORT).show();
            Log.d(TAG, "Location not Detected");
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Connection Suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.i(TAG, "Connection failed. Error: " + connectionResult.getErrorCode());

    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    private void startLocationUpdates() {
        // Create the location request
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL);
        // Request location updates
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_COARSE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(mContext,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);

//                nearbyGymLayout.setVisibility(View.GONE);
                return;
            }
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                mLocationRequest, this);
        Log.d("reque", "--->>>>");
    }

    @Override
    public void onLocationChanged(Location location) {
        String msg = "Updated Location: " +
                location.getLatitude() + "," +
                location.getLongitude();

        Log.d(TAG, "onLocationChanged: " + msg);

        if (location.getLatitude() > 0 && location.getLongitude() > 0) {
            userAddress(location.getLatitude(), location.getLongitude());
            // get category bundle data
            getBundleCategory = getArguments();

            //get type id from bundle
            if (getBundleCategory != null) {

                typeId = getBundleCategory.getString("categoryId");

                String singletonId = CategorySingleton.getInstance().categoryId;
                int singletonIsFood = CategorySingleton.getInstance().isFood;

                // get listing from server
                if (singletonIsFood == 0) {
                    if (singletonId != null) {
                        getFlowerListing(singletonId, location.getLatitude(), location.getLongitude(), languageId);
                    } else {
                        getFlowerListing(typeId, location.getLatitude(), location.getLongitude(), languageId);
                    }
                } else {
                    selectedFragment = new HomeFragment();
                    if (getFragmentManager() != null) {
                        getFragmentManager()
                                .beginTransaction()
                                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                                .replace(R.id.main_content, selectedFragment)
                                .commit();
                    }
                }

                Log.d(TAG, "bundle values " + " typeId " + typeId + " Singleton ID: " + CategorySingleton.getInstance().categoryId);
            } else {
                Log.d(TAG, "Bundle is empty");
            }

            Log.d(TAG, "catId:" + typeId);
        }

        Log.d(TAG, "onLocationChanged Lat: " + location.getLatitude() + " long: " + location.getLongitude());
    }

    private void checkLocation() {
        if (!isLocationEnabled()) {
            showAlert();
        } else
            isLocationEnabled();
    }

    private boolean isLocationEnabled() {
        mLocationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        assert mLocationManager != null;
        return mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private void showAlert() {
        final androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(mContext);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.location_dialog, null);
        builder.setView(dialogView);

        final androidx.appcompat.app.AlertDialog alertDialog = builder.show();

        Button btnLocation = dialogView.findViewById(R.id.btn_location_dialog_settings);
        Button btnCancel = dialogView.findViewById(R.id.btn_location_dialog_cancel);

        btnLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(myIntent);
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
    }

    private void userAddress(double lat, double longg) {
        try {
            Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(lat, longg, 1);

            String address = addresses.get(0).getAddressLine(0);
            String locality = addresses.get(0).getLocality();
            String subLocality = addresses.get(0).getSubLocality();
            String premises = addresses.get(0).getPremises();

//            mPref.saveUserArea(address);

            Log.d(TAG, "Address: " + address + "\n" + "Locality: " + locality + "\n" + "Sub Locality: " + subLocality + "\n" + "Premises: " + premises);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}