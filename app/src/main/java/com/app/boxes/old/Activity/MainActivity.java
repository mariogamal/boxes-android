package com.app.boxes.old.Activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.boxes.old.API.API;
import com.app.boxes.old.Adapter.DrawerItemAdapter;
import com.app.boxes.old.Listener.RecyclerItemClickListener;
import com.app.boxes.old.Model.DrawerMenu;
import com.app.boxes.old.Model.Language;
import com.app.boxes.old.Model.User;
import com.app.boxes.old.OnBackPressListener;
import com.app.boxes.R;
import com.app.boxes.old.RestaurantSearchSingleton;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.app.boxes.old.UI.LoginBottomSheet;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import android.os.CountDownTimer;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.view.GravityCompat;
import androidx.appcompat.app.ActionBarDrawerToggle;

import android.view.LayoutInflater;
import android.view.MenuItem;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.navigation.NavController;
import androidx.navigation.NavOptions;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";
    private String languageId = "";
    public Toolbar toolbar;
    public TextView textViewToolbar, tvDrawerName, tvCartCount;
    private View snackbarView;
    private CircleImageView circleImageView;
    private ImageView logo;
    private BottomNavigationView bottomNavigationView;
    private DrawerLayout drawer;
    public FrameLayout cartLayout, loaderLayout;
    private NavigationView navigationView;
    private RecyclerView rvDrawerMenuItems;
    private DrawerItemAdapter drawerItemAdapter;

    private ArrayList<DrawerMenu> drawerMenuItems = new ArrayList<>();
    private boolean isItemClicked = false;
    private OnBackPressListener listener;
    private User user;
    private User.AccessToken token;
    private MySharedPreferences sPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sPref = new MySharedPreferences(this);

        sPref.saveFirstTime(false);

        toolbar = findViewById(R.id.mainToolbar);
        setSupportActionBar(toolbar);

        textViewToolbar = findViewById(R.id.tv_toolbar_title);
        textViewToolbar.setVisibility(View.GONE);
        textViewToolbar.setText(R.string.boxes);

        snackbarView = findViewById(android.R.id.content);

        drawer = findViewById(R.id.drawerLayout);

        //check if language and access token is not null then load the cart badge
        Language language = sPref.getLangObject();
        languageId = language.getId();

        token = sPref.getAccessTokenObject();

//        bottomNavigationView = findViewById(R.id.bnv_navigation);
        BottomNavigationMenuView navigationMenuView = (BottomNavigationMenuView) bottomNavigationView.getChildAt(0);

//        navigationView = findViewById(R.id.nav_view);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        // find views of header user name and image
        circleImageView = findViewById(R.id.civ_drawer_profile_img);
        tvDrawerName = findViewById(R.id.tv_drawer_name);

//        cartLayout = findViewById(R.id.main_activity_cart);
        cartLayout.setVisibility(View.GONE);
        loaderLayout = findViewById(R.id.loader_layout);
        loaderLayout.setVisibility(View.GONE);
//        tvCartCount = findViewById(R.id.tv_main_activity_notif_badge);

        //set the drawer menu item in navigation
//        rvDrawerMenuItems = findViewById(R.id.rv_drawer_menu_items);
        rvDrawerMenuItems.setLayoutManager(new LinearLayoutManager(this));

        cartLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int mCartCount;
                // check if cart count text contains "+" then replace it
                if (tvCartCount.getText().toString().contains("+")) {
                    String sCartCount = tvCartCount.getText().toString().replace("+", "");
                    mCartCount = Integer.parseInt(sCartCount);
                } else {
                    mCartCount = Integer.parseInt(tvCartCount.getText().toString());
                }

                if (mCartCount > 0) {
                    navigationController(R.id.placeOrderFragment, null);
                } else {
                    Snackbar snackbar = Snackbar.make(snackbarView, "Please add items to see cart", Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(getResources().getColor(R.color.active_tab_color));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(getResources().getColor(R.color.white));
                    snackbar.show();
                }
            }
        });

        user = sPref.getUserObject();
        Log.d(TAG, "User Object: " + user);

        if (user == null) {
            drawerMenuItems.add(new DrawerMenu(R.drawable.drawer_menu_setting_icon, getString(R.string.settings)));
            drawerMenuItems.add(new DrawerMenu(R.drawable.faq_icon, getString(R.string.faqs)));

            cartLayout.setVisibility(View.GONE);

            rvDrawerMenuItems.addOnItemTouchListener(new RecyclerItemClickListener(this, rvDrawerMenuItems, new RecyclerItemClickListener.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    Log.d("isItemClick1", isItemClicked + "");
                    if (!isItemClicked) {
                        switch (position) {
                            case 0:
                                // settings
                                if (isValidDestination(R.id.settingsFragment)) {
                                    NavOptions n = new NavOptions.Builder().setPopUpTo(R.id.flowerHomeFragment, false).build();

                                    navigationController(R.id.settingsFragment, n);
                                }

                                textViewToolbar.setText(R.string.settings);
                                break;

                            case 1:
                                // faqs
                                if (isValidDestination(R.id.faqFragment)) {
                                    NavOptions n = new NavOptions.Builder().setPopUpTo(R.id.flowerHomeFragment, false).build();

                                    navigationController(R.id.faqFragment, n);
                                }

                                textViewToolbar.setText(R.string.faqs);
                                break;

                            default:

                        }

                        DrawerLayout drawer = findViewById(R.id.drawerLayout);
                        drawer.closeDrawer(GravityCompat.START);

                        isItemClicked = true;
                        Log.d("isItemClick2", isItemClicked + "");

                        //timer to make isItemClicked = false
                        new CountDownTimer(1000, 1000) {
                            @Override
                            public void onTick(long l) {

                            }

                            @Override
                            public void onFinish() {
                                isItemClicked = false;
                                Log.d("isItemClick3", isItemClicked + "");
                            }
                        }.start();
                    }
                }

                @Override
                public void onLongItemClick(View view, int position) {

                }
            }));
        } else {
            drawerMenuItems.add(new DrawerMenu(R.drawable.order_history_icon, getString(R.string.order_history)));
            drawerMenuItems.add(new DrawerMenu(R.drawable.packages_icon, getString(R.string.my_package)));
            drawerMenuItems.add(new DrawerMenu(R.drawable.order_history_icon, getString(R.string.notification)));
            drawerMenuItems.add(new DrawerMenu(R.drawable.contact_icon, getString(R.string.contact_us)));
            drawerMenuItems.add(new DrawerMenu(R.drawable.drawer_menu_setting_icon, getString(R.string.settings)));
            drawerMenuItems.add(new DrawerMenu(R.drawable.faq_icon, getString(R.string.faqs)));
            drawerMenuItems.add(new DrawerMenu(R.drawable.logout_icon, getString(R.string.logout)));

            rvDrawerMenuItems.addOnItemTouchListener(new RecyclerItemClickListener(this, rvDrawerMenuItems, new RecyclerItemClickListener.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    RestaurantSearchSingleton.getInstance().reinit();
                    Log.d("isItemClick1", isItemClicked + "");
                    if (!isItemClicked) {
                        switch (position) {
                            case 0:
                                // order history
                                if (true
//                                        bottomNavigationView.getSelectedItemId() == R.id.nav_menu_home
//                                        || bottomNavigationView.getSelectedItemId() == R.id.nav_menu_box
//                                        || bottomNavigationView.getSelectedItemId() == R.id.nav_menu_history
                            ) {
                                    if (isValidDestination(R.id.orderHistoryFragment)) {
                                        NavOptions n = new NavOptions.Builder().setPopUpTo(R.id.flowerHomeFragment, false).build();
                                        navigationController(R.id.orderHistoryFragment, n);
                                    }
                                }
                                textViewToolbar.setText(R.string.order_history);
                                cartLayout.setVisibility(View.GONE);
                                break;

                            case 1:
                                // my packages
                                if (true
//                                        bottomNavigationView.getSelectedItemId() == R.id.nav_menu_home
//                                        || bottomNavigationView.getSelectedItemId() == R.id.nav_menu_box
//                                        || bottomNavigationView.getSelectedItemId() == R.id.nav_menu_history
                                ) {
                                    if (isValidDestination(R.id.myPackagesFragment)) {
                                        NavOptions n = new NavOptions.Builder().setPopUpTo(R.id.flowerHomeFragment, false).build();
                                        navigationController(R.id.myPackagesFragment, n);
                                    }
                                }
                                cartLayout.setVisibility(View.GONE);
                                textViewToolbar.setText(R.string.my_package);
                                break;

                            case 2:
                                // notification
                                if (true
//                                        bottomNavigationView.getSelectedItemId() == R.id.nav_menu_home
//                                        || bottomNavigationView.getSelectedItemId() == R.id.nav_menu_box
//                                        || bottomNavigationView.getSelectedItemId() == R.id.nav_menu_history
                            ) {
                                    if (isValidDestination(R.id.notificationFragment)) {
                                        NavOptions n = new NavOptions.Builder().setPopUpTo(R.id.flowerHomeFragment, false).build();
                                        navigationController(R.id.notificationFragment, n);
                                    }
                                }
                                cartLayout.setVisibility(View.GONE);
                                textViewToolbar.setText(R.string.notification);
                                break;

                            case 3:
                                // contact us
                                if (true
//                                        bottomNavigationView.getSelectedItemId() == R.id.nav_menu_home
//                                        || bottomNavigationView.getSelectedItemId() == R.id.nav_menu_box
//                                        || bottomNavigationView.getSelectedItemId() == R.id.nav_menu_history
                                ) {
                                    if (isValidDestination(R.id.contactUsFragment)) {
                                        NavOptions n = new NavOptions.Builder().setPopUpTo(R.id.flowerHomeFragment, false).build();
                                        navigationController(R.id.contactUsFragment, n);
                                    }
                                }
                                cartLayout.setVisibility(View.GONE);
                                textViewToolbar.setText(R.string.contact_us);
                                break;

                            case 4:
                                // settings
                                if (true
//                                        bottomNavigationView.getSelectedItemId() == R.id.nav_menu_home
//                                        || bottomNavigationView.getSelectedItemId() == R.id.nav_menu_box
//                                        || bottomNavigationView.getSelectedItemId() == R.id.nav_menu_history
                            ) {
                                    if (isValidDestination(R.id.settingsFragment)) {
                                        NavOptions n = new NavOptions.Builder().setPopUpTo(R.id.flowerHomeFragment, false).build();
                                        navigationController(R.id.settingsFragment, n);
                                    }
                                }
                                cartLayout.setVisibility(View.GONE);
                                textViewToolbar.setText(R.string.settings);
                                break;

                            case 5:
                                // faqs
                                if (true
//                                        bottomNavigationView.getSelectedItemId() == R.id.nav_menu_home
//                                        || bottomNavigationView.getSelectedItemId() == R.id.nav_menu_box
//                                        || bottomNavigationView.getSelectedItemId() == R.id.nav_menu_history
                                ) {
                                    if (isValidDestination(R.id.faqFragment)) {
                                        NavOptions n = new NavOptions.Builder().setPopUpTo(R.id.flowerHomeFragment, false).build();
                                        navigationController(R.id.faqFragment, n);
                                    }
                                }
                                cartLayout.setVisibility(View.GONE);
                                textViewToolbar.setText(R.string.faqs);
                                break;

                            case 6:
                                // logout

                                bottomNavigationView.setSelectedItemId(0);
                                if (token != null) {

                                    final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(MainActivity.this);

                                    LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                    View mView = vi.inflate(R.layout.custom_alert_dialog, null);
                                    builder.setView(mView);
                                    builder.setCancelable(false);

                                    final AlertDialog alertDialog = builder.show();

                                    TextView mDialogMsg = mView.findViewById(R.id.tv_custom_dialog_msg);
                                    Button mDialogBtnYes = mView.findViewById(R.id.btn_custom_dialog_yes);
                                    Button mDialogBtnNo = mView.findViewById(R.id.btn_custom_dialog_no);

                                    mDialogMsg.setText("Are you sure you want to logout?");

                                    mDialogBtnNo.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            alertDialog.dismiss();
                                        }
                                    });

                                    mDialogBtnYes.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            alertDialog.dismiss();

                                            String accessToken = token.getAccessToken();
                                            logoutUser(accessToken);
                                        }
                                    });
                                }
                                break;

                            default:

                        }

                        // close the drawer if open
                        drawer.closeDrawer(GravityCompat.START);

                        isItemClicked = true;
                        Log.d("isItemClick2", isItemClicked + "");

                        //timer to make isItemClicked = false
                        new CountDownTimer(1000, 1000) {
                            @Override
                            public void onTick(long l) {

                            }

                            @Override
                            public void onFinish() {
                                isItemClicked = false;
                                Log.d("isItemClick3", isItemClicked + "");
                            }
                        }.start();
                    }
                }

                @Override
                public void onLongItemClick(View view, int position) {

                }
            }));

        }

        drawerItemAdapter = new DrawerItemAdapter(this, drawerMenuItems);
        rvDrawerMenuItems.setAdapter(drawerItemAdapter);

        //setting custom drawer navigation icon
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.menu_icon);
        getSupportActionBar().show();

        if (sPref.getAccessTokenObject() != null) {
            token = sPref.getAccessTokenObject();

            String accessToken;
            if (token != null) {
                accessToken = token.getAccessToken();
                getAndSetUserDetails(accessToken);
            }
        }

        init();
    }

    public void updateBackPress(OnBackPressListener listener) {
        this.listener = listener;
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawerLayout);

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (listener != null) {
            listener.onMyBackPress();
        } else {
            int des = Navigation.findNavController(this, R.id.main_content).getCurrentDestination().getId();
            Log.d(TAG, "onBackPressed: destinationID: " + des);

            // IDs of home fragment, food fragment, calendar fragment
            if (des == R.id.flowerHomeFragment) {
                closeDialog();
            } else {
                super.onBackPressed();
            }
        }
    }

    private LoginBottomSheet loginBottomSheet() {
        LoginBottomSheet bottomSheet = new LoginBottomSheet();
        bottomSheet.setCancelable(false);
        bottomSheet.show(getSupportFragmentManager(), "login_bottom_sheet");

        return bottomSheet;
    }

    // get user details and set image and user name
    private void getAndSetUserDetails(final String accessToken) {
        final String url = API.getAPI() + "auth/user";
        Log.d(TAG, "User URL: " + url);

        RequestQueue requestQueue;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "User onResponse: " + response);

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    int status = jsonObject.getInt("status");

                    if (status == 1) {
                        JSONObject data = jsonObject.getJSONObject("data");

                        String id = data.optString("id");
                        String first_name = data.optString("first_name");
                        String last_name = data.optString("last_name");
                        String getEmail = data.optString("email");
                        String getPhoneNumber = data.optString("phone");
                        String getImage = data.getString("image");

                        User user = new User();

                        user.setId(id);
                        if (!first_name.equals("null") && !last_name.equals("null")) {
                            tvDrawerName.setText(first_name + " " + last_name);
                            user.setFirstName(first_name);
                            user.setLastName(last_name);
                        } else
                            tvDrawerName.setText("Name");

                        if (!getEmail.equals("null"))
                            user.setEmail(getEmail);

                        if (!getPhoneNumber.equals("null"))
                            user.setPhoneNumber(getPhoneNumber);

                        String imageUrl;
                        if (getImage.equals("null")) {
                            circleImageView.setImageResource(R.drawable.avatar);
                        } else if (getImage.equals("")) {
                            circleImageView.setImageResource(R.drawable.avatar);
                        } else {
                            imageUrl = API.getImageAPI() + getImage;
                            Picasso.get().load(imageUrl).fit().centerCrop().into(circleImageView);
                            user.setImage(imageUrl);
                        }
                        sPref.saveUserObject(user);
                        Log.d("User-Response", "Id: " + id + " first_name: " + first_name + " last_name: " + last_name + "  email: " + getEmail + " phone: " + getPhoneNumber);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Log.d(TAG, "Volley Error: " + error.toString());
                if (error instanceof NoConnectionError) {
                    Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.no_network), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
                    snackbar.show();
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (accessToken != null) {
                    params.put("Authorization", accessToken);
                    Log.d(TAG, "getHeaders " + accessToken);
                }
                return params;
            }
        };
        requestQueue = Volley.newRequestQueue(this);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    // logging out user from app and server
    void logoutUser(final String accessToken) {
        String URL = API.getAPI() + "auth/logout";
        Log.d(TAG, "Logout URL : " + URL);

        RequestQueue requestQueue;
        StringRequest stringRequest = new StringRequest(Request.Method.POST, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "onResponse " + response);

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    int status = jsonObject.optInt("status");
                    String msg = jsonObject.optString("message");

                    if (status == 1) {
                        sPref.clearAllPreferences();

                        Snackbar snackbar = Snackbar.make(snackbarView, msg, Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
                        snackbar.show();

                        new CountDownTimer(2000, 1000) {

                            @Override
                            public void onTick(long l) {

                            }

                            @Override
                            public void onFinish() {
                                startActivity(new Intent(MainActivity.this, SignInActivity.class));
                                finish();
                            }
                        }.start();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse " + error.toString());

                if (error instanceof NoConnectionError) {
                    Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.no_network), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
                    snackbar.show();
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (accessToken != null) {
                    params.put("Authorization", accessToken);
                    Log.d(TAG, "getHeaders " + accessToken);
                }
                return params;
            }
        };
        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    //bottom navigation menu item
    BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            try {
                int id = item.getItemId();
                if (id == R.id.home) {
                    NavOptions navOptions = new NavOptions.Builder().setPopUpTo(R.id.flowerHomeFragment, false).build();
                    if (isValidDestination(R.id.flowerHomeFragment))
                        navigationController(R.id.flowerHomeFragment, navOptions);
                    getSupportActionBar().show();
                    RestaurantSearchSingleton.getInstance().reinit();
                } else if (id == R.id.home) {
                    /*if (user != null) {
                        NavOptions navOptions = new NavOptions.Builder().setPopUpTo(R.id.flowerHomeFragment, false).build();
                        if (isValidDestination(R.id.foodFragment))
                            navigationController(R.id.foodFragment, navOptions);
                        textViewToolbar.setText(R.string.food);
                    } else {
                        loginBottomSheet();
                    }*/
                } else if (id == R.id.profile) {
                    if (user != null) {
                        NavOptions navOptions = new NavOptions.Builder().setPopUpTo(R.id.flowerHomeFragment, false).build();
                        if (isValidDestination(R.id.profileFragment))
                            navigationController(R.id.profileFragment, navOptions);
                        textViewToolbar.setText(R.string.user_profile);
                        RestaurantSearchSingleton.getInstance().reinit();
                    } else {
                        loginBottomSheet();
                    }
                }
            } catch (Exception ex) {
                Log.d("BottomNavigationView", ex.toString());
            }
            return true;
        }
    };

    private AlertDialog closeDialog() {
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(MainActivity.this);

        LayoutInflater vi = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View mView = vi.inflate(R.layout.custom_alert_dialog, null);
        builder.setView(mView);
        builder.setCancelable(false);

        final AlertDialog alertDialog = builder.show();

        TextView mDialogMsg = mView.findViewById(R.id.tv_custom_dialog_msg);
        Button mDialogBtnYes = mView.findViewById(R.id.btn_custom_dialog_yes);
        Button mDialogBtnNo = mView.findViewById(R.id.btn_custom_dialog_no);

        mDialogMsg.setText("Are you sure you want to exit?");

        mDialogBtnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        mDialogBtnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();

                finish();
            }
        });

        return alertDialog;
    }

    private void init() {
        NavController navController = Navigation.findNavController(this, R.id.main_content);
        NavigationUI.setupWithNavController(bottomNavigationView, navController);
        NavigationUI.setupWithNavController(navigationView, navController);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        if (SplashActivity.shouldShowProfile)
            bottomNavigationView.setSelectedItemId(R.id.profile);
    }

    public boolean isValidDestination(int des) {
        return des != (Navigation.findNavController(MainActivity.this, R.id.main_content).getCurrentDestination()).getId();
    }

    public void navigationController(int fragmentId, NavOptions navOptions) {
        if (navOptions != null)
            Navigation.findNavController(MainActivity.this, R.id.main_content).navigate(fragmentId, null, navOptions);
        else
            Navigation.findNavController(MainActivity.this, R.id.main_content).navigate(fragmentId);
    }

    public void navigationController(int fragmentId, Bundle bundle, NavOptions navOptions) {
        if (bundle != null)
            Navigation.findNavController(MainActivity.this, R.id.main_content).navigate(fragmentId, bundle, navOptions);
    }

    public void shahzain() {
        if (user != null) {
            NavOptions navOptions = new NavOptions.Builder().setPopUpTo(R.id.flowerHomeFragment, false).build();
            if (isValidDestination(R.id.foodFragment))
                navigationController(R.id.foodFragment, navOptions);
            textViewToolbar.setText(R.string.food);
        } else {
            loginBottomSheet();
        }
    }
}