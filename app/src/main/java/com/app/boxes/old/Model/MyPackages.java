package com.app.boxes.old.Model;

public class MyPackages {
    private String PlanName,StartDate,EndDate,PackageType;

    public MyPackages() {
    }

    public MyPackages(String planName, String startDate, String endDate, String packageType) {
        PlanName = planName;
        StartDate = startDate;
        EndDate = endDate;
        PackageType = packageType;
    }

    public String getPlanName() {
        return PlanName;
    }

    public void setPlanName(String planName) {
        PlanName = planName;
    }

    public String getStartDate() {
        return StartDate;
    }

    public void setStartDate(String startDate) {
        StartDate = startDate;
    }

    public String getEndDate() {
        return EndDate;
    }

    public void setEndDate(String endDate) {
        EndDate = endDate;
    }

    public String getPackageType() {
        return PackageType;
    }

    public void setPackageType(String packageType) {
        PackageType = packageType;
    }
}
