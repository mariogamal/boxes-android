package com.app.boxes.old.Model;

import java.util.ArrayList;

public class Menu {

    public String Category;
    private int outOff, total;
    public ArrayList<MenuItem> menuItems = new ArrayList<>();

//    public Menu(String category, ArrayList<MenuItem> menuItems) {
//        Category = category;
//        this.menuItems = menuItems;
//    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }

    public int getOutOff() {
        return outOff;
    }

    public void setOutOff(int outOff) {
        this.outOff = outOff;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public ArrayList<MenuItem> getMenuItems() {
        return menuItems;
    }
}
