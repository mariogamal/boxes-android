package com.app.boxes.old.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.boxes.old.Model.Faq;
import com.app.boxes.R;

import java.util.ArrayList;

public class ExpandableAdapter extends RecyclerView.Adapter<ExpandableAdapter.ViewHolder> {


    private Context context;
    private ArrayList<Faq> faqArrayList = new ArrayList<>();

    public ExpandableAdapter(Context context, ArrayList<Faq> faqs) {
        this.context = context;
        this.faqArrayList = faqs;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.expandable_faq_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        holder.tvHeader.setText(faqArrayList.get(position).getQuestions());
        holder.tvChild.setText(faqArrayList.get(position).getAnswer());
    }

    @Override
    public int getItemCount() {
        return faqArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvHeader, tvNumber, tvChild;
        ImageView ivDropDown;
        LinearLayout dropDownLinearLayout, childLinearLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvHeader = itemView.findViewById(R.id.tv_list_title);
            tvChild = itemView.findViewById(R.id.tv_text_subtitle);

            ivDropDown = itemView.findViewById(R.id.iv_list_icon);

            dropDownLinearLayout = itemView.findViewById(R.id.drop_down_linear_layout);
            childLinearLayout = itemView.findViewById(R.id.child_linear_layout);

            dropDownLinearLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (childLinearLayout.getVisibility() != View.VISIBLE) {
                        childLinearLayout.setVisibility(View.VISIBLE);
                        ivDropDown.setImageResource(R.drawable.arrow_down_up);
                    } else {
                        childLinearLayout.setVisibility(View.GONE);
                        ivDropDown.setImageResource(R.drawable.arrow_down);
                    }
                }
            });
        }
    }

}
