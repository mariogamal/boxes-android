package com.app.boxes.old.Fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.boxes.old.API.API;
import com.app.boxes.old.Activity.MainActivity;
import com.app.boxes.old.Adapter.CalenderMenuItemAdapter;
import com.app.boxes.old.Adapter.DayOfTheWeekAdapter;
import com.app.boxes.old.Adapter.SubscribedRestaurantAdapter;
import com.app.boxes.old.Model.DaysOfTheWeek;
import com.app.boxes.old.Model.DeliveryDates;
import com.app.boxes.old.Model.Menu;
import com.app.boxes.old.Model.MenuItem;
import com.app.boxes.old.Model.SubscribedRestaurant;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONObject;
import org.threeten.bp.LocalDate;
import org.threeten.bp.Period;
import org.threeten.bp.format.DateTimeFormatter;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import sun.bob.mcalendarview.CellConfig;
import sun.bob.mcalendarview.MarkStyle;
import sun.bob.mcalendarview.listeners.OnDateClickListener;
import sun.bob.mcalendarview.views.ExpCalendarView;
import sun.bob.mcalendarview.vo.DateData;

/**
 * A simple {@link Fragment} subclass.
 */
public class CalenderDetailsFragment extends Fragment implements
        DayOfTheWeekAdapter.DayOfTheWeekClickListener,
        DayOfTheWeekAdapter.DayOfTheWeekPauseClickListener {

    private Activity mContext;

    private String TAG = "CalenderDetailsFragment";
    private String listingId = "", languageId = "", restaurantName = ""/*menuItemId = ""/*, status = ""*/;

    private boolean isShow;
    private boolean isSelected = false;
    private int scrollRange;

    //widgets
//    private DiscreteScrollView dayrecyclerView, subscribedRestaurantRecyclerView;
    private ExpandableListView elMenuList;
    private Button btnContinue, btnUpdateOrder;
    private ImageView /*ivScrollLeft, */ivPauseResume, ivBtnBack;
    private ExpCalendarView mCalendarView;
    private AppBarLayout appBarLayout;
    private CollapsingToolbarLayout collapsingToolbar;
    private RelativeLayout dateCont;
    private LinearLayout parallaxCont, llPauseResume, llLoader;
    private TextView tvToolbarDate, tvPause;
    private View snackbarView, rootView;
    private TextView tvRestaurantName;

    //custom adapters
    private CalenderMenuItemAdapter menuItemAdapter;
    private DayOfTheWeekAdapter adapter;
    private SubscribedRestaurantAdapter subscribedRestaurantAdapter;

    //region arraylist of model classes
    private ArrayList<DaysOfTheWeek> daysOfTheWeekArrayList = new ArrayList<>();
    private ArrayList<SubscribedRestaurant> subscribedRestaurantArrayList = new ArrayList<>();
    private ArrayList<Menu> menuCategoryArrayList = new ArrayList<>();
    private ArrayList<String> selectedMenuIdArrayList = new ArrayList<>();
    private ArrayList<DeliveryDates> deliveryDatesArrayList = new ArrayList<>();
    private ArrayList<DeliveryDates> allDatesArrayList = new ArrayList<>();
    //endregion

    //shared preference
    private MySharedPreferences mPref;

    private LinearLayoutManager linearLayoutManager;

    //    private String deliveryStatus = "", menuCategoryName = "", orderId = "";
    private String userId = "", menuCategoryName = "", orderId = "", deliveryStatus = "", accessToken = "";
    private String selectedDate = "";

    private int thresholdInDays = 0, outOff = 0;

    private Map<Integer, String> MONTHS = new HashMap<Integer, String>();

    public CalenderDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_calendar_details, container, false);

        //hide the toolbar
        ((MainActivity) mContext).getSupportActionBar().hide();

        //initialize widgets
        Initializer(rootView);

        // get the data from bundle
        Bundle getData = getArguments();

        //check if language is arabic then change the back icon to forward icon
        if (mPref.getLocale().equals("ar")) {
            ivBtnBack.setImageResource(R.drawable.ic_arrow_forward_white);
        }

        //if bundle data and shared preference is not null then get restaurant id and language id
        if (getData != null && mPref.getLangObject() != null) {
            int date = getData.getInt("current_date");
            listingId = getData.getString("restaurant_id");
            thresholdInDays = getData.getInt("threshold_days");
            deliveryStatus = getData.getString("delivery_status");
            orderId = getData.getString("order_id");
            languageId = mPref.getLangObject().getId();
            userId = mPref.getUserObject().getId();
            restaurantName = getData.getString("restaurant_name");

            tvRestaurantName.setText(restaurantName + " Menu");

            Log.d(TAG, "get data: userID: " + userId);

            if (deliveryStatus != null) {
                if (deliveryStatus.contains("pause")) {
                    llPauseResume.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.btn_green_bg));
                    llPauseResume.setVisibility(View.VISIBLE);
                    tvPause.setText("Resume");
                    ivPauseResume.setImageResource(R.drawable.ic_play_arrow_black_24dp);
                    llPauseResume.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (mPref.getAccessTokenObject() != null)
                                pauseMeal(orderId, "pending", mPref.getAccessTokenObject().getAccessToken());
                        }
                    });

                    btnContinue.setVisibility(View.GONE);
                    btnUpdateOrder.setVisibility(View.VISIBLE);
                } else if (deliveryStatus.contains("pending")) {
                    llPauseResume.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.btn_red_bg));
                    llPauseResume.setVisibility(View.VISIBLE);
                    ivPauseResume.setImageResource(R.drawable.ic_pause_black_24dp);
                    tvPause.setText("Pause");
                    llPauseResume.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (mPref.getAccessTokenObject() != null)
                                pauseMeal(orderId, "pause", mPref.getAccessTokenObject().getAccessToken());
                        }
                    });

                    btnContinue.setVisibility(View.GONE);
                    btnUpdateOrder.setVisibility(View.VISIBLE);
                } else {
                    llPauseResume.setVisibility(View.GONE);
                }
            } else {
                llPauseResume.setVisibility(View.GONE);
            }
        } else {
            Log.d(TAG, "no data found");
        }

        // get menu list
        if (listingId != null)
            getMenuList(listingId, languageId, userId, selectedDate);

        //navigate to calendar fragment
        ivBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getFragmentManager() != null) {
                    getFragmentManager().beginTransaction().replace(R.id.main_content, new CalenderFragment()).commit();
                    getFragmentManager().popBackStackImmediate();
                }
            }
        });

        //region date click listener
        mCalendarView.setOnDateClickListener(new OnDateClickListener() {
            @Override
            public void onDateClick(View view, DateData date) {
                outOff = 0;
                //muneeb   mCalendarView.expand();

                selectedDate = date.getYear() + "-" + (date.getMonth() > 9 ? date.getMonth() : "0" + date.getMonth()) + "-" + (date.getDay() > 9 ? date.getDay() : "0" + date.getDay());
                Log.d(TAG, "selectedDate selected date: " + selectedDate);

                int year = Integer.parseInt(new SimpleDateFormat("yyyy", Locale.getDefault()).format(new Date()));
                int month = Integer.parseInt(new SimpleDateFormat("MM", Locale.getDefault()).format(new Date()));
                int cdate = Integer.parseInt(new SimpleDateFormat("dd", Locale.getDefault()).format(new Date()));

                LocalDate temp1 = LocalDate.of(date.getYear(), date.getMonth(), date.getDay());
                LocalDate localDate = LocalDate.of(year, month, cdate);

                Log.d("strDate", temp1.getYear() + "-" + temp1.getMonthValue() + "-" + temp1.getDayOfMonth() + "\nlocal date: " + localDate);

                if ((date.getMarkStyle().getColor() == mContext.getResources().getColor(R.color.alpha_green))
                        || (date.getMarkStyle().getColor() == mContext.getResources().getColor(R.color.alpha_red))
                        || (date.getMarkStyle().getColor() == mContext.getResources().getColor(R.color.alpha_blue))) {

                    Snackbar snackbar = Snackbar.make(snackbarView, "You can't select date from other restaurant", Snackbar.LENGTH_LONG);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                    snackbar.show();

                } else if (temp1.isAfter(localDate)) { // check if selected date is after current date
                    // prevent from selecting delivered date
                    if (date.getMarkStyle().getColor() == mContext.getResources().getColor(R.color.blue)) {

                        Snackbar snackbar = Snackbar.make(snackbarView, "Please select date other than delivered", Snackbar.LENGTH_LONG);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                        snackbar.show();
                    } else {
                        int daysAdded = 0;

                        LocalDate clickDate = LocalDate.of(date.getYear(), date.getMonth(), date.getDay());
                        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("EEE, MMM dd, yyyy");
                        String forToolbar = clickDate.format(formatter1);

                        mPref.saveSelectedDay(forToolbar);

                        // collapse the calender on date click
//                        appBarLayout.setExpanded(false, true);

                        Period intervalPeriod = Period.between(localDate, temp1);
                        Log.d("diff", String.valueOf(intervalPeriod.getDays()));

                        if (date.getYear() == year && date.getMonth() == month)
                            daysAdded = cdate + thresholdInDays;

                        Log.d(TAG, "onDateClick: threshold date: " + daysAdded);

//                if (intervalPeriod.getDays() > thresholdInDays - 1) {

                        Log.i("Size", "" + mCalendarView.getMarkedDates().getAll().size());
                        for (int i = 0; i < mCalendarView.getMarkedDates().getAll().size(); i++) {
                            //for laga k sab peelay unmark
                            Log.i("For Loop Dates", "" + mCalendarView.getMarkedDates().getAll().get(i).getDay() + mCalendarView.getMarkedDates().getAll().get(i).getMonth() + mCalendarView.getMarkedDates().getAll().get(i).getYear());
                            if (mCalendarView.getMarkedDates().getAll().get(i).getMarkStyle().getColor() == mContext.getResources().getColor(R.color.mint_orange)) {
                                mCalendarView.unMarkDate(mCalendarView.getMarkedDates().getAll().get(i));
                                break;
                            }
                        }

                        populateCalender(deliveryDatesArrayList, true);
                        populateCalender(allDatesArrayList, false);
                        // after removing previous selection, mark a new selection
                        //     mCalendarView.markDate(new DateData(date.getYear(), date.getMonth(), date.getDay()).setMarkStyle(new MarkStyle(MarkStyle.BACKGROUND, getResources().getColor(R.color.green))));

//                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
//                        MarkStyle style = new MarkStyle(MarkStyle.BACKGROUND,mContext.getResources().getColor(R.color.mint_orange,null));
//                        mCalendarView.markDate(date.setMarkStyle(style));
//                    }

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            mCalendarView.unMarkDate(date);
                            mCalendarView.markDate(new DateData(date.getYear(), date.getMonth(), date.getDay()).setMarkStyle(new MarkStyle(MarkStyle.BACKGROUND, mContext.getResources().getColor(R.color.mint_orange, null))));
                        }
                        // mCalendarView.markDate(new DateData(date.getYear(), date.getMonth(), date.getDay()).setMarkStyle(new MarkStyle(MarkStyle.BACKGROUND, getResources().getColor(R.color.mint_orange))));
                        mPref.saveSelectedDate(date.getDay());
                        mPref.saveSelectedMonth(date.getMonth());
                        mPref.saveSelectedYear(date.getYear());

                        btnContinue.setVisibility(View.VISIBLE);
                        btnUpdateOrder.setVisibility(View.GONE);

                        // check if date.getmonth length equals to 1 digit then concat "0" before month count
                        if (mPref.getAccessTokenObject() != null)
                            checkedPreOrderMeals(selectedDate, languageId, mPref.getAccessTokenObject().getAccessToken());

                        getMenuList(listingId, languageId, userId, selectedDate);

                        if (date.getMarkStyle().getColor() == mContext.getResources().getColor(R.color.green)) {
                            for (final DeliveryDates dates : deliveryDatesArrayList) {
                                int mDay = dates.getDeliveryDay();
                                if (date.getDay() == mDay) {
                                    mPref.saveSelectedDate(date.getDay());
                                    mPref.saveSelectedMonth(date.getMonth());
                                    mPref.saveSelectedYear(date.getYear());

                                    deliveryStatus = dates.getDeliveryStatus();
                                    orderId = dates.getOrderDetailId();

                                    Log.d(TAG, "onDateClick: pending status order id: " + orderId);

                                    if (dates.getDeliveryStatus().contains("pending")) {
                                        llPauseResume.setVisibility(View.VISIBLE);
                                        llPauseResume.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.btn_red_bg));
                                        ivPauseResume.setImageResource(R.drawable.ic_pause_black_24dp);
                                        tvPause.setText("Pause");
                                        llPauseResume.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                if (mPref.getAccessTokenObject() != null)
                                                    orderId = dates.getOrderDetailId();
                                                pauseMeal(orderId, "pause", mPref.getAccessTokenObject().getAccessToken());
                                            }
                                        });
                                    }

                                    btnContinue.setVisibility(View.GONE);
                                    btnUpdateOrder.setVisibility(View.VISIBLE);

                            /*add break when condition is true
                            and it needs to be tested*/

                                }
                                Log.d(TAG, "onDateClick: arraylist size: " + deliveryDatesArrayList.size());
                            }
                        } else if (date.getMarkStyle().getColor() == mContext.getResources().getColor(R.color.red)) {
                            for (final DeliveryDates dates : deliveryDatesArrayList) {
                                int mDay = dates.getDeliveryDay();
                                if (date.getDay() == mDay) {
                                    mPref.saveSelectedDate(date.getDay());
                                    mPref.saveSelectedMonth(date.getMonth());
                                    mPref.saveSelectedYear(date.getYear());
                                    orderId = dates.getOrderDetailId();

                                    Log.d(TAG, "onDateClick: pause status order id: " + orderId);

                                    deliveryStatus = dates.getDeliveryStatus();

                                    if (dates.getDeliveryStatus().contains("pause")) {
                                        llPauseResume.setVisibility(View.VISIBLE);
                                        llPauseResume.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.btn_green_bg));
                                        tvPause.setText("Resume");
                                        ivPauseResume.setImageResource(R.drawable.ic_play_arrow_black_24dp);
                                        llPauseResume.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                if (mPref.getAccessTokenObject() != null)
                                                    orderId = dates.getOrderDetailId();
                                                pauseMeal(orderId, "pending", mPref.getAccessTokenObject().getAccessToken());
                                            }
                                        });
                                    }

                                    btnContinue.setVisibility(View.GONE);
                                    btnUpdateOrder.setVisibility(View.VISIBLE);
                                }
                                Log.d(TAG, "onDateClick: arraylist size: " + deliveryDatesArrayList.size());
                            }
                        } else {
                            llPauseResume.setVisibility(View.GONE);
                        }
                    }
                } else {
                    Snackbar snackbar = Snackbar.make(snackbarView, "You need to select date after current date", Snackbar.LENGTH_LONG);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                    snackbar.show();
                }
            }

            @Override
            public void onDateLongClick(View view, DateData date) {
                deleteDialog(date);
            }
        });
        //endregion

        //region continue button
        btnContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // commna separated product id
                String productId = "", deliveryDate;
                StringBuilder builder = new StringBuilder();

                // get selected menu ids from list
                for (int j = 0; j < selectedMenuIdArrayList.size(); j++) {

                    if (selectedMenuIdArrayList.get(j).lastIndexOf(j) == -1) {
                        builder.append(selectedMenuIdArrayList.get(j));
                        builder.append(",");
                    }
                }

                productId = builder.toString(); // append the all ids with ,

                deliveryDate = mPref.getSelectedYear() + "-" + (mPref.getSelectedMonth() > 9 ? mPref.getSelectedMonth() : "0" + mPref.getSelectedMonth()) + "-" + (mPref.getSelectedDate() > 9 ? mPref.getSelectedDate() : "0" + mPref.getSelectedDate());

                if (!productId.isEmpty()) {
                    productId = productId.substring(0, productId.length() - ",".length()); // remove , from end

                    //store user meal called
                    storeUserMeal(listingId, deliveryDate, productId, "pending", accessToken);
                } else {
                    Snackbar snackbar = Snackbar.make(snackbarView, "No Item Selected!", Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                    snackbar.show();
                }

                Log.d(TAG, "onClick: product id: " + productId);

                for (int groupid = 0; groupid < menuCategoryArrayList.size(); groupid++) {

                    for (int itemid = 0; itemid < menuCategoryArrayList.get(groupid).getMenuItems().size(); itemid++) {
                        menuCategoryArrayList.get(groupid).getMenuItems().get(itemid).setChecked(false);
                    }
                }
                selectedMenuIdArrayList.clear();
                //mm
            }
        });
        //endregion

        //region update order click listener
        btnUpdateOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String productId = "", deliveryDate;
                StringBuilder builder = new StringBuilder();

                // get selected menu ids from list
                for (int j = 0; j < selectedMenuIdArrayList.size(); j++) {

                    if (selectedMenuIdArrayList.get(j).lastIndexOf(j) == -1) {
                        builder.append(selectedMenuIdArrayList.get(j));
                        builder.append(",");
                    }
                }

                productId = builder.toString(); // append the all ids with ,

                deliveryDate = mPref.getSelectedYear() + "-" + (mPref.getSelectedMonth() > 9 ? mPref.getSelectedMonth() : "0" + mPref.getSelectedMonth()) + "-" + (mPref.getSelectedDate() > 9 ? mPref.getSelectedDate() : "0" + mPref.getSelectedDate());

                if (!productId.isEmpty()) {
                    productId = productId.substring(0, productId.length() - ",".length()); // remove , from end

                    //update user meal called
                    updateUserMeal(orderId, productId, accessToken);
                } else {
                    Snackbar snackbar = Snackbar.make(snackbarView, "No Item Selected!", Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                    snackbar.show();
                }

                Log.d(TAG, "onUpdateClick: product id: " + productId);

                for (int groupid = 0; groupid < menuCategoryArrayList.size(); groupid++) {

                    for (int itemid = 0; itemid < menuCategoryArrayList.get(groupid).getMenuItems().size(); itemid++) {
                        menuCategoryArrayList.get(groupid).getMenuItems().get(itemid).setChecked(false);
                    }
                }
                selectedMenuIdArrayList.clear();
            }
        });
        //endregion

        isShow = true;
        scrollRange = -1;

        //initialize the months
        initializeMonths();

        //region enabled the navigation on backpressed
        rootView.setFocusableInTouchMode(true);
        rootView.requestFocus();
        rootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_BACK) {

                    if (getFragmentManager() != null) {
                        getFragmentManager().beginTransaction().replace(R.id.main_content, new CalenderFragment()).commit();
//                        getFragmentManager().popBackStack();
                        getFragmentManager().popBackStackImmediate();
                    }

                    return true;
                }
                return false;
            }
        });
        //endregion

        return rootView;
    }

    //region populate calender method
    private void populateCalender(ArrayList<DeliveryDates> deliveryDatesArrayList) {
        if (deliveryDatesArrayList != null) {
            for (int i = 0; i < deliveryDatesArrayList.size(); i++) {

                // highlight the date according to status
                switch (deliveryDatesArrayList.get(i).getDeliveryStatus()) {
                    case "pending":
                        mCalendarView.markDate(new DateData(deliveryDatesArrayList.get(i).getDeliveryYear(),
                                deliveryDatesArrayList.get(i).getDeliveryMonth(), deliveryDatesArrayList.get(i).getDeliveryDay())
                                .setMarkStyle(new MarkStyle(MarkStyle.BACKGROUND, mContext.getResources().getColor(R.color.green))));
                        break;
                    case "pause":
                        mCalendarView.markDate(new DateData(deliveryDatesArrayList.get(i).getDeliveryYear(),
                                deliveryDatesArrayList.get(i).getDeliveryMonth(), deliveryDatesArrayList.get(i).getDeliveryDay())
                                .setMarkStyle(new MarkStyle(MarkStyle.BACKGROUND, mContext.getResources().getColor(R.color.red))));
                        break;
                    case "delivered":
                        mCalendarView.markDate(new DateData(deliveryDatesArrayList.get(i).getDeliveryYear(),
                                deliveryDatesArrayList.get(i).getDeliveryMonth(), deliveryDatesArrayList.get(i).getDeliveryDay())
                                .setMarkStyle(new MarkStyle(MarkStyle.BACKGROUND, mContext.getResources().getColor(R.color.blue))));
                        break;
                }
                Log.d(TAG, "onCreateView:  deliveryDay: " + deliveryDatesArrayList.get(i).getDeliveryDay() + " status: " + deliveryDatesArrayList.get(i).getDeliveryStatus());
            }
        }
    }

    //region populate calender method
    private void populateCalender(ArrayList<DeliveryDates> deliveryDatesArrayList, boolean isCurrent) {
        if (deliveryDatesArrayList != null) {

            MarkStyle fullGreenStyle = new MarkStyle(MarkStyle.BACKGROUND, mContext.getResources().getColor(R.color.green));
            MarkStyle fullRedStyle = new MarkStyle(MarkStyle.BACKGROUND, mContext.getResources().getColor(R.color.red));
            MarkStyle fullBlueStyle = new MarkStyle(MarkStyle.BACKGROUND, mContext.getResources().getColor(R.color.blue));

            MarkStyle halfGreenStyle = new MarkStyle(MarkStyle.BACKGROUND, mContext.getResources().getColor(R.color.alpha_green));
            MarkStyle halfRedStyle = new MarkStyle(MarkStyle.BACKGROUND, mContext.getResources().getColor(R.color.alpha_red));
            MarkStyle halfBlueStyle = new MarkStyle(MarkStyle.BACKGROUND, mContext.getResources().getColor(R.color.alpha_blue));

            MarkStyle finalGreen = isCurrent ? fullGreenStyle : halfGreenStyle;
            MarkStyle finalRed = isCurrent ? fullRedStyle : halfRedStyle;
            MarkStyle finalBlue = isCurrent ? fullBlueStyle : halfBlueStyle;

            for (int i = 0; i < deliveryDatesArrayList.size(); i++) {
                // highlight the date according to status
                switch (deliveryDatesArrayList.get(i).getDeliveryStatus()) {
                    case "pending":
                        mCalendarView.markDate(new DateData(deliveryDatesArrayList.get(i).getDeliveryYear(),
                                deliveryDatesArrayList.get(i).getDeliveryMonth(), deliveryDatesArrayList.get(i).getDeliveryDay()).setMarkStyle(finalGreen));
                        break;
                    case "pause":
                        mCalendarView.markDate(new DateData(deliveryDatesArrayList.get(i).getDeliveryYear(),
                                deliveryDatesArrayList.get(i).getDeliveryMonth(), deliveryDatesArrayList.get(i).getDeliveryDay()).setMarkStyle(finalRed));
                        break;
                    case "delivered":
                        mCalendarView.markDate(new DateData(deliveryDatesArrayList.get(i).getDeliveryYear(),
                                deliveryDatesArrayList.get(i).getDeliveryMonth(), deliveryDatesArrayList.get(i).getDeliveryDay()).setMarkStyle(finalBlue));
                        break;
                }
                Log.d(TAG, "onCreateView:  deliveryDay: " + deliveryDatesArrayList.get(i).getDeliveryDay() + " status: " + deliveryDatesArrayList.get(i).getDeliveryStatus());
            }
        }
    }
    //endregion

    //region get position method
    private int getPosition(ArrayList<DaysOfTheWeek> daysOfTheWeekArrayList) {
        for (int i = 0; i < daysOfTheWeekArrayList.size(); i++) {
            if (daysOfTheWeekArrayList.get(i).isHighlighted()) {
                return i;
            }
        }
        return 0;
    }
    //endregion

    //region initializer method
    private void Initializer(View rootView) {
        // initialize shared preference object
        mPref = new MySharedPreferences(mContext);
//        dayrecyclerView = rootView.findViewById(R.id.rv_days_of_the_week);

        elMenuList = rootView.findViewById(R.id.el_calendar_details_restaurant_list);
//        ivScrollLeft = rootView.findViewById(R.id.iv_left_scroll);
//        ivScrollRight = rootView.findViewById(R.id.iv_right_scroll);
        ivBtnBack = rootView.findViewById(R.id.iv_calendar_detail_back);
        ivPauseResume = rootView.findViewById(R.id.iv_calendar_detail_pause);

        btnContinue = rootView.findViewById(R.id.btn_calendar_detail_continue);
        btnContinue.setVisibility(View.GONE);
        btnUpdateOrder = rootView.findViewById(R.id.btn_calendar_detail_update);

        mCalendarView = rootView.findViewById(R.id.calendar_details_calendar);
        mCalendarView.hasTitle(true); //muneeb

        if (mCalendarView.getMarkedDates() != null)
            mCalendarView.getMarkedDates().getAll().clear();

        final int mDay = mPref.getSelectedDate();
        final int mMonth = mPref.getSelectedMonth();
        final int mYear = mPref.getSelectedYear();
        selectedDate = mYear + "-" + (mMonth > 9 ? mMonth : "0" + mMonth) + "-" + (mDay > 9 ? mDay : "0" + mDay);

        // get date from previous screen through shared pref and show that date to user
//        mCalendarView.travelTo(new DateData(mYear, mMonth, mDay));

        DateData dateData = new DateData(mYear, mMonth, mDay).setMarkStyle(new MarkStyle(MarkStyle.BACKGROUND, mContext.getResources().getColor(R.color.mint_orange)));
        CellConfig.Month2WeekPos = CellConfig.Week2MonthPos;
        CellConfig.ifMonth = false;
        CellConfig.weekAnchorPointDate = dateData;
        CellConfig.w2mPointDate = dateData;
        CellConfig.m2wPointDate = dateData;
        mCalendarView.markDate(dateData);
        mCalendarView.shrink();

        appBarLayout = rootView.findViewById(R.id.appbar_tb);
        collapsingToolbar = rootView.findViewById(R.id.collapse_tb);

        dateCont = rootView.findViewById(R.id.date_cont);

        parallaxCont = rootView.findViewById(R.id.parallax_cont);
        llPauseResume = rootView.findViewById(R.id.ll_pause_resume);

        tvToolbarDate = rootView.findViewById(R.id.tv_calendar_calendar_details_date);
        tvPause = rootView.findViewById(R.id.tv_calendar_detail_pause);
        tvRestaurantName = rootView.findViewById(R.id.tv_restaurant_heading);

        snackbarView = mContext.findViewById(android.R.id.content);

        llLoader = rootView.findViewById(R.id.ll_loader_calendar_detail);
        llLoader.setVisibility(View.GONE);
    }
    //endregion

    //region load days of month method
    private void loadDaysOfMonth() {
        daysOfTheWeekArrayList.clear();

        int selectedDay = mPref.getSelectedDate();
        //selectedDay = selectedDay-1;
        int selectedMonth = mPref.getSelectedMonth();
        selectedMonth = selectedMonth - 1;
        int year = mPref.getSelectedYear();

        Log.d("zain", selectedDay + "-" + selectedMonth + "-" + year + "");

        Calendar calendar = Calendar.getInstance();
        Calendar td = new GregorianCalendar(year, selectedMonth, 1);
        int totalDays = td.getActualMaximum(Calendar.DAY_OF_MONTH);
        //int totalDays = calendar.getActualMaximum(selectedMonth); //total days in month
//        int currentDay = calendar.get(Calendar.DAY_OF_MONTH);
//        int year = calendar.get(Calendar.YEAR);
//        int month = calendar.get(Calendar.MONTH) + 1;
//        int month = calendar.get(Calendar.MONTH);

        /*int year = calendar.get(Calendar.YEAR);*/
        int nextMonth = selectedMonth + 1;
        if (nextMonth == 12) {
            nextMonth = 0;
            year++;
        }

//        int iYear = 1999;
//        int iMonth = Calendar.FEBRUARY; // 1 (months begin with 0)
//        int iDay = 1;
//
//        // Create a calendar object and set year and month
//        Calendar mycal = new GregorianCalendar(year, nextMonth, currentDay);
//
//        // Get the number of days in that month
//        int daysInMonth = mycal.getActualMaximum(Calendar.DAY_OF_MONTH); // 28

       /* MonthDisplayHelper helper = new MonthDisplayHelper(year, nextMonth);
        int getnextmonthdays = helper.getNumberOfDaysInMonth();
*/
        Calendar monthStart = new GregorianCalendar(year, nextMonth, 1);
        int daysnextmonth = monthStart.getActualMaximum(Calendar.DAY_OF_MONTH);

        Log.d(TAG, "loadDaysOfMonth: total days in month " + totalDays + " current date " + selectedDay + " year " + year
                + " month " + selectedMonth + " next month: " + (nextMonth)
                + " total next month days: " + daysnextmonth);

        for (int i = selectedDay; i <= totalDays; i++) {
            DaysOfTheWeek days = new DaysOfTheWeek();

            if (mPref.getSelectedDate() == i) {
                days.setDays(i);
                days.setPaused(true);
                days.setIcon(R.drawable.ic_pause_black_24dp);
                days.setHighlighted(true);
//                if (!status.isEmpty())
//                    days.setStatus(status);
                daysOfTheWeekArrayList.add(days);

                Log.d(TAG, "" + mPref.getSelectedDate());
            } else {
                days.setDays(i);
                days.setPaused(false);
                days.setIcon(R.drawable.ic_play_arrow_black_24dp);
                days.setHighlighted(false);
                daysOfTheWeekArrayList.add(days);

                Log.d(TAG, "" + mPref.getSelectedDate());
            }
        }

        for (int i = 1; i <= daysnextmonth; i++) {
            DaysOfTheWeek days = new DaysOfTheWeek();
//            if (mPref.getSelectedDate() == i) {
            days.setDays(i);
            days.setPaused(false);
            days.setIcon(R.drawable.ic_pause_black_24dp);
            daysOfTheWeekArrayList.add(days);

            Log.d(TAG, "" + mPref.getSelectedDate());
//            } else {
//                days.setDays(i);
//                days.setStatus("Pause");
////                days.setIcon(R.drawable.ic_pause_black_24dp);
//                days.setHighlighted(false);
//                daysOfTheWeekArrayList.add(days);
//
//                Log.d(TAG, "" + mPref.getSelectedDate());
//            }
        }

        adapter = new DayOfTheWeekAdapter(mContext, daysOfTheWeekArrayList);

//        dayrecyclerView.setAdapter(adapter);
//        dayrecyclerView.smoothScrollToPosition(getPosition(daysOfTheWeekArrayList));

        adapter.setOnItemClickListener(this);
        adapter.setOnPauseClickListener(this);
    }
    //endregion

    //region onAttach method
    @Override
    public void onAttach(Context context) {
        this.mContext = (Activity) context;
        super.onAttach(context);
    }
    //endregion

    //region item click listener method
    @Override
    public void onItemClick(final int position) {
        adapter.selected(position);
    }
    //endregion

    //region get menu list method
    private void getMenuList(String listingId, final String languageId, String userId, String mSelectedDate) {
        String url = API.getAPI() + "menu?listing_id=" + listingId + "&language_id=" + languageId + "&user_id=" + userId + "&date=" + mSelectedDate;
        Log.d(TAG, "MYURL-getMenuList URL: " + url);
        llLoader.setVisibility(View.VISIBLE);

        RequestQueue requestQueue;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                llLoader.setVisibility(View.GONE);
                Log.d(TAG, response.toString());
                try {
                    menuCategoryArrayList.clear();

                    int status = response.getInt("status");
                    String message = response.getString("message");

                    if (status == 1) {
                        JSONArray jsonArrayData = response.getJSONArray("data");
                        MenuItem menuItem;
                        for (int i = 0; i < jsonArrayData.length(); i++) {
                            JSONObject jsonObject = jsonArrayData.getJSONObject(i);
                            String menuId = jsonObject.getString("id");
                            JSONArray jsonArrayProducts = jsonObject.getJSONArray("products");
                            menuCategoryName = jsonObject.getString("menu_cat_name");
                            int consumption = jsonObject.getInt("meal_consumption");
                            Menu menu = new Menu();
                            menu.setCategory(menuCategoryName);
                            menu.setOutOff(0);
                            menu.setTotal(consumption);

                            if (jsonArrayProducts.length() != 0) {
                                for (int j = 0; j < jsonArrayProducts.length(); j++) {
                                    JSONObject jsonProducts = jsonArrayProducts.getJSONObject(j);
                                    String menuItemId = jsonProducts.getString("id");
                                    String menuCategoryId = jsonProducts.getString("menu_category_id");
                                    String menuListingId = jsonProducts.getString("listing_id");
                                    String menuItemName = jsonProducts.getString("product_name");
                                    String menuItemCaptionName = jsonProducts.getString("caption_name");
                                    String menuItemPrice = jsonProducts.getString("price");
                                    String menuItemImage = jsonProducts.getString("image");
                                    String menuItemDescription = jsonProducts.getString("product_description");
                                    String menuItemDiscountPrice = jsonProducts.getString("discount");
                                    String menuItemDiscountUnit = jsonProducts.getString("discount_unit");
                                    String menuItemCarbohydrates = jsonProducts.getString("carbohydrates");
                                    String menuItemProteins = jsonProducts.getString("proteins");
                                    String menuItemCalories = jsonProducts.getString("calories");
                                    String menuItemRating = jsonProducts.getString("rating");

                                    float ratings = Float.parseFloat(menuItemRating);

                                    String imageUrl = API.getImageAPI() + menuItemImage;

                                    menuItem = new MenuItem();
                                    menuItem.setId(menuItemId);
                                    menuItem.setMenuCategoryId(menuCategoryId);
                                    menuItem.setListingId(menuListingId);
                                    menuItem.setProductName(menuItemName);
                                    menuItem.setCaptionName(menuItemCaptionName);
                                    menuItem.setImage(imageUrl);
                                    menuItem.setPrice(menuItemPrice);
                                    menuItem.setDecripton(menuItemDescription);
                                    menuItem.setDiscountPrice(menuItemDiscountPrice);
                                    menuItem.setDiscountUnit(menuItemDiscountUnit);
                                    menuItem.setCarbohydrates(menuItemCarbohydrates);
                                    menuItem.setProteins(menuItemProteins);
                                    menuItem.setCalories(menuItemCalories);
                                    menuItem.setRating(ratings);
                                    menuItem.setIcon(R.drawable.check_box_unactive);
                                    menuItem.setChecked(false);

                                    menu.menuItems.add(menuItem);
                                }
                            }
                            checkedPreOrderMeals(selectedDate, languageId, mPref.getAccessTokenObject().getAccessToken());

                            // add data on menu category arraylist
                            menuCategoryArrayList.add(menu);
                        }

                        // set menu item adapter
                        menuItemAdapter = new CalenderMenuItemAdapter(mContext, menuCategoryArrayList, null);
                        elMenuList.setAdapter(menuItemAdapter);
                        elMenuList.expandGroup(0);
                        menuItemAdapter.notifyDataSetChanged();

                        //region expandablelistview group click listener
                        elMenuList.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
                            @Override
                            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                                outOff = 0;
                                if (expandableListView.isGroupExpanded(i)) {
                                    Log.d(TAG, "onGroupClick: collapsed");
                                } else {
                                    Log.d(TAG, "onGroupClick: Expanded");
//                                    appBarLayout.setExpanded(false, true);
                                }

                                if (menuCategoryArrayList.get(i).getMenuItems().size() == 0) {
                                    MenuItem menuItem = new MenuItem();
                                    menuItem.setMessage("No meals found");
                                    menuCategoryArrayList.get(i).menuItems.add(menuItem);
                                }

                                return false;
                            }
                        });
                        //endregion

                        //region expandablelistview child click listener
                        elMenuList.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
                            @Override
                            public boolean onChildClick(ExpandableListView expandableListView, View view, int groupPos, int pos, long l) {
                                int expandPosition = groupPos;
                                outOff = menuCategoryArrayList.get(groupPos).getOutOff();

                                if (menuCategoryArrayList.get(groupPos).getMenuItems().get(pos).getId() != null) {
                                    Log.d(TAG, "onChildClick: arraylist size: " + selectedMenuIdArrayList.size());

                                    /*for (int i = 0; i < menuCategoryArrayList.get(groupPos).getMenuItems().size(); i++) {
                                        if (i == pos) {

                                        } else if (menuCategoryArrayList.get(groupPos).getMenuItems().get(i).isChecked()) {
                                            menuCategoryArrayList.get(groupPos).getMenuItems().get(i).setChecked(false);
                                            selectedMenuIdArrayList.remove(menuCategoryArrayList.get(groupPos).getMenuItems().get(i).getId());
                                        }
                                    }*/
                                    if (menuCategoryArrayList.get(groupPos).getMenuItems().get(pos).isChecked()) {
                                        menuCategoryArrayList.get(groupPos).getMenuItems().get(pos).setChecked(false);
                                        selectedMenuIdArrayList.remove(menuCategoryArrayList.get(groupPos).getMenuItems().get(pos).getId());
                                        if (menuCategoryArrayList.get(groupPos).getOutOff() > 0) {
                                            outOff--;
                                        }
                                        menuCategoryArrayList.get(groupPos).setOutOff(outOff);
                                    } else {
                                        menuCategoryArrayList.get(groupPos).getMenuItems().get(pos).setChecked(true);
                                        selectedMenuIdArrayList.add(menuCategoryArrayList.get(groupPos).getMenuItems().get(pos).getId());
                                        outOff++;
                                        expandPosition++;
                                        menuCategoryArrayList.get(groupPos).setOutOff(outOff);
                                        if (outOff == menuCategoryArrayList.get(groupPos).getTotal()) {
                                            if (expandPosition <= menuCategoryArrayList.get(groupPos).getMenuItems().size()) {
                                                if (menuCategoryArrayList.get(expandPosition).getMenuItems().size() == 0)
                                                    expandPosition++;
                                                elMenuList.expandGroup(expandPosition, true);
                                                outOff = 0;
                                            }
                                            elMenuList.collapseGroup(groupPos);
                                        } else {
                                            Snackbar snackbar = Snackbar.make(mContext.findViewById(android.R.id.content), "Limit should not be exceeded!", Snackbar.LENGTH_SHORT);
                                            View getSnackbarView = snackbar.getView();
                                            getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                                            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                                            tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                                            snackbar.show();
                                        }
//                                        appBarLayout.setExpanded(false, true);
                                    }
                                    menuItemAdapter.notifyDataSetChanged();
                                }

                                if (selectedMenuIdArrayList.size() > 0) {
                                    btnContinue.setVisibility(View.VISIBLE);
                                } else {
                                    btnContinue.setVisibility(View.GONE);
                                }
                                return true;
                            }
                        });
                        //endregion
                    } else {
                        Toast.makeText(mContext, "" + message, Toast.LENGTH_SHORT).show();
                    }
                } catch (Exception ex) {
                    Log.d(TAG, "" + ex.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                llLoader.setVisibility(View.GONE);
                Log.d(TAG, "onErrorResponse: " + error.toString());
            }
        });

        requestQueue = Volley.newRequestQueue(mContext);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }
    //endregion

    //region pause meal method
    private void pauseMeal(String orderId, String status, final String accessToken) {
        String url = API.getAPI() + "auth/pause-meal?order_detail_id=" + orderId + "&status=" + status;
        Log.d(TAG, "MYURL-pauseMeal: url " + url);
        llLoader.setVisibility(View.VISIBLE);

        RequestQueue requestQueue;
        final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "pauseMeal onResponse: " + response.toString());
                llLoader.setVisibility(View.GONE);

                try {
                    int status = response.getInt("status");
                    String message = response.getString("message");

                    if (status == 1) {
                        message = message.substring(0, 1).toUpperCase() + message.substring(1).toLowerCase();
                        Snackbar snackbar = Snackbar.make(mContext.findViewById(android.R.id.content), message, Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                        snackbar.show();

                        getCalenderDetail(listingId, languageId, accessToken);
                        populateCalender(deliveryDatesArrayList, true);

                        if (tvPause.getText().toString().contains("ause")) {
                            llPauseResume.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.btn_green_bg));
                            tvPause.setText("Resume");
                            ivPauseResume.setImageResource(R.drawable.ic_play_arrow_black_24dp);
                        } else if ((tvPause.getText().toString().contains("sume"))) {
                            llPauseResume.setBackgroundDrawable(mContext.getResources().getDrawable(R.drawable.btn_red_bg));
                            ivPauseResume.setImageResource(R.drawable.ic_pause_black_24dp);
                            tvPause.setText("Pause");
                        }
                    } else {
                        Snackbar snackbar = Snackbar.make(mContext.findViewById(android.R.id.content), message, Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                        snackbar.show();
                    }
                } catch (Exception ex) {
                    Log.d(TAG, "onException: " + ex.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                llLoader.setVisibility(View.GONE);

                Snackbar snackbar = Snackbar.make(mContext.findViewById(android.R.id.content), error.toString(), Snackbar.LENGTH_SHORT);
                View getSnackbarView = snackbar.getView();
                getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                snackbar.show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (accessToken != null) {
                    params.put("Authorization", accessToken);
                    Log.d(TAG, "getHeaders " + accessToken);
                }
                return params;
            }
        };

        requestQueue = Volley.newRequestQueue(mContext);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);

    }
    //endregion

    //region store user meal method

    private void storeUserMeal(final String listingId, final String deliveryDate, String productId, String status, final String accessToken) {
        int groupSize = menuCategoryArrayList.size();
        for (int i = 0; i < groupSize; i++) {
            if ((menuCategoryArrayList.get(i).getOutOff() != menuCategoryArrayList.get(i).getTotal())) {

                Snackbar snackbar = Snackbar.make(snackbarView, "Please select items according to limit", Snackbar.LENGTH_SHORT);
                View getSnackbarView = snackbar.getView();
                getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                snackbar.show();
                return;
            } else {
                if (i == groupSize - 1) {
                    String url = API.getAPI() + "auth/store-user-meal?listing_id=" + listingId + "&delivery_date=" + deliveryDate + "&product_ids=" + productId + "&status=" + status;
                    Log.d(TAG, "MYURL-store user meal URL: " + url);
                    llLoader.setVisibility(View.VISIBLE);

                    RequestQueue requestQueue;
                    final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                            url,
                            null, new Response.Listener<JSONObject>() {
                        @RequiresApi(api = Build.VERSION_CODES.M)
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d(TAG, "store user meal onResponse: " + response.toString());
                            llLoader.setVisibility(View.GONE);

                            try {
                                int status = response.getInt("status");
                                String message = response.getString("message");

                                if (status == 1) {
                                    outOff = 0;

                                    Snackbar snackbar = Snackbar.make(snackbarView, message, Snackbar.LENGTH_SHORT);
                                    View getSnackbarView = snackbar.getView();
                                    getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                                    tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                                    snackbar.show();

//                        appBarLayout.setExpanded(true, true);
                                    getCalenderDetail(listingId, languageId, accessToken);

//                        checkedPreOrderMeals(deliveryDate, languageId, accessToken);

                                    mCalendarView.unMarkDate(new DateData(mPref.getSelectedYear(), mPref.getSelectedMonth(), (mPref.getSelectedDate())));
                                    mCalendarView.markDate(new DateData(mPref.getSelectedYear(), mPref.getSelectedMonth(), (mPref.getSelectedDate() + 1)).setMarkStyle(new MarkStyle(MarkStyle.BACKGROUND, mContext.getResources().getColor(R.color.mint_orange, null))));
                                    selectedMenuIdArrayList.clear();
                                    for (int groupid = 0; groupid < menuCategoryArrayList.size(); groupid++) {
                                        menuCategoryArrayList.get(groupid).setOutOff(0);
                                        menuItemAdapter.notifyDataSetChanged();
                                    }
                                    int count = menuItemAdapter.getGroupCount();
                                    for (int i = 0; i < count; i++) {
                                        elMenuList.collapseGroup(i);
                                    }
                                } else {

                                    Snackbar snackbar = Snackbar.make(snackbarView, message, Snackbar.LENGTH_LONG);
                                    View getSnackbarView = snackbar.getView();
                                    getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                                    tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                                    snackbar.show();

//                        appBarLayout.setExpanded(true, true);
                                    getMenuList(listingId, languageId, userId, selectedDate);
                                }
                            } catch (Exception ex) {
                                Log.d(TAG, "onException: " + ex.toString());
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            llLoader.setVisibility(View.GONE);

                            Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                            View getSnackbarView = snackbar.getView();
                            getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                            tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                            snackbar.show();
                        }
                    }) {
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> params = new HashMap<>();
                            if (accessToken != null) {
                                params.put("Authorization", accessToken);
                                Log.d(TAG, "getHeaders " + accessToken);
                            }
                            return params;
                        }
                    };

                    requestQueue = Volley.newRequestQueue(mContext);
                    jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    requestQueue.add(jsonObjectRequest);
                }
            }
        }
    }
    //endregion

    //region update user meal method
    private void updateUserMeal(String orderId, String productId, final String accessToken) {
        int groupSize = menuCategoryArrayList.size();
        for (int i = 0; i < groupSize; i++) {
            if ((menuCategoryArrayList.get(i).getOutOff() != menuCategoryArrayList.get(i).getTotal())) {

                Snackbar snackbar = Snackbar.make(snackbarView, "Please select items according to limit", Snackbar.LENGTH_SHORT);
                View getSnackbarView = snackbar.getView();
                getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                snackbar.show();
                return;
            } else {
                if (i == groupSize - 1) {
                    String url = API.getAPI() + "auth/update-user-meal?order_detail_id=" + orderId + "&product_ids=" + productId;
                    Log.d(TAG, "MYURL-update user meal URL: " + url);
                    llLoader.setVisibility(View.VISIBLE);

                    RequestQueue requestQueue;
                    final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST,
                            url,
                            null, new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d(TAG, "update user meal onResponse: " + response.toString());
                            llLoader.setVisibility(View.GONE);

                            try {
                                int status = response.getInt("status");
                                String message = response.getString("message");

                                Snackbar snackbar = Snackbar.make(snackbarView, message, Snackbar.LENGTH_SHORT);
                                View getSnackbarView = snackbar.getView();
                                getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                                TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                                tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                                snackbar.show();

                                if (status == 1) {
                                    getCalenderDetail(listingId, languageId, accessToken);
                                    checkedPreOrderMeals(selectedDate, languageId, accessToken);
                                }
                            } catch (Exception ex) {
                                Log.d(TAG, "onException: " + ex.toString());
                            }
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            llLoader.setVisibility(View.GONE);

                            Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                            View getSnackbarView = snackbar.getView();
                            getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                            tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                            snackbar.show();
                        }
                    }) {
                        @Override
                        public Map<String, String> getHeaders() throws AuthFailureError {
                            Map<String, String> params = new HashMap<>();
                            if (accessToken != null) {
                                params.put("Authorization", accessToken);
                                Log.d(TAG, "getHeaders " + accessToken);
                            }
                            return params;
                        }
                    };

                    requestQueue = Volley.newRequestQueue(mContext);
                    jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                    requestQueue.add(jsonObjectRequest);
                }
            }
        }
    }
//endregion

    //region get calender detail method
    private void getCalenderDetail(final String listingId, String languageId,
                                   final String accessToken) {
//        mCalendarView = null;
//        mCalendarView = rootView.findViewById(R.id.calendar);
        deliveryDatesArrayList.clear();

        String url = API.getAPI() + "auth/calendar-detail?language_id=" + languageId;
        Log.d(TAG, "MYURL-getCalenderDetail " + "URL: " + url);
        llLoader.setVisibility(View.VISIBLE);

        RequestQueue requestQueue;
        final StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                Log.d(TAG, "onResponse: " + response);
                llLoader.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    String message = jsonObject.getString("message");

                    if (status == 1) {
                        JSONArray dataArray = jsonObject.getJSONArray("data");
                        deliveryDatesArrayList = new ArrayList<>();
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObj = dataArray.getJSONObject(i);
                            String deliveredlistingId = dataObj.getString("id");

//                            if (deliveredlistingId.equals(listingId)) {
                            Log.d(TAG, "onResponse: listingId: " + listingId);

                            //delivery details array
                            JSONArray deliveryDatesArray = dataObj.getJSONArray("delivery_dates");
                            DeliveryDates deliveryDates;

                            Log.d(TAG, "onResponse: delivery id: " + deliveredlistingId + " listing id: " + listingId);

                            for (int j = 0; j < deliveryDatesArray.length(); j++) {
                                if (!deliveredlistingId.equals(listingId))
                                    continue;
                                JSONObject deliveryDateObj = deliveryDatesArray.getJSONObject(j);

                                String orderDetailsId = deliveryDateObj.getString("order_detail_id");
                                String deliveryDate = deliveryDateObj.getString("delivery_date");
                                String deliveryStatus = deliveryDateObj.getString("status");

                                deliveryDates = new DeliveryDates();

                                deliveryDates.setOrderDetailId(orderDetailsId);
                                deliveryDates.setDeliveryDate(deliveryDate);
                                deliveryDates.setDeliveryStatus(deliveryStatus);
                                if (!deliveredlistingId.equals("")) {
                                    deliveryDates.setResId(deliveredlistingId);
                                }

                                String[] date = deliveryDate.split("-");
                                int year = Integer.parseInt(date[0]);
                                int month = Integer.parseInt(date[1]);
                                int day = Integer.parseInt(date[2]);

                                deliveryDates.setDeliveryDay(day);
                                deliveryDates.setDeliveryMonth(month);
                                deliveryDates.setDeliveryYear(year);

                                deliveryDatesArrayList.add(deliveryDates);

                                Log.d(TAG, "onResponse: date: " + year + "-" + month + "-" + day + " size " + deliveryDatesArrayList.size() + " delivery status: " + deliveryStatus);
                            }
                            populateCalender(deliveryDatesArrayList, true);
//                            } else {
                            //delivery details array
                            deliveryDatesArray = dataObj.getJSONArray("delivery_dates");
//                                DeliveryDates deliveryDates;

                            Log.d(TAG, "onResponse: delivery id: " + deliveredlistingId + " listing id: " + listingId);

                            allDatesArrayList = new ArrayList<>();
                            for (int j = 0; j < deliveryDatesArray.length(); j++) {

                                JSONObject deliveryDateObj = deliveryDatesArray.getJSONObject(j);

                                if (deliveredlistingId.equals(listingId))
                                    continue;

                                String orderDetailsId = deliveryDateObj.getString("order_detail_id");
                                String deliveryDate = deliveryDateObj.getString("delivery_date");
                                String deliveryStatus = deliveryDateObj.getString("status");

                                // deliveryStatus = deliveryStatus + "-alpha";
                                Log.d(TAG, "onResponse: not equals " + deliveryStatus);

                                deliveryDates = new DeliveryDates();

                                deliveryDates.setOrderDetailId(orderDetailsId);
                                deliveryDates.setDeliveryDate(deliveryDate);
                                deliveryDates.setDeliveryStatus(deliveryStatus);
                                if (!deliveredlistingId.equals("")) {
                                    deliveryDates.setResId(deliveredlistingId);
                                }

                                String[] date = deliveryDate.split("-");
                                int year = Integer.parseInt(date[0]);
                                int month = Integer.parseInt(date[1]);
                                int day = Integer.parseInt(date[2]);

                                deliveryDates.setDeliveryDay(day);
                                deliveryDates.setDeliveryMonth(month);
                                deliveryDates.setDeliveryYear(year);

                                allDatesArrayList.add(deliveryDates);

                            }
                            populateCalender(allDatesArrayList, false);
//                            }
                        }
                    } else if (status == 0) {

                        Snackbar snackbar = Snackbar.make(snackbarView, message, Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                        snackbar.show();

                        new CountDownTimer(1500, 1000) {

                            @Override
                            public void onTick(long l) {

                            }

                            @Override
                            public void onFinish() {
                                // open home fragment and show restaurant listing if user is not subscribed
                                if (getFragmentManager() != null) {
                                    getFragmentManager()
                                            .beginTransaction()
                                            .setCustomAnimations(android.R.anim.slide_in_left,
                                                    android.R.anim.slide_out_right)
                                            .replace(R.id.main_content, new HomeFragment())
                                            .commit();
                                }
                            }
                        }.start();
                    }
                } catch (
                        Exception ex) {
                    Log.d(TAG, "get calender fragment onException: " + ex.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                llLoader.setVisibility(View.GONE);

                Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                View getSnackbarView = snackbar.getView();
                getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                snackbar.show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (accessToken != null) {
                    params.put("Authorization", accessToken);
                    Log.d(TAG, "getHeaders " + accessToken);
                }
                return params;
            }
        };

        requestQueue = Volley.newRequestQueue(mContext);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);

    }
//endregion

    //region pause item click listener
    @Override
    public void onPauseItemClick(int position) {

//        Toast.makeText(mContext, "working", Toast.LENGTH_SHORT).show();
        String status;
        if (daysOfTheWeekArrayList.get(position).isPaused()) {
            daysOfTheWeekArrayList.get(position).setPaused(true);
            status = "pause";
//            Toast.makeText(mContext, "working pause", Toast.LENGTH_SHORT).show();
            daysOfTheWeekArrayList.get(position).setStatus(status);
        } else {
            daysOfTheWeekArrayList.get(position).setPaused(false);

            status = "pending";
//            Toast.makeText(mContext, "working pending", Toast.LENGTH_SHORT).show();
            daysOfTheWeekArrayList.get(position).setStatus(status);
        }

        //adapter.notifyDataSetChanged();

        adapter.notifyDataSetChanged();
        if (mPref.getAccessTokenObject() != null)
            pauseMeal(orderId, status, mPref.getAccessTokenObject().getAccessToken());
    }
    //endregion

    //region initialize months
    private void initializeMonths() {
        MONTHS.put(1, "January");
        MONTHS.put(2, "February");
        MONTHS.put(3, "March");
        MONTHS.put(4, "April");
        MONTHS.put(5, "May");
        MONTHS.put(6, "June");
        MONTHS.put(7, "July");
        MONTHS.put(8, "August");
        MONTHS.put(9, "September");
        MONTHS.put(10, "October");
        MONTHS.put(11, "November");
        MONTHS.put(12, "December");

    }
    //endregion

    //region checked pre order meals method
    private void checkedPreOrderMeals(final String date, String languageId,
                                      final String accessToken) {
//        auth/delete-meal?
        String url = API.getAPI() + "auth/calendar-detail?language_id=" + languageId;
        Log.d(TAG, "MYURL-checkedPreOrderMeals " + "URL: " + url);
        llLoader.setVisibility(View.VISIBLE);

        RequestQueue requestQueue;
        final StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                llLoader.setVisibility(View.GONE);
                Log.d(TAG, "checkedPreOrderMeals onResponse: " + response);
                try {
                    selectedMenuIdArrayList.clear();
                    menuItemAdapter.notifyDataSetChanged();

                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    String message = jsonObject.getString("message");
                    if (status == 1) {

                        // set check false to by default
                        /*for (int groupid = 0; groupid < menuCategoryArrayList.size(); groupid++) {
                            for (int itemid = 0; itemid < menuCategoryArrayList.get(groupid).getMenuItems().size(); itemid++) {
                                Log.d(TAG, "checkedPreOrderMeals onResponse: ");
                            }
                        }*/

                        // data array
                        final JSONArray dataArray = jsonObject.getJSONArray("data");
                        outerloop:
                        for (int dataItem = 0; dataItem < dataArray.length(); dataItem++) {
                            JSONObject dataObj = dataArray.getJSONObject(dataItem);

                            //delivery details array
                            JSONArray deliveryDatesArray = dataObj.getJSONArray("delivery_dates");

                            for (int deliveryDateItem = 0; deliveryDateItem < deliveryDatesArray.length(); deliveryDateItem++) {
                                JSONObject deliveryDateObj = deliveryDatesArray.getJSONObject(deliveryDateItem);
                                String deliveryDate = deliveryDateObj.getString("delivery_date");

                                if (deliveryDate.contains(date)) {
                                    JSONArray userMealArray = deliveryDateObj.getJSONArray("user_meals");
                                    if (userMealArray.length() == 0) {
                                        btnUpdateOrder.setVisibility(View.GONE);
                                        btnContinue.setVisibility(View.VISIBLE);
                                    } else {
                                        btnUpdateOrder.setVisibility(View.VISIBLE);
                                        btnContinue.setVisibility(View.GONE);
                                    }
                                    for (int item = 0; item < userMealArray.length(); item++) {
                                        JSONObject userMealObj = userMealArray.getJSONObject(item);

                                        String mealId = userMealObj.getString("id");

                                        Log.d(TAG, "checkedPreOrderMeals onResponse: meal id: " + mealId);
                                        Log.d(TAG, "checkedPreOrderMeals onResponse: user meal: " + userMealObj.toString());

                                        // set checked true if menu id is matched
                                        for (int groupid = 0; groupid < menuCategoryArrayList.size(); groupid++) {
                                            for (int itemid = 0; itemid < menuCategoryArrayList.get(groupid).getMenuItems().size(); itemid++) {
                                                String tempId = menuCategoryArrayList.get(groupid).getMenuItems().get(itemid).getId();
                                                if (tempId.equals(mealId)) {
                                                    if (elMenuList.isGroupExpanded(groupid)) {
                                                        elMenuList.collapseGroup(groupid);
                                                        menuItemAdapter.notifyDataSetChanged();
                                                    }
                                                    menuCategoryArrayList.get(groupid).getMenuItems().get(itemid).setChecked(true);
                                                    menuCategoryArrayList.get(groupid).setOutOff(1);
                                                    Log.d(TAG, "checkedPreOrderMeals onResponse: selecte d id: " + menuCategoryArrayList.get(groupid).getMenuItems().get(itemid).getId() + " " + menuCategoryArrayList.get(groupid).getMenuItems().get(itemid).isChecked() + "======" + menuCategoryArrayList.get(groupid).getMenuItems().get(itemid).getId());
                                                    selectedMenuIdArrayList.add(menuCategoryArrayList.get(groupid).getMenuItems().get(itemid).getId());

                                                    Log.d("menusize", "Size is: " + menuCategoryArrayList.size() + " Size is: " + selectedMenuIdArrayList.size());
                                                }
                                            }
                                        }
                                        menuItemAdapter.notifyDataSetChanged();
                                    }
                                    break outerloop;
                                } else {
                                    for (int groupid = 0; groupid < menuCategoryArrayList.size(); groupid++) {
                                        menuCategoryArrayList.get(groupid).setOutOff(0);
                                        menuItemAdapter.notifyDataSetChanged();
                                    }
                                }
                            }
                        }
                    } else if (status == 0) {

                        Snackbar snackbar = Snackbar.make(snackbarView, message, Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                        snackbar.show();

                        new CountDownTimer(1500, 1000) {

                            @Override
                            public void onTick(long l) {

                            }

                            @Override
                            public void onFinish() {
                                // open home fragment and show restaurant listing if user is not subscribed
                                if (getFragmentManager() != null) {
                                    getFragmentManager()
                                            .beginTransaction()
                                            .setCustomAnimations(android.R.anim.slide_in_left,
                                                    android.R.anim.slide_out_right)
                                            .replace(R.id.main_content, new HomeFragment())
                                            .commit();
                                }
                            }
                        }.start();
                    }
                } catch (Exception ex) {
                    Log.d(TAG, "checked calender fragment onException: " + ex.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                llLoader.setVisibility(View.GONE);

                Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                View getSnackbarView = snackbar.getView();
                getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                snackbar.show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (accessToken != null) {
                    params.put("Authorization", accessToken);
                    Log.d(TAG, "getHeaders " + accessToken);
                }
                return params;
            }
        };

        requestQueue = Volley.newRequestQueue(mContext);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }
    //endregion

    //region onResume method
    @Override
    public void onResume() {
        super.onResume();

        //if access token object is not null
        // get access token from shared preference
        if (mPref.getAccessTokenObject() != null)
            accessToken = mPref.getAccessTokenObject().getAccessToken();
        getCalenderDetail(listingId, languageId, accessToken);
    }
    //endregion`

    private void deleteDialog(final DateData date) {
        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(mContext);

        LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View mView = vi.inflate(R.layout.custom_alert_dialog, null);
        builder.setView(mView);
        builder.setCancelable(false);

        final AlertDialog alertDialog = builder.show();

        TextView mDialogMsg = mView.findViewById(R.id.tv_custom_dialog_msg);
        Button mDialogBtnYes = mView.findViewById(R.id.btn_custom_dialog_yes);
        Button mDialogBtnNo = mView.findViewById(R.id.btn_custom_dialog_no);

        mDialogMsg.setText("Are you sure you want to delete order for " + date.getDay() + "-" + date.getMonth() + "-" + date.getYear() + " ?");

        mDialogBtnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });

        mDialogBtnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();

                deleteOrderOnLongPress(date);
            }
        });
    }

    private void deleteMeal(String orderMealId, final DateData date,
                            final DeliveryDates deliveryDate) {
        String url = API.getAPI() + "auth/delete-meal?order_detail_id=" + orderMealId;
        Log.d(TAG, "MYURL-deleteMeal URL: " + date);

        llLoader.setVisibility(View.VISIBLE);

        RequestQueue requestQueue;
        final StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(final String response) {
                llLoader.setVisibility(View.GONE);
                Log.d(TAG, "deleteMeal onResponse: " + response);
                try {
                    if (date.getMarkStyle().getColor() == mContext.getResources().getColor(R.color.mint_orange)) {
                        llPauseResume.setVisibility(View.GONE);
                        btnUpdateOrder.setVisibility(View.GONE);
                    }

                    deliveryDatesArrayList.remove(deliveryDate);
                    mCalendarView.unMarkDate(
                            date.getYear(),
                            date.getMonth(),
                            date.getDay());
                } catch (Exception ex) {
                    Log.d(TAG, "delete meal onException: " + ex.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                llLoader.setVisibility(View.GONE);

                Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                View getSnackbarView = snackbar.getView();
                getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                snackbar.show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (accessToken != null) {
                    params.put("Authorization", accessToken);
                    Log.d(TAG, "getHeaders " + accessToken);
                }
                return params;
            }
        };

        requestQueue = Volley.newRequestQueue(mContext);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }

    private void deleteOrderOnLongPress(DateData date) {
        int year = Integer.parseInt(new SimpleDateFormat("yyyy", Locale.getDefault()).format(new Date()));
        int month = Integer.parseInt(new SimpleDateFormat("MM", Locale.getDefault()).format(new Date()));
        int cdate = Integer.parseInt(new SimpleDateFormat("dd", Locale.getDefault()).format(new Date()));

        LocalDate temp1 = LocalDate.of(date.getYear(), date.getMonth(), date.getDay());
        LocalDate localDate = LocalDate.of(year, month, cdate);

        Log.d("strDate", temp1.getYear() + "-" + temp1.getMonthValue() + "-" + temp1.getDayOfMonth() + "\nlocal date: " + localDate);

        if ((date.getMarkStyle().getColor() == mContext.getResources().getColor(R.color.alpha_green))
                || (date.getMarkStyle().getColor() == mContext.getResources().getColor(R.color.alpha_red))
                || (date.getMarkStyle().getColor() == mContext.getResources().getColor(R.color.alpha_blue))) {

            Snackbar snackbar = Snackbar.make(snackbarView, "You can't select date from other restaurant", Snackbar.LENGTH_LONG);
            View getSnackbarView = snackbar.getView();
            getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
            tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
            snackbar.show();

        } else if (temp1.isAfter(localDate)) { // check if selected date is after current date
            // prevent from selecting delivered date
            if ((date.getMarkStyle().getColor() == mContext.getResources().getColor(R.color.blue)) || (date.getMarkStyle().getColor() == mContext.getResources().getColor(R.color.red))) {

                Snackbar snackbar = Snackbar.make(snackbarView, "Please select date other than delivered or paused!", Snackbar.LENGTH_LONG);
                View getSnackbarView = snackbar.getView();
                getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                snackbar.show();
            } else {
                for (int i = 0; i < deliveryDatesArrayList.size(); i++) {
                    if (deliveryDatesArrayList.get(i).getDeliveryDay() == date.getDay()) {
                        Log.d("LongClick", "Date: " + date.getDay() + " " + deliveryDatesArrayList.get(i).getOrderDetailId());
                        deleteMeal(deliveryDatesArrayList.get(i).getOrderDetailId(), date, deliveryDatesArrayList.get(i));
                        break;
                    }
                }
            }
        }
    }
}