package com.app.boxes.old.Fragment;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.NavOptions;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.app.boxes.old.Activity.MainActivity;
import com.app.boxes.old.Adapter.SubscribedRestaurantListAdapter;
import com.app.boxes.old.Model.SingleRestaurant;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileSingleSubscriptionFragment extends Fragment {

    private TextView tvExpiry, tvDays;
    private RecyclerView recyclerView;
    private LinearLayout linearLayout, llExpiry;
    private ImageView ivBg, ivLogo;
    private Button btnChooseYourMeal;
    private View snackbarView, rootView;
//    private AVLoadingIndicatorView avi;

    private String TAG = "ProfileSingleSubscription";

    private String getPackageId = "";

    private MySharedPreferences sPref;

    public ProfileSingleSubscriptionFragment() {
        // Required empty public constructor
    }
    public ProfileSingleSubscriptionFragment(profilesingleclick profilesingleclick) {
        this.profilesingleclick = profilesingleclick;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_profile_single_subscription, container, false);

        sPref = new MySharedPreferences(mContext);

        snackbarView = mContext.findViewById(android.R.id.content);

        ivLogo = rootView.findViewById(R.id.iv_profile_subs_bg_logo);
        recyclerView = rootView.findViewById(R.id.rv_profile_single_subscription);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        tvDays = rootView.findViewById(R.id.tv_profile_subs_days);
        tvExpiry = rootView.findViewById(R.id.tv_profile_subs_validity);
        btnChooseYourMeal = rootView.findViewById(R.id.btn_profile_subs_choose_your_meal);
        linearLayout = rootView.findViewById(R.id.ll_profile_single_subscription);
        llExpiry = rootView.findViewById(R.id.ll_profile_subs_expiry);

        // initialize bundle object
        final Bundle sendBundle = new Bundle();

        // get data from bundle
        Bundle bundle = getArguments();

        assert bundle != null;
        ArrayList<SingleRestaurant> restaurantArrayList = (ArrayList<SingleRestaurant>) bundle.getSerializable("restaurant_list");
        if (restaurantArrayList != null) {
            Log.d(TAG, "restaurantArrayList size: " + restaurantArrayList.size());
            if (restaurantArrayList.size() > 0) {
                llExpiry.setVisibility(View.VISIBLE);

                ivLogo.setVisibility(View.GONE);
                linearLayout.setVisibility(View.GONE);

                //store the bundle data in another bundle
                sendBundle.putSerializable("subscribed_restaurant", restaurantArrayList);

                for (SingleRestaurant restaurant : restaurantArrayList) {

                    Log.d(TAG, "onCreateView package name: " + restaurant.getPackageName() + " type name: " + restaurant.getTypeName() + " restaurant name: " + restaurant.getName() + " " + " caption: " + restaurant.getCategory() + " image: " + restaurant.getImages());

                    tvDays.setText(restaurant.getPackageName());
                    tvExpiry.setText(restaurant.getExpiryDate());

                    SubscribedRestaurantListAdapter adapter = new SubscribedRestaurantListAdapter(mContext, restaurantArrayList);
                    recyclerView.setAdapter(adapter);
                }

                //navigate to calendar fragment
                btnChooseYourMeal.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                      /*  if (getFragmentManager() != null) {
                            getFragmentManager().beginTransaction().add(R.id.main_content, new CalenderFragment()).addToBackStack("null").commit();
                        }
*/
                   //     ((MainActivity) mContext).openFragment(new CalenderFragment(), null);
                        ((MainActivity) mContext).navigationController(R.id.calenderFragment, new NavOptions.Builder().build());
                       // Navigation.findNavController(mContext, R.id.main_content).navigate(R.id.calenderFragment);

                    }
                });
            } else {
                llExpiry.setVisibility(View.GONE);
                btnChooseYourMeal.setText(getString(R.string.subscribed_now));
                //navigate to calendar fragment
                btnChooseYourMeal.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        profilesingleclick.chooseYourMealClicked(1);
                    //    ((MainActivity) mContext).navigationController(R.id.foodFragment, new NavOptions.Builder().setPopUpTo(R.id.navigation_graph, true).build());
                    }
                });
            }
        } else {
            llExpiry.setVisibility(View.GONE);
            btnChooseYourMeal.setText(getString(R.string.subscribed_now));
            //navigate to calendar fragment
            btnChooseYourMeal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    profilesingleclick.chooseYourMealClicked(1);

 //                   ((MainActivity) mContext).navigationController(R.id.foodFragment, new NavOptions.Builder().setPopUpTo(R.id.navigation_graph, true).build());

//                    BottomNavigationView bnv = mContext.findViewById(R.id.bnv_navigation);
//                    bnv.setSelectedItemId(R.id.nav_menu_box);
                }
            });
        }

        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (rootView != null) {
            ViewGroup viewGroup = (ViewGroup) rootView.getParent();
            if (viewGroup != null) {
                viewGroup.removeAllViews();
            }
        }
    }

    private Activity mContext;

    @Override
    public void onAttach(Context context) {
        this.mContext = (Activity) context;
        super.onAttach(context);
    }
    profilesingleclick profilesingleclick;
    interface profilesingleclick{
        void chooseYourMealClicked(int pos);
    }
}