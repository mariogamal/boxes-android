package com.app.boxes.old.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.boxes.old.Model.DrawerMenu;
import com.app.boxes.R;

import java.util.ArrayList;

public class DrawerItemAdapter extends RecyclerView.Adapter<DrawerItemAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<DrawerMenu> drawerMenuArrayList;
    private LayoutInflater inflater;

    public DrawerItemAdapter(Context context, ArrayList<DrawerMenu> drawerMenuArrayList) {
        this.context = context;
        this.drawerMenuArrayList = drawerMenuArrayList;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView = inflater.inflate(R.layout.drawer_list_view_item, parent, false);

        return new MyViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        //set the item in list
        holder.ivIcon.setImageResource(drawerMenuArrayList.get(position).getIcon());
        holder.tvMenuName.setText(drawerMenuArrayList.get(position).getMenuName());
        holder.tvMenuName.setTextColor(Color.WHITE);

        //set the alternate color in row
        if (position % 2 == 0) {
            holder.llDrawerRow.setBackgroundColor(Color.parseColor("#1C1C20"));
        } else {
            holder.llDrawerRow.setBackgroundColor(Color.parseColor("#27262A"));
        }
    }

    @Override
    public int getItemCount() {
        return drawerMenuArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        LinearLayout llDrawerRow;
        TextView tvMenuName;
        ImageView ivIcon;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            llDrawerRow = itemView.findViewById(R.id.ll_drawer_rv);
            ivIcon = itemView.findViewById(R.id.iv_drawer_rv);
            tvMenuName = itemView.findViewById(R.id.tv_drawer_rv);
        }
    }
}
