package com.app.boxes.old.Fragment;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.boxes.old.API.API;
import com.app.boxes.old.Activity.MainActivity;
import com.app.boxes.old.Adapter.SingleRestaurantListAdapter;
import com.app.boxes.old.Model.SingleRestaurant;
import com.app.boxes.R;
import com.app.boxes.old.RestaurantSearchSingleton;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
//@Puppet
public class SingleRestaurantListFragment extends Fragment implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, SingleRestaurantListAdapter.SingleResturantClickListener {

    private RecyclerView recyclerView;
    private View snackbarView, rootView;
    private SingleRestaurantListAdapter adapter;
    private ArrayList<SingleRestaurant> singleRestaurantArrayList = new ArrayList<SingleRestaurant>();
    private LinearLayout loaderLayout;

    private boolean isItemClicked = false;

    private static String TAG = "singleRestaurantFragment";

    private MySharedPreferences mPref;

    float distance = 0;
    String id = "", name = "", image = "", caption = "", languageId = "", catSubId = "", catSubTypeName = "", catSubParentId = "", catId = "", typeId = "", getSubTypeId = "", getCatSubTypeName = "";

    private Bundle getCategoryBundle, sendRestaurantData;

    private Fragment selectedFragment = null;

    // for location
    private GoogleApiClient mGoogleApiClient;
    private Location mLocation;
    private LocationManager mLocationManager;

    private LocationRequest mLocationRequest;
    private long UPDATE_INTERVAL = 300000;
    private long FASTEST_INTERVAL = 300000;
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 5445;
    String tabId = "";

    private String query = "";

    public SingleRestaurantListFragment() {
        // Required empty public constructor
    }

    public SingleRestaurantListFragment(String tabId) {
        // get tabId
        this.tabId = tabId;
    }

    public SingleRestaurantListFragment(String tabId, String query) {
        // get tabId
        this.tabId = tabId;

        //get string data
        this.query = query;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_single_restaurant_list, container, false);

        ((MainActivity) mContext).getSupportActionBar().show();

        // intialize widgets
        recyclerView = rootView.findViewById(R.id.rv_single_restaurant_list);
//        avl = rootView.findViewById(R.id.avi_single_restaurant_list);
//        avl.setVisibility(View.GONE);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        snackbarView = mContext.findViewById(android.R.id.content);

        loaderLayout = rootView.findViewById(R.id.ll_loader_single_restaurant);
        loaderLayout.setVisibility(View.GONE);

        mPref = new MySharedPreferences(mContext);

        // check which language is selected
        if (mPref.getLangObject() != null) {
            languageId = mPref.getLangObject().getId();

            Log.d(TAG, "language id " + mPref.getLangObject().getId() + "parent id:"  /*mPref.getCategorySubTypeObject().getParentId()*/);
        }

        // intialize the bundle
        sendRestaurantData = new Bundle();

        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        mLocationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        checkLocation(); //check whether location service is enable or not in your  phone

//        if (bundle != null) {
//            search(bundle.getString("search_result"), languageId);
//        }

        Log.d(TAG, "onResume: query: oncreateview " + RestaurantSearchSingleton.getInstance().getQuery());

        return rootView;
    }

    private Activity mContext;

    @Override
    public void onAttach(Context context) {
        this.mContext = (Activity) context;
        super.onAttach(context);
    }

    @Override
    public void onResume() {
//        getMultiRestaurantListing(lll.getLatitude(), lll.getLongitude(), languageId);
        super.onResume();

        RestaurantSearchSingleton.getInstance().getQuery();

        Log.d(TAG, "onResume: query: resume " + RestaurantSearchSingleton.getInstance().getQuery());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (rootView != null) {
            ViewGroup viewGroup = (ViewGroup) rootView.getParent();
            if (viewGroup != null) {
                viewGroup.removeAllViews();
            }
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }

        RestaurantSearchSingleton.getInstance().getQuery();

        Log.d(TAG, "onResume: query: start " + RestaurantSearchSingleton.getInstance().getQuery());
    }

    @Override
    public void onHiddenChanged(boolean hidden) {
        super.onHiddenChanged(hidden);

        Log.d(TAG, "onHiddenChanged: " + hidden);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }

        RestaurantSearchSingleton.getInstance().getQuery();

        Log.d(TAG, "onResume: query: stop " + RestaurantSearchSingleton.getInstance().getQuery());
    }

    // get single restaurant listing from server
    private void getSingleRestaurantListing(double latitude, double longitude, String languageId) {
        String url = API.getAPI() + "listings?user_longitude=" + longitude + "&user_latitude=" + latitude + "&language_id=" + languageId;
        loaderLayout.setVisibility(View.VISIBLE);

        Log.d(TAG, "url " + url);
//        avl.setVisibility(View.VISIBLE);

        RequestQueue requestQueue;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                loaderLayout.setVisibility(View.GONE);

                Log.d(TAG, response.toString());
//                avl.setVisibility(View.GONE);

                try {
                    singleRestaurantArrayList.clear();

                    int status = response.getInt("status");
                    String message = response.getString("message");

                    if (status == 1) {
                        JSONArray dataArray = response.getJSONArray("data");

                        for (int i = 0; i < dataArray.length(); i++) {

                            JSONObject jsonObject = dataArray.getJSONObject(i);
                            id = jsonObject.getString("id");
                            name = jsonObject.getString("listing_name");
                            image = jsonObject.getString("image");
                            caption = jsonObject.getString("listing_caption");

                            String imageUrl = API.getImageAPI() + image;
                            Log.d(TAG, "onResponse: " + imageUrl);

                            SingleRestaurant singleRestaurant = new SingleRestaurant(id, imageUrl, name, caption);
                            singleRestaurantArrayList.add(singleRestaurant);

                            adapter = new SingleRestaurantListAdapter(mContext, singleRestaurantArrayList, SingleRestaurantListFragment.this);
                            adapter.notifyDataSetChanged();
                            recyclerView.setAdapter(adapter);
                        }

                      /*  recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(mContext, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {

                            }

                            @Override
                            public void onLongItemClick(View view, int position) {

                            }
                        }));*/

                    } else {
//                        Toast.makeText(mContext, "" + message, Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception ex) {
//                    avl.setVisibility(View.GONE);
                    Log.d(TAG, "onException" + ex.toString());

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loaderLayout.setVisibility(View.GONE);

                Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                View getSnackbarView = snackbar.getView();
                getSnackbarView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
                TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));
                snackbar.show();
            }
        });

        requestQueue = Volley.newRequestQueue(mContext);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED)
                Snackbar.make(snackbarView, "Permission denied by user", Snackbar.LENGTH_LONG).show();
            else if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                startLocationUpdates();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        // Request location updates
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_COARSE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(mContext,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);

                return;
            }
        }

        startLocationUpdates();

        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        if (mLocation == null) {
            startLocationUpdates();
        }

        if (mLocation != null) {
            // mLatitudeTextView.setText(String.valueOf(mLocation.getLatitude()));
            //mLongitudeTextView.setText(String.valueOf(mLocation.getLongitude()));
        } else {
//            Toast.makeText(activity, , Toast.LENGTH_SHORT).show();
            Log.d(TAG, "Location not Detected");
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i(TAG, "Connection Suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.i(TAG, "Connection failed. Error: " + connectionResult.getErrorCode());
    }


    private void startLocationUpdates() {
        // Create the location request
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL);
        // Request location updates
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext, android.Manifest.permission.ACCESS_COARSE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(mContext,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);

//                nearbyGymLayout.setVisibility(View.GONE);
                return;
            }
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                mLocationRequest, this);
        Log.d("reque", "--->>>>");
    }

    @Override
    public void onLocationChanged(Location location) {

        String msg = "Updated Location: " +
                location.getLatitude() + "," +
                location.getLongitude();

        Log.d(TAG, "onLocationChanged: " + msg);

        if (location != null) {
//            userAddress(location.getLatitude(), location.getLongitude());

            // get single restaurant listing from server
            getSingleRestaurantListing(location.getLatitude(), location.getLongitude(), languageId);

            Log.d(TAG, "cat id: " + catId + " parent id: " + catSubParentId + " type id: " + typeId);

        } else {
            Log.d(TAG, "category subtype Bundle is empty");
        }
        Log.d(TAG, "onLocationChanged Lat: " + location.getLatitude() + " long: " + location.getLongitude());

    }

    private void checkLocation() {
        if (!isLocationEnabled()) {
            showAlert();
        } else
            isLocationEnabled();
    }

    private boolean isLocationEnabled() {
        mLocationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        assert mLocationManager != null;
        return mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private void showAlert() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.location_dialog, null);
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.show();

        Button btnLocation = dialogView.findViewById(R.id.btn_location_dialog_settings);
        Button btnCancel = dialogView.findViewById(R.id.btn_location_dialog_cancel);

        btnLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(myIntent);
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
    }

    private void userAddress(double lat, double longg) {
        try {
            Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
            List<Address> addresses = geocoder.getFromLocation(lat, longg, 1);

            String address = addresses.get(0).getAddressLine(0);
            String locality = addresses.get(0).getLocality();
            String subLocality = addresses.get(0).getSubLocality();
            String premises = addresses.get(0).getPremises();

//            mPref.saveUserArea(address);

            Log.d(TAG, "Address: " + address + "\n" + "Locality: " + locality + "\n" + "Sub Locality: " + subLocality + "\n" + "Premises: " + premises);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // searching from server
    private void search(String query, String langId) {

        query = RestaurantSearchSingleton.getInstance().getQuery();

        if (query.contains(" "))
            query.replace(" ", "%20");
        String URL = API.getAPI() + "search?keyword=" + query + "&language_id=" + langId;
        Log.d(TAG, "Search URL: " + URL);
        loaderLayout.setVisibility(View.VISIBLE);

        RequestQueue requestQueue;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "Search Response: " + response);
                loaderLayout.setVisibility(View.GONE);

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    int status = jsonObject.optInt("status");
                    String message = jsonObject.optString("message");

                    Snackbar snackbar = Snackbar.make(snackbarView, message, Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));

                    if (status == 1) {
                        JSONArray dataArray = jsonObject.getJSONArray("data");

                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObj = dataArray.getJSONObject(i);

                            String id = dataObj.getString("id");
                            String listing_name = dataObj.getString("listing_name");
                            String image = dataObj.getString("image");
                            String listing_caption = dataObj.getString("listing_caption");
                            String type_id = dataObj.getString("type_id");

                            String imageUrl = API.getImageAPI() + image;

                            SingleRestaurant singleRestaurant = new SingleRestaurant(id, imageUrl, listing_name, listing_caption);
                            singleRestaurantArrayList.add(singleRestaurant);

                            Log.d(TAG, "Data Object: " + id + " " + listing_name + " " + API.getImageAPI() + image + " " + listing_caption + " " + type_id);
                        }
                        SingleRestaurantListAdapter adapter = new SingleRestaurantListAdapter(mContext, singleRestaurantArrayList, SingleRestaurantListFragment.this);
                        // adapter.filter(RestaurantSearchSingleton.getInstance().getQuery());
                        recyclerView.setAdapter(adapter);
                    } else if (status == 0) {
//                        singleRestaurantArrayList.clear();
                        snackbar.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loaderLayout.setVisibility(View.GONE);
                Log.d(TAG, "Search Error: " + error.toString());
                Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                View getSnackbarView = snackbar.getView();
                getSnackbarView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
                TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));
                snackbar.show();
            }
        });
        requestQueue = Volley.newRequestQueue(mContext);
        requestQueue.add(stringRequest);
    }

    public void refresh() {
        singleRestaurantArrayList.clear();
        // if (!RestaurantSearchSingleton.getInstance().getQuery().equals(""))
        search(RestaurantSearchSingleton.getInstance().getQuery(), mPref.getLangObject().getId());
    }

    public void refreshAll() {
        singleRestaurantArrayList.clear();
        getSingleRestaurantListing(mLocation.getLatitude(), mLocation.getLongitude(), mPref.getLangObject().getId());
    }

    @Override
    public void onMenuClick(int pos) {
        SingleRestaurant item = singleRestaurantArrayList.get(pos);
        String itemId = item.getId();
        Log.d(TAG, "Item Id: " + itemId);
        //send restaurant id
        sendRestaurantData.putSerializable("restaurant_data", singleRestaurantArrayList);
        sendRestaurantData.putString("restaurant_id", itemId);
        sendRestaurantData.putInt("position", 1);
        mPref.setListingId(itemId);

        //selected fragment
        selectedFragment = new RestaurantSubscriptionFragment();
        selectedFragment.setArguments(sendRestaurantData);

        ((MainActivity) mContext).navigationController(R.id.restaurantSubscriptionFragment, sendRestaurantData, null);
    }

    @Override
    public void onItemClicked(int pos) {
        Log.d("isItemClick1", isItemClicked + "");
        SingleRestaurant item = singleRestaurantArrayList.get(pos);
        String itemId = item.getId();
        Log.d(TAG, "Item Id: " + itemId);

        if (!isItemClicked) {

            //store subtype id
            mPref.setTypeId(tabId);

            //send restaurant id
            sendRestaurantData.putSerializable("restaurant_data", singleRestaurantArrayList);
            sendRestaurantData.putString("restaurant_id", itemId);
            sendRestaurantData.putInt("position", 0);
            mPref.setListingId(itemId);

            //selected fragment
            selectedFragment = new RestaurantSubscriptionFragment();
            selectedFragment.setArguments(sendRestaurantData);

            ((MainActivity) mContext).navigationController(R.id.restaurantSubscriptionFragment, sendRestaurantData, null);

                                    /*if (getFragmentManager() != null) {
                                        getFragmentManager()
                                                .beginTransaction()
                                                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                                                .replace(R.id.main_content, selectedFragment)
                                                .addToBackStack(null)
                                                .commit();
                                    }*/

            isItemClicked = true;
            Log.d("isItemClick2", isItemClicked + "");

            //timer to make isItemClicked = false
            new CountDownTimer(1000, 1000) {
                @Override
                public void onTick(long l) {

                }

                @Override
                public void onFinish() {
                    isItemClicked = false;
                    Log.d("isItemClick3", isItemClicked + "");
                }
            }.start();
        }
    }
}