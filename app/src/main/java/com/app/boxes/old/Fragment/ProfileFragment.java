package com.app.boxes.old.Fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.boxes.old.API.API;
import com.app.boxes.old.Activity.MainActivity;
import com.app.boxes.old.Adapter.TabAdapter;
import com.app.boxes.old.CategorySingleton;
import com.app.boxes.old.Model.Language;
import com.app.boxes.old.Model.SingleRestaurant;
import com.app.boxes.old.Model.User;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.app.boxes.old.UI.LoginBottomSheet;
import com.app.boxes.old.EventBus.MessageEvent;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileFragment extends Fragment implements ProfileSingleSubscriptionFragment.profilesingleclick {

    private static final String TAG = "ProfileFragment";

    private TabAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private TextView tvToolbarText, tvUserName, tvUserLocation;
    private ImageView ivEditProfile;
    private CircleImageView ivProfileIcon;
    private View snackbarView;
    private FrameLayout loaderLayout;

    private MySharedPreferences mPref;

    public ProfileFragment() {
        // Required empty public constructor
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);

        // show toolbar
        ((MainActivity) mContext).getSupportActionBar().show();
        ((MainActivity) mContext).cartLayout.setVisibility(View.GONE);

        //initialize widgets
        initializer(rootView);

        // set toolbar text
        tvToolbarText.setText(R.string.user_profile);

        // navigate to profile edit fragment
        ivEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) mContext).navigationController(R.id.profileEditFragment, null);
            }
        });

        //get user data from shared preference and set the data in cardview
        if (mPref.getUserObject() != null) {
            if (mPref.getAreaObject() != null) {
                if (!mPref.getAreaObject().getArea().equals(""))
                    tvUserLocation.setText(mPref.getAreaObject().getArea());
                else
                    tvUserLocation.setText("Location");
            }
        } else {
            LoginBottomSheet loginBottomSheet = new LoginBottomSheet();
            loginBottomSheet.setCancelable(false);
            if (getFragmentManager() != null) {
                loginBottomSheet.show(getFragmentManager(), "login_bottom_sheet");
            }
        }

        if (mPref.getLangObject() != null && mPref.getAccessTokenObject() != null) {

            final Language language = mPref.getLangObject();
            final User.AccessToken token = mPref.getAccessTokenObject();

            if (token != null) {
                getCurrentPackage(language.getId(), token.getAccessToken());
                getAndSetUserDetails(token.getAccessToken());
            }
        }

        return rootView;
    }

    private void initializer(View rootView) {
        ivEditProfile = rootView.findViewById(R.id.iv_edit_profile);
//        ivProfileIcon = rootView.findViewById(R.id.iv_profile_icon);

//        viewPager = rootView.findViewById(R.id.profile_viewPager);

//        tabLayout = rootView.findViewById(R.id.profile_tabLayout);

//        loaderLayout = rootView.findViewById(R.id.loader_layout);

        tvToolbarText = ((MainActivity) mContext).findViewById(R.id.tv_toolbar_title);
//        tvUserName = rootView.findViewById(R.id.tv_profile_name);
        tvUserLocation = rootView.findViewById(R.id.tv_profile_location);

        snackbarView = mContext.findViewById(android.R.id.content);

        mPref = new MySharedPreferences(mContext);
    }

    // get user details
    private void getAndSetUserDetails(final String accessToken) {
        String url = API.getAPI() + "auth/user";
        Log.d(TAG, "User URL: " + url);

        loaderLayout.setVisibility(View.VISIBLE);
        RequestQueue requestQueue;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "User onResponse: " + response);
                loaderLayout.setVisibility(View.GONE);

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    int status = jsonObject.optInt("status");

                    if (status == 1) {
                        JSONObject data = jsonObject.getJSONObject("data");

                        String first_name = data.optString("first_name");
                        String last_name = data.optString("last_name");
                        String getImage = data.getString("image");

                        if ((!first_name.equals("null")) && (!last_name.equals("null"))) {
                            tvUserName.setText(first_name + " " + last_name);
                        } else {
                            tvUserName.setText("Name");
                        }

                        String imageUrl = API.getImageAPI() + getImage;

                        if (!getImage.equals("null")) {
                            Picasso.get().load(imageUrl).fit().into(ivProfileIcon);
                        } else {
                            ivProfileIcon.setImageResource(R.drawable.avatar);
                        }

//                        sPref.saveUserObject(user);

//                        Log.d("User-Response", "Id: " + id + " first_name: " + first_name + " last_name: " + last_name + "  email: " + getEmail + " phone: " + getPhoneNumber);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loaderLayout.setVisibility(View.GONE);
                Log.d(TAG, "Volley Error: " + error.toString());
                if (error instanceof NoConnectionError) {
                    Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.no_network), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));
                    snackbar.show();
                } else {
                    Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));
                    snackbar.show();
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (accessToken != null) {
                    params.put("Authorization", accessToken);
                    Log.d(TAG, "getHeaders " + accessToken);
                }
                return params;
            }
        };
        requestQueue = Volley.newRequestQueue(mContext);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    private void getCurrentPackage(String languageId, final String accessToken) {
        loaderLayout.setVisibility(View.VISIBLE);
        String url = API.getAPI() + "auth/my-current-package?language_id=" + languageId;
        Log.d(TAG, "getCurrentPackage URL: " + url);

        RequestQueue requestQueue;
        final StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "onResponse: " + response.toString());
                loaderLayout.setVisibility(View.GONE);

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    String message = jsonObject.getString("message");

                    ArrayList<SingleRestaurant> restaurantList = new ArrayList<>();
                    if (status == 1) {
                        String id, typeName, subsId, typeId, listingId, packageName;
                        JSONObject dataObj = jsonObject.getJSONObject("data");

                        id = dataObj.getString("id");
                        typeName = dataObj.getString("type_name");
                        JSONObject userSubscriptionObj = dataObj.optJSONObject("user_subscription");

                        if (userSubscriptionObj != null) {
                            subsId = userSubscriptionObj.optString("id");
                            typeId = userSubscriptionObj.optString("type_id");
                            packageName = userSubscriptionObj.optString("package_name");
                            String meal_selection_case = userSubscriptionObj.getString("meal_selection_case");
                            String validity = userSubscriptionObj.getString("validity");

                            JSONArray listingArray = userSubscriptionObj.optJSONArray("user_subscribed_listing");
                            String listingObjId;
                            if (listingArray != null) {
                                SingleRestaurant singleRestaurant;
                                for (int i = 0; i < listingArray.length(); i++) {
                                    JSONObject listingObj = listingArray.getJSONObject(i);
                                    listingObjId = listingObj.getString("id");
                                    String subscriptionId = listingObj.getString("subscription_id");
                                    String consumedMeals = listingObj.getString("consumed_meals");
                                    String outOfMeals = listingObj.getString("out_of_meals");

                                    JSONObject listing = listingObj.getJSONObject("listing");
                                    String mListingId = listing.getString("id");
                                    String mListingName = listing.getString("listing_name");
                                    String mListingImage = listing.getString("image");
                                    String mListingCaption = listing.getString("caption_name");

                                    String imgURL = API.getImageAPI() + mListingImage;

                                    singleRestaurant = new SingleRestaurant();
                                    singleRestaurant.setId(listingObjId);
                                    singleRestaurant.setPackageName(packageName);
                                    singleRestaurant.setName(mListingName);
                                    singleRestaurant.setCategory(mListingCaption);
                                    singleRestaurant.setImages(imgURL);
                                    singleRestaurant.setTypeName(typeName);
                                    singleRestaurant.setExpiryDate(validity);

                                    restaurantList.add(singleRestaurant);
                                }
                            }
                        }
                        //set tab adapter
                        adapter = new TabAdapter(getChildFragmentManager());

                        // add fragment in tab adapter
//                        if (typeName != null && !typeName.equalsIgnoreCase("null")) {
                        // todo : back per fragment ka data load nahi hou raha
                        Fragment f = new ProfileSingleSubscriptionFragment(ProfileFragment.this);
                        Bundle b = new Bundle();
                        b.putSerializable("restaurant_list", restaurantList);
                        f.setArguments(b);
                        adapter.addFragment(f, typeName);//, restaurantList);
//                        }
                        //set adapter in viewpager
                        viewPager.setAdapter(adapter);
                        viewPager.getAdapter().notifyDataSetChanged();

                        // set viewpager with tablayout
                        tabLayout.setupWithViewPager(viewPager);

                        //set page listener for viewpager
                      /*  viewPager.addOnPageChangeListener(
                                new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

                        //set tab selected listener for tab layout
                        tabLayout.addOnTabSelectedListener(
                                new TabLayout.ViewPagerOnTabSelectedListener(viewPager));*/
                    } else {
                        //set tab adapter
                        adapter = new TabAdapter(getChildFragmentManager());
                        ProfileSingleSubscriptionFragment fragment = new ProfileSingleSubscriptionFragment(ProfileFragment.this);

                        Bundle b = new Bundle();
                        b.putSerializable("restaurant_list", restaurantList);
                        fragment.setArguments(b);
                        // add fragment in tab adapter
                        adapter.addFragment(fragment, "Single Restaurant");//, restaurantList);

                        //set adapter in viewpager
                        viewPager.setAdapter(adapter);
                        viewPager.getAdapter().notifyDataSetChanged();

                        // set viewpager with tablayout
                        tabLayout.setupWithViewPager(viewPager);

                        //set page listener for viewpager
                     /*   viewPager.addOnPageChangeListener(
                                new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

                        //set tab selected listener for tab layout
                        tabLayout.addOnTabSelectedListener(
                                new TabLayout.ViewPagerOnTabSelectedListener(viewPager));*/

                    }
                } catch (Exception ex) {
                    Log.d(TAG, "prof fragment onException: " + ex.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loaderLayout.setVisibility(View.GONE);
                Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                View getSnackbarView = snackbar.getView();
                getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                snackbar.show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (accessToken != null) {
                    params.put("Authorization", accessToken);
                    Log.d(TAG, "getHeaders " + accessToken);
                }
                return params;
            }
        };

        requestQueue = Volley.newRequestQueue(mContext);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);

    }

    private Activity mContext;

    @Override
    public void onAttach(Context context) {
        this.mContext = (Activity) context;
        super.onAttach(context);
    }

    @Override
    public void chooseYourMealClicked(int pos) {
        if (pos == 1) {
            ((MainActivity) mContext).navigationController(R.id.flowerHomeFragment, null);
            CategorySingleton.getInstance().categoryPosition = 0;
            CategorySingleton.getInstance().selectedPosition = 0;

            EventBus.getDefault().postSticky(new MessageEvent());

        } else
            ((MainActivity) mContext).navigationController(R.id.calenderFragment, null);
    }
}