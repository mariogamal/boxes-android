package com.app.boxes.old.Model;

public class Faq {

    String questions, answer;

    public Faq(String questions, String answer) {
        this.questions = questions;
        this.answer = answer;
    }

    public String getQuestions() {
        return questions;
    }

    public String getAnswer() {
        return answer;
    }
}
