package com.app.boxes.old.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class Flower implements Parcelable {

    private String id, images, name, caption, prodId, catId, prodName, prodCaption, prodPrice, prodNewPrice, prodImage, prodDesc, prodStock, prodDiscount, prodStockCount;

    public Flower(String id, String images, String name, String caption) {
        this.id = id;
        this.images = images;
        this.name = name;
        this.caption = caption;
    }

    protected Flower(Parcel in) {
        id = in.readString();
        images = in.readString();
        name = in.readString();
        prodId = in.readString();
        catId = in.readString();
        prodName = in.readString();
        prodCaption = in.readString();
        prodPrice = in.readString();
        prodNewPrice = in.readString();
        prodImage = in.readString();
        prodDesc = in.readString();
        prodStock = in.readString();
        prodDiscount = in.readString();
        prodStockCount = in.readString();
    }

    public static final Creator<Flower> CREATOR = new Creator<Flower>() {
        @Override
        public Flower createFromParcel(Parcel in) {
            return new Flower(in);
        }

        @Override
        public Flower[] newArray(int size) {
            return new Flower[size];
        }
    };

    public Flower(String prodId, String catId, String prodName, String prodCaption, String prodPrice, String prodNewPrice, String prodImage, String prodDesc, String prodStock, String prodDiscount, String prodStockCount) {
        this.prodId = prodId;
        this.catId = catId;
        this.prodName = prodName;
        this.prodCaption = prodCaption;
        this.prodPrice = prodPrice;
        this.prodNewPrice = prodNewPrice;
        this.prodImage = prodImage;
        this.prodDesc = prodDesc;
        this.prodStock = prodStock;
        this.prodDiscount = prodDiscount;
        this.prodStockCount = prodStockCount;
    }

    public String getId() {
        return id;
    }

    public String getImages() {
        return images;
    }

    public String getName() {
        return name;
    }

    public String getCaption() {
        return caption;
    }

    public String getProdId() {
        return prodId;
    }

    public String getCatId() {
        return catId;
    }

    public String getProdName() {
        return prodName;
    }

    public String getProdCaption() {
        return prodCaption;
    }

    public String getProdPrice() {
        return prodPrice;
    }

    public String getProdNewPrice() {
        return prodNewPrice;
    }

    public String getProdImage() {
        return prodImage;
    }

    public String getProdDesc() {
        return prodDesc;
    }

    public String getProdStock() {
        return prodStock;
    }

    public String getProdDiscount() {
        return prodDiscount;
    }

    public String getProdStockCount() {
        return prodStockCount;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(images);
        parcel.writeString(name);
        parcel.writeString(caption);
        parcel.writeString(prodId);
        parcel.writeString(catId);
        parcel.writeString(prodName);
        parcel.writeString(prodCaption);
        parcel.writeString(prodPrice);
        parcel.writeString(prodNewPrice);
        parcel.writeString(prodImage);
        parcel.writeString(prodDesc);
        parcel.writeString(prodStock);
        parcel.writeString(prodDiscount);
    }
}