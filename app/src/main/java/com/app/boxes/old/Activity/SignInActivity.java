package com.app.boxes.old.Activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.res.ResourcesCompat;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.app.boxes.old.API.API;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.google.android.material.snackbar.Snackbar;
import com.hbb20.CountryCodePicker;

import org.json.JSONObject;

public class SignInActivity extends AppCompatActivity {

    private static final String TAG = "SignInActivity";

    private TextView tvSkip, tvPhone;
    private EditText etPhone;
    private Button btnLogin;
    private LinearLayout loaderLayout;
    private View snackbarView;
    private CountryCodePicker ccp;

    private MySharedPreferences mPref;

    private String countryCode = "", phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //initialize mysharedpreferences object
        mPref = new MySharedPreferences(this);
        mPref.getLocale();
        Log.d(TAG, "Default Language is: " + mPref.getLocale());

        setContentView(R.layout.activity_signin);

        //initialize the widgets
        Initializer();

        // check if a user is logged in and user object is not null
        if (mPref.getUserObject() != null) {
            startActivity(new Intent(SignInActivity.this, MainActivity.class));
            finish();
        }


        //navigate to main activity
        tvSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SignInActivity.this, MainActivity.class));
                finish();
            }
        });


        //navigate to main activity
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginUser();
            }
        });
    }

    private void Initializer() {
        //initialize components
//        tvSkip = findViewById(R.id.tv_signin_skip);
//        etPhone = findViewById(R.id.et_signin_phone);
//        btnLogin = findViewById(R.id.btn_signin_login);

//        loaderLayout = findViewById(R.id.ll_loader_signin);
        loaderLayout.setVisibility(View.GONE);

        snackbarView = findViewById(android.R.id.content);

        ccp.setTypeFace(ResourcesCompat.getFont(this, R.font.ptsans_regular));
        countryCode = ccp.getSelectedCountryCodeWithPlus();
        ccp.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected() {
                countryCode = ccp.getSelectedCountryCodeWithPlus();
            }
        });

    }

    private void LoginUser() {
        phone = etPhone.getText().toString();

        if (phone.isEmpty()) {
            Snackbar snackbar = Snackbar.make(snackbarView, "Please enter your phone number", Snackbar.LENGTH_SHORT);
            View getSnackbarView = snackbar.getView();
            getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
            tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
            snackbar.show();
            etPhone.setError("Field should not be empty");
            etPhone.requestFocus();
        } else if (!Patterns.PHONE.matcher(phone).matches()) {
            Snackbar snackbar = Snackbar.make(snackbarView, "Invalid Phone number", Snackbar.LENGTH_SHORT);
            View getSnackbarView = snackbar.getView();
            getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
            tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
            snackbar.show();
        } else if (countryCode.isEmpty()) {
            Snackbar snackbar = Snackbar.make(snackbarView, "No Country Selected", Snackbar.LENGTH_SHORT);
            View getSnackbarView = snackbar.getView();
            getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
            tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
            snackbar.show();
        } else {

//            Intent otpIntent = new Intent(SignInActivity.this, OTPActivity.class);
//            otpIntent.putExtra("phone_number", countryCode + phone);
//            startActivity(otpIntent);

            /*InputMethodManager inputManager = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);

            if (inputManager != null) {
                if (getCurrentFocus() != null && getCurrentFocus().getWindowToken() != null)
                    inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                            InputMethodManager.HIDE_NOT_ALWAYS);
            }*/

            String url = API.getAPI() + "login?phone=" + countryCode + phone;
            Log.d(TAG, "URL: " + url);

            loaderLayout.setVisibility(View.VISIBLE);
            RequestQueue requestQueue;
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
                @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                @Override
                public void onResponse(JSONObject response) {
                    Log.d(TAG, "Response" + response.toString());
                    loaderLayout.setVisibility(View.GONE);

                    try {
                        int status = response.getInt("status");

                        if (status == 1) {
                            String msg = response.getString("message");

                            Snackbar snackbar = Snackbar.make(snackbarView, msg, Snackbar.LENGTH_SHORT);
                            View getSnackbarView = snackbar.getView();
                            getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
                            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                            tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
                            snackbar.show();

                            new CountDownTimer(2000, 1000) {

                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    Intent otpIntent = new Intent(SignInActivity.this, OTPActivity.class);
                                    otpIntent.putExtra("phone_number", countryCode + phone);
                                    startActivity(otpIntent);
                                }
                            }.start();

                        } else {
                            Toast.makeText(SignInActivity.this, getString(R.string.you_are_not_registered), Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception ex) {
                        Log.d(TAG, ex.toString());
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loaderLayout.setVisibility(View.GONE);
                    if (error instanceof NoConnectionError) {
                        Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.no_network), Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
                        snackbar.show();

                        Log.d(TAG, "onErrorResponse: No Route To Host");
                    } else {
                        Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
                        snackbar.show();

                        Log.d(TAG, "onErrorResponse: " + error.toString());

                    }
                }
            });

            requestQueue = Volley.newRequestQueue(this);
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(jsonObjectRequest);
        }
    }
}
