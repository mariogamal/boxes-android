package com.app.boxes.old.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class SelectedRestaurant implements Parcelable {

    private String id, name, description, images;

    public SelectedRestaurant() {
    }

    public SelectedRestaurant(String id, String name, String description, String images) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.images = images;
    }

    protected SelectedRestaurant(Parcel in) {
        id = in.readString();
        name = in.readString();
        description = in.readString();
        images = in.readString();
    }

    public static final Creator<SelectedRestaurant> CREATOR = new Creator<SelectedRestaurant>() {
        @Override
        public SelectedRestaurant createFromParcel(Parcel in) {
            return new SelectedRestaurant(in);
        }

        @Override
        public SelectedRestaurant[] newArray(int size) {
            return new SelectedRestaurant[size];
        }
    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImages() {
        return images;
    }

    public void setImages(String images) {
        this.images = images;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(name);
        parcel.writeString(description);
        parcel.writeString(images);
    }
}
