package com.app.boxes.old.Model;

import android.os.Parcel;
import android.os.Parcelable;

public class SingleRestaurant implements Parcelable {

    private String id, images, name, category, packageName, typeName, mealCount, mealOutOff, expiryDate;
    private int thresholdDays;
    private boolean isSelected;

    public SingleRestaurant() {
    }

    public SingleRestaurant(String id, String images, String name, String category) {
        this.id = id;
        this.images = images;
        this.name = name;
        this.category = category;
    }

    protected SingleRestaurant(Parcel in) {
        id = in.readString();
        images = in.readString();
        name = in.readString();
        category = in.readString();
        packageName = in.readString();
        typeName = in.readString();
        mealCount = in.readString();
        mealOutOff = in.readString();
        thresholdDays = in.readInt();
        isSelected = in.readByte() != 0;
        expiryDate = in.readString();
    }

    public static final Creator<SingleRestaurant> CREATOR = new Creator<SingleRestaurant>() {
        @Override
        public SingleRestaurant createFromParcel(Parcel in) {
            return new SingleRestaurant(in);
        }

        @Override
        public SingleRestaurant[] newArray(int size) {
            return new SingleRestaurant[size];
        }
    };

    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setImages(String images) {
        this.images = images;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public String getId() {
        return id;
    }

    public String getImages() {
        return images;
    }

    public String getName() {
        return name;
    }

    public String getCategory() {
        return category;
    }

    public String getPackageName() {
        return packageName;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getMealCount() {
        return mealCount;
    }

    public void setMealCount(String mealCount) {
        this.mealCount = mealCount;
    }

    public String getMealOutOff() {
        return mealOutOff;
    }

    public void setMealOutOff(String mealOutOff) {
        this.mealOutOff = mealOutOff;
    }

    public int getThresholdDays() {
        return thresholdDays;
    }

    public void setThresholdDays(int thresholdDays) {
        this.thresholdDays = thresholdDays;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(id);
        parcel.writeString(images);
        parcel.writeString(name);
        parcel.writeString(category);
        parcel.writeString(packageName);
        parcel.writeString(typeName);
        parcel.writeString(mealCount);
        parcel.writeString(mealOutOff);
        parcel.writeInt(thresholdDays);
        parcel.writeByte((byte) (isSelected ? 1 : 0));
        parcel.writeString(expiryDate);
    }
}