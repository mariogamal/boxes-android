package com.app.boxes.old.Fragment;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.boxes.old.Activity.MainActivity;
import com.app.boxes.old.Adapter.SelectedRestaurantAdapter;
import com.app.boxes.old.Model.MultiRestaurant;
import com.app.boxes.old.Model.RestaurantSubscriptionList;
import com.app.boxes.R;
import com.app.boxes.old.RestaurantSearchSingleton;
import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class SelectedRestaurantFragment extends Fragment implements SelectedRestaurantAdapter.SelectedRestaurantClickListener {

    private final String TAG = "Selected";

    private ArrayList<MultiRestaurant> restaurants;
  //  private ArrayList<String> listingId;
    private RecyclerView rvSelectedRestaurant;
    private ArrayList<RestaurantSubscriptionList> subscriptionLists;
    private Button btnCheckout;
    private ImageView ivBack;
    private TextView tvNoRestaurants;
    private View snackbarView;

    Bundle bundle = new Bundle();
    Activity mContext;

    public SelectedRestaurantFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        this.mContext = (Activity) context;
        super.onAttach(context);
    }

    @Override
    public void onResume() {
        ((MainActivity) mContext).getSupportActionBar().hide();
        super.onResume();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_selected_restaurant, container, false);

        // initialize components
        initializerWithListeners(rootView);

        return rootView;
    }

    private void initializerWithListeners(View v) {
        bundle = getArguments();
        if (bundle != null) {
            restaurants = (ArrayList<MultiRestaurant>) bundle.getSerializable("selected_restaurant");
            subscriptionLists = bundle.getParcelableArrayList("packages_list");
        } else {
            restaurants = new ArrayList<>();
        }
//        rvSelectedRestaurant = v.findViewById(R.id.rv_selected_restaurant);

//        btnCheckout = v.findViewById(R.id.btn_selected_restaurant_checkout);

        tvNoRestaurants = v.findViewById(R.id.tv_selected_restaurant_no);

        snackbarView = mContext.findViewById(R.id.main_content);

        btnCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d(TAG, "onClick: listingId size: " + RestaurantSearchSingleton.getInstance().selectedRestaurants.size());
                if (RestaurantSearchSingleton.getInstance().selectedRestaurants.size() > 1 && RestaurantSearchSingleton.getInstance().selectedRestaurants.size() <= 4) {
                    Bundle bundle = new Bundle();
                    bundle.putParcelableArrayList("packages_list", subscriptionLists);

                    ((MainActivity) mContext).navigationController(R.id.cardInfoForMultiFragment, bundle, null);
                } else {
                    Snackbar snackbar = Snackbar.make(snackbarView, "Please select at least 2 restaurants..", Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));
                    snackbar.show();
                }
            }
        });

//        ivBack = v.findViewById(R.id.iv_selected_restaurant_back);

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(mContext, R.id.main_content).navigateUp();
            }
        });

        setAdapter();
    }

    private SelectedRestaurantAdapter adapter;

    private void setAdapter() {
        adapter = new SelectedRestaurantAdapter(getContext(), restaurants, SelectedRestaurantFragment.this);
        rvSelectedRestaurant.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onItemDelete(MultiRestaurant multiRestaurant, int position) {
        restaurants.remove(position);
        /*for (int i = listingId.size() - 1; i >= 0; i--) {
            if (listingId.get(i).equals(multiRestaurant.getId())) {
                listingId.remove(i);
                // RestaurantSearchSingleton.getInstance().removeFromListingId(i);
            }
        }*/
        adapter.notifyDataSetChanged();
        //RestaurantSearchSingleton.getInstance().setSelectedRestaurantsList(listingId);
        RestaurantSearchSingleton.getInstance().setSelectedRestaurants(restaurants);
        RestaurantSearchSingleton.getInstance().selectionCount = restaurants.size();

        if (restaurants.size() < 2)
            btnCheckout.setVisibility(View.GONE);

        if (restaurants.size() == 0)
            tvNoRestaurants.setVisibility(View.VISIBLE);

        Log.d(TAG, "onItemDelete:  "  + restaurants.size());
    }
}
