package com.app.boxes.old.Fragment;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.boxes.old.Activity.MainActivity;
import com.app.boxes.old.Adapter.TabAdapter;
import com.app.boxes.old.Model.SingleRestaurant;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.google.android.material.tabs.TabLayout;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class RestaurantSubscriptionFragment extends Fragment {

    private TabAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private TextView tvTitle, tvRestaurantName, tvRestaurantCaption;

    private ImageView ivBtnBack, ivBackground;

    private Bundle getRestaurantData;

//    private SingleRestaurant singleRestaurant;

    private ArrayList<SingleRestaurant> singleRestaurantsList = new ArrayList<>();

    private String TAG = "RestaurantSubscriptionFragment";

    private String restaurantId = "";

    public RestaurantSubscriptionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_restaurant_subscription, container, false);

        ((MainActivity) mContext).getSupportActionBar().hide();

        ivBtnBack = rootView.findViewById(R.id.iv_restaurant_sub_back);
        ivBackground = rootView.findViewById(R.id.iv_restaurant_subscription_background);

        tvTitle = rootView.findViewById(R.id.tv_toolbar_title);
        tvRestaurantName = rootView.findViewById(R.id.tv_restaurant_subscription_name);
        tvRestaurantCaption = rootView.findViewById(R.id.tv_restaurant_subscription_caption);

        viewPager = rootView.findViewById(R.id.subscription_viewPager);
        tabLayout = rootView.findViewById(R.id.subscription_tabLayout);

        adapter = new TabAdapter(getChildFragmentManager());

        MySharedPreferences sPref = new MySharedPreferences(getActivity());
        sPref.setCardInfoFragment(0);

        getRestaurantData = new Bundle();

        getRestaurantData = getArguments();

        if (getRestaurantData != null) {
            restaurantId = getRestaurantData.getString("restaurant_id");
            getRestaurantData.putString("listing_id", restaurantId);

            ArrayList<SingleRestaurant> singleRestaurantArrayList = (ArrayList<SingleRestaurant>) getRestaurantData.getSerializable("restaurant_data");

            for (int i = 0; i < singleRestaurantArrayList.size(); i++) {

                if (restaurantId.equals(singleRestaurantArrayList.get(i).getId())) {
                    tvTitle.setText(singleRestaurantArrayList.get(i).getName());
                    tvRestaurantName.setText(singleRestaurantArrayList.get(i).getName());
                    tvRestaurantCaption.setText(singleRestaurantArrayList.get(i).getCategory());
                    Picasso.get().load(singleRestaurantArrayList.get(i).getImages()).into(ivBackground);
                }

                Log.d(TAG, "name " + singleRestaurantArrayList.get(i).getName());
            }

            Log.d(TAG, "restaurantId " + restaurantId);

            final int pos = getRestaurantData.getInt("position");
            viewPager.postDelayed(new Runnable()
            {
                @Override
                public void run()
                {
                    viewPager.setCurrentItem(pos, true);
                }
            }, 100);
           // viewPager.setCurrentItem(pos);
        }

        ivBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*if (getFragmentManager() != null) {
                    getFragmentManager()
                            .beginTransaction()
                            .replace(R.id.main_content, new FoodFragment())
//                                .addToBackStack(null)
                            .commit();
                }*/
                Navigation.findNavController(mContext, R.id.main_content).navigateUp();
            }
        });

        adapter.addFragment(new SubscriptionPlanFragment(), getString(R.string.subscription));
        Fragment selectedFragment = new MenuFragment();
        selectedFragment.setArguments(getRestaurantData);
        adapter.addFragment(selectedFragment, getString(R.string.menu));

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

        if (sPref.getLocale().equals("ar")) {
            ivBtnBack.setImageResource(R.drawable.ic_arrow_forward_white);

//            tabLayout.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
            ViewCompat.setLayoutDirection(tabLayout, ViewCompat.LAYOUT_DIRECTION_RTL);
        }

        viewPager.addOnPageChangeListener(
                new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(
                new TabLayout.ViewPagerOnTabSelectedListener(viewPager));

        //enable back pressed navigation
        /*rootView.setFocusableInTouchMode(true);
        rootView.requestFocus();
        rootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_BACK) {
                    if (getFragmentManager() != null) {
                        getFragmentManager()
                                .beginTransaction()
                                .replace(R.id.main_content, new FoodFragment())
//                                .addToBackStack(null)
                                .commit();
                    }
                    return true;
                }
                return false;
            }
        });*/

        return rootView;
    }

    private Activity mContext;

    @Override
    public void onAttach(Context context) {
        this.mContext = (Activity) context;
        super.onAttach(context);
    }

}