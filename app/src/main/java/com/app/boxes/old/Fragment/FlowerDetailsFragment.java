package com.app.boxes.old.Fragment;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.boxes.old.API.API;
import com.app.boxes.old.Activity.MainActivity;
import com.app.boxes.old.Model.Flower;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.app.boxes.old.UI.LoginBottomSheet;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class FlowerDetailsFragment extends Fragment {


    private String TAG = "FlowerDetailsFragment";
    private String productId = "", cartId = "", toolbarTitle = "", operator = "", prodVendorName = "", prodStockStatus = "";

    private int quantity = 0, stockCount = 0, finalStockCount = 0;


    private ImageView ivBack, ivAddItem, ivRemoveItem, ivFlower;
    private Button btnAddCart;
    private TextView tvQuantity, tvProductName, tvProductPrice, tvProductDescription, tvProductDiscount, tvProductStatus, tvCartCount, tvToolbar;
    private View snackbarView;
    private FrameLayout cartLayout;
    private AVLoadingIndicatorView avi;

    private ArrayList<Flower> flowerArrayList;

    private MySharedPreferences sPref;

    public FlowerDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_flower_details, container, false);

        ((MainActivity) mContext).getSupportActionBar().hide();

        //initialize widgets
        initializer(rootView);

        // get selected category name from shared preference and set to toolbar
        if (sPref.getCategoryName() != null)
            toolbarTitle = sPref.getCategoryName() + " " + getString(R.string.details);
//        tvToolbar.setText(toolbarTitle);

        //get data from bundle
        if (getArguments() != null) {
            flowerArrayList = getArguments().getParcelableArrayList("mFlowers_list");
            if (flowerArrayList != null) {
                for (Flower flower : flowerArrayList) {
                    Log.d(TAG, "Flower Arraylist Size: " + flowerArrayList.size() + "\nName: " + flower.getProdName() + "\nImage: " + flower.getProdImage() + "\nPRice: " + flower.getProdPrice());

                    //get product id
                    productId = flower.getProdId();

                    // set product name, product price and product description
                    tvProductDiscount.setText(flower.getProdDiscount());
                    prodStockStatus = flower.getProdStock();
                    tvProductStatus.setText(prodStockStatus);
                    prodVendorName = flower.getName();
                    tvProductName.setText(flower.getProdName());
                    tvProductPrice.setText(flower.getProdPrice());
                    tvProductDescription.setText(flower.getProdDesc());
                    Picasso.get().load(flower.getProdImage()).into(ivFlower);
                    stockCount = Integer.parseInt(flower.getProdStockCount());

                    Log.d(TAG, "onCreateView: stock count: " + flower.getProdStockCount() + " listing Name: " + prodVendorName);

                    // hide add to cart button if product is out of stock
                    if (flower.getProdStock().toLowerCase().contains("out"))
                        btnAddCart.setVisibility(View.GONE);
                }
            }
        }

        if (sPref.getLocale().equals("ar"))
            ivBack.setImageResource(R.drawable.ic_arrow_forward_white);

        //navigate to Flower listing fragment
        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mContext.onBackPressed();
            }
        });

        //display quantity item count
        quantity = 1;
        tvQuantity.setText(String.valueOf(quantity));

        finalStockCount = stockCount - quantity;

        Log.d(TAG, "onCreateView: stock count " + finalStockCount);

        //add item quantity
        ivAddItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //increment the quantity
                if (quantity > 0 && finalStockCount > 0) {
                    quantity++;
                    tvQuantity.setText(String.valueOf(quantity));

                    // if finalStockCount is greater than stockCount then decrement from finalStockCount
                    if (finalStockCount != stockCount) {
                        finalStockCount--;

                        Log.d(TAG, "add item click: " + finalStockCount);

                        // if finalStockCount == 0 then set the status and hide the add to cart button
                        if (finalStockCount == 0) {

                            // set the product status and hide the add to cart button and set the finalStockCount = 0
                            tvProductStatus.setText("Out of stock".toUpperCase());
                            btnAddCart.setVisibility(View.GONE);
                            finalStockCount = 0;
                        }

                        operator = "";
                    }
                }
            }
        });

        //remove item quantity
        ivRemoveItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // if quantity is equal to 1 then quantity set to 1
                if (Integer.parseInt(tvQuantity.getText().toString()) == 1) {
                    quantity = 1;
                    tvQuantity.setText(String.valueOf(quantity));

                } else {
                    //decrement the quantity
                    // if finalStockCount is less the stockCount then increment the finalStockCount value and set the status
                    if (Integer.parseInt(tvQuantity.getText().toString()) > 1) {
                        quantity--;
                        tvQuantity.setText(String.valueOf(quantity));
                    }

                    if (finalStockCount < stockCount) {

                        finalStockCount++;
                        Log.d(TAG, "onClick: remove item " + finalStockCount);

                        //set the status and show the add to cart button
                        tvProductStatus.setText("IN STOCK");
                        btnAddCart.setVisibility(View.VISIBLE);

                    }
                    // if finalStockCount is equals to stockCount then print the finalStockCount
                    else if (finalStockCount == stockCount) {
                        Log.d(TAG, "onClick: value " + finalStockCount);
                    }

                    operator = "";
                }
            }
        });

        if (sPref.getAccessTokenObject() != null && sPref.getLangObject() != null) {
            myCart(sPref.getLangObject().getId(), sPref.getAccessTokenObject().getAccessToken());
            // add item to cart
            btnAddCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    addCartItem(productId, Integer.parseInt(tvQuantity.getText().toString()), operator, sPref.getLangObject().getId(), sPref.getAccessTokenObject().getAccessToken());
                }
            });
        } else {
            LoginBottomSheet loginBottomSheet = new LoginBottomSheet();
            loginBottomSheet.setCancelable(false);
            if (getFragmentManager() != null) {
                loginBottomSheet.show(getFragmentManager(), "login_bottom_sheet");
            }
        }

        cartLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int mCartCount;
                // check if cart count text contains "+" then replace it
                if (tvCartCount.getText().toString().contains("+")) {
                    String sCartCount = tvCartCount.getText().toString().replace("+", "");
                    mCartCount = Integer.parseInt(sCartCount);
                } else {
                    mCartCount = Integer.parseInt(tvCartCount.getText().toString());
                }

                if (mCartCount > 0) {
                    /*if (getFragmentManager() != null) {
                        getFragmentManager()
                                .beginTransaction()
                                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                                .replace(R.id.main_content, new PlaceOrderFragment())
                                .addToBackStack("place_order")
                                .commit();
                    }*/
                    ((MainActivity) mContext).navigationController(R.id.placeOrderFragment, null);

                } else {
                    Snackbar snackbar = Snackbar.make(snackbarView, "Please add items to see cart", Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                    snackbar.show();
                }
            }
        });

//        rootView.setFocusableInTouchMode(true);
//        rootView.requestFocus();
//        rootView.setOnKeyListener(new View.OnKeyListener() {
//            @Override
//            public boolean onKey(View view, int i, KeyEvent keyEvent) {
//                if (i == KeyEvent.KEYCODE_BACK) {
//
//                    getActivity().getSupportFragmentManager();
//                    FragmentTransaction trans = null;
//                    if (getFragmentManager() != null) {
//                        trans = getFragmentManager().beginTransaction();
//                        trans.remove(FlowerDetailsFragment.this);
//                        trans.commit();
//                    }
//
//
//                    if (getFragmentManager() != null) {
//                        getFragmentManager().popBackStack();
//                    }
//
//                    return true;
//                }
//                return false;
//            }
//        });

        return rootView;
    }

    private void initializer(View rootView) {
        ivBack = rootView.findViewById(R.id.iv_flower_detail_back);
        ivAddItem = rootView.findViewById(R.id.iv_flower_details_add_item);
        ivRemoveItem = rootView.findViewById(R.id.iv_flower_details_remove_item);
        ivFlower = rootView.findViewById(R.id.iv_flower_detail_icon);

        tvProductDiscount = rootView.findViewById(R.id.tv_flower_detail_discount);
        tvProductStatus = rootView.findViewById(R.id.tv_flower_detail_status);
        tvQuantity = rootView.findViewById(R.id.tv_flower_details_quantity);
        tvProductName = rootView.findViewById(R.id.tv_flower_details_product_name);
        tvProductPrice = rootView.findViewById(R.id.tv_flower_details_product_price);
        tvProductDescription = rootView.findViewById(R.id.tv_flower_details_product_description);
        tvCartCount = rootView.findViewById(R.id.tv_notif_badge);
        tvToolbar = rootView.findViewById(R.id.tv_toolbar_flower_details_title);

        avi = rootView.findViewById(R.id.avi_flower_details);
        avi.setVisibility(View.GONE);

        cartLayout = rootView.findViewById(R.id.flower_detail_cart);

        btnAddCart = rootView.findViewById(R.id.btn_flower_details_add_to_cart);

        snackbarView = mContext.findViewById(android.R.id.content);

        flowerArrayList = new ArrayList<>();

        sPref = new MySharedPreferences(mContext);
    }

    private Activity mContext;

    @Override
    public void onAttach(Context context) {
        this.mContext = (Activity) context;
        super.onAttach(context);
    }

    private void myCart(String languageId, final String accessToken) {
        String url = API.getAPI() + "auth/cart/my-cart?language_id=" + languageId;
        Log.d(TAG, url);

        avi.setVisibility(View.VISIBLE);
        RequestQueue requestQueue;
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "onResponse: " + response.toString());
                avi.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    String msg = jsonObject.getString("message");

                    if (status == 1) {
                        JSONObject jsonObj = jsonObject.getJSONObject("data");
                        Log.d(TAG, "onResponse jsonObj: " + jsonObj);

                        //get the total JsonObject and total items in cart
                        JSONObject totalObj = jsonObj.getJSONObject("total");
                        Log.d(TAG, "onResponse detailsObj: " + totalObj);

                        cartId = jsonObj.getString("id");

                        String cartCount = totalObj.getString("cart_count");
                        int mCartCount = Integer.parseInt(cartCount);
                        if (mCartCount > 99) {
                            tvCartCount.setText("99+");
                        } else {
                            tvCartCount.setText(cartCount);
                        }
                    } else if (status == 0) {
                        tvCartCount.setText(0 + "");
                    }
                } catch (Exception ex) {
                    Log.d(TAG, "onException: " + ex.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse: " + error.getMessage());
                avi.setVisibility(View.GONE);
                if (error instanceof NoConnectionError) {
                    Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.no_network), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                    snackbar.show();
                } else {
                    Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                    snackbar.show();
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (accessToken != null) {
                    params.put("Authorization", accessToken);
                    Log.d(TAG, "getHeaders: " + accessToken);
                }
                return params;
            }
        };

        requestQueue = Volley.newRequestQueue(mContext);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);

    }

    private void addCartItem(final String productId, final int quantity, String operator,
                             final String languageId, final String accessToken) {
        String url = API.getAPI() + "auth/cart/add-cart-item?product_id=" + productId + "&quantity=" + quantity + "&opr=" + operator;
        Log.d(TAG, url);

        avi.setVisibility(View.VISIBLE);
        RequestQueue requestQueue;
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "onResponse: " + response.toString());
                avi.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    String msg = jsonObject.getString("message");

                    if (status == 1) {
                        Snackbar snackbar = Snackbar.make(snackbarView, msg, Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                        snackbar.show();

                        myCart(languageId, accessToken);
                    } else if (status == 2) {

                        final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(mContext);

                        LayoutInflater vi = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        View mView = vi.inflate(R.layout.custom_alert_dialog, null);
                        builder.setView(mView);
                        builder.setCancelable(false);

                        final AlertDialog alertDialog = builder.show();

                        TextView mDialogMsg = mView.findViewById(R.id.tv_custom_dialog_msg);
                        Button mDialogBtnYes = mView.findViewById(R.id.btn_custom_dialog_yes);
                        Button mDialogBtnNo = mView.findViewById(R.id.btn_custom_dialog_no);

                        mDialogMsg.setText(msg);

                        mDialogBtnNo.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                alertDialog.dismiss();
                            }
                        });

                        mDialogBtnYes.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                //clear user cart
                                clearCart(languageId, accessToken);
                                alertDialog.dismiss();
                            }
                        });

                    } else {
                        Snackbar snackbar = Snackbar.make(snackbarView, msg, Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                        snackbar.show();
                    }
                } catch (Exception ex) {
                    Log.d(TAG, "onException: " + ex.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse: " + error.toString());
                avi.setVisibility(View.GONE);

                Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                View getSnackbarView = snackbar.getView();
                getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                snackbar.show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (accessToken != null) {
                    params.put("Authorization", accessToken);
                    Log.d(TAG, "getHeaders: " + accessToken);
                }
                return params;
            }
        };


        requestQueue = Volley.newRequestQueue(mContext);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }

    private void clearCart(final String languageId, final String accessToken) {
        String url = API.getAPI() + "auth/cart/clear-cart";
        Log.d(TAG, "ClearCart url: " + url);

        RequestQueue requestQueue;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "onResponse: " + response.toString());
                try {
                    int status = response.getInt("status");
                    String msg = response.getString("message");

                    Snackbar snackbar = Snackbar.make(snackbarView, msg, Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                    snackbar.show();

                    if (status == 1) {
                        //add item in cart
                        addCartItem(productId, Integer.parseInt(tvQuantity.getText().toString()), operator, languageId, accessToken);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse: " + error.getMessage());
                if (error instanceof NoConnectionError) {
                    Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.no_network), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                    snackbar.show();
                } else {
                    Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                    snackbar.show();
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (accessToken != null) {
                    params.put("Authorization", accessToken);
                    Log.d(TAG, "getHeaders: " + accessToken);
                }
                return params;
            }
        };

        requestQueue = Volley.newRequestQueue(mContext);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }

}