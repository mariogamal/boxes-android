package com.app.boxes.old.Fragment;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.boxes.old.API.API;
import com.app.boxes.old.Activity.MainActivity;
import com.app.boxes.old.Activity.SearchActivity;
import com.app.boxes.old.Adapter.CategoryAdapter;
import com.app.boxes.old.Adapter.FlowerHomeListAdapter;
import com.app.boxes.old.CategorySingleton;
import com.app.boxes.old.Listener.RecyclerItemClickListener;
import com.app.boxes.old.Model.Category;
import com.app.boxes.old.Model.CategorySubType;
import com.app.boxes.old.Model.Flower;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.app.boxes.old.EventBus.MessageEvent;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.snackbar.Snackbar;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class FlowerHomeFragment2 extends Fragment {// implements

    private static String TAG = "flowerHomeFragment";
    private String listingId = "", listingName = "", listingImage = "", listingCaption = "", categoryId = "", categoryName = "", categoryImage = "",
            categoryDefault = "", categoryIsFood = "", subCategoryId = "", subCategoryParentId = "", subCategoryType = "", languageId = "", typeId = "";
    private ArrayList<Flower> flowerHomeArrayList = new ArrayList<Flower>();
    private static ArrayList<Category> categoryImagesList = new ArrayList<>();
    private ArrayList<CategorySubType> categorySubTypeArrayList = new ArrayList<>();
    private boolean isItemClicked = false;
    private static boolean isData = false;
    private boolean isHighlighted = false;
    private LocationManager mLocationManager;
    private static final int MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 5445;
    private Location locationGlobal;
    private MySharedPreferences mPref;
    private FlowerHomeListAdapter flowerHomeListAdapter;
    private CategoryAdapter categoryAdapter;
    private Bundle bundleCategories, bundleSubCategory, getBundleCategory;

    private LinearLayout llSearchView;
    private RecyclerView categoryRecyclerView, flowerListRecyclerView;
    private RelativeLayout rlNoResult, foodParent, flowerParent, loaderLayout;
    private Fragment selectedFragment;
    private View snackbarView;

    public FlowerHomeFragment2() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //inflate the layout
        View parentView = inflater.inflate(R.layout.fragment_flower_home, container, false);

//        //show the actionbar
        ((MainActivity) mContext).getSupportActionBar().show();
        ((MainActivity) mContext).textViewToolbar.setText("Boxes");

        // move bottom view selection to nav_menu_home
//        BottomNavigationView bnv = mContext.findViewById(R.id.bnv_navigation);
//        bnv.setSelectedItemId(R.id.nav_menu_home);

        //initialize widgets
        llSearchView = parentView.findViewById(R.id.ll_flower_search_view);
        rlNoResult = parentView.findViewById(R.id.rl_flower_home_no_result);
        rlNoResult.setVisibility(View.GONE);
        flowerParent = parentView.findViewById(R.id.flowers_parent);
        foodParent = parentView.findViewById(R.id.food_parent);
        categoryRecyclerView = parentView.findViewById(R.id.rv_horizontal_home_categories);
        flowerListRecyclerView = parentView.findViewById(R.id.rv_flower_home_listing);
        snackbarView = mContext.findViewById(android.R.id.content);
        loaderLayout = parentView.findViewById(R.id.loader_layout);
        loaderLayout.setVisibility(View.GONE);

        mPref = new MySharedPreferences(mContext);

        bundleSubCategory = new Bundle();

        LinearLayoutManager horizontalLayout = new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false);
        categoryRecyclerView.setLayoutManager(horizontalLayout);
        int temp = CategorySingleton.getInstance().categoryPosition;
        horizontalLayout.scrollToPosition(temp);
        flowerListRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, RecyclerView.VERTICAL, false));

        categoryAdapter = new CategoryAdapter(mContext, categoryImagesList);
        categoryRecyclerView.setAdapter(categoryAdapter);

        //region zain
        Category category = new Category("0", "drawable://" + R.drawable.food_icon, "Food", "1", categoryIsFood, isHighlighted);

        if (!categoryImagesList.contains(category))
            categoryImagesList.add(category);
        //endregion zain

        if (mPref != null) {
            languageId = mPref.getLangObject().getId();
            getCategories(languageId);
        }

        flowerListRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(mContext, flowerListRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Log.d("isItemClick1", isItemClicked + "");
                if (!isItemClicked) {

                    Flower item = flowerHomeArrayList.get(position);

                    bundleCategories = new Bundle();
                    String itemId = item.getId();
                    String itemName = item.getName();
                    String itemCaption = item.getCaption();
                    String itemImage = item.getImages();

                    bundleCategories.putString("flower_id", itemId);
                    bundleCategories.putString("flower_name", itemName);
                    bundleCategories.putString("flower_caption", itemCaption);
                    bundleCategories.putString("flower_image", itemImage);

                    //set vendor name in shared preference
                    mPref.setListingName(itemName);

                    selectedFragment = new FlowerListingFragment();
                    selectedFragment.setArguments(bundleCategories);

                    //Navigation.findNavController(getActivity(), R.id.main_content).navigate(fragmentId);

                    //   if (((MainActivity)getActivity()).isValidDestination(R.id.flowerListingFragment)) {
                    ((MainActivity) getActivity()).navigationController(R.id.flowerListingFragment, bundleCategories, null);

                    Log.d("isItemClick2", isItemClicked + "");
                }
            }

            @Override
            public void onLongItemClick(View view, int position) {

            }
        }));

        llSearchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, SearchActivity.class);
                intent.putExtra("type_id", typeId);
                startActivity(intent);
            }
        });

/*        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();*/

        mLocationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        checkLocation(); //check whether location service is enable or not in your  phone

        return parentView;
    }

    @Override
    public void onResume() {
        super.onResume();

        TextView tvCartCount = ((MainActivity) mContext).tvCartCount;
        if (mPref.getAccessTokenObject() != null && mPref.getLangObject() != null) {
            myCart(mPref.getLangObject().getId(), mPref.getAccessTokenObject().getAccessToken(), tvCartCount);
        }

        if (CategorySingleton.getInstance().selectedPosition == 0) {
            showFood();
        } else {
            hideFood();
        }
    }

    private Activity mContext;

    @Override
    public void onAttach(Context context) {
        this.mContext = (Activity) context;
        super.onAttach(context);
    }

    private void getCategories(String language_id) {
        categorySubTypeArrayList.clear();

        String url = API.getAPI() + "types?language_id=" + language_id;
        Log.d(TAG, "URL: " + url);

        RequestQueue requestQueue;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d(TAG, response.toString());

                try {
                    int status = response.getInt("status");
                    String message = response.getString("message");

                    if (status == 1) {

                        JSONArray dataArray = response.getJSONArray("data");

                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject jsonDataObject = dataArray.getJSONObject(i);

                            categoryId = jsonDataObject.getString("id");
                            categoryName = jsonDataObject.getString("type_name");
                            categoryImage = jsonDataObject.getString("image");
                            categoryDefault = jsonDataObject.getString("default");
                            categoryIsFood = jsonDataObject.getString("is_food");
                            FusedLocationProviderClient fusedLocationClient;
                            if (categoryDefault.equals("1")) {
                                if (CategorySingleton.getInstance().categoryId != null) {
                                    typeAhad = CategorySingleton.getInstance().categoryId;
                                } else {
                                    typeAhad = categoryId;
                                }

                                fusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
                                fusedLocationClient.getLastLocation()
                                        .addOnSuccessListener(getActivity(), new OnSuccessListener<Location>() {
                                            @Override
                                            public void onSuccess(Location location) {
                                                if (location != null) {
                                                    locationGlobal = location;
                                                    getFlowerListing(typeAhad, location.getLatitude(), location.getLongitude(), mPref.getLangObject().getId());
                                                }
                                            }
                                        });
                            }
                            String imageUrl = API.getImageAPI() + categoryImage;

                            if (mPref.getCurrentCategory() == 1) {
                                isHighlighted = false;
                            } else {
                                isHighlighted = true;
                            }

                            //todo category default ka masla
                            Category category = new Category(categoryId, imageUrl, categoryName, "0", categoryIsFood, isHighlighted);
                            Log.d(TAG, "on response: " + categoryId + " " + categoryName + " " + categoryImage + " " + categoryDefault + " " + categoryIsFood);

                            if (!categoryImagesList.contains(category) && !isData)
                                categoryImagesList.add(category);

                            JSONArray subDataArray = jsonDataObject.getJSONArray("sub_type");

                            for (int j = 0; j < subDataArray.length(); j++) {
                                JSONObject jsonSubDataObject = subDataArray.getJSONObject(j);

                                subCategoryId = jsonSubDataObject.getString("id");
                                subCategoryParentId = jsonSubDataObject.getString("parent_id");
                                subCategoryType = jsonSubDataObject.getString("sub_type_name");

                                Log.d(TAG, "on response subTypes: " + subCategoryId + " " + subCategoryParentId + " " + subCategoryType);

                                CategorySubType categorySubType = new CategorySubType();
                                categorySubType.setId(subCategoryId);
                                categorySubType.setParentId(subCategoryParentId);
                                categorySubType.setSubType(subCategoryType);

                                categorySubTypeArrayList.add(categorySubType);

                                bundleSubCategory.putSerializable("category", categoryImagesList);
                                bundleSubCategory.putSerializable("categorySubType", categorySubTypeArrayList);
                            }
                        }
                        isData = true;
//                        CategorySingleton.getInstance().setCategoryArrayList(categoryImagesList);

                        //navigate to home fragment on recyclerview item click
                        categoryRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(mContext, categoryRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                if (position == 0) {
                                    showFood();
                                } else {
                                    hideFood();
                                }
                                CategorySingleton.getInstance().categoryPosition = position;
                                CategorySingleton.getInstance().selectedPosition = position;
                                onCategoryClick(position);
                            }

                            @Override
                            public void onLongItemClick(View view, int position) {

                            }
                        }));

                    } else {
                        Log.d(TAG, message);

                    }

                } catch (Exception ex) {
                    Log.d(TAG, "exception: " + ex.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onError: " + error.toString());
            }
        });

        requestQueue = Volley.newRequestQueue(mContext);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }

    private void showFood() {
        foodParent.setVisibility(View.VISIBLE);
        flowerParent.setVisibility(View.GONE);
        ((MainActivity) mContext).cartLayout.setVisibility(View.GONE);
    }

    private void hideFood() {
        foodParent.setVisibility(View.GONE);
        flowerParent.setVisibility(View.VISIBLE);
        ((MainActivity) mContext).cartLayout.setVisibility(View.VISIBLE);
    }

    private void onCategoryClick(int position) {
        Log.d(TAG, "onCategoryClick: position: " + position);
        bundleCategories = new Bundle();
        String categoryId = categoryImagesList.get(position).getId();

        for (int i = 0; i < categoryImagesList.size(); i++) {
            if (categoryImagesList.get(i).getCheckDefault().equals("1")) {
                categoryImagesList.get(i).setCheckDefault("0");
                break;
            }
        }

        categoryImagesList.get(position).setCheckDefault("1");
        categoryAdapter.notifyDataSetChanged();

        CategorySingleton.getInstance().categoryId = categoryId;
        CategorySingleton.getInstance().setCategoryArrayList(categoryImagesList);
        categoryAdapter.notifyDataSetChanged();

        bundleCategories.putString("categoryId", categoryId);

        //save category in shared preferences
        mPref.setCategoryName(CategorySingleton.getInstance().getCategoryArrayList().get(position).getName());

        if (locationGlobal != null) {
            getFlowerListing(categoryId, locationGlobal.getLatitude(), locationGlobal.getLongitude(), mPref.getLangObject().getId());
        }
    }

    private String typeAhad = "";

    //get flower listing from server
    private void getFlowerListing(String typeId, double latitude, double longitude, String languageId) {
        String url = API.getAPI() + "listings?" + "type_id=" + typeId + "&user_longitude=" + longitude + "&user_latitude=" + latitude + "&language_id=" + languageId;
        Log.d(TAG, "url " + url);

        loaderLayout.setVisibility(View.VISIBLE);

        RequestQueue requestQueue;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d(TAG, response.toString());
                loaderLayout.setVisibility(View.GONE);

                try {
                    flowerHomeArrayList.clear();
                    int status = response.getInt("status");
                    String message = response.getString("message");
                    if (status == 1) {
                        rlNoResult.setVisibility(View.GONE);
                        flowerListRecyclerView.setVisibility(View.VISIBLE);

                        JSONArray dataArray = response.getJSONArray("data");

                        for (int i = 0; i < dataArray.length(); i++) {

                            JSONObject jsonObject = dataArray.getJSONObject(i);
                            listingId = jsonObject.getString("id");
                            listingName = jsonObject.getString("listing_name");
                            listingImage = jsonObject.getString("image");
                            listingCaption = jsonObject.getString("listing_caption");

                            String imageUrl = API.getImageAPI() + listingImage;

                            Flower flower = new Flower(listingId, imageUrl, listingName, listingCaption);

                            //add data on flower home arraylist


                            flowerHomeArrayList.add(flower);

                            //set the flower home listing
                            flowerHomeListAdapter = new FlowerHomeListAdapter(mContext, flowerHomeArrayList);
                            flowerListRecyclerView.setAdapter(flowerHomeListAdapter);
                        }

                        //navigate to flower listing fragment on recyclerview item click

                    } else {
                        rlNoResult.setVisibility(View.VISIBLE);
                        flowerListRecyclerView.setVisibility(View.GONE);
                    }

                } catch (Exception ex) {
                    Log.d(TAG, "onException" + ex.toString());

                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loaderLayout.setVisibility(View.GONE);
                Log.d(TAG, "onErrorResponse:" + error.toString());
            }
        });

        requestQueue = Volley.newRequestQueue(mContext);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED)
                Snackbar.make(snackbarView, "Permission denied by user", Snackbar.LENGTH_LONG).show();
            else if (grantResults[0] == PackageManager.PERMISSION_GRANTED)
                Snackbar.make(snackbarView, "Permission granted by user", Snackbar.LENGTH_LONG).show();

            // startLocationUpdates();
        }
    }

    private void myCart(String languageId, final String accessToken, final TextView tv) {
        String url = API.getAPI() + "auth/cart/my-cart?language_id=" + languageId;
        Log.d(TAG, url);

//        avi.setVisibility(View.VISIBLE);
        RequestQueue requestQueue;
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "onResponse: " + response.toString());
//                avi.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    String msg = jsonObject.getString("message");

                    if (status == 1) {
                        JSONObject jsonObj = jsonObject.getJSONObject("data");
                        Log.d(TAG, "onResponse jsonObj: " + jsonObj);

                        //get the total JsonObject and total items in cart
                        JSONObject totalObj = jsonObj.getJSONObject("total");
                        Log.d(TAG, "onResponse detailsObj: " + totalObj);

                        String cartCount = totalObj.getString("cart_count");
                        int mCartCount = Integer.parseInt(cartCount);
                        if (mCartCount > 99) {
                            tv.setText("99+");
                        } else {
                            tv.setText(cartCount);
                        }
//                        Toast.makeText(mContext, cartCount, Toast.LENGTH_SHORT).show();
                    } else if (status == 0) {
                        tv.setText(0 + "");
                    }
                } catch (Exception ex) {
                    Log.d(TAG, "onException: " + ex.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse: " + error.toString());
//                avi.setVisibility(View.GONE);
                if (error instanceof NoConnectionError) {
                    Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.no_network), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                    snackbar.show();
                } else {
                    try {
                        Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                        snackbar.show();
                    } catch (Exception ex) {
                        Log.d(TAG, "onErrorResponse: " + ex.toString());
                    }
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (accessToken != null) {
                    params.put("Authorization", accessToken);
                    Log.d(TAG, "getHeaders: " + accessToken);
                }
                return params;
            }
        };

        requestQueue = Volley.newRequestQueue(mContext);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);

    }

    private void checkLocation() {
        if (!isLocationEnabled()) {
            showAlert();
        } else
            isLocationEnabled();
    }

    private boolean isLocationEnabled() {
        mLocationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        assert mLocationManager != null;
        return mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private void showAlert() {
        final androidx.appcompat.app.AlertDialog.Builder builder = new androidx.appcompat.app.AlertDialog.Builder(mContext);
        LayoutInflater inflater = getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.location_dialog, null);
        builder.setView(dialogView);

        final androidx.appcompat.app.AlertDialog alertDialog = builder.show();

        Button btnLocation = dialogView.findViewById(R.id.btn_location_dialog_settings);
        Button btnCancel = dialogView.findViewById(R.id.btn_location_dialog_cancel);

        btnLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(myIntent);
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageEvent event) {
        for (int i = 0; i < categoryImagesList.size(); i++) {
            if (categoryImagesList.get(i).getCheckDefault().equals("1")) {
                categoryImagesList.get(i).setCheckDefault("0");
                break;
            }
        }

        categoryImagesList.get(0).setCheckDefault("1");
        categoryAdapter.notifyDataSetChanged();

        /* Do something */};
}