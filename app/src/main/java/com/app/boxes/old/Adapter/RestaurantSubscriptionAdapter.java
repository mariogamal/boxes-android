package com.app.boxes.old.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.boxes.old.Model.RestaurantSubscriptionList;
import com.app.boxes.R;

import java.util.ArrayList;

public class RestaurantSubscriptionAdapter extends RecyclerView.Adapter<RestaurantSubscriptionAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<RestaurantSubscriptionList> restaurantSubscriptionListArrayList;
    private LayoutInflater inflater;

    public RestaurantSubscriptionAdapter(Context context, ArrayList<RestaurantSubscriptionList> restaurantSubscriptionListArrayList) {
        this.context = context;
        this.restaurantSubscriptionListArrayList = restaurantSubscriptionListArrayList;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView = inflater.inflate(R.layout.list_restaurant_subscription, parent, false);

        return new MyViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.tvSubscriptionPlan.setText(restaurantSubscriptionListArrayList.get(position).getSubscriptionPlan());
        holder.tvSubscriptionPrice.setText(restaurantSubscriptionListArrayList.get(position).getSubcriptionPrice());
        holder.tvSubscriptionStatus.setText(restaurantSubscriptionListArrayList.get(position).getSubcriptionStatus());
    }

    @Override
    public int getItemCount() {
        return restaurantSubscriptionListArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvSubscriptionPlan, tvSubscriptionPrice, tvSubscriptionStatus;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

//            tvSubscriptionPlan = itemView.findViewById(R.id.tv_restaurant_subscription_plan);
//            tvSubscriptionPrice = itemView.findViewById(R.id.tv_restaurant_subscription_price);
            tvSubscriptionStatus = itemView.findViewById(R.id.tv_restaurant_subscription_status);
        }
    }
}
