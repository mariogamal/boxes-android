package com.app.boxes.old.Fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.app.boxes.old.API.API;
import com.app.boxes.old.Activity.MainActivity;
import com.app.boxes.old.Adapter.RestaurantSubscriptionAdapter;
import com.app.boxes.old.Listener.RecyclerItemClickListener;
import com.app.boxes.old.Model.RestaurantSubscriptionList;
import com.app.boxes.old.Model.User;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.app.boxes.old.UI.LoginBottomSheet;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class SubscriptionPlanFragment extends Fragment {

    private String TAG = "SubscriptionFragment";
    private String subscriptionId = "", subscriptionPlan = "", subscriptionPrice = "", subscriptionMeals = "", subscriptionTypeId = "", subscriptionStatus = "";
    private String typeId = "", languageId = "", getPackageId = "";

    private RecyclerView recyclerView;
    private View snackbarView, rootView;

    ArrayList<RestaurantSubscriptionList> subscriptionListArrayList = new ArrayList<>();
    RestaurantSubscriptionAdapter adapter;

    private MySharedPreferences mPref;

    private boolean isItemClicked = false;

    private Bundle bundleSubscriptionPackage;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_subscription_plan, container, false);

        //initialize the widgets
//        recyclerView = rootView.findViewById(R.id.rv_subscription_plan);

        snackbarView = mContext.findViewById(android.R.id.content);

        //set layout manager for recyclerview
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));

        //initialize the bundle class
        bundleSubscriptionPackage = new Bundle();

        //initialize the mysharedpreferences
        mPref = new MySharedPreferences(mContext);

        if (mPref.getLangObject() != null) {
            //check which language is selected
            languageId = mPref.getLangObject().getId();

            //get listing id from single restaurant list sub type category
            typeId = mPref.getTypeId();

            //get the packages list from server
            if (typeId != null)
                getPackages(typeId, languageId);

            Log.d(TAG, "language id " + mPref.getLangObject().getId() + " " + typeId);

        } else {
            Log.d(TAG, "No value found");
        }

        // check if user access token is not null then get access token
        if (mPref.getAccessTokenObject() != null) {
            User.AccessToken token = mPref.getAccessTokenObject();
            if (token != null) {
                //get access token
                String accessToken = token.getAccessToken();
                //get current package
                getCurrentPackage(languageId, accessToken);
            }
        } else {
            LoginBottomSheet loginBottomSheet = new LoginBottomSheet();
            loginBottomSheet.setCancelable(false);
            if (getFragmentManager() != null) {
                loginBottomSheet.show(getFragmentManager(), "login_bottom_sheet");
            }
        }


        return rootView;
    }

    private Activity mContext;

    @Override
    public void onAttach(Context context) {
        this.mContext = (Activity) context;
        super.onAttach(context);
    }

    @Override
    public void onDestroyView() {
        if (rootView != null) {
            ViewGroup viewGroup = (ViewGroup) rootView.getParent();

            if (viewGroup != null) {
                viewGroup.removeAllViews();
            }
        }
        super.onDestroyView();
    }

    private void getCurrentPackage(String languageId, final String accessToken) {
        String url = API.getAPI() + "auth/my-current-package?language_id=" + languageId;
        Log.d(TAG, "getCurrentPackage URL: " + url);

        RequestQueue requestQueue;
        final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "onResponse: " + response.toString());

                try {
                    int status = response.getInt("status");
                    String message = response.getString("message");

                    if (status == 1) {
//                        JSONObject jsonObject = response.getJSONObject("data");
//                        getPackageId = jsonObject.getString("package_id");
//
//                        bundleSubscriptionPackage.putString("package_id", getPackageId);
//
//                        Log.d(TAG, "onCurrentPackageMethod: " + getPackageId);
                    }
                } catch (Exception ex) {
                    Log.d(TAG, "onException: " + ex.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                View getSnackbarView = snackbar.getView();
                getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                snackbar.show();
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (accessToken != null) {
                    params.put("Authorization", accessToken);
                    Log.d(TAG, "getHeaders " + accessToken);
                }
                return params;
            }
        };

        requestQueue = Volley.newRequestQueue(mContext);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);

    }

    private void getPackages(final String mTypeId, String languageId) {
        String url = API.getAPI() + "packages?type_id=" + mTypeId + "&language_id=" + languageId;
        Log.d(TAG, "getPackages URL: " + url);

        RequestQueue requestQueue;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                try {
                    int status = response.getInt("status");
                    String message = response.getString("message");
                    subscriptionListArrayList.clear();

                    if (status == 1) {
                        JSONArray jsonArrayData = response.getJSONArray("data");
                        for (int i = 0; i < jsonArrayData.length(); i++) {

                            JSONObject jsonObject = jsonArrayData.getJSONObject(i);
                            subscriptionId = jsonObject.getString("id");
                            subscriptionPlan = jsonObject.getString("package_name");
                            subscriptionPrice = jsonObject.getString("price");
                            subscriptionMeals = jsonObject.getString("meals");
                            subscriptionTypeId = jsonObject.getString("type_id");

                            RestaurantSubscriptionList restaurantSubscriptionList = new RestaurantSubscriptionList();
//                            Log.d(TAG, "id: " + subscriptionId);
                            //check which package is subscribed

                            restaurantSubscriptionList.setId(subscriptionId);
                            restaurantSubscriptionList.setSubscriptionPlan(subscriptionPlan);
                            restaurantSubscriptionList.setSubcriptionPrice(subscriptionPrice);
                            restaurantSubscriptionList.setSunscriptionMeals(subscriptionMeals);
                            restaurantSubscriptionList.setSubscriptionTypeId(subscriptionTypeId);

                            subscriptionListArrayList.add(restaurantSubscriptionList);

                            if (!subscriptionListArrayList.get(i).getId().equals(getPackageId)) {
                                subscriptionStatus = "";
                                Log.d(TAG, "if block package id " + subscriptionId);
                            } else {
                                subscriptionStatus = "Subscribed";
                                Log.d(TAG, "else block package id " + getPackageId);
                            }

                            restaurantSubscriptionList.setSubcriptionStatus(subscriptionStatus);

                            adapter = new RestaurantSubscriptionAdapter(mContext, subscriptionListArrayList);
                            recyclerView.setAdapter(adapter);
                        }

                        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(mContext, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                Log.d("isItemClick1", isItemClicked + "");
                                if (!isItemClicked) {

                                    User.AccessToken token = mPref.getAccessTokenObject();
                                    if (token != null) {

                                        //get access token
                                        String accessToken = token.getAccessToken();

                                        //get subscription current id
                                        RestaurantSubscriptionList item = subscriptionListArrayList.get(position);
                                        String packageId = item.getId();
                                        String packageName = item.getSubscriptionPlan();
                                        String packagePrice = item.getSubcriptionPrice();

                                        if (mPref.getListingId() != null) {
                                            String listingId = mPref.getListingId();
                                            String[] temp = listingId.split("@");

                                            Log.d(TAG, "onItemClick: 0" + temp[0]);

                                            //save the selected item data in bundle
                                            bundleSubscriptionPackage.putString("package_id", packageId);
                                            bundleSubscriptionPackage.putString("package_name", packageName);
                                            bundleSubscriptionPackage.putString("package_price", packagePrice);
                                            bundleSubscriptionPackage.putString("listing_id", temp[0]);
                                            bundleSubscriptionPackage.putString("type_id", typeId);

                                            Fragment selectedFragment = new CardInfoFragment();
                                            selectedFragment.setArguments(bundleSubscriptionPackage);

                                            ((MainActivity)mContext).navigationController(R.id.cardInfoFragment, bundleSubscriptionPackage, null);

                                            /*if (getFragmentManager() != null) {
                                                getFragmentManager()
                                                        .beginTransaction()
                                                        .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                                                        .add(R.id.main_content, selectedFragment)
                                                        .addToBackStack(null)
                                                        .commit();
                                            }*/
                                        }

                                        isItemClicked = true;
                                        Log.d("isItemClick2", isItemClicked + "");

                                        //timer to make isItemClicked = false
                                        new CountDownTimer(1000, 1000) {
                                            @Override
                                            public void onTick(long l) {

                                            }

                                            @Override
                                            public void onFinish() {
                                                isItemClicked = false;
                                                Log.d("isItemClick3", isItemClicked + "");
                                            }
                                        }.start();
                                    }
                                }
                            }

                            @Override
                            public void onLongItemClick(View view, int position) {

                            }

                        }));
                    }
                } catch (Exception ex) {

                    Log.d(TAG, "" + ex.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                View getSnackbarView = snackbar.getView();
                getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                snackbar.show();
            }
        });
        requestQueue = Volley.newRequestQueue(mContext);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }

}