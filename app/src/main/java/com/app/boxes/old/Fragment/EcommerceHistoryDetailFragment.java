package com.app.boxes.old.Fragment;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.boxes.old.API.API;
import com.app.boxes.old.Activity.MainActivity;
import com.app.boxes.old.Adapter.EcommerceHistoryDetailProductsAdapter;
import com.app.boxes.old.Model.EcommerceHistory;
import com.app.boxes.old.Model.Language;
import com.app.boxes.old.Model.User;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class EcommerceHistoryDetailFragment extends Fragment {

    private String TAG = "EcommerceHistoryDetailFragment";

    private ImageView /*ivBack,*/ ivImage;
    private TextView tvTitle, tvVendor, tvQuantity, tvPrice, tvSubtotal, tvDeliveryFee, tvDiscount, tvTotal;
    private RecyclerView rvOrderProducts;
    private LinearLayout loaderLayout;
    private ImageView ivBack;

    private Activity mContext;

    private MySharedPreferences sPref;

    private ArrayList<EcommerceHistory> ecommerceHistoryArrayList = new ArrayList<>();

    public EcommerceHistoryDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        this.mContext = (Activity) context;
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_ecommerce_history_detail, container, false);

        ((MainActivity) mContext).getSupportActionBar().hide();

        // initialize widgets
        initializer(rootView);

        if (getArguments() != null) {
            String orderId = getArguments().getString("orderId");
            Log.d(TAG, "onCreate Order ID: " + orderId);

            if (sPref.getAccessTokenObject() != null) {
                User.AccessToken token = sPref.getAccessTokenObject();
                Language language = sPref.getLangObject();

                if (token != null && language != null)
                    getOrderDetails(language.getId(), orderId, token.getAccessToken());
            }
        }

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(mContext, R.id.main_content).navigateUp();
            }
        });

        return rootView;
    }

    private void initializer(View view) {
        loaderLayout = view.findViewById(R.id.ll_loader_ecommerce_order_detail);
        //hide the avloading
        loaderLayout.setVisibility(View.GONE);

        ivBack = view.findViewById(R.id.iv_ecommerce_history_detail_back);
        ivImage = view.findViewById(R.id.iv_ecommerce_detail);

        rvOrderProducts = view.findViewById(R.id.rv_ecommerce_order_history_detail);
        rvOrderProducts.setLayoutManager(new LinearLayoutManager(mContext));

        tvTitle = view.findViewById(R.id.tv_ecommerce_detail_title);
        tvSubtotal = view.findViewById(R.id.tv_ecommerce_detail_subtotal);
        tvDeliveryFee = view.findViewById(R.id.tv_ecommerce_detail_delivery_fee);
        tvDiscount = view.findViewById(R.id.tv_ecommerce_detail_discount);
        tvTotal = view.findViewById(R.id.tv_ecommerce_detail_total);

        sPref = new MySharedPreferences(mContext);

        Bundle bundle = getArguments();
        if (bundle != null) {
            String vendorName = bundle.getString("vendor_name");
            String vendorImage = bundle.getString("vendor_image");

            tvTitle.setText(vendorName);
            Picasso.get().load(vendorImage).into(ivImage);
        }
    }

    private void getOrderDetails(String langId, String orderId, final String accessToken) {
        String BASE_URL = API.getAPI() + "auth/cart/my-order-details?language_id=" + langId + "&order_id=" + orderId;
        Log.d(TAG, "Order History URL: " + BASE_URL);

        loaderLayout.setVisibility(View.VISIBLE);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, BASE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "onResponse: " + response);
                loaderLayout.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    int status = jsonObject.optInt("status");
                    String msg = jsonObject.optString("messge");
                    JSONObject dataObj = jsonObject.getJSONObject("data");

                    if (status == 1) {
                        EcommerceHistory ecommerceHistory;

                        //total detail object
                        JSONObject totalObj = dataObj.getJSONObject("total");

                        String subtotal = totalObj.getString("sub_total");
                        String deliveryCharges = totalObj.getString("delivery_charges");
                        String totalPrice = totalObj.getString("total_price");
                        String totalDiscount = totalObj.getString("total_discount");

                        tvSubtotal.setText(subtotal);
                        tvDeliveryFee.setText(deliveryCharges);
                        tvTotal.setText(totalPrice);
                        tvDiscount.setText(totalDiscount);

                        // order detail array
                        JSONArray orderDetailsArray = dataObj.getJSONArray("order_details");
                        for (int i = 0; i < orderDetailsArray.length(); i++) {
                            JSONObject orderDetailsObj = orderDetailsArray.getJSONObject(i);

//                            String id = orderDetailsObj.getString("id");
                            String prodName = orderDetailsObj.getString("product_name");
//                            String image = orderDetailsObj.getString("image");
//                            String captionName = orderDetailsObj.getString("caption_name");
                            String price = orderDetailsObj.getString("price");
                            String quantity = orderDetailsObj.getString("quantity");
//                            String discount = orderDetailsObj.getString("discount");
//                            String totalProdPrice = orderDetailsObj.getString("total_price");

                            ecommerceHistory = new EcommerceHistory();

                            ecommerceHistory.setProducts(prodName);
                            ecommerceHistory.setQuantity(quantity);
                            ecommerceHistory.setPrice(price);

                            Log.d(TAG, "onResponse: " + prodName + " " + price + " " + quantity);
                            ecommerceHistoryArrayList.add(ecommerceHistory);
                        }
                        rvOrderProducts.setAdapter(new EcommerceHistoryDetailProductsAdapter(mContext, ecommerceHistoryArrayList));
                    }
                } catch (JSONException e) {
                    Log.d(TAG, "JSONException: " + e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loaderLayout.setVisibility(View.GONE);
                Log.d(TAG, "onErrorResponse: " + error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (accessToken != null) {
                    params.put("Authorization", accessToken);
                    Log.d(TAG, "getHeaders: " + accessToken);
                }
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }

}