package com.app.boxes.old.Model;

public class DrawerMenu {
    private int icon;
    private String menuName;

    public DrawerMenu(int icon, String menuName) {
        this.icon = icon;
        this.menuName = menuName;
    }

    public int getIcon() {
        return icon;
    }

    public String getMenuName() {
        return menuName;
    }
}
