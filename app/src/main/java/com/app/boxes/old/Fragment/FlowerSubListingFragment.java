package com.app.boxes.old.Fragment;


/*import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.boxes.old.API.API;
import com.app.boxes.old.Activity.MainActivity;
import com.app.boxes.old.Adapter.FlowerListAdapter;
import com.app.boxes.old.Listener.RecyclerItemClickListener;
import com.app.boxes.old.Model.Flower;
import com.app.boxes.R;

import java.util.ArrayList;

*//**
 * A simple {@link Fragment} subclass.
 *//*
public class FlowerSubListingFragment extends Fragment {
    private RecyclerView recyclerView;
    private FlowerListAdapter adapter;
    private ArrayList<Flower> flowerArrayList = new ArrayList<Flower>();

    private boolean isItemClicked = false;


    public FlowerSubListingFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_flower_sub_listing, container, false);

        //initialize widgets
        recyclerView = rootView.findViewById(R.id.rv_flower_listing);

        //set the layout manager for recyclerview
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));

        //clear and add the new data on flower arraylist
        flowerArrayList.clear();
        flowerArrayList.add(new Flower("", "english_flag@3x.png", getString(R.string.flwr_boutique), getString(R.string.celebrate_the_moments)));
        flowerArrayList.add(new Flower("", "english_flag@3x.png", getString(R.string.flwr_boutique), getString(R.string.celebrate_the_moments)));
        flowerArrayList.add(new Flower("", "english_flag@3x.png", getString(R.string.flwr_boutique), getString(R.string.celebrate_the_moments)));
        flowerArrayList.add(new Flower("", "english_flag@3x.png", getString(R.string.flwr_boutique), getString(R.string.celebrate_the_moments)));
        flowerArrayList.add(new Flower("", "english_flag@3x.png", getString(R.string.flwr_boutique), getString(R.string.celebrate_the_moments)));

        //initialize flower list adapter
        adapter = new FlowerListAdapter(mContext, flowerArrayList);

        //set the flower list adapter on recyclerview
        recyclerView.setAdapter(adapter);

        //navigate to flower details fragment on recyclerview item click
        recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(mContext, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Log.d("isItemClick1", isItemClicked + "");
                if (!isItemClicked) {
                    getFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                            .replace(R.id.main_content, new FlowerDetailsFragment())
                            .commit();

                    isItemClicked = true;
                    Log.d("isItemClick2", isItemClicked + "");

                    //timer to make isItemClicked = false
                    new CountDownTimer(1000, 1000) {
                        @Override
                        public void onTick(long l) {

                        }

                        @Override
                        public void onFinish() {
                            isItemClicked = false;
                            Log.d("isItemClick3", isItemClicked + "");
                        }
                    }.start();
                }
            }

            @Override
            public void onLongItemClick(View view, int position) {

            }
        }));


        return rootView;
    }

    private Activity mContext;

    @Override
    public void onAttach(Context context) {
        this.mContext = (Activity) context;
        super.onAttach(context);
    }

    private void getFlowerSubListing() {
        String url = API.getAPI();
    }
}*/

//package com.app.boxes.old.Fragment;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.app.boxes.old.Activity.MainActivity;
import com.app.boxes.old.Adapter.FlowerListAdapter;
import com.app.boxes.old.Listener.RecyclerItemClickListener;
import com.app.boxes.old.Model.Flower;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */

public class FlowerSubListingFragment extends Fragment {

    private static final String TAG = "FlowerSubListing";

    private RecyclerView recyclerView;
    private FlowerListAdapter adapter;
    private ArrayList<Flower> flowerArrayList = new ArrayList<>();
    private TextView tvTitle;

    private boolean isItemClicked = false;

    private MySharedPreferences sPref;

    View rootView;

    public FlowerSubListingFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        // Inflate the layout for this fragment
        if (rootView == null) {
            rootView = inflater.inflate(R.layout.fragment_flower_sub_listing, container, false);
        }

        sPref = new MySharedPreferences(mContext);

        //initialize widgets
        tvTitle = rootView.findViewById(R.id.tv_flower_sub_listing_title);

        recyclerView = rootView.findViewById(R.id.rv_flower_listing);

        //set the layout manager for recyclerview
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));

        //get the selected category name and set the title
        if (sPref.getCategoryName() != null) {
            String categoryName = sPref.getCategoryName();
            tvTitle.setText(categoryName.toUpperCase());
        }

        //clear and add the new data on flower arraylist
        if (getArguments() != null) {
            Log.d(TAG, "onCreateView: getArugments()");
            // get parcelable lists from bundle
            flowerArrayList = getArguments().getParcelableArrayList("flowers_list");

//            flowerArrayList = sPref.getFlowerArrayList();

            // check if arraylist are not null
            if (flowerArrayList != null) {
                savedInstanceState = new Bundle();
                savedInstanceState.putParcelableArrayList("saved_flower_list", flowerArrayList);

                Log.d(TAG, "onCreateView: Flower Arraylist Size: " + flowerArrayList.size());

                //initialize flower list adapter
                adapter = new FlowerListAdapter(mContext, flowerArrayList);

                //set the flower list adapter on recyclerview
                recyclerView.setAdapter(adapter);

                //navigate to flower details fragment on recyclerview item click
                recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(mContext, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // get recyclerview item clicked
                        Flower flower = flowerArrayList.get(position);
                        // create a new arraylist and put clicked item in that arraylist
                        ArrayList<Flower> mList = new ArrayList<>();
                        Log.d("isItemClick1", isItemClicked + "");
                        if (!isItemClicked) {
                            // adding clicked item in arraylist
                            mList.add(flower);
                            if (getFragmentManager() != null) {
                                Bundle bundle = new Bundle();
                                // putting that arraylist in bundle
                                bundle.putParcelableArrayList("mFlowers_list", mList);

                                Fragment fragment = new FlowerDetailsFragment();
                                fragment.setArguments(bundle);

                                ((MainActivity)mContext).navigationController(R.id.flowerDetailsFragment, bundle, null);

                                /*getFragmentManager()
                                        .beginTransaction()
                                        .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                                        .replace(R.id.main_content, fragment)
                                        .addToBackStack(null)
                                        .commit();*/
                            }

                            isItemClicked = true;
                            Log.d("isItemClick2", isItemClicked + "");

                            //timer to make isItemClicked = false
                            new CountDownTimer(1000, 1000) {
                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {
                                    isItemClicked = false;
                                    Log.d("isItemClick3", isItemClicked + "");
                                }
                            }.start();
                        }
                    }

                    @Override
                    public void onLongItemClick(View view, int position) {

                    }
                }));
            }

        }

        return rootView;
    }

    private Activity mContext;

    @Override
    public void onAttach(Context context) {
        this.mContext = (Activity) context;
        super.onAttach(context);
    }

//    @Override
//    public void onResume() {
//        super.onResume();
//
//        //set the layout manager for recyclerview
//        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
//
//        //clear and add the new data on flower arraylist
//        if (getArguments() != null) {
//            Log.d(TAG, "onResume: getArugments()");
//            // get parcelable lists from bundle
//            flowerArrayList = getArguments().getParcelableArrayList("flowers_list");
//
//            // check if arraylist are not null
//            if (flowerArrayList != null) {
//
//                Log.d(TAG, "onResume: Flower Arraylist Size: " + flowerArrayList.size());
//
//                //initialize flower list adapter
//                adapter = new FlowerListAdapter(mContext, flowerArrayList);
//                adapter.notifyDataSetChanged();
//
//                //set the flower list adapter on recyclerview
//                recyclerView.setAdapter(adapter);
//            }
//        }
//
//        Log.d(TAG, "onResume: called");
//    }
//
//    @Override
//    public void onStart() {
//        super.onStart();
//
//        //set the layout manager for recyclerview
//        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
//
//        //clear and add the new data on flower arraylist
//        if (getArguments() != null) {
//            Log.d(TAG, "onStart: getArugments()");
//            // get parcelable lists from bundle
//            flowerArrayList = getArguments().getParcelableArrayList("flowers_list");
//
//            // check if arraylist are not null
//            if (flowerArrayList != null) {
//
//                Log.d(TAG, "onStart: Flower Arraylist Size: " + flowerArrayList.size());
//
//                //initialize flower list adapter
//                adapter = new FlowerListAdapter(mContext, flowerArrayList);
//                adapter.notifyDataSetChanged();
//
//                //set the flower list adapter on recyclerview
//                recyclerView.setAdapter(adapter);
//            }
//        }
//
//        Log.d(TAG, "onStart: called");
//    }
}