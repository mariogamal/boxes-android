package com.app.boxes.old.Fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.app.boxes.old.API.API;
import com.app.boxes.old.Activity.MainActivity;
import com.app.boxes.old.Activity.SearchActivity;
import com.app.boxes.old.Adapter.CategoryAdapter;
import com.app.boxes.old.Adapter.TabAdapter;
import com.app.boxes.old.CategorySingleton;
import com.app.boxes.old.Listener.RecyclerItemClickListener;
import com.app.boxes.old.Model.Category;
import com.app.boxes.old.Model.CategorySubType;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.app.boxes.old.Fragment.FlowerHomeFragment.categoryImagesList;
import static com.app.boxes.old.Fragment.FlowerHomeFragment.isData;

/**
 * A simple {@link Fragment} subclass.
 */
//@Puppet
public class HomeFragment extends Fragment {

    //    private static ArrayList<Category> categoryImagesList = new ArrayList<>();
    private ArrayList<CategorySubType> categorySubTypeArrayList = new ArrayList<>();

    private TabAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private LinearLayout llSearch;
    private RecyclerView categoryRecyclerView;

    private TextView tvToolbarText;
    private EditText etSearch;

    private CategoryAdapter categoryAdapter;

    private boolean isItemClicked = false;
//    static boolean isData = false;

    MySharedPreferences mPref;

    private static String TAG = "HomeFragment";

    private String languageId = "", categoryId = "", categoryName = "", categoryImage = "", categoryDefault = "",
            categoryIsFood = "", subCategoryId = "", subCategoryParentId = "", subCategoryType = "", typeId = "";

    private Bundle bundleCategories, bundleSubCategory;

    private Fragment selectedFragment;

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View parentView = inflater.inflate(R.layout.fragment_home, container, false);

//        ((MainActivity) mContext).getSupportActionBar().show();

        //initialize widgets
        tabLayout = parentView.findViewById(R.id.tabLayout);
        viewPager = parentView.findViewById(R.id.viewPager);
        tvToolbarText = ((MainActivity) mContext).findViewById(R.id.tv_toolbar_title);
//        categoryRecyclerView = parentView.findViewById(R.id.rv_horizontal_categories);

//        llSearch = parentView.findViewById(R.id.ll_search_view);

//        etSearch = parentView.findViewById(R.id.search_view_home);

        bundleCategories = new Bundle();
        bundleSubCategory = new Bundle();

        mPref = new MySharedPreferences(mContext);

        if (mPref.getLangObject() != null) {
            languageId = mPref.getLangObject().getId();

            //get categories from server
            getCategories(languageId);

            Log.d(TAG, "language id " + mPref.getLangObject().getId());
        }

        //set the toolbar text
        tvToolbarText.setText(R.string.boxes);

        categoryRecyclerView.setLayoutManager(new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false));

        Bundle getBundleCategories = getArguments();

        if (getBundleCategories != null) {

            typeId = getBundleCategories.getString("categoryId");

            Log.d(TAG, "bundle values " + " catId " + typeId);
        } else {
            Log.d(TAG, "Bundle is empty");
        }

        llSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, SearchActivity.class);
                intent.putExtra("type_id", typeId);
                startActivity(intent);
            }
        });

        return parentView;
    }

    private Activity mContext;

    @Override
    public void onAttach(Context context) {
        this.mContext = (Activity) context;
        super.onAttach(context);
    }

    boolean isHighlighted = false;

    private void getCategories(String language_id) {
        String url = API.getAPI() + "types?language_id=" + language_id;
        Log.d(TAG, "URL: " + url);

        RequestQueue requestQueue;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                try {
                    //categoryImagesList.clear();
                    categorySubTypeArrayList.clear();

                    int status = response.getInt("status");
                    String message = response.getString("message");

                    if (status == 1) {

                        JSONArray dataArray = response.getJSONArray("data");

                        for (int i = 0; i < dataArray.length(); i++) {
                            final JSONObject jsonDataObject = dataArray.getJSONObject(i);

                            categoryId = jsonDataObject.getString("id");
                            categoryName = jsonDataObject.getString("type_name");
                            categoryImage = jsonDataObject.getString("image");
                            categoryDefault = jsonDataObject.getString("default");
                            categoryIsFood = jsonDataObject.getString("is_food");

                            String imageUrl = API.getImageAPI() + categoryImage;

                            Category category = new Category(categoryId, imageUrl, categoryName, categoryDefault, categoryIsFood, isHighlighted);
                            Log.d(TAG, "on response: " + categoryId + " " + categoryName + " " + categoryImage + " " + categoryDefault + " " + categoryIsFood);

                            if (!categoryImagesList.contains(category) && !isData)
                                categoryImagesList.add(category);

//                            categoryImagesList = CategorySingleton.getInstance().getCategoryArrayList();

                            categoryAdapter = new CategoryAdapter(mContext, categoryImagesList);

                            categoryRecyclerView.setAdapter(categoryAdapter);

                        }
                        isData = true;

//                        CategorySingleton.getInstance().setCategoryArrayList(categoryImagesList);

                        categoryRecyclerView.addOnItemTouchListener(new RecyclerItemClickListener(mContext, categoryRecyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                            @Override
                            public void onItemClick(View view, int position) {
                                Log.d("isItemClick1", isItemClicked + "");
//                                    if (!isItemClicked) {
                                String item = categoryImagesList.get(position).getIsFood();
                                String categoryId = categoryImagesList.get(position).getId();
                                String catName = categoryImagesList.get(position).getName();

                                for (int i = 0; i < categoryImagesList.size(); i++) {
                                    if (categoryImagesList.get(i).getCheckDefault().equals("1")) {
                                        categoryImagesList.get(i).setCheckDefault("0");
                                        break;
                                    }
                                }

                                categoryImagesList.get(position).setCheckDefault("1");
                                CategorySingleton.getInstance().categoryId = categoryId;
                                CategorySingleton.getInstance().isFood = Integer.parseInt(item);
//                                CategorySingleton.getInstance().setCategoryArrayList(categoryImagesList);
                                categoryAdapter.notifyDataSetChanged();
                                // save selected Category name in shared preferences
                                mPref.setCategoryName(catName);

                                bundleCategories.putString("categoryId", categoryId);

                                Log.d(TAG, "onItemClick: is food " + item + " id " + categoryId + " position " + position);

                                if (Integer.parseInt(item) == 1) {
                                    selectedFragment = new HomeFragment();
                                    selectedFragment.setArguments(bundleCategories);

                                    if (getFragmentManager() != null/* && getFragmentManager().findFragmentByTag("home") == null*/) {

                                        getFragmentManager()
                                                .beginTransaction()
//                                                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                                                .replace(R.id.main_content, selectedFragment, "home")
                                                .addToBackStack(null)
                                                .commit();
                                    }
                                } else {
                                    selectedFragment = new FlowerHomeFragment2();
                                    selectedFragment.setArguments(bundleCategories);

                                    if (getFragmentManager() != null /*&& getFragmentManager().findFragmentByTag("flower_home") == null*/) {
                                        getFragmentManager()
                                                .beginTransaction()
//                                                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                                                .add(R.id.main_content, selectedFragment, "flower_home")
                                                .addToBackStack(null)
                                                .commit();
                                    }
                                }

                                isItemClicked = true;
                                Log.d("isItemClick2", isItemClicked + "");

                                //timer to make isItemClicked = false
                                new CountDownTimer(1000, 1000) {
                                    @Override
                                    public void onTick(long l) {

                                    }

                                    @Override
                                    public void onFinish() {
                                        isItemClicked = false;
                                        Log.d("isItemClick3", isItemClicked + "");
                                    }
                                }.start();
                            }

                            @Override
                            public void onLongItemClick(View view, int position) {

                            }
                        }));

                    } else {
                        Log.d(TAG, message);
                    }

                } catch (Exception ex) {
                    Log.d(TAG, "exception: " + ex.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onError: " + error.toString());
            }
        });

        requestQueue = Volley.newRequestQueue(mContext);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }

    @Override
    public void onResume() {
        super.onResume();

//        ((MainActivity) mContext).getSupportActionBar().show();

        if (mPref.getLangObject() != null) {
            languageId = mPref.getLangObject().getId();

            //get categories from server
            getCategories(languageId);

            Log.d(TAG, "language id " + mPref.getLangObject().getId());
        }

    }
}