package com.app.boxes.old.Service;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.app.boxes.R;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static int count = 0;
    private static final String TAG = "FirebaseMsgNotif";

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Log.d(TAG, "onMessageReceived data " + remoteMessage.getData().toString() + "");
        Log.d(TAG, "onMessageReceived body " + remoteMessage.getNotification().getBody() + "");
        Log.d(TAG, "onMessageReceived title " + remoteMessage.getData().get("title") + "");
        Log.d(TAG, "onMessageReceived text " + remoteMessage.getData().get("text") + "");
        Log.d(TAG, "onMessageReceived description: " + remoteMessage.getData().get("description") + "");

        if (remoteMessage.getNotification() != null) {
            //Calling method to generate notification
            sendNotification(remoteMessage.getNotification().getTitle(),
                    remoteMessage.getNotification().getBody(), remoteMessage.getData());
        }
    }

    //This method is only generating push notification
    private void sendNotification(String messageTitle, String messageBody, Map<String, String> row) {
        PendingIntent pendingIntent = null;

        Log.d(TAG, "Notification Data: " + row.get("title") + " Body: " + row.get("body"));

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setLargeIcon(BitmapFactory.decodeResource(getApplication().getResources(), R.drawable.launcher_icon))
                .setSmallIcon(R.drawable.launcher_icon)
                .setContentTitle(messageTitle)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (notificationManager != null) {
            notificationManager.notify(count, notificationBuilder.build());
        }
        count++;
    }

    @Override
    public void onNewToken(String s) {
        super.onNewToken(s);

        String refreshToken = FirebaseInstanceId.getInstance().getToken();
        Log.d("Firebase-Token ", refreshToken);
    }
}
