package com.app.boxes.old.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.app.boxes.old.Model.Menu;
import com.app.boxes.old.Model.MenuItem;
import com.app.boxes.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import jp.wasabeef.picasso.transformations.RoundedCornersTransformation;

public class CalenderMenuItemAdapter extends BaseExpandableListAdapter {

    private Context context;
    private ArrayList<Menu> menuArrayList = new ArrayList<Menu>();
    private LayoutInflater inflater;
    OnMenuClickListnener listener;
    private static int isCheckedItem = 0;

    //check whether checkbox is set unactive by default
    boolean flag = false;

    public CalenderMenuItemAdapter(Context context, ArrayList<Menu> menuArrayList, OnMenuClickListnener listener) {
        this.context = context;
        this.menuArrayList = menuArrayList;
        /*   this.listener = listener;*/

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    //get number of category
    @Override
    public int getGroupCount() {
        return menuArrayList.size();
    }

    //get number of menu item
    @Override
    public int getChildrenCount(int groupPosition) {
        return menuArrayList.get(groupPosition).menuItems.size();
    }

    //get menu category
    @Override
    public Object getGroup(int groupPosition) {
        return menuArrayList.get(groupPosition);
    }

    //get single menu
    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return menuArrayList.get(groupPosition).menuItems.get(childPosition);
    }

    //get menu category id
    @Override
    public long getGroupId(int i) {
        return 0;
    }

    //get menu item id
    @Override
    public long getChildId(int i, int i1) {
        return 0;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    //get menu category row
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = inflater.inflate(R.layout.menu_category, null);
        }

        ImageView ivIcon = convertView.findViewById(R.id.iv_icon);

        if (isExpanded) {
            ivIcon.animate().rotation(0f).start();
        } else {
            ivIcon.animate().rotation(180f).start();
        }
        //get menu item
        Menu menu = (Menu) getGroup(groupPosition);

        //set menu item
        TextView menuName = convertView.findViewById(R.id.menuCategory);
        TextView menuOutOff = convertView.findViewById(R.id.menu_category_out_off);
        TextView menuTotal = convertView.findViewById(R.id.menu_category_total);

        String category = menu.Category;
        menuName.setText(category);
        menuOutOff.setText(String.valueOf(menu.getOutOff()));
        menuTotal.setText(String.valueOf(menu.getTotal()));

//        ExpandableListView mExpandableListView = (ExpandableListView) parent;
//        mExpandableListView.expandGroup(groupPosition);

//        ((ExpandableListView) parent).expandGroup(groupPosition);

        return convertView;
    }

    //get menu item row
    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final MenuItem child = (MenuItem) getChild(groupPosition, childPosition);

        if (child != null) {
            convertView = inflater.inflate(R.layout.menu_item_list, null);

            ImageView menuImage = convertView.findViewById(R.id.iv_menu_list_icon);
            TextView menuName = convertView.findViewById(R.id.menu_name);
            TextView menuDesc = convertView.findViewById(R.id.menu_desc);
            final ImageView menuChecked = convertView.findViewById(R.id.iv_menu_list_checked);
            menuChecked.setVisibility(View.VISIBLE);
            TextView menuCalories = convertView.findViewById(R.id.menu_calories);
            TextView menuCarbs = convertView.findViewById(R.id.menu_carbs);
            TextView menuProtein = convertView.findViewById(R.id.menu_protein);
            TextView menuRating = convertView.findViewById(R.id.tv_menu_list_rating);

            if (child.getMessage() != null && child.getMessage().equals("No meals found")) {
                convertView = inflater.inflate(R.layout.menu_item_message, null);
                TextView tvMsg = convertView.findViewById(R.id.tv_msg);
                tvMsg.setText(child.getMessage());

            } else {
                Picasso.get().load(child.getImage()).transform(new RoundedCornersTransformation(50, 0)).placeholder(R.drawable.boxes_logo).into(menuImage);
                menuName.setText(child.getProductName());
                menuDesc.setText(child.getDecripton());
                menuChecked.setImageResource(child.getIcon());
                menuCalories.setText(child.getCalories());
                menuCarbs.setText(child.getCarbohydrates());
                menuProtein.setText(child.getProteins());
                menuRating.setText(child.getRating() + "");
//                tvMsg.setVisibility(View.GONE);

                if (child.isChecked()) {
                    menuChecked.setImageResource(R.drawable.check_box_active);
                } else {
                    menuChecked.setImageResource(R.drawable.check_box_unactive);
                }
            }
        }

//        parent.getExpandableListAdapter().getChild(groupPosition, childPosition);

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int i, int i1) {
        return true;
    }

    public interface OnMenuClickListnener {
        void mOnMenuItemClickListener(MenuItem menu, int pos, int childPos);
    }
}