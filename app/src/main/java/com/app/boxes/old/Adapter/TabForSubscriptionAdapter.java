package com.app.boxes.old.Adapter;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.app.boxes.old.Fragment.MultiRestaurantListFragment;
import com.app.boxes.old.Fragment.SingleRestaurantListFragment;
import com.app.boxes.old.Model.SingleRestaurant;
import com.app.boxes.old.Model.TabModel;
import com.app.boxes.old.SharedPreference.MySharedPreferences;

import java.util.ArrayList;

public class TabForSubscriptionAdapter extends FragmentStatePagerAdapter {

    private ArrayList<TabModel> tabName;
    private ArrayList<ArrayList<SingleRestaurant>> restaurantList;
    private Context context;
    private MySharedPreferences mPref;
    private Bundle getBundleData;

    private String searchQuery;

    static SingleRestaurantListFragment singleRestaurantListFragment;
    static MultiRestaurantListFragment multiRestaurantListFragment;

    public TabForSubscriptionAdapter(FragmentManager fm, ArrayList<String> tabName, ArrayList<ArrayList<SingleRestaurant>> restaurantList, Context c) {
        super(fm);
//        this.tabName = tabName;
        this.restaurantList = restaurantList;
        this.context = c;

        mPref = new MySharedPreferences(context);
    }

    public TabForSubscriptionAdapter(FragmentManager fm, ArrayList<TabModel> tabName, Context c/*,String searchQuery*/) {
        super(fm);
        this.tabName = tabName;
        this.context = c;
        this.searchQuery = searchQuery;

        getBundleData = new Bundle();

        mPref = new MySharedPreferences(context);
    }

    @Override
    public Fragment getItem(int position) {
        if (tabName.get(position).getTabName().toLowerCase().contains("single")) {
            singleRestaurantListFragment = new SingleRestaurantListFragment(tabName.get(position).getId()/*, searchQuery*/);
            return singleRestaurantListFragment;
        } else {
            multiRestaurantListFragment = new MultiRestaurantListFragment(tabName.get(position).getId()/*, searchQuery*/);
            return multiRestaurantListFragment;
        }
    }

    @Override
    public int getCount() {
        return tabName.size();
    }

    //for tab names
    @Override
    public CharSequence getPageTitle(int position) {
        return tabName.get(position).getTabName();
    }

    public void refresh(int position) {
        try {
            if (tabName.get(position).getTabName().toLowerCase().contains("single")) {
                singleRestaurantListFragment.refresh();
            } else {
                multiRestaurantListFragment.refresh();
            }
        } catch (Exception e) {
            String ex = e.toString();
            Log.d("TabSubscription", "refresh expcetion: " + ex);
        }
    }

    public void refreshAll(int positionTab) {
        if (tabName.get(positionTab).getTabName().toLowerCase().contains("single")) {
            singleRestaurantListFragment.refreshAll();
        } else {
            multiRestaurantListFragment.refreshAll();
        }
    }
}