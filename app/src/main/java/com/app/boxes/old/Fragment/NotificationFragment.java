package com.app.boxes.old.Fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.app.boxes.old.API.API;
import com.app.boxes.old.Adapter.NotificationAdapter;
import com.app.boxes.old.Model.Language;
import com.app.boxes.old.Model.Notification;
import com.app.boxes.old.Model.User;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.google.android.material.snackbar.Snackbar;
import com.wang.avi.AVLoadingIndicatorView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
//@Puppet
public class NotificationFragment extends Fragment {

    //region view variable
    private RecyclerView recyclerView;
    private AVLoadingIndicatorView avl;
    private View snackbarView;
    private RelativeLayout rlNoResult;
    private Button btnHome;
    //endregion

    //region adapter variable
    NotificationAdapter adapter;
    //endregion

    //region arraylist variable
    ArrayList<Notification> notificationArrayList = new ArrayList<Notification>();
    //endregion

    //region string variable
    private static String TAG = "NotificationFragment";
    //endregion

    private Activity mContext;

    private MySharedPreferences mPref;

    public NotificationFragment() {
        // Required empty public constructor
    }

    //region onCreateView method
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_notification, container, false);

        initializer(rootView);

        // check if access token and language object is not null then get the access token and language id
        if (mPref.getAccessTokenObject() != null && mPref.getLangObject() != null) {
            User.AccessToken token = mPref.getAccessTokenObject();
            Language language = mPref.getLangObject();
            if (token != null && language != null) {
                //get access token
                String accessToken = token.getAccessToken();
                String languageId = language.getId();
                // get notification from server
                getNotification(languageId, accessToken);
            }
        }

        return rootView;
    }
    //endregion

    //region alert dialog
    private void alertDialog(final Context context) {

        final AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(getString(R.string.message));
        alertDialog.setMessage(getString(R.string.message_content));
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getString(R.string.yes), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                alertDialog.dismiss();
                dialogInterface.dismiss();
                mContext.finish();
            }
        });
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getString(R.string.no), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
//                alertDialog.cancel();
                alertDialog.dismiss();
//                dialogInterface.dismiss();

                //getFragmentManager().popBackStackImmediate();
            }
        });

        alertDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialogInterface, int keyCode, KeyEvent keyEvent) {
                // TODO Auto-generated method stub
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    alertDialog.dismiss();
                }
                return true;
            }
        });
        alertDialog.show();
    }
    //endregion

    //region onAttach method
    @Override
    public void onAttach(Context context) {
        this.mContext = (Activity) context;
        super.onAttach(context);
    }
    //endregion

    //region initializer method
    private void initializer(View view) {
        snackbarView = mContext.findViewById(R.id.main_content);

        avl = view.findViewById(R.id.avi_notification);
        avl.setVisibility(View.GONE);

        recyclerView = view.findViewById(R.id.rv_notification);

        rlNoResult = view.findViewById(R.id.rl_notification_no_result);
        rlNoResult.setVisibility(View.GONE);

        mPref = new MySharedPreferences(mContext);

        btnHome = view.findViewById(R.id.btn_notification_no);

        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                startActivity(new Intent(mContext, MainActivity.class));
                mContext.onBackPressed();
            }
        });
    }
    //endregion

    //region getNotification method
    private void getNotification(String languageId, final String accessToken) {
        String url = API.getAPI() + "auth/notifications?language_id=" + languageId;
        Log.d(TAG, "URL: " + url);

        avl.setVisibility(View.VISIBLE);

        RequestQueue requestQueue;

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d(TAG, response.toString());
                avl.setVisibility(View.GONE);

                try {
                    notificationArrayList.clear();

                    int status = response.getInt("status");
                    String message = response.getString("message");

                    if (status == 1) {
                        rlNoResult.setVisibility(View.GONE);
                        JSONArray dataArray = response.getJSONArray("data");

                        Notification notification = new Notification();

                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject jsonDataObject = dataArray.getJSONObject(i);

                            String nTitle = jsonDataObject.getString("title");
                            String nMessage = jsonDataObject.getString("message");
                            String nDate = jsonDataObject.getString("notification_date");

                            notification.setTitle(nTitle);
                            notification.setMessage(nMessage);
                            notification.setDate(nDate);

                            notificationArrayList.add(notification);
                        }

                        // set the layout manager for recyclerview
                        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));

                        // set custom adapter
                        adapter = new NotificationAdapter(mContext, notificationArrayList);
                        recyclerView.setAdapter(adapter);
                    } else {
                        rlNoResult.setVisibility(View.VISIBLE);
                    }
                } catch (Exception ex) {
                    Log.d(TAG, "exception: " + ex.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                avl.setVisibility(View.GONE);

                Log.d(TAG, "Volley Error: " + error.toString());
                if (error instanceof NoConnectionError) {
                    Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.no_network), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));
                    snackbar.show();
                } else {
                    Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));
                    snackbar.show();
                }
                Log.d(TAG, "onError: " + error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (accessToken != null) {
                    params.put("Authorization", accessToken);
                    Log.d(TAG, "getHeaders " + accessToken);
                }
                return params;
            }
        };
        ;

        requestQueue = Volley.newRequestQueue(mContext);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);

    }
//endregion
}