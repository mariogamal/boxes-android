package com.app.boxes.old.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.boxes.old.Model.Cart;
import com.app.boxes.R;

import java.util.ArrayList;

public class MyCartAdapter extends RecyclerView.Adapter<MyCartAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<Cart> cartArrayList;
    private LayoutInflater inflater;
    private MyCartAdapterCallback callback;

    public MyCartAdapter(Context context, ArrayList<Cart> cartArrayList, MyCartAdapterCallback callback) {
        this.context = context;
        this.cartArrayList = cartArrayList;
        this.callback = callback;

        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView = inflater.inflate(R.layout.list_cart_item, parent, false);

        return new MyViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        final Cart cart = cartArrayList.get(position);

        holder.tvProductName.setText(cart.getProdName());
        holder.tvProductQuantity.setText(cart.getProdQuantity());
        holder.tvProductPrice.setText(cart.getProdPrice());

        holder.ivBtnMinus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                int quantity = Integer.parseInt(holder.tvProductQuantity.getText().toString()) - 1;
//                holder.tvProductQuantity.setText(String.valueOf(quantity));
                callback.onMyCartSubClicked(cart, position);
                //callback.onMyCartClickCallback(cart);
            }
        });

        holder.ivBtnPlus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                int quantity = Integer.parseInt(holder.tvProductQuantity.getText().toString()) + 1;
//                holder.tvProductQuantity.setText(String.valueOf(quantity));
                // callback.onMyCartClickCallback(cart);
                callback.onMyCartAddClicked(cart);

            }
        });
    }

    @Override
    public int getItemCount() {
        return cartArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView ivBtnPlus, ivBtnMinus;
        TextView tvProductQuantity, tvProductName, tvProductPrice;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            ivBtnMinus = itemView.findViewById(R.id.iv_cart_item_btn_minus);
            ivBtnPlus = itemView.findViewById(R.id.iv_cart_item_btn_plus);
            tvProductQuantity = itemView.findViewById(R.id.tv_cart_item_quantity);
            tvProductName = itemView.findViewById(R.id.tv_cart_item_name);
            tvProductPrice = itemView.findViewById(R.id.tv_cart_item_price);

        }
    }

    public interface MyCartAdapterCallback {
        void onMyCartAddClicked(Cart cart);

        void onMyCartSubClicked(Cart cart, int position);
    }
}