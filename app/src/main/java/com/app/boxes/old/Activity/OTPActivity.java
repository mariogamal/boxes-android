package com.app.boxes.old.Activity;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.app.boxes.old.API.API;
import com.app.boxes.old.Model.User;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.mukesh.OnOtpCompletionListener;
import com.mukesh.OtpView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.concurrent.TimeUnit;

public class OTPActivity extends AppCompatActivity {

    private static final String TAG = "OTPActivity";
    private String mFirebaseToken = null;
    private String getPhoneNo = null, otpCode = "";

    // declare components
    private TextView tvCodeVerif, tvTermsCond, tvResend;
    private ImageView btnBack;
    private Button btnSubmit;
    private View snackbarView;
    private LinearLayout loaderLayout;

    private OtpView otpView;

    private MySharedPreferences sPref;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private String mVerificationId;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);

        // initialize components
        initializeComponents();

        Intent getIntent = getIntent();
        getPhoneNo = getIntent.getStringExtra("phone_number");

        if (getPhoneNo != null) {
            tvCodeVerif.setText(getResources().getString(R.string.please_enter_the_verification_code_sent_n_to) + " " + getPhoneNo);
            mAuth = FirebaseAuth.getInstance();
            initCallbacks();
            sendOTP();
        }

        otpView.setOtpCompletionListener(new OnOtpCompletionListener() {
            @Override
            public void onOtpCompleted(String otp) {
                otpCode = otp;
                // do Stuff
                Log.d("onOtpCompleted=>", otp);
            }
        });


        // change terms and conditions text color
        changeSubStringColor();

        //get the firebase token id
        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }

                        if (task.getResult() != null) {
                            // Get new Instance ID token
                            mFirebaseToken = task.getResult().getToken();
                            Log.d(TAG, "Firebase-Token: " + mFirebaseToken);
                        }

                        // Log and toast
                        Log.d(TAG, "Firebase-Message Token: " + mFirebaseToken);
                    }
                });

//        requestFocus();

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();
                signInWithPhoneAuthCredential(PhoneAuthProvider.getCredential(mVerificationId, otpCode));
            }
        });

        tvResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard();
                sendOTP();
            }
        });
    }

    private void sendOTP() {

        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                getPhoneNo,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks);
    }

    private void initCallbacks() {
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {

            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                // This callback will be invoked in two situations:
                // 1 - Instant verification. In some cases the phone number can be instantly
                //     verified without needing to send or enter a verification code.
                // 2 - Auto-retrieval. On some devices Google Play services can automatically
                //     detect the incoming verification SMS and perform verification without
                //     user action.
                Log.d(TAG, "onVerificationCompleted:" + credential);

                signInWithPhoneAuthCredential(credential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                // This callback is invoked in an invalid request for verification is made,
                // for instance if the the phone number format is not valid.
                Log.w(TAG, "onVerificationFailed", e);

                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    // Invalid request
                    // ...
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    // The SMS quota for the project has been exceeded
                    // ...
                }

                // Show a message and update the UI
                // ...
            }

            @Override
            public void onCodeSent(@NonNull String verificationId,
                                   @NonNull PhoneAuthProvider.ForceResendingToken token) {
                // The SMS verification code has been sent to the provided phone number, we
                // now need to ask the user to enter the code and then construct a credential
                // by combining the code with a verification ID.
                Log.d(TAG, "onCodeSent:" + verificationId);

                // Save verification ID and resending token so we can use them later
                mVerificationId = verificationId;
                mResendToken = token;

                // ...
            }
        };
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");

                            FirebaseUser user = task.getResult().getUser();
                            sendOTPToServer();
                        } else {
                            // Sign in failed, display a message and update the UI
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                            }
                        }
                    }
                });
    }

    private void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }

    // initializer
    void initializeComponents() {
        sPref = new MySharedPreferences(this);

        tvCodeVerif = findViewById(R.id.tv_code_sent_otp);
//        tvResend = findViewById(R.id.tv_code_resend_otp);
        tvTermsCond = findViewById(R.id.tv_terms_conditions_otp);

        btnBack = findViewById(R.id.iv_back_otp);

//        btnSubmit = findViewById(R.id.btn_submit_otp);

//        otpView = findViewById(R.id.otp_view);

//        loaderLayout = findViewById(R.id.ll_loader_otp);
        loaderLayout.setVisibility(View.GONE);

        snackbarView = findViewById(android.R.id.content);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    // change text color
    void changeSubStringColor() {
        String text = "By creating your account, you agree with our Terms and Conditions";

        //get text which we want to change color of specific substring
        SpannableString spannableString = new SpannableString(text/*getString(R.string.by_creating_your_account_you_agree_with_our_n_terms_and_conditions)*/);
        //================================================================

        //get color for text
        ForegroundColorSpan frgred = new ForegroundColorSpan(Color.parseColor("#5DC04B"));
        //====================================================================

        //set color in specific string length
        spannableString.setSpan(frgred, 44, 65, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        //====================================================================

        //Terms and condition in textview is clicable
        ClickableSpan termAndConditionClick = new ClickableSpan() {
            @Override
            public void onClick(@NonNull View view) {
                Log.d("termandcondition", "clicked");
                startActivity(new Intent(OTPActivity.this, TermsAndConditionActivity.class));
            }

            @Override
            public void updateDrawState(@NonNull TextPaint ds) {
                super.updateDrawState(ds);

                ds.setColor(getResources().getColor(R.color.active_tab_color));
                ds.setUnderlineText(false);
            }
        };

        //set color in specific string length
        spannableString.setSpan(termAndConditionClick, 44, 62, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        //====================================================================

        //set spannable text
        tvTermsCond.setText(spannableString);
        tvTermsCond.setMovementMethod(LinkMovementMethod.getInstance());
        //=============================================================
    }

    private String firstName = null, lastName = null, email = null;

    // sending OTP request to server
    void sendOTPToServer() {
        if (getPhoneNo != null && mFirebaseToken != null) {
            String URL = API.getAPI() + "verify-otp?otp_code=1234" +
                    "&phone=" + getPhoneNo +
                    "&device_type=android" +
                    "&device_token=" + mFirebaseToken;
            Log.d(TAG, "OTP URL : " + URL);

            loaderLayout.setVisibility(View.VISIBLE);
            RequestQueue requestQueue;
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, URL, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d(TAG, "OTP Response: " + response);
                    loaderLayout.setVisibility(View.GONE);

                    try {
                        int status = response.getInt("status");
                        String msg = response.getString("message");

                        Snackbar snackbar = Snackbar.make(snackbarView, msg, Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
                        snackbar.show();

                        if (status == 1) {
                            JSONObject dataObj = response.getJSONObject("data");
                            String access_token = dataObj.getString("access_token");
                            String token_type = dataObj.getString("token_type");

                            String accessToken = token_type + " " + access_token;
                            Log.d(TAG, "OTP AccessToken: " + accessToken);

                            final JSONObject userObj = dataObj.getJSONObject("user");
                            int isSurveyFilled = 0, isProfileFilled = 0;
                            if (dataObj.has("user")) {
                                String userId = userObj.getString("id");
                                firstName = userObj.getString("first_name");
                                lastName = userObj.getString("last_name");
                                email = userObj.optString("email");
                                String phoneNo = userObj.getString("phone");
                                isSurveyFilled = userObj.getInt("is_survey_filled");
                                isProfileFilled = userObj.getInt("is_profile_filled");

                                User user = new User();

                                user.setId(userId);
                                user.setFirstName(firstName);
                                user.setLastName(lastName);
                                user.setEmail(email);
                                user.setPhoneNumber(phoneNo);

                                sPref.saveUserObject(user);
                            }

                            User.AccessToken token = new User.AccessToken();
                            token.setAccessToken(accessToken);
                            sPref.saveAccessTokenObject(token);

                            if (isSurveyFilled == 1 && isProfileFilled == 1) {
                                startActivity(new Intent(OTPActivity.this, MainActivity.class));
                                finish();
                                finishAffinity();
                            } else if (isProfileFilled == 0 && isSurveyFilled == 0){
                                startActivity(new Intent(OTPActivity.this, AddProfileActivity.class));
                                finish();
                                finishAffinity();
                            } else if (isProfileFilled == 1 && isSurveyFilled == 0) {
                                startActivity(new Intent(OTPActivity.this, SurveyFormActivity.class));
                                finish();
                                finishAffinity();
                            }
                        } else if (status == 0) {

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    loaderLayout.setVisibility(View.GONE);
                    if (error instanceof NoConnectionError) {
                        Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.no_network), Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
                        snackbar.show();
                    } else {
                        Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
                        snackbar.show();
                    }
                }
            });
            requestQueue = Volley.newRequestQueue(this);
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(jsonObjectRequest);
        }
    }

    // resend OTP to user
    void resendOTPToPhone() {
        String url = API.getAPI() + "login?phone=" + getPhoneNo;
        Log.d(TAG, "Resend OTP URL: " + url);

        loaderLayout.setVisibility(View.VISIBLE);
        RequestQueue requestQueue;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, "Resend OTP Response" + response.toString());
                loaderLayout.setVisibility(View.GONE);

                try {
                    int status = response.getInt("status");

                    if (status == 1) {
                        String msg = response.getString("message");

                        Snackbar snackbar = Snackbar.make(snackbarView, msg, Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
                        snackbar.show();
                    } else {
                        Toast.makeText(OTPActivity.this, "You are not registered", Toast.LENGTH_SHORT).show();
                    }

                } catch (Exception ex) {
                    Log.d(TAG, ex.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loaderLayout.setVisibility(View.GONE);
                if (error instanceof NoConnectionError) {
                    Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.no_network), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
                    snackbar.show();
                } else {
                    Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(getResources().getColor(android.R.color.white));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(getResources().getColor(R.color.active_tab_color));
                    snackbar.show();
                }
            }
        });

        requestQueue = Volley.newRequestQueue(this);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }
}