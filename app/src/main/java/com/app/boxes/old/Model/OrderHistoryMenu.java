package com.app.boxes.old.Model;

import java.util.ArrayList;

public class OrderHistoryMenu {
    public String Category;

    public ArrayList<OrderHistoryMenuItem> orderHistoryMenuItemArrayList = new ArrayList<OrderHistoryMenuItem>();

    public OrderHistoryMenu(String category) {
        Category = category;
    }

    public String getCategory() {
        return Category;
    }

    public void setCategory(String category) {
        Category = category;
    }
}
