package com.app.boxes.old.SharedPreference;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.app.boxes.old.Model.Area;
import com.app.boxes.old.Model.DeliveryDates;
import com.app.boxes.old.Model.Flower;
import com.app.boxes.old.Model.Language;
import com.app.boxes.old.Model.User;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class MySharedPreferences {

    Context context;
    SharedPreferences sp;

//    static ArrayList<Customer> customers;

    //    static ArrayList<Area> areas;
    public MySharedPreferences(Context context) {
        this.context = context;
    }

    //    //SHARED_FILE
    public boolean saveUserObject(User user) {
        Gson gson = new Gson();
        String stringUser = gson.toJson(user);
        sp = context.getSharedPreferences("SHARED_FILE", Context.MODE_PRIVATE);

        return sp.edit().putString("userObj", stringUser).commit();
    }

    public User getUserObject() {
        sp = context.getSharedPreferences("SHARED_FILE", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sp.getString("userObj", null);

        return gson.fromJson(json, User.class);
    }

    public void saveFlowerArrayList(ArrayList<Flower> flower) {
        Gson gson = new Gson();
        String jsonString = gson.toJson(flower);
        sp = context.getSharedPreferences("SHARED_FILE", Context.MODE_PRIVATE);
        sp.edit().putString("flower_arraylist", jsonString).apply();
    }

    public ArrayList<ArrayList<Flower>> getFlowerArrayList() {
        ArrayList<ArrayList<Flower>> flowerArrayList;
        sp = context.getSharedPreferences("SHARED_FILE", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        Type listOfObjects = new TypeToken<List<String>>() {
        }.getType();

        String json = sp.getString("flower_arraylist", "");
        flowerArrayList = gson.fromJson(json, listOfObjects);

        return flowerArrayList;
    }

    public boolean saveAccessTokenObject(User.AccessToken accessToken) {
        Gson gson = new Gson();
        String stringToken = gson.toJson(accessToken);
        sp = context.getSharedPreferences("SHARED_FILE", Context.MODE_PRIVATE);

        return sp.edit().putString("accessTokenObj", stringToken).commit();
    }

    public User.AccessToken getAccessTokenObject() {
        sp = context.getSharedPreferences("SHARED_FILE", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sp.getString("accessTokenObj", null);

        return gson.fromJson(json, User.AccessToken.class);
    }

    public boolean getAppFirstTime() {
        sp = context.getSharedPreferences("FIRST_TIME", Context.MODE_PRIVATE);
        return sp.getBoolean("isFirstTime", true);
    }

    public void saveFirstTime(boolean isFirstTime) {
        sp = context.getSharedPreferences("FIRST_TIME", Context.MODE_PRIVATE);
        sp.edit().putBoolean("isFirstTime", isFirstTime).apply();
    }

    /*public String getUserArea() {
        sp = context.getSharedPreferences("SHARED_FILE", Context.MODE_PRIVATE);
        return sp.getString("userAddress", null);
    }

    public void saveUserArea(String userAddress) {
        sp = context.getSharedPreferences("SHARED_FILE", Context.MODE_PRIVATE);
        sp.edit().putString("userAddress", userAddress).apply();
    }*/

    public boolean saveLangObject(Language language) {
        Gson gson = new Gson();
        String stringLang = gson.toJson(language);
        sp = context.getSharedPreferences("LANGUAGE", Context.MODE_PRIVATE);

        return sp.edit().putString("langObj", stringLang).commit();
    }

    public Language getLangObject() {
        sp = context.getSharedPreferences("LANGUAGE", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sp.getString("langObj", "");

        return gson.fromJson(json, Language.class);
    }

    public void saveSelectedDay(String day) {
        sp = context.getSharedPreferences("SHARED_FILE", Context.MODE_PRIVATE);
        sp.edit().putString("selected_day", day).apply();
    }

    public String getSelectedDay() {
        sp = context.getSharedPreferences("SHARED_FILE", Context.MODE_PRIVATE);
        return sp.getString("selected_day", null);
    }

    public void saveSelectedDate(int date) {
        sp = context.getSharedPreferences("SHARED_FILE", Context.MODE_PRIVATE);
        sp.edit().putInt("selected_date", date).apply();
    }

    public int getSelectedDate() {
        sp = context.getSharedPreferences("SHARED_FILE", Context.MODE_PRIVATE);
        return sp.getInt("selected_date", 0);
    }

    public void saveCurrentCategory(int isHighlighted) {
        sp = context.getSharedPreferences("SHARED_FILE", Context.MODE_PRIVATE);
        sp.edit().putInt("highlight_category", isHighlighted).apply();
    }

    public int getCurrentCategory() {
        sp = context.getSharedPreferences("SHARED_FILE", Context.MODE_PRIVATE);
        return sp.getInt("highlight_category", 0);
    }

    public boolean saveDeliveryDateObj(DeliveryDates date) {
        Gson gson = new Gson();
        String dateObj = gson.toJson(date);
        sp = context.getSharedPreferences("SHARED_FILE", Context.MODE_PRIVATE);

        return sp.edit().putString("date_obj", dateObj).commit();
    }

    public DeliveryDates getDeliveryDateObj() {
        sp = context.getSharedPreferences("SHARED_FILE", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sp.getString("date_obj", "");

        return gson.fromJson(json, DeliveryDates.class);
    }

    public int getSelectedMonth() {
        sp = context.getSharedPreferences("SHARED_FILE", Context.MODE_PRIVATE);
        return sp.getInt("selectedMonth", 0);
    }

    public void saveSelectedMonth(int month) {
        sp = context.getSharedPreferences("SHARED_FILE", Context.MODE_PRIVATE);
        sp.edit().putInt("selectedMonth", month).apply();
    }

    public void saveSelectedYear(int year) {
        sp = context.getSharedPreferences("SHARED_FILE", Context.MODE_PRIVATE);
        sp.edit().putInt("selectedYear", year).apply();
    }

    public int getSelectedYear() {
        sp = context.getSharedPreferences("SHARED_FILE", Context.MODE_PRIVATE);
        return sp.getInt("selectedYear", 0);
    }


    public int getCurrentPosition() {
        sp = context.getSharedPreferences("SHARED_FILE", Context.MODE_PRIVATE);
        return sp.getInt("selectedPosition", 0);
    }

    public void saveCurrentPosition(int position) {
        sp = context.getSharedPreferences("SHARED_FILE", Context.MODE_PRIVATE);
        sp.edit().putInt("currentPosition", position).apply();
    }

    //todo
    /*public int getColorCount() {
        sp = context.getSharedPreferences("SHARED_FILE", Context.MODE_PRIVATE);
        return sp.getInt("count", 0);
    }

    public void setColorCount(int count) {
        sp = context.getSharedPreferences("SHARED_FILE", Context.MODE_PRIVATE);
        sp.edit().putInt("count", count).apply();
    }*/

    public String getListingId() {
        sp = context.getSharedPreferences("SHARED_FILE", Context.MODE_PRIVATE);
        return sp.getString("listingId", "");
    }

    public void setListingId(String listingName) {
        sp = context.getSharedPreferences("SHARED_FILE", Context.MODE_PRIVATE);
        sp.edit().putString("listingId", listingName).apply();
    }

    public String getListingName() {
        sp = context.getSharedPreferences("SHARED_FILE", Context.MODE_PRIVATE);
        return sp.getString("listingName", "");
    }

    public void setListingName(String listingName) {
        sp = context.getSharedPreferences("SHARED_FILE", Context.MODE_PRIVATE);
        sp.edit().putString("listingName", listingName).apply();
    }

    public String getTypeId() {
        sp = context.getSharedPreferences("SHARED_FILE", Context.MODE_PRIVATE);
        return sp.getString("typeId", "");
    }

    public void setTypeId(String typeId) {
        sp = context.getSharedPreferences("SHARED_FILE", Context.MODE_PRIVATE);
        sp.edit().putString("typeId", typeId).apply();
    }

    public int getCardInfoFragment() {
        sp = context.getSharedPreferences("SHARED_FILE", Context.MODE_PRIVATE);
        return sp.getInt("CardInfo", 0);
    }

    public void setCardInfoFragment(int count) {
        sp = context.getSharedPreferences("SHARED_FILE", Context.MODE_PRIVATE);
        sp.edit().putInt("CardInfo", count).apply();
    }

    public String getLocale() {
        sp = context.getSharedPreferences("LANGUAGE", Context.MODE_PRIVATE);
        String lang = sp.getString("my_language", "en");
        saveLocale(lang);
        return lang;
    }

    public void saveLocale(String lang) {
        sp = context.getSharedPreferences("LANGUAGE", Context.MODE_PRIVATE);
        sp.edit().putString("my_language", lang).apply();
    }

    public boolean saveAreaObject(Area userAddress) {
        Gson gson = new Gson();
        String stringLang = gson.toJson(userAddress);
        sp = context.getSharedPreferences("SHARED_FILE", Context.MODE_PRIVATE);

        return sp.edit().putString("areaObj", stringLang).commit();
    }

    public Area getAreaObject() {
        sp = context.getSharedPreferences("SHARED_FILE", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sp.getString("areaObj", "");

        return gson.fromJson(json, Area.class);
    }

    public boolean saveUserContactInfo(User user) {
        Gson gson = new Gson();
        String stringUser = gson.toJson(user);
        sp = context.getSharedPreferences("SHARED_FILE", Context.MODE_PRIVATE);

        return sp.edit().putString("userContactInfo", stringUser).commit();
    }

    public User getUserContactInfo() {
        sp = context.getSharedPreferences("SHARED_FILE", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sp.getString("userContactInfo", null);

        return gson.fromJson(json, User.class);
    }

    public void setCategoryName(String categoryName) {
        sp = context.getSharedPreferences("SHARED_FILE", Context.MODE_PRIVATE);
        sp.edit().putString("category_name", categoryName).apply();
    }

    public String getCategoryName() {
        sp = context.getSharedPreferences("SHARED_FILE", Context.MODE_PRIVATE);
        return sp.getString("category_name", "");
    }

    public void saveSearchHistoryArrayList(ArrayList<String> history) {
        Gson gson = new Gson();
        String jsonString = gson.toJson(history);
        sp = context.getSharedPreferences("SHARED_FILE", Context.MODE_PRIVATE);
        sp.edit().putString("search_arraylist", jsonString).apply();
    }

    public ArrayList<String> getSearchHistroyArrayList() {
        ArrayList<String> history;
        sp = context.getSharedPreferences("SHARED_FILE", Context.MODE_PRIVATE);
        Gson gson = new Gson();
        Type listOfObjects = new TypeToken<List<String>>() {
        }.getType();

        String json = sp.getString("search_arraylist", "");
        history = gson.fromJson(json, listOfObjects);

        return history;
    }

    public void clearAllPreferences() {
        clearPreferences("SHARED_FILE");
    }

    public void clearPreferences(String pref) {
        try {
            SharedPreferences prefs = context.getSharedPreferences(pref,
                    Context.MODE_PRIVATE);
            SharedPreferences.Editor prefEditor = prefs.edit();
            prefEditor.clear().commit();

        } catch (Exception e) {
            Log.i("", "Exception : " + e.toString());
        }
    }
}