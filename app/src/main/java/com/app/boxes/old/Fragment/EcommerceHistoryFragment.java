package com.app.boxes.old.Fragment;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.boxes.old.API.API;
import com.app.boxes.old.Activity.MainActivity;
import com.app.boxes.old.Adapter.EcommerceHistoryAdapter;
import com.app.boxes.old.Listener.RecyclerItemClickListener;
import com.app.boxes.old.Model.EcommerceHistory;
import com.app.boxes.old.Model.Language;
import com.app.boxes.old.Model.User;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.app.boxes.old.UI.LoginBottomSheet;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class EcommerceHistoryFragment extends Fragment {

    private String TAG = "EcommerceHistoryFragment";

    private RecyclerView recyclerView;
    private TextView textView;
    private View snackbarView;
    private RelativeLayout rlEmptyState;
    private LinearLayout loaderLayout;
    private View rootView;
    private Button btnNo;

    private EcommerceHistoryAdapter adapter;

    private ArrayList<EcommerceHistory> historyArrayList = new ArrayList<>();

    private boolean isItemClicked = false;

    private Activity mContext;

    public EcommerceHistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        rootView = inflater.inflate(R.layout.fragment_ecommerce_history, container, false);

        ((MainActivity) mContext).getSupportActionBar().show();

        loaderLayout = rootView.findViewById(R.id.ll_loader_ecommerce_history);
        loaderLayout.setVisibility(View.GONE);

        snackbarView = mContext.findViewById(android.R.id.content);

        textView = rootView.findViewById(R.id.tv_ecommerce_order_history);

        recyclerView = rootView.findViewById(R.id.rv_ecommerce_order_history);
        recyclerView.setLayoutManager(new LinearLayoutManager(mContext));

        rlEmptyState = rootView.findViewById(R.id.rl_ecommerce_no_result);
       // rlEmptyState.setVisibility(View.GONE);

        btnNo = rootView.findViewById(R.id.btn_ecommerce_no);
        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.onBackPressed();
            }
        });

        MySharedPreferences sPref = new MySharedPreferences(mContext);
        Language language = sPref.getLangObject();

        if (sPref.getAccessTokenObject() != null) {
            User.AccessToken token = sPref.getAccessTokenObject();
            if (token != null && language != null) {
//                String token1 = "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjYxNjRjNWM2ZTM3YWM5YTM3OGY1NDYyMTViODU2M2E4ZTJhMjM2ZGQzYTU4YWQwZTkyYzRhYzI2NzkzNzc1MGFjZWM2NjA2YjE4YjU1Yjc5In0.eyJhdWQiOiIxIiwianRpIjoiNjE2NGM1YzZlMzdhYzlhMzc4ZjU0NjIxNWI4NTYzYThlMmEyMzZkZDNhNThhZDBlOTJjNGFjMjY3OTM3NzUwYWNlYzY2MDZiMThiNTViNzkiLCJpYXQiOjE1NzE2NDA2MTIsIm5iZiI6MTU3MTY0MDYxMiwiZXhwIjoxNjAzMjYzMDEyLCJzdWIiOiIxYWVlMGFkYi1jODFiLTQ4MmEtYWM2YS02MWJlYzM5YzgyMTYiLCJzY29wZXMiOltdfQ.alo3y4DptJ5-X0PhD31fIz5PK3oEAw2V2rvDsulOpek3P0OXLo4GGwNp-uij8v1ULIOopwFhgu7_4nYU7gqI2J23yX2iLqCfub1tPPvWdKZ4Q2TqWY8_tz6ZJ8McmE2H84qSpUrp7PjIsSdG5ejOtsJjAhroFLTbBQnd48yALqJaC39nULH3XKDZvUdZTEx9RbdpbRs3qohFWTyqyZ7Pt-0w7cQPGYbEFrYOfXkRfz4-y1T6lXJww644Xj1C-pRB3cS_ApDuBt-fiKtjUcRzVfHb5VwFmKpnH0BRugzVWaWzw2sVrm48q07udVRwJJKZVKqV9FA1vMvp1W3YTxFo2NDQohegwNGalpKEMvniKkCx5idX8gBBKWRTTu18H8T5cJ71PYxyvxnZxMOaST4IvVRsemhaCWFnNYGQxf88U04TZHcBng56tVqi1fMsZt8fWU70HWWNPGSCbx43TPAnvlnSoa_KGv7HLeKrfmQWUIlvyA4D1cmPqY_0FbQoJ7Cghm-BU6rqRj7sylt0gCc1IVnxIjVXuAr85OCaI6CXbHGkPO_RyLL_lV6z4-2CJ3l3GeQtzMIh9b1y-q8PKy0Fbd85ahwz_oFIbpXJOu-B0qyaCEWtfxsmMMWIGwmVY9d-b626evO9vZvAXEcOOopJ5bZHavwDq5d9AypusXy6slU";
                getEcommerceHitsory(language.getId(), token.getAccessToken());
            } else {
                LoginBottomSheet loginBottomSheet = new LoginBottomSheet();
                loginBottomSheet.setCancelable(false);
                if (getFragmentManager() != null) {
                    loginBottomSheet.show(getFragmentManager(), "login_bottom_sheet");
                }
            }
        }

        return rootView;
    }

    @Override
    public void onAttach(Context context) {
        this.mContext = (Activity) context;
        super.onAttach(context);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (rootView != null) {
            ViewGroup viewGroup = (ViewGroup) rootView.getParent();
            if (viewGroup != null)
                viewGroup.removeAllViews();
        }
    }

    private void getEcommerceHitsory(String langId, final String accessToken) {
        String BASE_URL = API.getAPI() + "auth/cart/my-orders?language_id=" + langId;
        Log.d(TAG, "Ecommerce Order History URL: " + BASE_URL);

        loaderLayout.setVisibility(View.VISIBLE);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, BASE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "onResponse: " + response);
                loaderLayout.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    int status = jsonObject.optInt("status");
                    String msg = jsonObject.optString("message");

                    EcommerceHistory history;

                    if (status == 1) {
                        textView.setVisibility(View.GONE);
                        historyArrayList.clear();
                        JSONArray dataArray = jsonObject.optJSONArray("data");
                        if (dataArray != null) {
                            String listingName = null, imgURL = null;
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObj = dataArray.getJSONObject(i);

                                String id = dataObj.getString("id");
                                String totalPrice = dataObj.getString("total_price");
                                String image = dataObj.getString("image");
                                listingName = dataObj.getString("listing_name");
                                String products = dataObj.getString("products");

                                imgURL = API.getImageAPI() + image;

                                history = new EcommerceHistory();
                                history.setId(id);
                                history.setImage(imgURL);
                                history.setTotalPrice(totalPrice);
                                history.setListingName(listingName);
                                history.setProducts(products);

                                historyArrayList.add(history);
                            }

                            adapter = new EcommerceHistoryAdapter(mContext, historyArrayList);
                            recyclerView.setAdapter(adapter);
                            if(historyArrayList.size()==0)
                                rlEmptyState.setVisibility(View.VISIBLE);


                            recyclerView.addOnItemTouchListener(new RecyclerItemClickListener(mContext, recyclerView, new RecyclerItemClickListener.OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                    Log.d("isItemClick1", isItemClicked + "");
                                    if (!isItemClicked) {

                                        EcommerceHistory orderHistory = historyArrayList.get(position);
                                        String itemClicked = orderHistory.getId();
                                        String itemVendorName = orderHistory.getListingName();
                                        String itemVendorImage = orderHistory.getImage();

                                        Bundle bundle = new Bundle();
                                        bundle.putString("orderId", itemClicked);
                                        bundle.putString("vendor_name", itemVendorName);
                                        bundle.putString("vendor_image", itemVendorImage);

                                        EcommerceHistoryDetailFragment fragment = new EcommerceHistoryDetailFragment();
//                                        fragment.setArguments(bundle);

                                        ((MainActivity) mContext).navigationController(R.id.ecommerceHistoryDetailFragment, bundle, null);

                                        /*if (getFragmentManager() != null) {
                                            getFragmentManager()
                                                    .beginTransaction()
                                                    .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                                                    .replace(R.id.main_content, fragment)
                                                    .addToBackStack("test")
                                                    .commit();
                                        }*/

                                        isItemClicked = true;
                                        Log.d("isItemClick2", isItemClicked + "");

                                        //timer to make isItemClicked = false
                                        new CountDownTimer(1000, 1000) {
                                            @Override
                                            public void onTick(long l) {

                                            }

                                            @Override
                                            public void onFinish() {
                                                isItemClicked = false;
                                                Log.d("isItemClick3", isItemClicked + "");
                                            }
                                        }.start();
                                    }
                                }

                                @Override
                                public void onLongItemClick(View view, int position) {

                                }
                            }));
                        }
                    } else {
                        rlEmptyState.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    Log.d(TAG, "JSONException: " + e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loaderLayout.setVisibility(View.GONE);
                Log.d(TAG, "onErrorResponse: " + error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (accessToken != null) {
                    params.put("Authorization", accessToken);
                    Log.d(TAG, "getHeaders: " + accessToken);
                }
                return params;
            }
        };
        requestQueue.add(stringRequest);
    }
}