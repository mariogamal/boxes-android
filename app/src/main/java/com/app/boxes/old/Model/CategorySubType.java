package com.app.boxes.old.Model;

import java.io.Serializable;

public class CategorySubType implements Serializable {

    private String id, parentId, subType, resType;

    public CategorySubType() {
    }

    public CategorySubType(String id, String parentId, String subType) {
        this.id = id;
        this.parentId = parentId;
        this.subType = subType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public String getResType() {
        return resType;
    }

    public void setResType(String resType) {
        this.resType = resType;
    }
}