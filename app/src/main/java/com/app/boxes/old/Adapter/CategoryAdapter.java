package com.app.boxes.old.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.app.boxes.old.Model.Category;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {

    Context context;
    ArrayList<Category> categories;

    private int flag = 0;
    private MySharedPreferences mPref;

    public CategoryAdapter(Context context, ArrayList<Category> categories) {
        this.context = context;
        this.categories = categories;
        mPref = new MySharedPreferences(context);
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.horizontal_category_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        Picasso.get().load(categories.get(position).getIcons()).placeholder(R.drawable.boxes_logo).into(holder.ivCategory);
        holder.tvCategoryText.setText(categories.get(position).getName());

        if (position == 0) {
            holder.ivCategory.setImageResource(R.drawable.food_icon);
        }

        if (Integer.parseInt(categories.get(position).getCheckDefault()) == 1) {
            holder.tvCategoryText.setTextColor(ContextCompat.getColor(context, R.color.active_tab_color));
            holder.ivCategory.setBorderColor(Color.parseColor("#5DC04B"));
            holder.ivCategory.setBorderWidth(5);
        } else {
            holder.tvCategoryText.setTextColor(ContextCompat.getColor(context, R.color.inactive_tab_color));
            holder.ivCategory.setBorderColor(Color.parseColor("#ffffff"));
            holder.ivCategory.setBorderWidth(5);
        }
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CircleImageView ivCategory;
        private TextView tvCategoryText;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ivCategory = itemView.findViewById(R.id.iv_horizontal_category);
            tvCategoryText = itemView.findViewById(R.id.tv_home_category_text);
        }
    }
    //  interface onFoodClicked()

}
