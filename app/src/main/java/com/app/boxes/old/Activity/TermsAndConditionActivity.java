package com.app.boxes.old.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.app.boxes.old.API.API;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.wang.avi.AVLoadingIndicatorView;


import org.json.JSONArray;
import org.json.JSONObject;

public class TermsAndConditionActivity extends AppCompatActivity {

    private static String TAG = "TermsAndConditionsActivity";

    private ImageView ivBtnBack;
    private TextView tvDescription, tvStatus;
    private AVLoadingIndicatorView avl;

    private String pageName = "terms_and_conditions", languageId = "";

    MySharedPreferences mPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_and_condition);

        //initialize widgets
        ivBtnBack = findViewById(R.id.iv_tnt_back);

        tvDescription = findViewById(R.id.tv_tot_activity_description);
        tvStatus = findViewById(R.id.tv_tot_status);

        avl = findViewById(R.id.avi_terms_and_conditions);
        avl.setVisibility(View.GONE);

        //shared preferences for check localization
        mPref = new MySharedPreferences(this);
        if (mPref.getLocale().equals("ar"))
            ivBtnBack.setImageResource(R.drawable.ic_arrow_forward_white);

        //get languageId
        if (mPref != null) {
            languageId = mPref.getLangObject().getId();
            Log.d(TAG, "language id " + mPref.getLangObject().getId());
        }

        //navigate to sign up activity
        ivBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        //get terms and conditions from server
        getTermsAndConditions(pageName, languageId);
    }

    private void getTermsAndConditions(String page_name, String language_id) {
        String url = API.getAPI() + "page?page_name=" + page_name + "&language_id=" + language_id;
        Log.d(TAG, "URL: " + url);

        avl.setVisibility(View.VISIBLE);

        RequestQueue requestQueue;
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {

                Log.d(TAG, response.toString());
                avl.setVisibility(View.GONE);
                try {
                    int status = response.getInt("status");
                    String message = response.getString("message");

                    if (status == 1) {

                        JSONArray dataArray = response.getJSONArray("data");

                        tvStatus.setVisibility(View.GONE);

                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject jsonDataObject = dataArray.getJSONObject(i);

                            String pageDesc = jsonDataObject.getString("page_desc");

                            tvDescription.setText(Html.fromHtml(pageDesc));
                        }

                    } else {
                        tvStatus.setVisibility(View.VISIBLE);
                        Log.d(TAG, message);
                    }

                } catch (Exception ex) {
                    Log.d(TAG, "exception: " + ex.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                avl.setVisibility(View.GONE);
                Log.d(TAG, "onError: " + error.toString());
            }
        });

        requestQueue = Volley.newRequestQueue(this);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);

    }
}
