package com.app.boxes.old.Model;

import androidx.annotation.Nullable;

import java.io.Serializable;

public class Category implements Serializable {
    private String id, icons, name, catId, checkDefault, isFood;
    private boolean isHighlighted;

    public Category() {
    }

    public Category(String id, String icons, String name, String checkDefault, String isFood, boolean isHighlighted) {
        this.id = id;
        this.icons = icons;
        this.name = name;
        this.catId = catId;
        this.checkDefault = checkDefault;
        this.isFood = isFood;
        this.isHighlighted = isHighlighted;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setIcons(String icons) {
        this.icons = icons;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCatId() {
        return catId;
    }

    public void setCatId(String catId) {
        this.catId = catId;
    }

    public void setIsFood(String isFood) {
        this.isFood = isFood;
    }

    public void setHighlighted(boolean highlighted) {
        isHighlighted = highlighted;
    }

    public String getId() {
        return id;
    }

    public String getIcons() {
        return icons;
    }

    public String getName() {
        return name;
    }

    public void setCheckDefault(String checkDefault) {
        this.checkDefault = checkDefault;
    }

    public String getCheckDefault() {
        return checkDefault;
    }

    public String getIsFood() {
        return isFood;
    }

    public boolean isHighlighted() {
        return isHighlighted;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        boolean result = false;
        if (obj == null || obj.getClass() != getClass()) {
            result = false;
        } else {
            Category employee = (Category) obj;
            if (this.id.equals(employee.getId()) && this.name.equals(employee.getName())) {
                result = true;
            }
        }
        return result;
    }
}
