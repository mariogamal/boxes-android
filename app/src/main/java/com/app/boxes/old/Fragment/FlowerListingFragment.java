package com.app.boxes.old.Fragment;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import androidx.viewpager.widget.ViewPager;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;


import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.boxes.old.API.API;
import com.app.boxes.old.Activity.MainActivity;
import com.app.boxes.old.Adapter.FlowerPagerAdapter;

import com.app.boxes.old.Model.Flower;
import com.app.boxes.old.Model.Language;
import com.app.boxes.old.Model.Product;
import com.app.boxes.old.Model.User;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class FlowerListingFragment extends Fragment {

    private FlowerPagerAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private ImageView ivBtnBack, ivBackground;
    private TextView tvFlowerName, tvFlowerCaption, tvCartCount, tvToolbar;
    private FrameLayout cartLayout;
    private View snackbarView;

    private Bundle getFlowerData;

    private String flowerId = "", toolbarTitle = "";

    private String TAG = "FlowerListingFragment";

    private MySharedPreferences sPref;

    private Language language;
    private User.AccessToken token;

    public FlowerListingFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_flower_listing, container, false);

        sPref = new MySharedPreferences(mContext);

        //hide the actionbar
        ((MainActivity) mContext).getSupportActionBar().hide();

        //initialize widgets
        ivBtnBack = rootView.findViewById(R.id.iv_flower_back);
        ivBackground = rootView.findViewById(R.id.iv_flower_listing_background);

        viewPager = rootView.findViewById(R.id._flower_viewPager);

        tabLayout = rootView.findViewById(R.id.flower_tabLayout);

        tvFlowerName = rootView.findViewById(R.id.tv_flower_listing_name);
        tvFlowerCaption = rootView.findViewById(R.id.tv_flower_listing_caption);
        tvCartCount = rootView.findViewById(R.id.tv_flower_listing_notif_badge);
        tvToolbar = rootView.findViewById(R.id.tv_flower_listing_toolbar_title);

        cartLayout = rootView.findViewById(R.id.flower_listing_cart);

        snackbarView = mContext.findViewById(android.R.id.content);

        //get bundle data
        getFlowerData = getArguments();

        if (getFlowerData != null) {
            flowerId = getFlowerData.getString("flower_id");
            tvFlowerName.setText(getFlowerData.getString("flower_name"));
            tvFlowerCaption.setText(getFlowerData.getString("flower_caption"));
            Picasso.get().load(getFlowerData.getString("flower_image")).into(ivBackground);

//            flowerId = "40169b38-c1bf-4d8d-b570-adeea09e6b19";

            language = sPref.getLangObject();
            token = sPref.getAccessTokenObject();

            if (language != null) {
                getTabs(flowerId, language.getId());
            }
        } else {
            Log.d(TAG, "flower id: " + flowerId);
        }

        // final MainActivity ma = MainActivity();


        //navigate to flower home fragment
        ivBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((MainActivity) getActivity()).onBackPressed();
              /*  NavOptions no = new NavOptions.Builder().setPopUpTo(R.id.navigation_graph, true).build();
                if (((MainActivity) getActivity()).isValidDestination(R.id.placeOrderFragment))
                    ((MainActivity) getActivity()).navigationController(R.id.main_content, no);*/
            }
        });

        MySharedPreferences sPref = new MySharedPreferences(mContext);
        if (sPref.getLocale().equals("ar")) {
            ivBtnBack.setImageResource(R.drawable.ic_arrow_forward_white);
        }

        // get selected category name from shared preference and set to toolbar
        if (sPref.getCategoryName() != null)
            toolbarTitle = sPref.getCategoryName();
//        tvToolbar.setText(toolbarTitle);

        Log.d(TAG, "categoryName: " + toolbarTitle);

        cartLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int mCartCount;
                // check if cart count text contains "+" then replace it
                if (tvCartCount.getText().toString().contains("+")) {
                    String sCartCount = tvCartCount.getText().toString().replace("+", "");
                    mCartCount = Integer.parseInt(sCartCount);
                } else {
                    mCartCount = Integer.parseInt(tvCartCount.getText().toString());
                }

                if (mCartCount > 0) {
                    ((MainActivity) getActivity()).navigationController(R.id.placeOrderFragment, null);
                } else {
                    Snackbar snackbar = Snackbar.make(snackbarView, "Please add items to see cart", Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                    snackbar.show();
                }
            }
        });

        viewPager.addOnPageChangeListener(
                new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(
                new TabLayout.ViewPagerOnTabSelectedListener(viewPager));

//        rootView.setFocusableInTouchMode(true);
//        rootView.requestFocus();
//        rootView.setOnKeyListener(new View.OnKeyListener() {
//            @Override
//            public boolean onKey(View view, int i, KeyEvent keyEvent) {
//                if (keyEvent.getAction() == KeyEvent.ACTION_DOWN && i == KeyEvent.KEYCODE_BACK) {
//                    if (getFragmentManager() != null) {
//                        getFragmentManager().popBackStack();
//                    }
//                }
//                return false;
//            }
//        });

//                    getActivity().getSupportFragmentManager();
//                    FragmentTransaction trans = null;
//                    if (getFragmentManager() != null) {
//                        trans = getFragmentManager().beginTransaction();
//                        trans.remove(FlowerListingFragment.this);
//                        trans.commit();
//                    }

        return rootView;
    }

    private Activity mContext;


    @Override
    public void onAttach(Context context) {
        this.mContext = (Activity) context;
        super.onAttach(context);

//        Toast.makeText(context, "on attached called", Toast.LENGTH_SHORT).show();

        Log.d(TAG, "onAttach: called");
    }

    private ArrayList<Flower> flowersList;

    // get tabs from server
    private void getTabs(final String listingId, final String languageId) {
        String URL = API.getAPI() + "listing-categories-with-products?listing_id=" + listingId + "&language_id=" + languageId;
        Log.d(TAG, "Flower Listing With Category URL: " + URL);

        final ArrayList<String> tabList = new ArrayList<>();
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        final JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, URL, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d(TAG, response.toString());
                try {
                    int status = response.getInt("status");
                    String msg = response.getString("message");

                    if (status == 1) {
                        Flower flower;
                        Product product;
                        JSONArray dataArray = response.getJSONArray("data");
                        String listing_cat_name = null;

                        final ArrayList<ArrayList<Flower>> mFlowersList = new ArrayList<>();
                        for (int i = 0; i < dataArray.length(); i++) {
                            JSONObject dataObj = dataArray.getJSONObject(i);
                            flowersList = new ArrayList<>();
                            String id = dataObj.getString("id");
                            String listing_id = dataObj.getString("listing_id");
                            listing_cat_name = dataObj.getString("listing_cat_name");

                            JSONArray productArray = dataObj.getJSONArray("products");
                            for (int j = 0; j < productArray.length(); j++) {
                                JSONObject prodObj = productArray.getJSONObject(j);
                                String productId = prodObj.getString("id");
                                String listing_cat_id = prodObj.getString("listing_cat_id");
                                String product_name = prodObj.getString("product_name");
                                String caption_name = prodObj.getString("caption_name");
                                String price = prodObj.getString("price");
                                String newPrice = prodObj.getString("new_price");
                                String image = prodObj.getString("image");
                                String product_description = prodObj.getString("product_description");
                                String stock = prodObj.getString("stock");
                                String discount = prodObj.getString("discount");
                                String stockCount = prodObj.getString("stock_count");

                                String img = API.getImageAPI() + image;
                                Log.d(TAG, "IMAGE URL: " + img);

                                flower = new Flower(productId, listing_cat_id, product_name, caption_name, price, newPrice, img, product_description, stock, discount, stockCount);

                                flowersList.add(flower);
                                if (flowersList.size() > 0)
                                    mFlowersList.add(flowersList);
                            }
                            tabList.add(listing_cat_name);
                        }

                        try {
                            if (tabList.size() > 0 && mFlowersList.size() > 0)
                                adapter = new FlowerPagerAdapter(getFragmentManager(), tabList, mFlowersList, mContext);
                            viewPager.setAdapter(adapter);
                            viewPager.getAdapter().notifyDataSetChanged();
                            viewPager.setOffscreenPageLimit(4);
                            tabLayout.setupWithViewPager(viewPager);
                        } catch (Exception ex) {
                            Log.d(TAG, "onResponse: exception " + ex.getMessage());
                        }

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "On error: " + error.getMessage());
            }
        });
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }

    private void myCart(String languageId, final String accessToken) {
        String url = API.getAPI() + "auth/cart/my-cart?language_id=" + languageId;
        Log.d(TAG, url);

//        avi.setVisibility(View.VISIBLE);
        RequestQueue requestQueue;
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "onResponse: " + response.toString());
//                avi.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    String msg = jsonObject.getString("message");

                    if (status == 1) {
                        JSONObject jsonObj = jsonObject.getJSONObject("data");
                        Log.d(TAG, "onResponse jsonObj: " + jsonObj);

                        //get the total JsonObject and total items in cart
                        JSONObject totalObj = jsonObj.getJSONObject("total");
                        Log.d(TAG, "onResponse detailsObj: " + totalObj);

                        String cartCount = totalObj.getString("cart_count");
                        int mCartCount = Integer.parseInt(cartCount);
                        if (mCartCount > 99) {
                            tvCartCount.setText("99+");
                        } else {
                            tvCartCount.setText(cartCount);
                        }
//                        Toast.makeText(mContext, cartCount, Toast.LENGTH_SHORT).show();
                    }else if (status == 0) {
                        tvCartCount.setText(0 + "");
                    }
                } catch (Exception ex) {
                    Log.d(TAG, "onException: " + ex.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse: " + error.toString());
//                avi.setVisibility(View.GONE);
                if (error instanceof NoConnectionError) {
                    Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.no_network), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                    snackbar.show();
                } else {
                    try {
                        Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                        snackbar.show();
                    } catch (Exception ex) {
                        Log.d(TAG, "onErrorResponse: " + ex.toString());
                    }
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (accessToken != null) {
                    params.put("Authorization", accessToken);
                    Log.d(TAG, "getHeaders: " + accessToken);
                }
                return params;
            }
        };

        requestQueue = Volley.newRequestQueue(mContext);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);

    }

    @Override
    public void onResume() {
        super.onResume();

//        Toast.makeText(mContext, "in on reumne", Toast.LENGTH_SHORT).show();

        language = sPref.getLangObject();
        token = sPref.getAccessTokenObject();

        if (token != null) {
            myCart(language.getId(), token.getAccessToken());

            Log.d(TAG, "onResume: called");
        }

        //get bundle data
        getFlowerData = getArguments();

        if (getFlowerData != null) {
            flowerId = getFlowerData.getString("flower_id");
            tvFlowerName.setText(getFlowerData.getString("flower_name"));
            tvFlowerCaption.setText(getFlowerData.getString("flower_caption"));
            Picasso.get().load(getFlowerData.getString("flower_image")).into(ivBackground);

//            flowerId = "40169b38-c1bf-4d8d-b570-adeea09e6b19";

            language = sPref.getLangObject();
            token = sPref.getAccessTokenObject();

            if (language != null) {
                getTabs(flowerId, language.getId());
            }
        } else {
            Log.d(TAG, "flower id: " + flowerId);
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        language = sPref.getLangObject();
        token = sPref.getAccessTokenObject();

        if (token != null) {
            myCart(language.getId(), token.getAccessToken());

            Log.d(TAG, "onStart: called");
        }
    }

//    @Override
//    public void onHiddenChanged(boolean hidden) {
//        super.onHiddenChanged(hidden);
//        if (hidden) {
//            Toast.makeText(mContext, "hidded", Toast.LENGTH_SHORT).show();
//        } else {
//            Toast.makeText(mContext, "not hidden", Toast.LENGTH_SHORT).show();
//
//        }
//    }


}