package com.app.boxes.old.Model;

public class Cart {

    private String prodId, prodName, prodQuantity, prodPrice, prodStockCount;

    public String getProdId() {
        return prodId;
    }

    public void setProdId(String prodId) {
        this.prodId = prodId;
    }

    public void setProdName(String prodName) {
        this.prodName = prodName;
    }

    public void setProdQuantity(String prodQuantity) {
        this.prodQuantity = prodQuantity;
    }

    public void setProdPrice(String prodPrice) {
        this.prodPrice = prodPrice;
    }

    public String getProdName() {
        return prodName;
    }

    public String getProdQuantity() {
        return prodQuantity;
    }

    public String getProdPrice() {
        return prodPrice;
    }

    public String getProdStockCount() {
        return prodStockCount;
    }

    public void setProdStockCount(String prodStockCount) {
        this.prodStockCount = prodStockCount;
    }
}