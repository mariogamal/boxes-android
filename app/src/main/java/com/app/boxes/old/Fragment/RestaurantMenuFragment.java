package com.app.boxes.old.Fragment;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.app.boxes.old.Activity.MainActivity;
import com.app.boxes.old.Adapter.TabAdapter;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.google.android.material.tabs.TabLayout;
import com.squareup.picasso.Picasso;

/**
 * A simple {@link Fragment} subclass.
 */
public class RestaurantMenuFragment extends Fragment {

    private TabAdapter adapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private TextView tvToolbarTitle, tvResTitle, tvResDesc;
    private ImageView ivBackground;
    private CollapsingToolbarLayout toolbarLayout;

    private static final String TAG = "RestaurantMenuFragment";

    public RestaurantMenuFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.activity_restaurant_details, container, false);

        ((MainActivity) mContext).getSupportActionBar().hide();

        ImageView ivBtnBack = rootView.findViewById(R.id.iv_restaurant_sub_back);

        MySharedPreferences sPref = new MySharedPreferences(getActivity());
        if (sPref.getLocale().equals("ar"))
            ivBtnBack.setImageResource(R.drawable.ic_arrow_forward_white);

        ivBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*if (getFragmentManager() != null) {
                    getFragmentManager()
                            .beginTransaction()
                            .replace(R.id.main_content, new FoodFragment())
//                                .addToBackStack(null)
                            .commit();
                }*/
                mContext.onBackPressed();
            }
        });

//        tvToolbarTitle = rootView.findViewById(R.id.tv_restaurant_menu_toolbar_title);
//        ivBackground = rootView.findViewById(R.id.iv_restaurant_menu_background);
//        tvResTitle = rootView.findViewById(R.id.tv_restaurant_menu_title);
//        tvResDesc = rootView.findViewById(R.id.tv_restaurant_menu_type);
//        viewPager = rootView.findViewById(R.id.menu_viewPager);
//        tabLayout = rootView.findViewById(R.id.menu_tabLayout);

        Bundle getData = getArguments();

        if (getData != null) {
            tvToolbarTitle.setText(getData.getString("listing_name"));
            tvResTitle.setText(getData.getString("listing_name"));
            tvResDesc.setText(getData.getString("listing_desc"));
            Picasso.get().load(getData.getString("listing_image")).placeholder(R.drawable.boxes_logo).into(ivBackground);
        }

        adapter = new TabAdapter(getFragmentManager());
//        adapter.addFragment(new SubscriptionPlanFragment(), getString(R.string.subscription));
        adapter.addFragment(new MenuFragment(), getString(R.string.menu));

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setCurrentItem(1);

        viewPager.addOnPageChangeListener(
                new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(
                new TabLayout.ViewPagerOnTabSelectedListener(viewPager));

        //enable back pressed navigation
        /*rootView.setFocusableInTouchMode(true);
        rootView.requestFocus();
        rootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_BACK) {

//                    alertDialog(mContext);
                    if (getFragmentManager() != null) {
                        getFragmentManager()
                                .beginTransaction()
                                .replace(R.id.main_content, new FoodFragment())
                                .commit();
                    }
                 *//*   if (getFragmentManager() != null)
                        getFragmentManager().popBackStack();*//*
                    return true;
                }
                return false;
            }
        });*/

        return rootView;
    }

    private Activity mContext;

    @Override
    public void onAttach(Context context) {
        this.mContext = (Activity) context;
        super.onAttach(context);
    }
}