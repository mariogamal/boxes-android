package com.app.boxes.old.Fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.provider.MediaStore;
import android.text.InputType;
import android.util.Log;
import android.util.Patterns;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.boxes.old.API.API;
import com.app.boxes.old.Activity.MainActivity;
import com.app.boxes.old.Model.User;
import com.app.boxes.old.Multipart.VolleyMultipartRequest;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.google.android.material.snackbar.Snackbar;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ProfileEditFragment extends Fragment {

    private static final String TAG = "ProfileEditFragment";
    private String image = "";

    private static int GALLERY = 1;

    private EditText etFirstName, etLastName, etEmail, etPhone, etAddress;
    private ImageView btnUploadImg;
    private Button btnSave;
    private View snackbarView;
    private CircleImageView circleImageView;
    private LinearLayout loaderLayout;

    private Bitmap bitmap;

    private MySharedPreferences sPref;

    public ProfileEditFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_profile_edit, container, false);

        requestMultiplePermissions();

        sPref = new MySharedPreferences(mContext);

        snackbarView = mContext.findViewById(android.R.id.content);

        TextView toolbarText = ((MainActivity) mContext).findViewById(R.id.tv_toolbar_title);
        toolbarText.setText(getString(R.string.profile_edit));

        ((MainActivity) mContext).getSupportActionBar().hide();

        ImageView ivBack = rootView.findViewById(R.id.iv_profile_edit_back);
        btnUploadImg = rootView.findViewById(R.id.iv_upload_profile_edit);
        circleImageView = rootView.findViewById(R.id.civ_profile_edit);

        MySharedPreferences sPref = new MySharedPreferences(mContext);
        if (sPref.getLocale().equals("ar"))
            ivBack.setImageResource(R.drawable.ic_arrow_forward_white);

        User user = sPref.getUserObject();
        User.AccessToken token = sPref.getAccessTokenObject();

        btnSave = rootView.findViewById(R.id.btn_save_profile_edit);

        etFirstName = rootView.findViewById(R.id.et_first_name_profile_edit);
        etLastName = rootView.findViewById(R.id.et_last_name_profile_edit);
        etEmail = rootView.findViewById(R.id.et_email_profile_edit);
        etPhone = rootView.findViewById(R.id.et_phone_profile_edit);
        etAddress = rootView.findViewById(R.id.et_address_profile_edit);
        etAddress.setImeOptions(EditorInfo.IME_ACTION_DONE);
        etAddress.setRawInputType(InputType.TYPE_CLASS_TEXT);

//        if (sPref.getUserArea() != null)
//            etAddress.setText(sPref.getUserArea());

        loaderLayout = rootView.findViewById(R.id.ll_loader_profile_edit);
        loaderLayout.setVisibility(View.GONE);

        ivBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(mContext, R.id.main_content).navigateUp();
            }
        });

        circleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "Upload Image");
                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(galleryIntent, GALLERY);
            }
        });

        btnUploadImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "Upload Image");
                Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

                startActivityForResult(galleryIntent, GALLERY);
            }
        });

        final String accessToken;
        if (token != null) {
            accessToken = token.getAccessToken();
            Log.d(TAG, "onCreate: AccessToken " + accessToken);
            getAndSetUserDetails(accessToken);

            btnSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    updateUserInfo(accessToken);
                }
            });
        }

        // set focus on enter tap
        etFirstName.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_ENTER) {
                    etLastName.requestFocus();
                    return true;
                }
                return false;
            }
        });

        return rootView;
    }

    private Activity mContext;

    @Override
    public void onAttach(Context context) {
        this.mContext = (Activity) context;
        super.onAttach(context);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == mContext.RESULT_CANCELED) {
            return;
        }
        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {

                    bitmap = MediaStore.Images.Media.getBitmap(mContext.getContentResolver(), contentURI);
                    bitmap= getScaledBitmap(bitmap, 500, 500);
                    circleImageView.setImageBitmap(bitmap);
                    image = "image";

                } catch (IOException e) {
                    e.printStackTrace();
                    Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.failed), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));
                    snackbar.show();
                }
            }
        }
    }

    public static Bitmap getScaledBitmap(Bitmap b, int reqWidth, int reqHeight) {
        Matrix m = new Matrix();
        m.setRectToRect(new RectF(0, 0, b.getWidth(), b.getHeight()), new RectF(0, 0, reqWidth, reqHeight), Matrix.ScaleToFit.CENTER);
        return Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), m, true);
    }

    private byte[] getFileDataFromDrawable(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        if (bitmap != null)
            bitmap.compress(Bitmap.CompressFormat.PNG, 20, byteArrayOutputStream);
        return byteArrayOutputStream.toByteArray();
    }

    private void requestMultiplePermissions() {
        Dexter.withActivity(mContext)
                .withPermissions(

                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
//                                Snackbar.make(activity.findViewById(android.R.id.content), "Permission granted to access gallery", Snackbar.LENGTH_SHORT).show();
                            Log.d(TAG, "Permission granted to access gallery");
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.permission_required_to_access_gallery), Snackbar.LENGTH_SHORT);
                            View getSnackbarView = snackbar.getView();
                            getSnackbarView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
                            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                            tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));
                            snackbar.show();

                            Log.d(TAG, "Permission required to access gallery");
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Log.d(TAG, "DexterError: " + error.toString());

                        Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.some_error), Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));
                        snackbar.show();
                    }
                })
                .onSameThread()
                .check();
    }

    // get user details
    private void getAndSetUserDetails(final String accessToken) {
        String url = API.getAPI() + "auth/user";
        Log.d(TAG, "User URL: " + url);

        loaderLayout.setVisibility(View.VISIBLE);
        RequestQueue requestQueue;
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "User onResponse: " + response);
                loaderLayout.setVisibility(View.GONE);

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    int status = jsonObject.optInt("status");

                    if (status == 1) {
                        JSONObject data = jsonObject.getJSONObject("data");

                        String id = data.optString("id");
                        String first_name = data.optString("first_name");
                        String last_name = data.optString("last_name");
                        String getEmail = data.optString("email");
                        String getPhoneNumber = data.optString("phone");
                        String getImage = data.getString("image");
                        String address = data.getString("address");

                        User user = new User();

                        user.setId(id);
                        if (!first_name.equals("null")) {
                            etFirstName.setText(first_name);
                            user.setFirstName(first_name);
                        }
                        if (!last_name.equals("null")) {
                            etLastName.setText(last_name);
                            user.setLastName(last_name);
                        }
                        if (!getEmail.equals("null")) {
                            etEmail.setText(getEmail);
                            user.setEmail(getEmail);
                        }
                        if (!getPhoneNumber.equals("null")) {
                            etPhone.setText(getPhoneNumber);
                            user.setPhoneNumber(getPhoneNumber);
                        }
                        if (!address.equals("null")) {
                            etAddress.setText(address);
                        }

                        String imageUrl = API.getImageAPI() + getImage;

                        if (!getImage.equals("null")) {
                            Picasso.get().load(imageUrl).fit().centerCrop().into(circleImageView);
                            user.setImage(getImage);
                        } else {
                            circleImageView.setImageResource(R.drawable.avatar);
                        }

                        sPref.saveUserObject(user);

                        Log.d("User-Response", "Id: " + id + " first_name: " + first_name + " last_name: " + last_name + "  email: " + getEmail + " phone: " + getPhoneNumber);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loaderLayout.setVisibility(View.GONE);
                Log.d(TAG, "Volley Error: " + error.toString());
                if (error instanceof NoConnectionError) {
                    Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.no_network), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));
                    snackbar.show();
                } else {
                    Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));
                    snackbar.show();
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (accessToken != null) {
                    params.put("Authorization", accessToken);
                    Log.d(TAG, "getHeaders " + accessToken);
                }
                return params;
            }
        };
        requestQueue = Volley.newRequestQueue(mContext);
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    // edit and update user details
    private void updateUserInfo(final String accessToken) {
        String firstName = etFirstName.getText().toString();
        String lastName = etLastName.getText().toString();
        String email = etEmail.getText().toString();
        String address = etAddress.getText().toString();

        Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.please_fill_the_required_field), Snackbar.LENGTH_SHORT);
        View getSnackbarView = snackbar.getView();
        getSnackbarView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
        tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));

        if (firstName.isEmpty()) {
            etFirstName.requestFocus();
            snackbar.show();
        } else if (lastName.isEmpty()) {
            etLastName.requestFocus();
            snackbar.show();
        } else if (email.isEmpty()) {
            etEmail.requestFocus();
            snackbar.show();
        } else if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            etEmail.requestFocus();
            etEmail.setError("Please enter valid email");
        } else if (address.isEmpty()) {
            etAddress.requestFocus();
            snackbar.show();
        } else if (address.equals(" ")) {
            etAddress.requestFocus();
            etAddress.setError("Please enter valid email");
        } else if (image.isEmpty()) {
            Snackbar snackbar1 = Snackbar.make(snackbarView, getString(R.string.please_select_an_image), Snackbar.LENGTH_SHORT);
            View getSnackbarView1 = snackbar1.getView();
            getSnackbarView1.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
            TextView tvSnackBar1 = getSnackbarView1.findViewById(com.google.android.material.R.id.snackbar_text);
            tvSnackBar1.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));
            snackbar1.show();
        } else {

            if (address.contains(" "))
                address = address.replace(" ", "%20");

            final String BASE_URL = API.getAPI() + "auth/store-profile?" +
                    "first_name=" + firstName +
                    "&last_name=" + lastName +
                    "&email=" + email +
                    "&address=" + address;

            loaderLayout.setVisibility(View.VISIBLE);
            final RequestQueue rQueue = Volley.newRequestQueue(mContext);
            VolleyMultipartRequest volleyMultipartRequest = new VolleyMultipartRequest(Request.Method.POST, BASE_URL,
                    new Response.Listener<NetworkResponse>() {
                        @Override
                        public void onResponse(NetworkResponse response) {
                            Log.d("ressssssoo", new String(response.data));
                            rQueue.getCache().clear();
                            loaderLayout.setVisibility(View.GONE);

                            try {
                                JSONObject jsonObject = new JSONObject(new String(response.data));

                                int status = jsonObject.optInt("status");
                                String message = jsonObject.optString("message");
                                JSONObject paramObj = jsonObject.optJSONObject("parameters");

                                Snackbar snackbar1 = Snackbar.make(snackbarView, message, Snackbar.LENGTH_SHORT);
                                View getSnackbarView1 = snackbar1.getView();
                                getSnackbarView1.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
                                TextView tvSnackBar1 = getSnackbarView1.findViewById(com.google.android.material.R.id.snackbar_text);
                                tvSnackBar1.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));
                                snackbar1.show();

                                if (status == 1) {
                                    User user = new User();

                                    JSONObject dataObj = jsonObject.getJSONObject("data");
                                    String userId = dataObj.getString("id");
                                    String firstName = dataObj.getString("first_name");
                                    String lastName = dataObj.getString("last_name");
                                    String phoneNo = dataObj.getString("phone");
                                    String email = dataObj.getString("email");
                                    String image = dataObj.getString("image");

                                    user.setId(userId);
                                    user.setFirstName(firstName);
                                    user.setLastName(lastName);
                                    user.setPhoneNumber(phoneNo);
                                    user.setEmail(email);

                                    if (!image.equals("null")) {
                                        String imgUrl = API.getImageAPI() + image;
                                        user.setImage(imgUrl);
                                    } else {
                                        user.setImage(null);
                                    }

                                    sPref.saveUserObject(user);
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            loaderLayout.setVisibility(View.GONE);
                            Log.d(TAG, "OnErrorResponse: " + error.toString());
                            if (error instanceof NoConnectionError) {
                                Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.no_network), Snackbar.LENGTH_SHORT);
                                View getSnackbarView = snackbar.getView();
                                getSnackbarView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
                                TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                                tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));
                                snackbar.show();
                            } else {
                                Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                                View getSnackbarView = snackbar.getView();
                                getSnackbarView.setBackgroundColor(mContext.getResources().getColor(android.R.color.white));
                                TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                                tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.active_tab_color));
                                snackbar.show();
                            }
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    if (accessToken != null) {
                        params.put("Authorization", accessToken);
                        Log.d(TAG, "getHeaders " + accessToken);
                    }
                    return params;
                }

                /*
                 *pass files using below method
                 * */
                @Override
                protected Map<String, DataPart> getByteData() {
                    Map<String, DataPart> params = new HashMap<>();
                    if (bitmap != null) {
                        long imagename = System.currentTimeMillis();
                        params.put("image", new DataPart(imagename + ".png", getFileDataFromDrawable(bitmap)));
                        Log.d("GetByteData", "image is: " + new DataPart(imagename + ".png", getFileDataFromDrawable(bitmap)));
                    }
                    return params;
                }
            };
            volleyMultipartRequest.setRetryPolicy(new DefaultRetryPolicy(
                    30000,
                    0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            rQueue.add(volleyMultipartRequest);
        }

    }
}
