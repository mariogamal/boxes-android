package com.app.boxes.old.Fragment;


import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.app.boxes.old.API.API;
import com.app.boxes.old.Activity.MainActivity;
import com.app.boxes.old.Activity.TermsAndConditionActivity;
import com.app.boxes.old.Adapter.MyCartAdapter;
import com.app.boxes.old.Model.Area;
import com.app.boxes.old.Model.Cart;
import com.app.boxes.old.Model.User;
import com.app.boxes.R;
import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.app.boxes.old.UI.LoginBottomSheet;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A simple {@link Fragment} subclass.
 */
public class PlaceOrderFragment extends Fragment implements MyCartAdapter.MyCartAdapterCallback {

    private static final String TAG = "PlaceOrderFragment";

    private ImageView ivPlaceOrderBack, ivCheck, ivUnCheck, ivVoucherClose;
    private TextView tvCardSelection, tvTax, tvSubtotal, tvAddress, tvDeliveryCharges, tvDiscount, tvTotal, tvVoucher, tvEmail, tvDeliveryTime, tvToolbar, tvTermsConditions;
    private EditText etVoucher;
    private Button btnVoucher, btnPlaceOrder;
    private View snackbarView;
    private LinearLayout llVoucherPanel, llContactInfo, llDeliveryDetails, llPaymentType, loaderLayout;
    private RecyclerView rvCartItems;

    private String cardType = "";

    private int checked = 0;

    private MySharedPreferences sPref;

    private ArrayList<Cart> cartArrayList = new ArrayList<>();

    private MyCartAdapter myCartAdapter;
    private Activity mContext;

    public PlaceOrderFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_place_order, container, false);

        //intialize widgets
        initializer(rootView);

        ((MainActivity) mContext).toolbar.setVisibility(View.GONE);

        rvCartItems.setLayoutManager(new LinearLayoutManager(mContext));
        cardType = mContext.getResources().getString(R.string.cash_on_delivery);
        //if arabic language is selected so icon changed
        if (sPref.getLocale().equals("ar"))
            ivPlaceOrderBack.setImageResource(R.drawable.ic_arrow_forward_white);

        //navigate back to flower details
        ivPlaceOrderBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mContext.onBackPressed();
            }
        });

        //display cart data
        if (sPref.getLangObject() != null && sPref.getAccessTokenObject() != null) {
            myCart(sPref.getLangObject().getId(), sPref.getAccessTokenObject().getAccessToken());
        }

        //set the vendo
        if (sPref.getListingName() != null) {
//            tvToolbar.setText(sPref.getListingName());
        }

        //show and hide voucher panel
        tvVoucher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (llVoucherPanel.getVisibility() != View.VISIBLE) {
                    llVoucherPanel.setVisibility(View.VISIBLE);
                } else {
                    llVoucherPanel.setVisibility(View.GONE);
                }
            }
        });

        //show and hide voucher panel
        ivVoucherClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (llVoucherPanel.getVisibility() != View.VISIBLE) {
                    llVoucherPanel.setVisibility(View.VISIBLE);
                } else {
                    llVoucherPanel.setVisibility(View.GONE);
                }
            }
        });

        btnVoucher.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String voucherText = etVoucher.getText().toString();
                if (voucherText.isEmpty()) {
                    Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.please_fill_the_required_field), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                    snackbar.show();
                } else if (sPref.getAccessTokenObject() != null) {
                    String token = sPref.getAccessTokenObject().getAccessToken();

                    applyVoucher(voucherText, token);
                }
            }
        });

        // contact info to get user contact info
        llContactInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*if (getFragmentManager() != null) {
                    getFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                            .replace(R.id.main_content, new ContanctInfoFragment())
                            .addToBackStack("place_order")
                            .commit();
                }*/
                ((MainActivity) mContext).navigationController(R.id.contanctInfoFragment, null);
            }
        });

        // contact info to get user contact info
        llDeliveryDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*if (getFragmentManager() != null) {
                    getFragmentManager()
                            .beginTransaction()
                            .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                            .replace(R.id.main_content, new ContanctInfoFragment())
                            .addToBackStack("place_order")
                            .commit();
                }*/
                ((MainActivity) mContext).navigationController(R.id.deliveryDetailsFragment, null);
            }
        });

        //alert dialog for card selection
        llPaymentType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CardSelectionDialog(mContext);
            }
        });

        //check if checkBox is unchecked
        ivCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ivUnCheck.setVisibility(View.VISIBLE);
                ivCheck.setVisibility(View.GONE);
                checked = 0;
            }
        });

        //check if checkBox is checked
        ivUnCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ivCheck.setVisibility(View.VISIBLE);
                ivUnCheck.setVisibility(View.GONE);
                checked = 1;
            }
        });

        //Open Terms and Conditions Activity
        tvTermsConditions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(mContext, TermsAndConditionActivity.class));
            }
        });

        return rootView;
    }

    private void initializer(View rootView) {
        ivPlaceOrderBack = rootView.findViewById(R.id.iv_place_order_back);
        ivCheck = rootView.findViewById(R.id.iv_place_order_check);
        ivUnCheck = rootView.findViewById(R.id.iv_place_order_uncheck);
        ivVoucherClose = rootView.findViewById(R.id.iv_place_order_close);

        tvToolbar = rootView.findViewById(R.id.tv_place_order_toolbar_title);
        tvCardSelection = rootView.findViewById(R.id.tv_card_selection);
        tvSubtotal = rootView.findViewById(R.id.tv_order_subtotal_price);
        tvDeliveryCharges = rootView.findViewById(R.id.tv_order_delivery_price);
        tvDiscount = rootView.findViewById(R.id.tv_order_discount);
        tvTotal = rootView.findViewById(R.id.tv_order_total);
        tvVoucher = rootView.findViewById(R.id.tv_order_voucher);
        tvAddress = rootView.findViewById(R.id.tv_place_order_location);
        tvTax = rootView.findViewById(R.id.tv_place_order_tax);
        tvEmail = rootView.findViewById(R.id.tv_place_order_email);
        tvDeliveryTime = rootView.findViewById(R.id.tv_order_delivery_time);
        tvTermsConditions = rootView.findViewById(R.id.tv_place_order_terms_conditions);

        tvTermsConditions.setPaintFlags(tvTermsConditions.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tvTermsConditions.setText(getString(R.string.terms_and_conditions));

        etVoucher = rootView.findViewById(R.id.et_place_order_voucher_text);

        btnVoucher = rootView.findViewById(R.id.btn_apply_voucher);
        btnPlaceOrder = rootView.findViewById(R.id.btn_place_order);

        snackbarView = mContext.findViewById(android.R.id.content);

        rvCartItems = rootView.findViewById(R.id.rv_place_order_cart_items);

        llVoucherPanel = rootView.findViewById(R.id.ll_voucher_panel);
        llContactInfo = rootView.findViewById(R.id.ll_place_order_contact_info);
        llDeliveryDetails = rootView.findViewById(R.id.ll_place_order_delivery_details);
        llPaymentType = rootView.findViewById(R.id.ll_place_order_payment);
        loaderLayout = rootView.findViewById(R.id.ll_loader_place_order);
        loaderLayout.setVisibility(View.GONE);

        sPref = new MySharedPreferences(mContext);
    }

    @Override
    public void onAttach(Context context) {
        this.mContext = (Activity) context;
        super.onAttach(context);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
    }

    private void CardSelectionDialog(final Context context) {

        final Dialog cardSelectionDialog = new Dialog(mContext);

        View view = getLayoutInflater().inflate(R.layout.cards_alert_dialog, null, false);
        final RadioButton rbCreditCard = view.findViewById(R.id.rb_credit_card);
        RadioButton cod = view.findViewById(R.id.rb_visa_card);

        cardSelectionDialog.setContentView(view);


        if (cardType.equals(mContext.getResources().getString(R.string.cash_on_delivery))) {
            rbCreditCard.setChecked(true);
            cod.setChecked(false);
        } else {
            rbCreditCard.setChecked(false);
            cod.setChecked(true);
        }

        view.findViewById(R.id.btn_custom_dialog_yes).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rbCreditCard.isChecked()) {
                    cardType = mContext.getResources().getString(R.string.cash_on_delivery);
                } else {
                    cardType = mContext.getResources().getString(R.string.credit_card);
                }
                tvCardSelection.setText(cardType);
                cardSelectionDialog.dismiss();
            }
        });

        cardSelectionDialog.show();
    }

    private void addCartItem(String productId, int quantity, String operator, final String accessToken) {
        String url = API.getAPI() + "auth/cart/add-cart-item?product_id=" + productId + "&quantity=" + quantity + "&opr=" + operator;
        Log.d(TAG, url);

        loaderLayout.setVisibility(View.VISIBLE);
        RequestQueue requestQueue;
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "addCardItem onResponse: " + response.toString());
                loaderLayout.setVisibility(View.GONE);

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    String msg = jsonObject.getString("message");

                    if (status == 1) {
                        JSONObject dataObj = jsonObject.getJSONObject("data");
                        String subTotal = dataObj.getString("sub_total");
                        String voucherDiscount = dataObj.getString("voucher_discount");
                        String totalDiscount = dataObj.getString("total_discount");
                        String deliveryCharges = dataObj.getString("delivery_charges");
                        String tax = dataObj.getString("tax");
                        String total = dataObj.getString("total");

                        tvSubtotal.setText(subTotal);
                        tvDiscount.setText(totalDiscount);
                        tvDeliveryCharges.setText(deliveryCharges);
                        tvTax.setText(tax);
                        tvTotal.setText(total);

                        Snackbar snackbar = Snackbar.make(snackbarView, msg, Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                        snackbar.show();
                    } else {
                        Snackbar snackbar = Snackbar.make(snackbarView, msg, Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                        snackbar.show();
                    }
                } catch (Exception ex) {
                    Log.d(TAG, "onException: " + ex.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse: " + error.getMessage());
                loaderLayout.setVisibility(View.GONE);
                if (error instanceof NoConnectionError) {
                    Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.no_network), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                    snackbar.show();
                } else {
                    Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                    snackbar.show();
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (accessToken != null) {
                    params.put("Authorization", accessToken);
                    Log.d(TAG, "getHeaders: " + accessToken);
                }
                return params;
            }
        };
        requestQueue = Volley.newRequestQueue(mContext);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }

    private void removeCartItem(String productId, final String accessToken) {
        String url = API.getAPI() + "auth/cart/remove-cart-item?product_id=" + productId;
        Log.d(TAG, url);

        loaderLayout.setVisibility(View.VISIBLE);
        RequestQueue requestQueue;
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "onResponse: " + response.toString());
                loaderLayout.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    String message = jsonObject.getString("message");

                    if (status == 1) {
                        Snackbar snackbar = Snackbar.make(snackbarView, message, Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                        snackbar.show();

                        tvSubtotal.setText("0");
                        tvDeliveryCharges.setText("0");
                        tvDiscount.setText("0");
                        tvTax.setText("0");
                        tvTotal.setText("0");

                        Navigation.findNavController(mContext, R.id.main_content).navigateUp();
                    } else {
                        Snackbar snackbar = Snackbar.make(snackbarView, message, Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                        snackbar.show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse: " + error.getMessage());
                loaderLayout.setVisibility(View.GONE);
                if (error instanceof NoConnectionError) {
                    Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.no_network), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                    snackbar.show();
                } else {
                    Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                    snackbar.show();
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (accessToken != null) {
                    params.put("Authorization", accessToken);
                    Log.d(TAG, "getHeaders: " + accessToken);
                }
                return params;
            }
        };

        requestQueue = Volley.newRequestQueue(mContext);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);

    }

    private void myCart(String languageId, final String accessToken) {
        String url = API.getAPI() + "auth/cart/my-cart?language_id=" + languageId;
        Log.d(TAG, url);

        loaderLayout.setVisibility(View.VISIBLE);
        RequestQueue requestQueue;
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "onResponse: " + response.toString());
                loaderLayout.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    String msg = jsonObject.getString("message");

                    if (status == 1) {
                        JSONObject jsonObj = jsonObject.getJSONObject("data");
                        Log.d(TAG, "onResponse jsonObj: " + jsonObj);
                        cartArrayList.clear();

                        Cart cart;

                        //get the cart array item
                        JSONArray cartArray = jsonObj.getJSONArray("cart_items");
                        for (int i = 0; i < cartArray.length(); i++) {

                            Log.d(TAG, "onResponse cartArray: " + cartArray);

                            JSONObject jsonCart = cartArray.getJSONObject(i);

                            String id = jsonCart.getString("id");
                            String prodName = jsonCart.getString("product_name");
                            String prodPrice = jsonCart.getString("price");
                            String discountPrice = jsonCart.getString("discount_price");
                            String quantity = jsonCart.getString("quantity");
                            String stockCount = jsonCart.getString("stock_count");

//                            tvProductName.setText(prodName);
//                            tvProductPrice.setText(prodPrice);
//                            tvQuantity.setText(quantity);

                            cart = new Cart();
                            cart.setProdId(id);
                            cart.setProdName(prodName);
                            cart.setProdPrice(prodPrice);
                            cart.setProdQuantity(quantity);
                            cart.setProdStockCount(stockCount);

                            Log.d(TAG, "onResponse cart array:" + id + " " + prodName + stockCount);
                            cartArrayList.add(cart);
                        }

                        myCartAdapter = new MyCartAdapter(mContext, cartArrayList, PlaceOrderFragment.this);
                        rvCartItems.setAdapter(myCartAdapter);

                        //get the details item
                        JSONObject totalObj = jsonObj.getJSONObject("total");
                        Log.d(TAG, "onResponse detailsObj: " + totalObj);

                        String subTotal = totalObj.getString("sub_total");
                        String voucherDiscount = totalObj.getString("voucher_discount");
                        String productDiscount = totalObj.getString("total_discount");
                        String deliveryCharges = totalObj.getString("delivery_charges");
                        String tax = totalObj.getString("tax");
                        String total = totalObj.getString("total");

                        tvSubtotal.setText(subTotal);
                        tvDeliveryCharges.setText(deliveryCharges);
                        tvDiscount.setText(productDiscount);
                        tvTotal.setText(total);
                        tvTax.setText(tax);
                    } else {
                        Snackbar snackbar = Snackbar.make(snackbarView, msg, Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                        snackbar.show();
                    }
                } catch (Exception ex) {
                    Log.d(TAG, "onException: " + ex.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse: " + error.getMessage());
                loaderLayout.setVisibility(View.GONE);
                if (error instanceof NoConnectionError) {
                    Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.no_network), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                    snackbar.show();
                } else {
                    Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                    snackbar.show();
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (accessToken != null) {
                    params.put("Authorization", accessToken);
                    Log.d(TAG, "getHeaders: " + accessToken);
                }
                return params;
            }
        };

        requestQueue = Volley.newRequestQueue(mContext);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);

    }

    private void applyVoucher(String voucherName, final String accessToken) {
        if (voucherName.contains(" "))
            voucherName = voucherName.replace(" ", "%20");
        String url = API.getAPI() + "auth/cart/apply-voucher?voucher_name=" + voucherName;
        Log.d(TAG, url);

        loaderLayout.setVisibility(View.VISIBLE);
        RequestQueue requestQueue;
        StringRequest jsonObjectRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "voucher onResponse: " + response.toString());
                loaderLayout.setVisibility(View.GONE);

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    String msg = jsonObject.getString("message");

                    if (status == 1) {

                        Snackbar snackbar = Snackbar.make(snackbarView, msg, Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                        snackbar.show();

                        JSONObject dataObj = jsonObject.getJSONObject("data");
                        JSONObject totalObj = dataObj.getJSONObject("total");

                        String subtotal = totalObj.getString("sub_total");
                        String productDiscount = totalObj.getString("product_discount");
                        String voucherDiscount = totalObj.getString("voucher_discount");
                        String totalDiscount = totalObj.getString("total_discount");
                        String deliveryCharges = totalObj.getString("delivery_charges");
                        String tax = totalObj.getString("tax");
                        String total = totalObj.getString("total");

                        tvSubtotal.setText(subtotal);
                        tvDiscount.setText(productDiscount);
                        tvDeliveryCharges.setText(deliveryCharges);
                        tvTax.setText(tax);
                        tvTotal.setText(total);

                    } else {
                        Snackbar snackbar = Snackbar.make(snackbarView, msg, Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                        snackbar.show();
                    }
                } catch (Exception ex) {
                    Log.d(TAG, "onException: " + ex.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onErrorResponse: " + error.getMessage());
                loaderLayout.setVisibility(View.GONE);
                if (error instanceof NoConnectionError) {
                    Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.no_network), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                    snackbar.show();
                } else {
                    Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                    snackbar.show();
                }
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (accessToken != null) {
                    params.put("Authorization", accessToken);
                    Log.d(TAG, "getHeaders: " + accessToken);
                }
                return params;
            }
        };
        requestQueue = Volley.newRequestQueue(mContext);
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(jsonObjectRequest);
    }

    private void placeOrder(String firstName, String lastName, String email, String phone, String currentLocation, String building, String street, String area, String addIns, final String accessToken) {
        /*if (currentLocation.isEmpty()) {
            Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.no_location_provided), Snackbar.LENGTH_SHORT);
            View getSnackbarView = snackbar.getView();
            getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
            tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
            snackbar.show();
        } else */
        if (building.isEmpty()) {
            Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.no_building_provided), Snackbar.LENGTH_SHORT);
            View getSnackbarView = snackbar.getView();
            getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
            tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
            snackbar.show();
        } else if (street.isEmpty()) {
            Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.no_street_provided), Snackbar.LENGTH_SHORT);
            View getSnackbarView = snackbar.getView();
            getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
            tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
            snackbar.show();
        } else if (area.isEmpty()) {
            Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.no_area_provided), Snackbar.LENGTH_SHORT);
            View getSnackbarView = snackbar.getView();
            getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
            tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
            snackbar.show();
        } else {
//            if (currentLocation.contains(" "))
//                currentLocation = currentLocation.replace(" ", "%20");
            if (building.contains(" "))
                building = building.replace(" ", "%20");
            if (street.contains(" "))
                street = street.replace(" ", "%20");
            if (area.contains(" "))
                area = area.replace(" ", "%20");
            if (addIns.contains(" "))
                addIns = addIns.replace(" ", "%20");
            String url = API.getAPI() + "auth/cart/place-order?first_name=" + firstName + "&last_name=" + lastName + "&email=" + email + "&phone=" + phone
                    + "&building=" + building + "&street=" + street + "&area=" + area + /*"&current_location=" + currentLocation +*/ "&additional_instruction=" + addIns;
            Log.d(TAG, url);

            loaderLayout.setVisibility(View.VISIBLE);
            RequestQueue requestQueue;
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.d(TAG, "onResponse: " + response.toString());
                    loaderLayout.setVisibility(View.GONE);
                    try {
                        int status = response.getInt("status");
                        String msg = response.getString("message");

                        if (status == 1) {
//                        JSONObject jsonObj = response.getJSONObject("data");
//                        Log.d(TAG, "onResponse jsonObj: " + jsonObj);

                            Snackbar snackbar = Snackbar.make(snackbarView, msg, Snackbar.LENGTH_SHORT);
                            View getSnackbarView = snackbar.getView();
                            getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                            tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                            snackbar.show();

                            new CountDownTimer(2000, 1000) {

                                @Override
                                public void onTick(long l) {

                                }

                                @Override
                                public void onFinish() {

                                    Bundle bundle = new Bundle();
                                    bundle.putString("from", "placeOrder");
                                    ((MainActivity) mContext).navigationController(R.id.thankYouFragment, bundle, null);

                                    /*if (getFragmentManager() != null) {
                                        getFragmentManager()
                                                .beginTransaction()
                                                .setCustomAnimations(android.R.anim.slide_in_left, android.R.anim.slide_out_right)
                                                .replace(R.id.main_content, new ThankYouFragment())
                                                .commit();
                                    }*/
//                                    startActivity(new Intent(mContext, MainActivity.class));
//                                    mContext.finish();
                                }
                            }.start();

                        } else {
                            Snackbar snackbar = Snackbar.make(snackbarView, msg, Snackbar.LENGTH_SHORT);
                            View getSnackbarView = snackbar.getView();
                            getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                            tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                            snackbar.show();
                        }
                    } catch (Exception ex) {
                        Log.d(TAG, "onException: " + ex.toString());
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(TAG, "onErrorResponse: " + error.toString());
                    loaderLayout.setVisibility(View.GONE);
                    if (error instanceof NoConnectionError) {
                        Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.no_network), Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                        snackbar.show();
                    } else {
                        Snackbar snackbar = Snackbar.make(snackbarView, error.toString(), Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                        snackbar.show();
                    }
                }
            }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> params = new HashMap<>();
                    if (accessToken != null) {
                        params.put("Authorization", accessToken);
                        Log.d(TAG, "getHeaders: " + accessToken);
                    }
                    return params;
                }
            };
            requestQueue = Volley.newRequestQueue(mContext);
            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            requestQueue.add(jsonObjectRequest);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        checked = 0;
        //check if user area is not null then set the location
        if (sPref.getAreaObject() != null) {
            // check if user area has populated in Shared Preferences and it is not null// set the user address null
            Area area = sPref.getAreaObject();
            if (((area.getBuildingNo() != null) && ((area.getStreetNo() != null) && ((area.getArea() != null))))) {
                if ((!area.getBuildingNo().equals("null")) && (!area.getStreetNo().equals("null")) && (!area.getArea().equals("null"))) {
                    tvAddress.setText("Building# " + area.getBuildingNo() + " Floor# " + area.getFloor() + " Street# " + area.getStreetNo() + " Area: " + area.getArea());
                    Log.d(TAG, "onResume: " + " " + area.getArea() + " " + area.getBuildingNo() + " " + area.getStreetNo() + " " + area.getFloor() + " " + area.getAddInst());
                }
            } else {
                tvAddress.setText(R.string.please_enter_your_address);
            }
        } else {
            tvAddress.setText(R.string.please_enter_your_address);
        }

        //check if user object is not null
        if (sPref.getUserObject() != null) {
            User contactEmail = sPref.getUserObject();
            if (contactEmail.getEmail() != null) {
                tvEmail.setText(contactEmail.getEmail());
            } else {
                tvEmail.setText(R.string.no_email);
            }
        } else {
            tvEmail.setText(R.string.no_email);
        }

        //place order
        btnPlaceOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // check if access token object is not null
                if (sPref.getAccessTokenObject() != null) {
                    // check if area object is not null
                    if (sPref.getAreaObject() != null) {
                        final Area area = sPref.getAreaObject();
                        // check if user contact info object is not null
                        if (sPref.getUserObject() != null) {
                            final User user = sPref.getUserObject();

                            // check if terms and conditions checkbox is checked
                            if (checked == 0) {
                                Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.you_must_agree_with_our_terms_and_conditions), Snackbar.LENGTH_SHORT);
                                View getSnackbarView = snackbar.getView();
                                getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                                TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                                tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                                snackbar.show();
                            } else {
                                Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.please_provide_contact_info), Snackbar.LENGTH_SHORT);
                                View getSnackbarView = snackbar.getView();
                                getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                                TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                                tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                                if (area.getBuildingNo() != null && area.getStreetNo() != null && area.getArea() != null) {
                                    if (!area.getBuildingNo().equals("null") && !area.getStreetNo().equals("null") && !area.getArea().equals("null")) {
                                        if (user != null) {
                                            if (user.getFirstName() != null) {
                                                if (!user.getFirstName().equals("null")) {
                                                    if (user.getLastName() != null) {
                                                        if (!user.getLastName().equals("null")) {
                                                            if (user.getEmail() != null) {
                                                                if (!user.getEmail().equals("null")) {
                                                                    if (user.getPhoneNumber() != null) {
                                                                        if (!user.getPhoneNumber().equals("null")) {
                                                                            placeOrder(user.getFirstName(), user.getLastName(), user.getEmail(), user.getPhoneNumber(),
                                                                                    "", area.getBuildingNo(), area.getStreetNo(), area.getArea(), area.getAddInst(),
                                                                                    sPref.getAccessTokenObject().getAccessToken());
                                                                        } else {
                                                                            snackbar.show();
                                                                        }
                                                                    } else {
                                                                        snackbar.show();
                                                                    }
                                                                } else {
                                                                    snackbar.show();
                                                                }
                                                            } else {
                                                                snackbar.show();
                                                            }
                                                        } else {
                                                            snackbar.show();
                                                        }
                                                    } else {
                                                        snackbar.show();
                                                    }
                                                } else {
                                                    snackbar.show();
                                                }
                                            } else {
                                                snackbar.show();
                                            }
                                        }
                                    } else {
                                        snackbar.setText(getString(R.string.please_provide_delivery_details));
                                        snackbar.show();
                                    }
                                } else {
                                    snackbar.setText(getString(R.string.please_provide_delivery_details));
                                    snackbar.show();
                                }
                            }
                        } else {
                            Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.no_contact_info), Snackbar.LENGTH_SHORT);
                            View getSnackbarView = snackbar.getView();
                            getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                            TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                            tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                            snackbar.show();
                        }
                    } else {
                        Snackbar snackbar = Snackbar.make(snackbarView, getString(R.string.no_location_provided), Snackbar.LENGTH_SHORT);
                        View getSnackbarView = snackbar.getView();
                        getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                        TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                        tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                        snackbar.show();
                    }
                } else {
                    LoginBottomSheet bottomSheet = new LoginBottomSheet();
                    bottomSheet.setCancelable(false);
                    if (getFragmentManager() != null) {
                        bottomSheet.show(getFragmentManager(), "bottom_sheet");
                    }
                }
            }
        });
    }

    @Override
    public void onMyCartAddClicked(Cart cart) {
        if (sPref.getAccessTokenObject() != null) {
            int intCount = Integer.parseInt(cart.getProdQuantity());
            int quantity = 0;

            if (Integer.parseInt(cart.getProdQuantity()) != Integer.parseInt(cart.getProdStockCount())) {
                quantity = intCount + 1;
                String operator = "add";
                addCartItem(cart.getProdId(), quantity, operator, sPref.getAccessTokenObject().getAccessToken());
                cart.setProdQuantity(String.valueOf(quantity));
                myCartAdapter.notifyDataSetChanged();
            } else {
                quantity = Integer.parseInt(cart.getProdQuantity());
                cart.setProdQuantity(String.valueOf(quantity));
                myCartAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onMyCartSubClicked(Cart cart, int position) {
        if (Integer.parseInt(cart.getProdQuantity()) == 1) {
            cartArrayList.remove(position);
            myCartAdapter.notifyDataSetChanged();
            if (sPref.getAccessTokenObject() != null) {
                removeCartItem(cart.getProdId(), sPref.getAccessTokenObject().getAccessToken());
                myCartAdapter.notifyDataSetChanged();
            }
        } else {
            int intCount = Integer.parseInt(cart.getProdQuantity());
            int quantity = intCount - 1;
            String operator = "minus";
            cart.setProdQuantity(String.valueOf(quantity));

            if (sPref.getAccessTokenObject() != null) {
                addCartItem(cart.getProdId(), quantity, operator, sPref.getAccessTokenObject().getAccessToken());
                myCartAdapter.notifyDataSetChanged();
            }
        }
    }

}