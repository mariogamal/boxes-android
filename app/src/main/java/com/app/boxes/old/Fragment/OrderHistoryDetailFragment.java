package com.app.boxes.old.Fragment;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.app.boxes.old.API.API;
import com.app.boxes.old.Activity.MainActivity;
import com.app.boxes.old.Adapter.OrderHistoryMenuListAdapter;

import com.app.boxes.old.Model.DeliveryDates;
import com.app.boxes.old.Model.Language;
import com.app.boxes.old.Model.OrderHistoryMenu;
import com.app.boxes.old.Model.OrderHistoryMenuItem;
import com.app.boxes.old.Model.User;
import com.app.boxes.R;

import com.app.boxes.old.SharedPreference.MySharedPreferences;
import com.google.android.material.snackbar.Snackbar;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.threeten.bp.LocalDate;
import org.threeten.bp.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import sun.bob.mcalendarview.MCalendarView;
import sun.bob.mcalendarview.MarkStyle;
import sun.bob.mcalendarview.listeners.OnDateClickListener;
import sun.bob.mcalendarview.vo.DateData;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrderHistoryDetailFragment
        extends Fragment
        implements OrderHistoryMenuListAdapter.OnReviewClickListener/*, OnBackPressListener*/ {

    private static final String TAG = "HistoryDetailFragment";

    // private ImageView ivBtnBack;
    private MCalendarView mCalendarView;
    private TextView tvTitle, tvMealDate, tvNo;
    private ExpandableListView elOrderHistoryMenuList;
    private OrderHistoryMenuListAdapter adapter;
    private LinearLayout loaderLayout;
    private ImageView ivBtnBack;

    private ArrayList<OrderHistoryMenu> orderHistoryMenuArrayList = new ArrayList<>();
    private ArrayList<DeliveryDates> deliveryDatesArrayList = new ArrayList<>();

    private MySharedPreferences sPref;

    private Activity mContext;

    public OrderHistoryDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        this.mContext = (Activity) context;
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_order_history_detail, container, false);

        ((MainActivity) mContext).getSupportActionBar().hide();

        sPref = new MySharedPreferences(mContext);

        loaderLayout = rootView.findViewById(R.id.ll_loader_order_history_detail);
        loaderLayout.setVisibility(View.GONE);

        ivBtnBack = rootView.findViewById(R.id.iv_order_history_detail_back);
        tvTitle = rootView.findViewById(R.id.tv_order_history_detail_restaurant_name);
        tvNo = rootView.findViewById(R.id.tv_order_history_detail_no);
        tvNo.setVisibility(View.GONE);
        tvMealDate = rootView.findViewById(R.id.tv_order_history_detail_meal_date);
        mCalendarView = rootView.findViewById(R.id.order_history_detail_calendar);
        if (mCalendarView.getMarkedDates() != null) {
            mCalendarView.getMarkedDates().removeAdd();
        }
//        mCalendarView.setOnDateClickListener(new OnDateClickListener() {
//            @Override
//            public void onDateClick(View view, DateData date) {
//                Toast.makeText(mContext, "Date: " + date.getDayString() + "-" + date.getMonth() + "-" + date.getYear(), Toast.LENGTH_SHORT).show();
//            }
//        });
//        Log.d(TAG, "onCreateView: markedDates: " + );
//        TextView tvDate = rootView.findViewById(R.id.tv_order_history_detail_date);
        elOrderHistoryMenuList = rootView.findViewById(R.id.el_order_history_menu_list);

        if (getArguments() != null) {
            String subscriptionId = getArguments().getString("id");
//            String resName = getArguments().getString("restaurant_name");
//            String date = getArguments().getString("delivery_date");
            Log.d(TAG, "onCreate Subscription ID: " + subscriptionId /*+ "Res Name: " + resName + " date: " + date*/);

//            tvTitle.setText(resName);
//            tvDate.setText(date);

            User.AccessToken token = sPref.getAccessTokenObject();
            Language language = sPref.getLangObject();

            if (token != null && language != null)
                getDeliveryDate(language.getId(), subscriptionId, token.getAccessToken());
        }

//        ((MainActivity) mContext).getSupportActionBar().show();

//        if (sPref.getLocale().equals("ar"))
//            ivBtnBack.setImageResource(R.drawable.ic_arrow_forward_white);
//
//        ((MainActivity) mContext).updateBackPress(this);

        ivBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Navigation.findNavController(mContext, R.id.main_content).navigateUp();
            }
        });

        /*rootView.setFocusableInTouchMode(true);
        rootView.requestFocus();
        rootView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_BACK) {

                    if (getFragmentManager() != null) {

                        getFragmentManager().beginTransaction().replace(R.id.main_content, new OrderHistoryFragment(),"order history").commit();
                    }

                    return true;
                }
                return false;
            }
        });*/

        return rootView;
    }

    private void getDeliveryDate(final String langId, String subscriptionId, final String accessToken) {
        String BASE_URL = API.getAPI() + "auth/delivery-dates-by-subscription?language_id=" + langId + "&subscription_id=" + subscriptionId;
        Log.d(TAG, "Delivery Date URL: " + BASE_URL);

        loaderLayout.setVisibility(View.VISIBLE);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, BASE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "onResponse: " + response);
                loaderLayout.setVisibility(View.GONE);
                tvNo.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    int status = jsonObject.optInt("status");
                    String msg = jsonObject.optString("messge");

                    OrderHistoryMenu menu = null;

                    if (status == 1) {
                        JSONArray dataArray = jsonObject.optJSONArray("data");
                        if (dataArray != null) {
                            for (int i = 0; i < dataArray.length(); i++) {
                                JSONObject dataObj = dataArray.getJSONObject(i);

                                String mealOrderDetailId = dataObj.getString("meal_order_detail_id");
                                String deliveryDate = dataObj.getString("delivery_date");
                                String deliveryStatus = dataObj.getString("status");

                                DeliveryDates deliveryDates = new DeliveryDates();
                                deliveryDates.setMealOrderDetailId(mealOrderDetailId);
                                deliveryDates.setDeliveryDate(deliveryDate);
                                deliveryDates.setDeliveryStatus(deliveryStatus);

                                deliveryDatesArrayList.add(deliveryDates);

                                // get the date in string array then split it with "-"
                                String[] date = deliveryDate.split("-");
                                int year = Integer.parseInt(date[0]);
                                int month = Integer.parseInt(date[1]);
                                int day = Integer.parseInt(date[2]);

                                deliveryDates.setDeliveryDay(day);
                                deliveryDates.setDeliveryMonth(month);
                                deliveryDates.setDeliveryYear(year);

                                // check if status is "pending or pause or delivered" then mark the date
                                switch (deliveryStatus) {
                                    case "pending":
                                        mCalendarView.markDate(new DateData(year, month, day).setMarkStyle(new MarkStyle(MarkStyle.BACKGROUND, getResources().getColor(R.color.green))));
                                        break;
                                    case "pause":
                                        mCalendarView.markDate(new DateData(year, month, day).setMarkStyle(new MarkStyle(MarkStyle.BACKGROUND, getResources().getColor(R.color.red))));
                                        break;
                                    case "delivered":
                                        mCalendarView.markDate(new DateData(year, month, day).setMarkStyle(new MarkStyle(MarkStyle.BACKGROUND, getResources().getColor(R.color.blue))));
                                        break;
                                }
                            }

                            if (deliveryDatesArrayList.size() < 1)
                                tvNo.setVisibility(View.VISIBLE);

                            // show data according to first date
                            try {
                                String firstOrderId = deliveryDatesArrayList.get(0).getMealOrderDetailId();
//                            tvMealDate.setText("Meals for " + deliveryDatesArrayList.get(0).getDeliveryDate());
                                LocalDate clickDate = LocalDate.of(deliveryDatesArrayList.get(0).getDeliveryYear(), deliveryDatesArrayList.get(0).getDeliveryMonth(), deliveryDatesArrayList.get(0).getDeliveryDay());
                                DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("dd-MMM-yyyy");
                                String forDateHeading = clickDate.format(formatter1);

                                tvMealDate.setText("Meals for " + forDateHeading);

                                Log.d(TAG, "onResponse: first order date: " + firstOrderId);
                                getDeliveryDateDetails(langId, firstOrderId, accessToken);

                                mCalendarView.setOnDateClickListener(new OnDateClickListener() {
                                    @Override
                                    public void onDateClick(View view, DateData date) {
                                        // check if the date that user clicked exists in our date, if yes then show the ordered items
                                        for (DeliveryDates dates : deliveryDatesArrayList) {
                                            int tempDay = dates.getDeliveryDay();
                                            if (tempDay == date.getDay()) {

                                                LocalDate clickDate = LocalDate.of(dates.getDeliveryYear(), dates.getDeliveryMonth(), dates.getDeliveryDay());
                                                DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("dd-MMM-yyyy");
                                                String forDateHeading = clickDate.format(formatter1);
                                                tvMealDate.setText("Meals for " + forDateHeading);
                                                // set date marker style
//                                                mCalendarView.markDate(new DateData(dates.getDeliveryYear(), dates.getDeliveryMonth(), tempDay).setMarkStyle(new MarkStyle(MarkStyle.BACKGROUND, MarkStyle.defaultColor)));

                                                // get meal order detail id
                                                String tempId = dates.getMealOrderDetailId();
                                                getDeliveryDateDetails(langId, tempId, accessToken);

                                                Log.d(TAG, "onDateClick: date: " + dates.getDeliveryDay() + "-" + dates.getDeliveryMonth() + " " + tempId);
                                            }
                                        }
                                    }

                                    @Override
                                    public void onDateLongClick(View view, DateData date) {
                                    }
                                });

//                            adapter = new OrderHistoryMenuListAdapter(mContext, orderHistoryMenuArrayList);
//                            elOrderHistoryMenuList.setAdapter(adapter);
                            } catch (Exception ex) {
                                Log.d(TAG, "onResponse: exception: " + ex.toString());
                            }
                        }
                    } else {
                        tvNo.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    Log.d(TAG, "JSONException: " + e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loaderLayout.setVisibility(View.GONE);
                Log.d(TAG, "onErrorResponse: " + error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (accessToken != null) {
                    params.put("Authorization", accessToken);
                    Log.d(TAG, "getHeaders: " + accessToken);
                }
                return params;
            }
        };

        requestQueue.add(stringRequest);
    }

    private void getDeliveryDateDetails(String langId, String mealOrderDetailId, final String accessToken) {
        String BASE_URL = API.getAPI() + "auth/delivery-date-details?language_id=" + langId + "&meal_order_detail_id=" + mealOrderDetailId;
        Log.d(TAG, "Delivery Date Detail URL: " + BASE_URL);

        loaderLayout.setVisibility(View.VISIBLE);
        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.GET, BASE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "onResponse: " + response);
                loaderLayout.setVisibility(View.GONE);
                try {
                    orderHistoryMenuArrayList.clear();
                    JSONObject jsonObject = new JSONObject(response);

                    int status = jsonObject.optInt("status");
                    String msg = jsonObject.optString("messge");

                    OrderHistoryMenu menu;

                    if (status == 1) {
                        JSONObject dataObj = jsonObject.optJSONObject("data");
                        if (dataObj != null) {
//                            for (int i = 0; i < dataArray.length(); i++) {
//                                JSONObject dataObj = dataArray.getJSONObject(i);

                            String id = dataObj.getString("id");
                            String listing_name = dataObj.getString("listing_name");
                            tvTitle.setText(listing_name);
//                                JSONArray deliveryDatesArray = dataObj.getJSONArray("delivery_dates");

//                                 get products array from data array
                            JSONArray menuArray = dataObj.optJSONArray("menu");

//                                 iterate products array
                            if (menuArray != null) {
                                for (int j = 0; j < menuArray.length(); j++) {

                                    JSONObject menuObj = menuArray.getJSONObject(j);
                                    String categoryname = menuObj.getString("category_name");
                                    JSONObject productObj = menuObj.getJSONObject("product");
                                    String productId = productObj.getString("id");
                                    String name = productObj.getString("name");
                                    String caption = productObj.getString("caption");
                                    String image = productObj.getString("image");
                                    String rating = productObj.getString("rating");
                                    String calories = productObj.getString("calories");
                                    String proteins = productObj.getString("proteins");
                                    String carbohydrates = productObj.getString("carbohydrates");

                                    String imageUrl = API.getImageAPI() + image;

                                    menu = new OrderHistoryMenu(categoryname);

                                    // store the products array data in model
//                                        if (menu != null) {
                                    menu.orderHistoryMenuItemArrayList.add(new OrderHistoryMenuItem(
                                            productId,
                                            imageUrl,
                                            name,
                                            caption,
                                            calories,
                                            carbohydrates,
                                            proteins,
                                            0,
                                            R.drawable.add_review_button));
//                                        }
                                    orderHistoryMenuArrayList.add(menu);
                                }
                            }
//                            }

                            adapter = new OrderHistoryMenuListAdapter(mContext, orderHistoryMenuArrayList, OrderHistoryDetailFragment.this);
                            adapter.notifyDataSetChanged();
                            elOrderHistoryMenuList.setAdapter(adapter);

                            for (int i = 0; i < adapter.getGroupCount(); i++)
                                elOrderHistoryMenuList.expandGroup(i);
                        }
                    }
                } catch (JSONException e) {
                    Log.d(TAG, "JSONException: " + e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loaderLayout.setVisibility(View.GONE);
                Log.d(TAG, "onErrorResponse: " + error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (accessToken != null) {
                    params.put("Authorization", accessToken);
                    Log.d(TAG, "getHeaders: " + accessToken);
                }
                return params;
            }
        };

        requestQueue.add(stringRequest);
    }

    // rate meal
    private void rateMeal(String prodId, String rate, final String accessToken) {
        String BASE_URL = API.getAPI() + "auth/rate-meal?product_id=" + prodId + "&rate=" + rate;
        Log.d(TAG, "Order History URL: " + BASE_URL);
        loaderLayout.setVisibility(View.VISIBLE);

        RequestQueue requestQueue = Volley.newRequestQueue(mContext);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, BASE_URL, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "onResponse: " + response);
                loaderLayout.setVisibility(View.GONE);
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    int status = jsonObject.getInt("status");
                    String msg = jsonObject.getString("message");

                    Snackbar snackbar = Snackbar.make(mContext.findViewById(android.R.id.content), msg, Snackbar.LENGTH_SHORT);
                    View getSnackbarView = snackbar.getView();
                    getSnackbarView.setBackgroundColor(mContext.getResources().getColor(R.color.active_tab_color));
                    TextView tvSnackBar = getSnackbarView.findViewById(com.google.android.material.R.id.snackbar_text);
                    tvSnackBar.setTextColor(mContext.getResources().getColor(R.color.white));
                    snackbar.show();

                } catch (JSONException e) {
                    Log.d(TAG, "JSONException: " + e.toString());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                loaderLayout.setVisibility(View.GONE);
                Log.d(TAG, "onErrorResponse: " + error.toString());
            }
        }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> params = new HashMap<>();
                if (accessToken != null) {
                    params.put("Authorization", accessToken);
                    Log.d(TAG, "getHeaders: " + accessToken);
                }
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(30000, 0, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }

    // show dialog to rate meals
    private void reviewDialog(final Context context, final OrderHistoryMenuItem menuItem, final String accessToken) {
        final AlertDialog.Builder alertDialog = new AlertDialog.Builder(context);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View view = null; //get the add review layout
        if (inflater != null) {
            view = inflater.inflate(R.layout.add_review_dialog, null);
        }

        //get the ids of add review layout
        TextView tvProductName = null;
        if (view != null) {
            tvProductName = view.findViewById(R.id.tv_add_review_product_name);
            ImageView ivProductIcon = view.findViewById(R.id.iv_add_review_product_image);
            final RatingBar rbProductRating = view.findViewById(R.id.rb_product_rating);
            Button ivSubmit = view.findViewById(R.id.iv_add_review_submit);
            //set the selected item on add review layout
            tvProductName.setText(menuItem.getName());
            Picasso.get().load(menuItem.getImages()).placeholder(R.drawable.boxes_logo).into(ivProductIcon);
//        ivProductIcon.setImageResource(menuItem.getImages());

            alertDialog.setView(view); //set the custom view
            final Dialog dialog = alertDialog.show(); //show the alert dialog

            rbProductRating.setIsIndicator(false);
            ivSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialog.dismiss();
                    rateMeal(menuItem.getId(), String.valueOf(rbProductRating.getRating()), accessToken);
                }
            });
        }

    }

    // review button click listener
    @Override
    public void onReviewButtonClick(OrderHistoryMenuItem item) {
        if (sPref.getAccessTokenObject() != null)
            reviewDialog(mContext, item, sPref.getAccessTokenObject().getAccessToken());
    }

//    @Override
//    public void onMyBackPress() {
//
//    }
}