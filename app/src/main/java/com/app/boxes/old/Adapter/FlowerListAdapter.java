package com.app.boxes.old.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.boxes.old.Model.Flower;
import com.app.boxes.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class FlowerListAdapter extends RecyclerView.Adapter<FlowerListAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<Flower> flowerArrayList;
    private LayoutInflater inflater;

    public FlowerListAdapter(Context context, ArrayList<Flower> flowerArrayList) {
        this.context = context;
        this.flowerArrayList = flowerArrayList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView = inflater.inflate(R.layout.sub_list_flower, parent, false);

        return new MyViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Picasso.get().load(flowerArrayList.get(position).getProdImage()).into(holder.ivImage);
        holder.tvHeading.setText(flowerArrayList.get(position).getProdName());
        holder.tvContent.setText(flowerArrayList.get(position).getProdCaption());
    }

    @Override
    public int getItemCount() {
        return flowerArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        CircleImageView ivImage;
        TextView tvHeading, tvContent;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            ivImage = itemView.findViewById(R.id.iv_flower_image);
            tvHeading = itemView.findViewById(R.id.tv_flower_heading);
            tvContent = itemView.findViewById(R.id.tv_flower_content);
        }
    }
}