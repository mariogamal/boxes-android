package com.app.boxes.old.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.app.boxes.old.Model.SingleRestaurant;
import com.app.boxes.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

public class SingleRestaurantListAdapter extends RecyclerView.Adapter<SingleRestaurantListAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<SingleRestaurant> singleRestaurantArrayList;
    private ArrayList<SingleRestaurant> arraylist;
    LayoutInflater inflater;
    private SingleResturantClickListener listener;

    public SingleRestaurantListAdapter(Context context, ArrayList<SingleRestaurant> singleRestaurantArrayList, SingleResturantClickListener clickListener) {
        this.context = context;
        this.singleRestaurantArrayList = singleRestaurantArrayList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.arraylist = new ArrayList<SingleRestaurant>();
        this.arraylist.addAll(singleRestaurantArrayList);
        listener = clickListener;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View rootView = inflater.inflate(R.layout.list_single_restaurant, parent, false);

        return new MyViewHolder(rootView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        Picasso.get().load(singleRestaurantArrayList.get(position).getImages()).fit().placeholder(R.drawable.boxes_logo).into(holder.ivImage);
        holder.tvRestaurantName.setText(singleRestaurantArrayList.get(position).getName());
        holder.tvRestaurantCategory.setText(singleRestaurantArrayList.get(position).getCategory());

        holder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onMenuClick(position);
            }
        });
        holder.ivbg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onItemClicked(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return singleRestaurantArrayList.size();
    }


    public SingleRestaurant getItem(int position) {
        return singleRestaurantArrayList.get(position);
    }

    // Filter Class
    public void filter(String charText) {
        charText = charText.toLowerCase(Locale.getDefault());
        singleRestaurantArrayList.clear();
        if (charText.length() == 0) {
            singleRestaurantArrayList.addAll(arraylist);
        } else {
            for (SingleRestaurant wp : arraylist) {
                if (wp.getName().toLowerCase(Locale.getDefault()).contains(charText)) {
                    singleRestaurantArrayList.add(wp);
                }
            }
        }
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private ImageView ivImage;
        private TextView tvRestaurantName, tvRestaurantCategory;
        private LinearLayout linearLayout;
        private RelativeLayout ivbg;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
//            ivImage = itemView.findViewById(R.id.iv_single_restaurant_icon);
//            tvRestaurantName = itemView.findViewById(R.id.tv_single_restaurant_name);
//            tvRestaurantCategory = itemView.findViewById(R.id.tv_single_restaurant_category);
            linearLayout = itemView.findViewById(R.id.ll_show_menu);
            ivbg = itemView.findViewById(R.id.iv_bg);
        }
    }

    public interface SingleResturantClickListener {
        void onMenuClick(int pos);
        void onItemClicked(int pos);
    }
}
